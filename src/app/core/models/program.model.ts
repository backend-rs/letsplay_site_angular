import { Time } from '@angular/common';

export class Program {
    id: string;
    _id: string;
    isFav: boolean;
    name: string;
    description: string;
    type: string;
    email: string;
    price: string;
    location: string;
    code: string;
    programId: string;
    time: any = {};
    date: any = {}
    ageGroup: any = {};
    bookingCancelledIn: any = {};
    duration: string;
    isFree: boolean;
    pricePerParticipant: string;
    priceForSiblings: string;
    specialInstructions: string;
    adultAssistanceIsRequried: boolean;
    capacity: any = {};
    emails: [];
    batches: any[];
    status: string;
    programCoverPic: string;
    userId: string;
    addresses: [];
    categoryId: string;
    tags: any = [];
    timelinePics: any[];
    user: string;

    constructor(obj?: any) {

        if (!obj) {
            return;
        }

        this.id = obj.id;
        this._id = obj._id
        this.userId = obj.userId;
        this.programId = obj.programId;
        this.name = obj.name;
        this.programCoverPic = obj.programCoverPic;
        this.email = obj.name;
        this.description = obj.description;
        this.type = obj.type;
        this.price = obj.price;
        this.location = obj.location;
        this.code = obj.code;
        this.status = obj.status;
        this.time = obj.time;
        this.date = obj.date;
        this.ageGroup = obj.ageGroup;
        this.bookingCancelledIn = obj.bookingCancelledIn;
        this.duration = obj.duration;
        this.isFree = obj.isFree;
        this.pricePerParticipant = obj.pricePerParticipant;
        this.priceForSiblings = obj.priceForSiblings;
        this.specialInstructions = obj.specialInstructions;
        this.adultAssistanceIsRequried = obj.adultAssistanceIsRequried;
        this.capacity = obj.capacity;
        this.emails = obj.emails;
        this.userId = obj.userId;;
        this.addresses = obj.addresses;
        this.tags = obj.tags;
        this.categoryId = obj.categoryId;
        this.timelinePics = obj.timelinePics;
        this.user = obj.user;

    }
}

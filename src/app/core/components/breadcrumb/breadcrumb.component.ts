import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  routeName: string
  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.snapshot.data.breadcrumb
    this.routeName = this.activatedRoute.snapshot.data.breadcrumb
    // switch (this.router.url) {
    //   case '/program-list': this.routeName = 'programs'; break
    //   case '/program': this.routeName = 'programs'; break
    //   case '/setting': this.routeName = 'setting'; break
    //   case '/profile': this.routeName = 'profile'; break
    //   case '/associates': this.routeName = 'associates'; break
    //   default: this.routeName = ''
    // }
  }


}

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  ActivatedRoute,
  Router,
  Event,
  ActivationStart,
  ActivatedRouteSnapshot,
  ActivationEnd,
  NavigationEnd,
  NavigationStart
} from '@angular/router';
import { User } from '../../models';
import { AuthService } from '../../services/auth.service';
import { ApiService } from '../../services/api.service.service';
import { _getOptionScrollPosition, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { UserDataService } from '../../services/user-data.service';
declare const $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  bcLoadedData;
  bcForDisplay;
  user = new User;
  routeName: string
  program: string = ""
  setting: string = ""
  parentData: any;

  @Input()
  showMenu = false;

  @Output()
  menuClicked: EventEmitter<boolean> = new EventEmitter()
  initialUrl: any;
  _user: any = {};

  userData: any = {};
  profileProgressResponse: any;
  progressBarValue: any;
  profileProgress: string = ''


  message: string = 'Please Login First!';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';


  constructor(
    private router: Router,
    private auth: AuthService,
    private activatedRoute: ActivatedRoute,
    private apiservice: ApiService,
    private userdataservice: UserDataService,
    private snack: MatSnackBar

  ) {
    this.user = this.auth.currentUser();
    this.auth.userChanges.subscribe(user => this.user = user)
    // var retrievedObject = localStorax```ge.getItem('userData');
    // this.userData = JSON.parse(retrievedObject);

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    this.getProfileProgress()
  }
  logo() {
    this.router.navigate(['/search']);
  }
  addProgram() {
    this.router.navigate(['/program/add']);
  }
  profile() {

    if (this.user.role === "parent") {
      this.router.navigate(['parent/parent-profile']);
    }
    else {
      if (this.user.role === "provider") {
        this.router.navigate(['/profile']);
      }

      else {
        let msg = 'Something Went Wrong!';
        this.snack.open(msg, 'OK', { duration: 5000 });

      }
      // }

    }
  }
  getProfileProgress() {
    this.apiservice.getProfileProgress(this.user.id, this.user.role).subscribe((res: any) => {
      this.profileProgress = res.data.profileProgress.toString()
      $("#progress").attr("data-percentage", this.profileProgress);
    });
  }
  getUserById() {
    this.apiservice.getUserById(this.user.id).subscribe(res => {
      console.log('got user Data by Id (header.component)', res);
      this.user = res;
    });
  }

  ngOnInit() {
    console.log('header', this.user)
    // this.getProfileProgress();
  }

  logout() {
    // this.auth.logout();
    this.userdataservice.logout();
    this.router.navigate(['/login']);
  }

}

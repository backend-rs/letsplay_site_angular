import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { User } from '../models';
import { environment } from 'src/environments/environment';
import { Child } from '../models/child.model';
import { Category } from '../models/category.model';
import { Program } from '../models/program.model';
import { Tag } from '../models/tag.model';
import { LocalStorageService } from '.';
import { Claim } from '../models/claim.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    root = environment.apiUrls.reports;
    userResponse: any;
    usersData: any;
    categoryResponse: any;
    tagResponse: any;
    token = ''
    header = {}


    constructor(private http: HttpClient, private store: LocalStorageService) {

        console.log('constructor called')
        // this.getToken()

    }


    getHeader() {
        this.token = this.store.getItem('token')
        console.log('token', this.token)

        if (this.token) {
            let header = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'x-access-token': this.token,
                })
            };
            return header
        } else {
            let header = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                })
            }
            return header

        }
    }

    signin(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    otpRequest(email): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/otp?email=${email}`).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    otpVerify(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/otpVerify`, model, { headers: null }).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    forgotPassword(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/forgotPassword`, model, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);

        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    resetPassword(id, model): Observable<User> {
        const subject = new Subject<User>();
        this.http.put(`${this.root}/users/resetPassword/${id}`, model, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }


    uploadUserImage(id, model): Observable<any> {
        const subject = new Subject<any>();
        this.http.put(`${this.root}/users/uploadProfilePic/${id}`, model, {
            headers: new HttpHeaders({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }
    getPicUrl(pic) {
        const subject = new Subject<any>();
        this.http.post(`${this.root}/uploads/getPicUrl`, pic, {
            headers: new HttpHeaders({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }
    uploadProviderBanner(id, model): Observable<any> {
        const subject = new Subject<any>();
        this.http.put(`${this.root}/providers/uploadBannerPic/${id}`, model, {
            headers: new HttpHeaders({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }
    uploadChildImage(model): Observable<any> {
        const subject = new Subject<any>();
        this.http.post(`${this.root}/child/uploadChildPic`, model, {
            headers: new HttpHeaders({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }
    addUser(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/register`, data, { headers: null }).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getParentById(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/parents/getById/${id}`, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    updateParent(id, data): Observable<User[]> {
        console.log('data before updata api', data);
        const subject = new Subject<User[]>();
        this.http.put(`${this.root}/parents/update/${id}`, data, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    tellFriend(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/tellAFriend`, model).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    activeDeactiveUser(id, isActivated): Observable<User> {
        const subject = new Subject<User>();
        let m;
        this.http.put(`${this.root}/users/activeOrDeactive?id=${id}&isActivated=${isActivated}`, m, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    giveFeedback(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/feedback`, model).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    claimRequest(model): Observable<Claim> {
        const subject = new Subject<Claim>();
        this.http.post(`${this.root}/claims/request`, model, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });
        return subject.asObservable();
    }


    ClaimRquestListByProvider(id): Observable<Claim[]> {
        const subject = new Subject<Claim[]>();
        this.http.get(`${this.root}/claims/requestListByProvider?id=${id}`, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getChildByParentId(id): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.get(`${this.root}/child/byParentId/${id}`, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    addChild(model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.post(`${this.root}/child/add`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    addGuardian(model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/guardians/add`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    updateChild(id, model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.put(`${this.root}/child/update/${id}`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    updateProviderById(id, model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.put(`${this.root}/providers/update/${id}`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getCategory(): Observable<Category> {
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/categories/list`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    searchCategory(key): Observable<Category> {
        console.log('TOKEN', this.token)
        const subject = new Subject<Category>();
        console.log('key', key)
        this.http.get(`${this.root}/categories/search?name=${key}`, this.getHeader()).subscribe((responseData: any) => {

            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    programSearch(key): Observable<Category> {
        console.log('TOKEN', this.token)
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/programs/search?name=${key}`, this.getHeader()).subscribe((responseData: any) => {
            console.log('res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    programFilter(min, max, fromDate, toDate, fromTime, toTime, pageNo, pageSize): Observable<Category> {

        const subject = new Subject<Category>();
        this.http.get(`${this.root}/programs/byFilter?ageFrom=${min}&ageTo=${max}&fromDate=${fromDate}&toDate=${toDate}&fromTime=${fromTime}&toTime=${toTime}&pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeader()).subscribe((responseData: any) => {
            console.log('res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    searchTag(key): Observable<Tag> {
        console.log('TOKEN', this.token)
        const subject = new Subject<Tag>();
        console.log('key', key)
        this.http.get(`${this.root}/tags/search?name=${key}`, this.getHeader()).subscribe((responseData: any) => {

            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    getTag(): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/tags/list`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    getTagByCategoryId(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/tags/byCategoryId?catrgoryIds=${id}`, this.getHeader()).subscribe((responseData: any) => {
            console.log('res from server', responseData);
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }



    getFavouriteByParentId(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/favourites/getByParentId?parentId=${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    getGuardianByParentId(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/guardians/byParentId/${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    getUserById(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/getById/${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    getProviderById(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/providers/getById/${id}`, this.getHeader()).subscribe((responseData: any) => {
            console.log('res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }


    getProfileProgress(id, role): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/getProfileProgress?id=${id}&role=${role}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    getCategoryBySearch(name): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/categories/search?name=${name}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    getProgram(pageNo, pageSize): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/list?pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.items);
        }, (error) => {
            subject.next(error.error);
        });

        return subject.asObservable();
    }

    getProgramByProvider(userId, pageNo, pageSize): Observable<Program> {

        // if (!this.token) {
        //     this.getToken
        // }
        console.log('TOKEN', this.token)
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/byProvider?userId=${userId}&pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            console.log('error', error)
            subject.next(error);
        });
        return subject.asObservable();
    }

    getProgramById(id): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/getById/${id}`, this.getHeader()).subscribe((responseData: any) => {
            console.log('get program bye id res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    addFavProgram(model): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.post(`${this.root}/favourites/add`, model, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    deleteFavProgram(id): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.delete(`${this.root}/favourites/delete/${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    addProgram(model): Observable<Program[]> {
        const subject = new Subject<Program[]>();
        this.http.post(`${this.root}/programs/add`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    programActiveInActive(id, status) {
        const subject = new Subject<Program[]>();
        let m;
        this.http.put(`${this.root}/programs/activeOrDecactive?id=${id}&status=${status}`, m, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getView(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/getViewsCount?userId=${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    graphData(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/getGraphData?id=${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    getProgramCount(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/count?userId=${id}`, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });
        return subject.asObservable();
    }

    updateProgram(id, model): Observable<Program[]> {
        const subject = new Subject<Program[]>();
        this.http.put(`${this.root}/programs/update/${id}`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    verifyAns(id, model): Observable<User> {
        const subject = new Subject<User>();
        this.http.put(`${this.root}/users/verifySecuirtyAns/${id}`, model, this.getHeader()).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    searchProgram(key): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/search?name=${key}`, this.getHeader()).subscribe((responseData: any) => {

            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });
        return subject.asObservable();
    }
    addAction(data): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.post(`${this.root}/programs/addProgramAction`, data, this.getHeader()).subscribe((res: any) => {
            subject.next(res);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

}


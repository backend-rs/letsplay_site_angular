import { Injectable } from '@angular/core';
import { User } from '../../../app/core/models';
import { GenericApi, IApi } from '@open-age/ng-api';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { Http, Headers } from '@angular/http';
import { ToastyService } from 'ng2-toasty';

@Injectable()
export class UserService {

  

  constructor() {}



  // root = environment.apiUrls.reports;
  // users: IApi<User>;

  // headers: Headers;
  // constructor(private http: Http,
  //   private toasty: ToastyService) {
  //   this.headers = new Headers();
  //   this.headers.append('Content-Type', 'application/json');

  // }

  // createUser(model): Observable<User[]> {
  //   const subject = new Subject<User[]>();

  //   this.http.post(`${this.root}/users`, model, { headers: this.headers }).subscribe((responseData) => {
  //     if (responseData.status !== 200) {
  //       throw new Error('This request has failed ' + responseData.status);
  //     }
  //     const dataModel = responseData.json();

  //     if (!dataModel.isSuccess) {
  //       if (responseData.status === 200) {
  //         throw new Error(dataModel.code || dataModel.message || 'failed');
  //       } else {
  //         throw new Error(responseData.status + '');
  //       }
  //     }

  //     subject.next(dataModel.data);
  //   });

  //   return subject.asObservable();

  // }

  // getUsers(): Observable<User[]> {
  //   const subject = new Subject<User[]>();

  //   this.http.get(`${this.root}/users`, { headers: this.headers }).subscribe((responseData) => {
  //     if (responseData.status !== 200) {
  //       throw new Error('This request has failed ' + responseData.status);
  //     }

  //     const dataModel = responseData.json();

  //     if (!dataModel.isSuccess) {
  //       if (responseData.status === 200) {
  //         throw new Error(dataModel.code || dataModel.message || 'failed');
  //       } else {
  //         throw new Error(responseData.status + '');
  //       }
  //     }

  //     subject.next(dataModel.items);
  //   });

  //   return subject.asObservable();

  // }

  // updateUser(id, model): Observable<User[]> {
  //   const subject = new Subject<User[]>();

  //   this.http.put(`${this.root}/users/${id}`, model, { headers: this.headers }).subscribe((responseData) => {
  //     if (responseData.status !== 200) {
  //       throw new Error('This request has failed ' + responseData.status);
  //     }
  //     const dataModel = responseData.json();

  //     if (!dataModel.isSuccess) {
  //       if (responseData.status === 200) {
  //         throw new Error(dataModel.code || dataModel.message || 'failed');
  //       } else {
  //         throw new Error(responseData.status + '');
  //       }
  //     }

  //     subject.next(dataModel.data);
  //   });

  //   return subject.asObservable();

  // }
}

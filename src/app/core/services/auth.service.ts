import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models';
import { Subject } from 'rxjs';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { IApi, GenericApi } from '@open-age/ng-api';
import { ToastyService } from 'ng2-toasty';
import { LocalStorageService } from '.';

@Injectable()
export class AuthService {
  root = environment.apiUrls.reports;
  users: IApi<User>;

  private _user: User;

  private _currentUserSubject = new Subject<User>()
  userChanges = this._currentUserSubject.asObservable()

  constructor(
    private http: Http,
    private toasty: ToastyService,
    private store: LocalStorageService
  ) {
    this.users = new GenericApi(environment.apiUrls.reports, 'users', this.http)
  }

  private setUser(user: User) {
    if (user) {
      this.store.setObject('userData', user);
      this.store.setItem('token', user.token);
    } else {
      this.store.clear();
    }
    this._user = user;
    this._currentUserSubject.next(user);
  }

  currentUser(): User {
    if (this._user) { return this._user }

    this._user = this.store.getObject('userData') as User;

    return this._user;
  }


  login(model): Observable<User[]> {
    const subject = new Subject<User[]>();

    this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData) => {
      if (responseData.status !== 200) {
        throw new Error('This request has failed ' + responseData.status);
      }
      const dataModel = responseData.json();
      if (!dataModel.isSuccess) {
        if (responseData.status === 200) {
          this.toasty.error(dataModel.error);
          throw new Error(dataModel.code || dataModel.message || 'failed');
        } else {
          throw new Error(responseData.status + '');
        }
      }
      this.setUser(dataModel.data)
      subject.next(dataModel.data);
    },
      (responseData) => {
        const dataModel = responseData.json();
        this.toasty.error(dataModel.error);
        console.log('errrrrrrrr', dataModel.error)
        // subject.next(dataModel.error);

      });

    return subject.asObservable();

  }

  //   const subject = new Subject<User>();
  //   this.users.post(model, 'login').subscribe(user => {
  //     console.log(user)
  //     
  //     subject.next(user);
  //   }, err => {
  //     this.toasty.error(err.message);
  //     subject.error(err)
  //   });
  //   return subject.asObservable();
  // }

  logout() {
    localStorage.removeItem('token');
    this.setUser(null)
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { CustomValidators } from 'ng2-validation';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  otpSendForm: FormGroup;
  otpVerifyForm: FormGroup;
  passwordResetForm: FormGroup;
  isForgot = true;
  isOTP = false;
  isNewPassword = false;
  otpResponse: any = {};
  isConfirmPassword = false;
  credentials = {
    email: '',
    password: '',
    newPassword: '',
    confirmPassword: '',
    otp: '',
    otpToken: '',
  }
  otpVerifyResponse: any
  resetPasswordResponse: any;


  message: string = '';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';


  constructor(private router: Router,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private ngxLoader: NgxUiLoaderService
  ) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }
  otpRequest() {

    this.ngxLoader.start();
    this.apiservice.otpRequest(this.credentials.email).subscribe((res) => {
      this.otpResponse = res;
      this.ngxLoader.stop();
      if (this.otpResponse.isSuccess === true) {
        this.isOTP = true;
        this.isNewPassword = false;
        this.isForgot = false;
        this.credentials.otpToken = this.otpResponse.message.otpToken,
          console.log('response', this.otpResponse);
        this.snack.open(this.otpResponse.message.message, 'OK', { duration: 5000 });
      }
      else {
        if (this.otpResponse.isSuccess === false) {
          this.isForgot = true;
          this.isOTP = false;
          this.isNewPassword = false;
          this.snack.open(this.otpResponse.error, 'OK', { duration: 5000 });
        }
        else {
          this.isForgot = true;
          this.isOTP = false;
          this.isNewPassword = false;
          let msg = 'Something Went Wrong!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
      }


    });
  }
  otpVerify() {
    this.ngxLoader.start();
    this.apiservice.otpVerify(this.credentials).subscribe((res) => {
      this.otpVerifyResponse = res;
      this.ngxLoader.stop();
      if (this.otpVerifyResponse.isSuccess === true) {
        this.isOTP = false;
        this.isNewPassword = true;
        this.isForgot = false;
        console.log('verifyotp response', res);
        this.snack.open(this.otpVerifyResponse.message.message, 'OK', { duration: 5000 });
      }
      else {
        if (this.otpVerifyResponse.isSuccess === false) {
          this.isOTP = true;
          this.isNewPassword = false;
          this.isForgot = false;
          this.snack.open(this.otpVerifyResponse.error, 'OK', { duration: 5000 });

        }
        else {
          this.isOTP = true;
          this.isNewPassword = false;
          this.isForgot = false;
          let msg = 'Something went Wrong!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
      }
    });
  }
  resetPassword() {

    this.ngxLoader.start();
    this.apiservice.forgotPassword(this.credentials).subscribe((res) => {
      this.resetPasswordResponse = res;
      this.ngxLoader.stop();

      if (this.resetPasswordResponse.isSuccess === true) {
        this.router.navigate(['/login']);
        console.log('resetpassword response', res);
        this.snack.open(this.resetPasswordResponse.message, 'OK', { duration: 5000 });
      }
      else {
        if (this.resetPasswordResponse.isSuccess === false) {
          this.isOTP = false;
          this.isNewPassword = true;
          this.isForgot = false;
          this.snack.open(this.resetPasswordResponse.error, 'OK', { duration: 5000 });
        }
        else {
          this.isOTP = false;
          this.isNewPassword = true;
          this.isForgot = false;
          let msg = 'Something went Wrong!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
      }

    });
  }
  cancel() {
    this.router.navigate(['/login']);
  }
  ngOnInit() {

    this.otpSendForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
    this.otpVerifyForm = new FormGroup({
      otp: new FormControl('', [Validators.required, Validators.minLength(4)]),
    });

    let newPassword = new FormControl('', [Validators.required]);
    let confirmPassword = new FormControl('', [Validators.required,
    CustomValidators.equalTo(newPassword)]);
    this.passwordResetForm = new FormGroup({
      newPassword: newPassword,
      confirmPassword: confirmPassword

    });
  }

}

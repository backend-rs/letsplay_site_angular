import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramDetailComponent } from './program-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { CoreModule } from 'src/app/core/core.module';
import { AgmCoreModule } from '@agm/core';


const routes: Routes = [
  {
    path: '', component: ProgramDetailComponent, children: [

    ]
  }
]
@NgModule({
  declarations: [ProgramDetailComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCayIBisLl_xmSOmS3g524FAzEI-ZhT1sc'
    }),
    CoreModule,
    NgxUiLoaderModule,
    RouterModule.forChild(routes)
  ]
})
export class ProgramDetailModule { }

import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';
import { Router } from '@angular/router';
import { Program, User } from 'src/app/core/models';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MapService } from 'src/app/services/map.service';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { ToastyService } from 'ng2-toasty';
@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: ['./program-detail.component.css']
})
export class ProgramDetailComponent implements OnInit {
  lat: number = 51.673858;
  lng: number = 7.815982;
  userData: any = {};
  program = new Program;
  User = new User;
  minValue: number = 0;
  maxValue: number = 100;
  options: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + ' YRS';
    }
  };
  isLogin = false;
  providerRole: boolean = false;

  message: string = 'claim request submited!';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private router: Router,
    private ngxLoader: NgxUiLoaderService,
    private map: MapService,
    private snack: MatSnackBar,
    private apiservice: ApiService,
    private toastyService: ToastyService

  ) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.program = JSON.parse(localStorage.getItem('program'));
    if (this.userData) {
      this.isLogin = true;
      if (this.userData.role === "provider") {
        this.providerRole = true;
      }
    }
  }
  getProgramById() {
    this.apiservice.getProgramById(this.program._id).subscribe(res => {
      console.log('response get program', res);
      localStorage.removeItem('program');
      localStorage.setItem('program', JSON.stringify(res));
    });
    this.ngxLoader.stop();
  }
  claimBusiness() {
    if (this.userData && this.userData.role === 'provider') {
      this.User.providerId = this.userData.id;
      this.User.userId = this.userData.id;
      this.User.programId = this.program._id;
      this.ngxLoader.start();
      this.apiservice.claimRequest(this.User).subscribe(res => {
        console.log('claim request response ', res);
        this.toastyService.info({ title: 'Info', msg: this.message })
        // this.snack.open(this.message, 'OK', { duration: 4000 });
        this.ngxLoader.stop();

      }); this.ngxLoader.stop();

    } else {
      if (this.userData && this.userData.role === 'parent') {
        this.ngxLoader.start();
        let msg = 'please  register or login as provider to claim this business!';
        this.snack.open(msg, 'OK', { duration: 4000 });
        this.ngxLoader.stop();

      } else {
        this.ngxLoader.start();
        let msg = 'please login to claim this business and try again!'
        this.snack.open(msg, 'OK', { duration: 4000 });
        this.router.navigate(['/login']);
        this.ngxLoader.stop();
      }
    }

  }
  signup() {
    this.router.navigate(['/sign-up']);
  }

  login() {
    this.router.navigate(['/login']);
  }

  ngOnInit() {

    this.getProgramById();
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  isMap: boolean = true;
  isLogin = false;
  userData: any = {};
  constructor(private router: Router,) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    if (this.userData) {
      this.isLogin = true;
    }
  }

  search() {
    this.router.navigate(['/search']);
  }

  ngOnInit() {

  }
}

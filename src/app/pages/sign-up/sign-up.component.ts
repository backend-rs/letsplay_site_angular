import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  parentForm: FormGroup;
  providerForm: FormGroup;

  userData: any = {
    firstName: '',
    email: '',
    password: '',
    role: 'parent'
  }

  providerData: any = {
    firstName: '',
    email: '',
    type: '',
    password: '',
    role: 'provider'
  }

  message: string = 'Registered Successfully!';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  categoryResponse: any;
  response: any;
  hide: boolean = true;

  constructor(private router: Router,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private ngxLoader: NgxUiLoaderService) {

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;

  }

  onPassword() {
    this.hide = !this.hide;
  }

  selectParent() {
    this.userData.role = "parent";


  }
  selectProvider() {
    this.userData.role = "provider";


  }
  signup() {
    if (this.userData.role === "parent") {
      this.ngxLoader.start();
      this.apiservice.addUser(this.userData).subscribe((res) => {
        console.log('res from use model ', res);
        this.response = res;
        this.ngxLoader.stop();
        if (this.response.isSuccess === true) {
          this.snack.open(this.message, 'OK', { duration: 7000 });
          this.router.navigate(['/login']);
        } else {
          if (this.response.isSuccess === false && this.response.error === 'Email already resgister') {
            let msg = 'Email already registered!';
            this.snack.open(msg, 'OK', { duration: 7000 });
          }
          else {
            let msg = 'Something Went Wrong!';
            this.snack.open(msg, 'OK', { duration: 7000 });
          }
        }
      });
    }

    if (this.userData.role === "provider") {
      this.ngxLoader.start();
      this.apiservice.addUser(this.providerData).subscribe((res) => {
        console.log('res from use model ', res);
        this.response = res;
        this.ngxLoader.stop();
        if (this.response.isSuccess === true) {
          this.snack.open(this.message, 'OK', { duration: 7000 });
          this.router.navigate(['/login']);
        } else {
          if (this.response.isSuccess === false && this.response.error === 'Email already resgister') {
            let msg = 'Email already registered!';
            this.snack.open(msg, 'OK', { duration: 7000 });
          }
          else {
            let msg = 'Something Went Wrong!';
            this.snack.open(msg, 'OK', { duration: 7000 });
          }
        }
      });
    }
  }

  ngOnInit() {
    this.providerForm = new FormGroup({
      type: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      // rememberMe: new FormControl(false)
    });

    this.parentForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      // rememberMe: new FormControl(false)
    });

  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'
import { ToastyService } from 'ng2-toasty';;
// import { User } from '../../core/models/index'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  signinForm: FormGroup;
  credentials = {
    email: '',
    password: ''
  }
  isLoading = false;
  hide: boolean = true;
  response: any;
  message: string = 'Logged In Successfully!';
  action: boolean = true;
  activeDeactiveResponse: any;
  isModal = false;

  constructor(private router: Router,
    private auth: AuthService,
    private fb: FormBuilder,
    private apiservice: ApiService,
    private toastyService: ToastyService,
    private ngxLoader: NgxUiLoaderService) {
  }
  onPassword() {
    this.hide = !this.hide;
  }
  cancel() {

    this.router.navigate(['/landing']);

  }
  signin() {
    localStorage.removeItem('userId');
    this.ngxLoader.start();
    this.auth.login(this.credentials).subscribe((res: any) => {
      this.toastyService.success({ title: 'Success', msg: this.message })
      if (res.role === 'provider' && res.isActivated === true) {
        this.ngxLoader.stop();
        this.router.navigate(['program/home']);
      }
      else {
        if (res.isOnBoardingDone) {
          this.router.navigate(['/search']);
        }
        else {
          this.router.navigate(['/parent/login-parent']);
        }
      }
    });
    this.ngxLoader.stop();
  }
  activeDeactiveUser() {
    var booleanValue = true;
    this.ngxLoader.start();
    this.apiservice.activeDeactiveUser(this.response.data.id, booleanValue).subscribe(res => {
      this.activeDeactiveResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', this.activeDeactiveResponse);
      if (this.activeDeactiveResponse.isSuccess === true) {
        console.log('active user data', this.activeDeactiveResponse);
        return this.signin();
      } else {
        this.toastyService.error({ title: 'Error', msg: this.activeDeactiveResponse.error })
      }
    });
    this.ngxLoader.stop();
  }

  onForgotPassword() {
    this.router.navigate(['/forgot-password']);
  }


  ngOnInit() {
    this.signinForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.min(1)]),
      // rememberMe: new FormControl(false)
    });

  }

}

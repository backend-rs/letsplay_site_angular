import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MapService } from 'src/app/services/map.service';
import { Marker } from 'src/app/models/google-map-marker.model';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Program } from 'src/app/core/models/program.model';
import { Options } from 'ng5-slider';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  isDateFilter: boolean = false;
  isTimeFilter: boolean = false;
  isAgeFilter: boolean = false;
  isDateModal: boolean = false;
  isTimeModal: boolean = false;
  isAgeModal: boolean = false;
  isFav: boolean = false;
  favPrograms: any;
  // lat: string = " ";
  // lng: string = "";
  title = 'Hamm';
  lat: number = 51.673858;
  lng: number = 7.815982;

  isMap: boolean = true;
  userData: any = {};
  pageNo: number = 1;
  pageSize: number = 20;
  programs: any;
  isLogin: Boolean = false;
  key: string = '';
  providerRole: boolean = false;
  parentRole: boolean = false;
  favProgramRes: any;
  keyword = 'name';
  searchKey = "";
  isSearched = false;
  isScrol = true;
  fav: any = {
    userId: '',
    programId: '',
  };
  searchedPrograms: any = [];
  searchedProgram: any = [];
  loaderPostion = "center-center"
  loaderType = "ball-spin-clockwise"
  fromDate: any;
  toDate: any;
  fromTime: any;
  toTime: any;
  //  ng5slider start age group
  minAge: number = 0;
  maxAge: number = 100;
  ageOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + ' YRS';
    }
  };
  // ng5slider end

  showReset = false;
  deleteProgramRes: any;
  constructor(
    private router: Router,
    private map: MapService,
    private apiservice: ApiService,
    private ngxLoader: NgxUiLoaderService,
  ) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);
    if (this.userData) {
      this.isLogin = true;
      if (this.userData.role === "provider") {
        this.providerRole = true;
      }
      if (this.userData.role === "parent") {
        this.parentRole = true;
      }
    }

  }
  openModal() { }
  closePopup() {
    if (this.isDateModal) {
      this.fromDate = null;
      this.toDate = null;
      this.isDateModal = false;
      this.isDateFilter = false
    }
    else if (this.isTimeModal) {
      this.toTime = null;
      this.fromTime = null;
      this.isTimeModal = false;
      this.isAgeFilter = false;
    }
    else if (this.isAgeModal) {
      this.maxAge = 100
      this.maxAge = 0
      this.isAgeModal = false;
      this.isAgeFilter = false
    }
  }

  resetFilter() {
    this.showReset = false
    this.isAgeFilter = false
    this.isAgeFilter = false;
    this.isDateFilter = false
    this.fromDate = null;
    this.toDate = null;
    this.toTime = null;
    this.fromTime = null;
    this.maxAge = 100
    this.maxAge = 0
  }

  goToProgramDetail(data) {
    if (this.parentRole) {
      this.addAction(data._id)
    }
    console.log('programId', data._id)
    this.router.navigate(['program/detail', data._id]);
  }

  onScroll() {
    if (this.isScrol) {
      this.isScrol = false
      this.loadMore()
    }
  }

  profile() {
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    if (this.userData) {
      this.router.navigate(['parent/parent-profile']);
    }
  }

  getProgram() {
    this.ngxLoader.start();
    if (this.isSearched) {
      this.programs = this.searchedProgram;
      // this.getFavouriteByParentId();
    }
    else {

      this.apiservice.getProgram(this.pageNo, this.pageSize).subscribe(res => {
        if (res) {
          this.programs = res;
          this.isScrol = true
          this.ngxLoader.stop();
          // this.getFavouriteByParentId();
        }
        this.ngxLoader.stop();
      });

    }
  }
  addFavProgram(userId, programId, index) {
    this.programs[index].isFav = true
    this.fav.userId = userId;
    this.fav.programId = programId;
    this.apiservice.addFavProgram(this.fav).subscribe(res => {
      this.favProgramRes = res;
    });
  }

  deleteFavProgram(favId, index) {
    this.programs[index].isFav = false
    this.apiservice.deleteFavProgram(favId).subscribe(res => {
      this.deleteProgramRes = res;
      console.log('deleted from fav program >>', this.deleteProgramRes);
    });
  }

  addAction(programId) {
    let body = {
      action: "click",
      programId: programId
    }
    this.apiservice.addAction(body).subscribe((res: any) => {
    });
  }

  loadMore() {
    this.loaderType = "three-bounce"
    this.loaderPostion = "bottom-center"
    this.pageSize += 20;
    if (this.showReset) {
      this.programFilter()
    } else {
      this.getProgram();
    }
  }
  // selectEvent(item) {
  //   this.searchedProgram = [];
  //   this.searchedProgram.push(item)
  //   this.isSearched = true;
  //   this.programs = this.searchedProgram;
  //   console.log('search selected program list in search page', this.programs);
  //   this.getProgram();
  // }

  onSearch(val: string) {
    this.programSearch(val)
    console.log('onChangeSearch', val);
  }

  // onFocused(e) {
  //   console.log('onFocused', e)
  //   // do something when input is focused
  // }

  programSearch(key) {
    this.ngxLoader.start();
    this.apiservice.programSearch(key).subscribe((res: any) => {
      if (res) {
        this.programs = res;
        this.ngxLoader.stop();
      }

    });

  }

  programFilter() {
    this.ngxLoader.start();
    this.showReset = true
    this.apiservice.programFilter(this.minAge, this.maxAge, this.fromDate, this.toDate, this.fromTime, this.toTime, this.pageNo, this.pageSize).subscribe((res: any) => {
      if (res) {
        this.programs = res;
        this.isScrol = true
        this.ngxLoader.stop();
      }

    });
    // this.closePopup();
  }
  addFilter() {
    this.programFilter()

  }
  // getFavouriteByParentId() {
  //   if (this.userData) {
  //     this.apiservice.getFavouriteByParentId(this.userData.id).subscribe(res => {
  //       this.favPrograms = res;
  //       for (let i = 0; i < this.favPrograms.length; i++) {
  //         for (let j = 0; j < this.programs.length; j++) {
  //           var fav = this.favPrograms[i];
  //           var program = this.programs[j];
  //           if (fav.program === program) {
  //             this.isFav = true;
  //           } else {
  //             this.isFav = false;
  //           }
  //         }
  //       }
  //     });
  //   }
  // }

  markers: any[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'Technology (A)',
      mapType: 'satellite',
      icon: 'assets/user_group.svg',
      info: 'The Local School of Technology 1',
      draggable: true
    },
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'Technology (A)',
      info: 'The Local School of Technology 1',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'Technology (B)',
      info: 'The Local School of Technology 2',
      draggable: false
    },
    {
      lat: 51.323858,
      lng: 7.255982,
      label: 'Technology (C)',
      info: 'The Local School of Technology 3',
      draggable: false
    },
    {
      lat: 51.293858,
      lng: 7.285982,
      label: 'Technology (D)',
      info: 'The Local School of Technology 4',
      draggable: false
    },
    {
      lat: 51.353858,
      lng: 7.305982,
      label: 'Technology (E)',
      info: 'The Local School of Technology 5',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'Technology (F)',
      info: 'The Local School of Technology 6',
      draggable: true
    }
  ]

  // getLocation() {
  //   this.map.getLocation().subscribe(data => {
  //     console.log(data)
  //     this.lat = data.latitude;
  //     this.lng = data.longitude;
  //   });

  // }

  ngOnInit() {
    this.getProgram();
    // this.getFavouriteByParentId()

    //     var check= ["044", "451"],
    // data = ["343", "333", "044", "123", "444", "555"];

    // var res = check.filter( function(n) { return !this.has(n) }, new Set(data) );

    // console.log(res);



    // var a1 = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
    // var a2 = ['a', 'b', 'c', 'd'];

    // let missing = a1.filter(item => a2.indexOf(item) < 0);
    // console.log(missing);

    // console.log(this.markers);
    // this.getLocation();

  }

}


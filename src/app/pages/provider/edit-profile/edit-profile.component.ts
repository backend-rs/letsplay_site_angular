import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ApiService } from 'src/app/core/services/api.service.service';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { HeaderComponent } from 'src/app/core/components/header/header.component';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  message: string = 'Updated Successfully';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  userData: any = {};
  user = new User;
  updateResponse: any = {};
  providerImgResponse: any;
  formData = new FormData();
  fileData: File = null;
  imagePath;
  bannerPath;
  msg: string;
  providerImgURL: any;
  bannerImgURL: any;

  @ViewChild(HeaderComponent, { static: true }) headerComponent: HeaderComponent;


  constructor(private _location: Location,
    private ngxLoader: NgxUiLoaderService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private router: Router) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;

  }
  getProviderById() {
    this.ngxLoader.start();
    this.apiservice.getProviderById(this.userData.id).subscribe((res) => {
      this.user = res;
      console.log('provider data', this.user);
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }
  updateProviderById(id) {
    console.log('provider data before update', this.user);
    this.ngxLoader.start();
    this.apiservice.updateProviderById(id, this.user).subscribe((res) => {
      this.updateResponse = res;
      this.ngxLoader.stop();
      console.log('update provider data', this.updateResponse);
      if (this.updateResponse.isSuccess === true) {
        this.headerComponent.getProfileProgress();
        this.headerComponent.getUserById();
        this.snack.open(this.message, 'OK', { duration: 4000 });
        this.router.navigate(['/profile']);
      } else {
        this.ngxLoader.stop();
        this.snack.open(this.updateResponse.error, 'OK', { duration: 4000 });
      }
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }

  uploadProviderImage(event, id) {
    console.log('event', event);
    this.fileData = event.target.files[0];
    this.formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.providerImgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

    this.ngxLoader.start();
    this.apiservice.uploadUserImage(id, this.formData).subscribe(res => {
      this.providerImgResponse = res;
      this.ngxLoader.stop();
      console.log('parent IMAGE Response from server = >>> ', this.providerImgResponse);
      if (this.providerImgResponse.isSuccess === true) {
        this.headerComponent.getProfileProgress();
        this.headerComponent.getUserById();
      } else {
        this.snack.open(this.providerImgResponse.error, 'OK', { duration: 4000 });

      }
    });
    this.ngxLoader.stop();
  }

  uploadProviderBanner(event, id) {

    this.fileData = event.target.files[0];
    this.formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.bannerPath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.bannerImgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

    this.ngxLoader.start();
    this.apiservice.uploadProviderBanner(id, this.formData).subscribe(res => {
      this.getProviderById();
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }

  ngOnInit() {
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    this.getProviderById();
  }
  back() {
    this._location.back();
  }
}

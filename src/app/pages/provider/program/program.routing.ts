import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component'
import { AddComponent } from './add/add.component'
import { DetailComponent } from './detail/detail.component'
import { SettingComponent } from './setting/setting.component'

export const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home', component: HomeComponent,
    data: { title: 'home', breadcrumb: 'Home' }
  },
  {
    path: 'list',
    component: ListComponent,
    data: { title: 'List', breadcrumb: 'Programs' }
  },
  {
    path: 'add',
    component: AddComponent,
    data: { title: 'Add', breadcrumb: 'Add' }
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    // data: { title: 'Detail', breadcrumb: 'Program-Detail' }
  },
  {
    path: 'setting',
    component: SettingComponent,
    data: { title: 'Setting', breadcrumb: 'Setting' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramRoutingModule { }




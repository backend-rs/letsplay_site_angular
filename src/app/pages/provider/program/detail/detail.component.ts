import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Options } from 'ng5-slider';
import { FormGroup, FormControl } from '@angular/forms';
import { Program, User } from 'src/app/core/models';
import { ApiService } from 'src/app/core/services/api.service.service';
import { MatSnackBar, MatAutocompleteSelectedEvent, MatChipInputEvent, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable } from 'rxjs';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { ToastyService } from 'ng2-toasty';
import { Claim } from 'src/app/core/models/claim.model';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  programUpdateForm: FormGroup;
  userData: any = {};
  program = new Program;
  // minValue: number = 0;
  // maxValue: number = 100;
  options: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + ' YRS';
    }
  };

  isLogin = false;
  providerRole: boolean = false;
  claim = new Claim;
  updateProgramResponse: any;
  batchData: any;
  isName = false;
  isTag = false;
  isDescription = false;
  isBookingCancle = false;
  isInstruction = false;
  isBatch = false;
  isPricePerParticipant = false;
  formData = new FormData();
  fileData: File = null;
  imagePath;
  msg: string;

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  keyword = 'name';
  separatorKeysCodes: number[] = [ENTER, COMMA];

  fruitCtrl = new FormControl();

  filteredFruits: Observable<any[]>;
  filteredValues: Observable<any[]>;
  getTagResponse: any;

  tags: any = [];
  @ViewChild('fruitInput', { static: true }) fruitInput: ElementRef;

  message: string = 'claim request submited!';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  programImgURL: any;
  getUrl: any;
  user: User;
  constructor(private apiservice: ApiService,
    private snack: MatSnackBar,
    private router: Router,
    private ngxLoader: NgxUiLoaderService,
    private toastyService: ToastyService,
    private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.program.id = params['id'];
      this.getProgramById()
    });
    this.userData = JSON.parse(localStorage.getItem('userData'));
    // this.program = JSON.parse(localStorage.getItem('program'));
    if (this.userData) {
      this.isLogin = true;
      if (this.userData.role === "provider") {
        this.providerRole = true;
      }
    }
  }
  openModal(data, batch: any) {
    this.batchData = batch;
    console.log('batch', this.batchData);
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.program.tags.push(
        {
          _id: '',
          name: value.trim()
        }
      );
      console.log('tagsssss', this.program.tags);
    }
    if (input) {
      input.value = '';
    }

    this.fruitCtrl.setValue(null);
    console.log('tagsssss', this.program.tags);
  }

  remove(fruit, indx): void {
    this.program.tags.splice(indx, 1);
    console.log('tagsssss', this.program.tags);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.program.tags.push(event.option.value);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  searchTag(key) {
    this.apiservice.searchTag(key).subscribe(res => {
      this.getTagResponse = res;
      this.tags = this.getTagResponse;
      console.log('category list categories', this.tags);
    });

  }

  selectEvent(item) {
    this.program.tags.push(item)
    console.log('selectEvent', item);
    console.log('intr   ests', this.program.tags);

    // do something with selected item
  }
  goToProfile() {
    this.router.navigate(['/profile']);
    localStorage.setItem('userId', JSON.stringify(this.program.user));
  }

  onChangeSearch(val: string) {
    this.searchTag(val)
    console.log('onChangeSearch', val)
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    console.log('onFocused', e)
    // do something when input is focused
  }
  closePopup() {
    this.isName = false;
    this.isTag = false;
    this.isDescription = false;
    this.isBookingCancle = false;
    this.isInstruction = false;
    this.isBatch = false;
    this.isPricePerParticipant = false;
    this.isBookingCancle = false;

  }

  uploadCoverPic(event, id) {

    this.fileData = event.target.files[0];
    this.formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.programImgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

    this.ngxLoader.start();
    this.apiservice.getPicUrl(this.formData).subscribe(res => {
      console.log('res', res);
      this.getUrl = res.data;
      this.updateProgram(id);
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }

  updateProgram(id) {
    var batch: any;
    this.closePopup();
    this.program.programCoverPic = this.getUrl;
    this.program.userId = this.userData.id;
    let totalBatch = this.program.batches.length - 1
    if (this.batchData) {
      for (let i = 0; i <= totalBatch; i++) {
        batch = this.program.batches[i];

        if (this.batchData._id === batch._id) {
          this.program.batches[i] = this.batchData;
          console.log('batchData in for loop', this.batchData);
          console.log('program.batches[i] in for loop', this.program.batches[i]);
        }
      }
    }
    console.log('program with batch update ', this.program);
    this.apiservice.updateProgram(id, this.program).subscribe(res => {
      this.updateProgramResponse = res;
      console.log('updated program', this.updateProgramResponse);
      this.ngxLoader.stop();
      if (this.updateProgramResponse.isSuccess === true) {
        this.snack.open('Program Updated successfully', 'OK', { duration: 5000 });
      } else {
        this.snack.open(this.updateProgramResponse.error, 'OK', { duration: 5000 });
      }
    });
    this.ngxLoader.stop();
  }
  getProgramById() {
    this.apiservice.getProgramById(this.program.id).subscribe(res => {
      // localStorage.removeItem('program');
      this.program = res
      // localStorage.setItem('program', JSON.stringify(res));
      this.getProviderById();
    });
    this.ngxLoader.stop();
  }

  claimBusiness() {

    if (this.userData && this.userData.role === 'provider') {
      this.claim.status = "in-progress";
      this.claim.requestBy = this.userData.id;
      this.claim.requestOn = this.program.user;
      this.ngxLoader.start();
      this.apiservice.claimRequest(this.claim).subscribe(res => {
        console.log('claim request response ', res);
        this.toastyService.info({ title: 'Info', msg: this.message })
        // this.snack.open(this.message, 'OK', { duration: 4000 });
        this.ngxLoader.stop();

      }); this.ngxLoader.stop();

    } else {
      if (this.userData && this.userData.role === 'parent') {
        this.ngxLoader.start();
        let msg = 'please  register or login as provider to claim this business!';
        this.toastyService.info({ title: 'Info', msg: msg })
        this.router.navigate(['/login']);
        this.ngxLoader.stop();

      } else {
        this.ngxLoader.start();
        let msg = 'please login to claim this business and try again!'
        this.toastyService.info({ title: 'Info', msg: msg })
        this.router.navigate(['/login']);
        this.ngxLoader.stop();
      }
    }

  }
  getProviderById() {

    this.ngxLoader.start();
    this.apiservice.getProviderById(this.program.user).subscribe(res => {
      if (!this.providerRole) {
        this.addAction(this.program.id)
      }

      this.user = res;
      console.log('user>>', this.user);
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }
  addAction(programId) {
    let body = {
      action: "view",
      programId: programId
    }
    this.apiservice.addAction(body).subscribe((res: any) => {
      console.log('action response', res)
    });
  }

  ngOnInit() {
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    this.programImgURL = this.program.programCoverPic;

    this.getProgramById();
    // this.getProviderById();

  }

}

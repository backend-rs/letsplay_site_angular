import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ApiService } from 'src/app/core/services/api.service.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userData: any = {};
  activeProgramCount: number = 0
  viewCount: number = 0
  isGraphData: Boolean = true
  sharedChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
      position: 'bottom'
    }
  };
  chartColors: Array<any> = [{

    borderColor: ' #777CEA;',
    pointBackgroundColor: ' #777CEA;',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }, {
    backgroundColor: '#F15C20',
    borderColor: '#F15C20',
    pointBackgroundColor: '#F15C20',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }, {
    backgroundColor: ' #FFB206',
    borderColor: ' #FFB206',
    pointBackgroundColor: ' #FFB206',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }];
  /*
  * Bar Chart
  */
  barChartLabels: string[] = ["program1", "program2", "program3", "program4"]  //program name
  barChartType = 'bar';
  barChartLegend = true;
  // barChartData: any[] = []
  barChartData: any[] = [{}]
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  constructor(private apiservice: ApiService, private ngxLoader: NgxUiLoaderService, ) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);
  }
  programCount() {
    this.ngxLoader.start();
    this.apiservice.getProgramCount(this.userData.id).subscribe((res: any) => {
      this.activeProgramCount = res
      this.ngxLoader.stop();
      console.log('program count>>>>>>>>>>', res);
    });
    this.ngxLoader.stop();
  }

  getViewCount() {
    this.ngxLoader.start();
    this.apiservice.getView(this.userData.id).subscribe((res: any) => {
      this.viewCount = res.count
      this.ngxLoader.stop();
      console.log('View count>>>>>>>>>>', res);
    });
    this.ngxLoader.stop();
  }

  getGraphData() {
    this.ngxLoader.start();
    this.apiservice.graphData(this.userData.id).subscribe((res: any) => {
      if (res.barChartLabels.length) {
        this.isGraphData = true
        this.barChartLabels = res.barChartLabels
        this.barChartData = res.barChartData

      }
      this.ngxLoader.stop();
      console.log('getGraphData', this.barChartData);
    });
    this.ngxLoader.stop();
  }

  ngOnInit() {
    this.programCount()
    this.getViewCount()
    this.getGraphData()
  }

}

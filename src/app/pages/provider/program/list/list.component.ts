import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig } from '@angular/material';
import { Program, User } from 'src/app/core/models';
import { async } from '@angular/core/testing';

import { DataSource } from '@angular/cdk/collections';
import { Observable, of } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';





@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})



export class ListComponent implements OnInit {
  pageNo = 1;
  pageSize = 20;
  message: string = '';
  action: boolean = true;
  isScrol: boolean = true;
  loaderPostion = "center-center"
  loaderType = "ball-spin-clockwise"
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  userData: any = new User;
  programs = new Program;
  batchData = [];
  constructor(private router: Router,
    private apiservice: ApiService,
    private ngxLoader: NgxUiLoaderService,
  ) {

    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);
  }

  onScroll() {
    if (this.isScrol) {
      this.isScrol = false
      this.loadMore()
    }
  }

  addProgram() {
    this.router.navigate(['/program/add']);
  }
  programDetail(data) {

    if (data.user === this.userData.id) {
      console.log('programId', data._id)
      this.router.navigate(['program/detail', data._id]);
    }

  }
  programActiveInActive(program) {
    let programStatus = '';
    if (program.status === 'active') {
      programStatus = 'inactive';
      this.apiservice.programActiveInActive(program._id, programStatus).subscribe(res => {
        this.getProviderProgram()
      });

    }
    else {
      programStatus = 'active';
      this.apiservice.programActiveInActive(program._id, programStatus).subscribe(res => {
        this.getProviderProgram()
      });

    }


  }
  getProviderProgram = async () => {
    this.ngxLoader.start();
    await this.apiservice.getProgramByProvider(this.userData.id, this.pageNo, this.pageSize).subscribe((res) => {
      this.isScrol = true
      this.programs = res
      this.ngxLoader.stop();
    });
  }


  loadMore() {
    this.loaderType = "three-bounce"
    this.loaderPostion = "bottom-center"
    this.pageSize += 20
    this.getProviderProgram();
  }

  getBatchData(batches) {
    this.batchData = batches;
    console.log('batches', this.batchData);
  }

  ngOnInit() {
    this.getProviderProgram();
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    // this.getProgramByProvider();


  }

}





import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MatDialog, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatSnackBar, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { AddBatchPopupComponent } from './add-batch-popup/add-batch-popup.component';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';
import { Options } from 'ng5-slider';
import { Program } from '../../../../core/models';
import { Observable } from 'rxjs';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import * as moment from 'moment';
import { Batch } from 'src/app/core/models/batch.model';
declare var $: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  formData = new FormData();
  fileData: File = null;
  addProgramForm: FormGroup;
  isSingleEvent = false;
  isScheduled = false;
  isUnScheduled = false;
  isAddBatch = true;
  isFree = false;
  adultAssistanceIsRequried = false;
  selectedPlan: string;
  program = new Program;
  fromDate = new Date;
  toDate = new Date;
  fromTime = new Date;
  toTime = new Date;
  response: any
  userData: any = {};
  getTagResponse: any;


  // ---------------autucomplete-------------  
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  keyword = 'name';

  separatorKeysCodes: number[] = [ENTER, COMMA];

  fruitCtrl = new FormControl();

  filteredFruits: Observable<any[]>;
  filteredValues: Observable<any[]>;

  tags: any = [];
  tag: any = [];
  batches: any = [];

  //  ng5slider start age group

  minAge: number = 3;
  maxAge: number = 10;

  ageOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + ' YRS';
    }

  };
  // ng5slider end

  //  ng5slider start capacity

  minCapacity: number = 0;
  maxCapacity: number = 30;

  capacityOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + '';
    }

  };
  // ng5slider end
  email: string;
  bookingCancelledIn = {
    days: "",
    hours: ""
  };



  time = {
    from: Date(),
    to: Date(),
  };
  date = {
    from: Date(),
    to: Date()
  };
  batch = new Batch;
  isLoading: Boolean = false
  message: string = 'Updated Successfully';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private fb: FormBuilder, private dialog: MatDialog,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private router: Router,
    // private confirmDialogService: ConfirmDialogService,
    private ngxLoader: NgxUiLoaderService) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }
  onChange(data) { }

  // valueChange(value: number,): void {
  //   this.minAge = value;
  //   console.log('changed value', this.minAge);
  // }
  openPopUp(): void {
    const dialogRef = this.dialog.open(AddBatchPopupComponent, {
      width: '500px',
      data: { name: 'satnm', animal: 'abc' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });


  }
  selectSingleEvent() {
    this.isSingleEvent = true;
    this.isScheduled = false;
    this.isUnScheduled = false;
    this.isAddBatch = true;
    this.program.type = 'single'
  }
  selectScheduled() {
    this.isSingleEvent = false;
    this.isScheduled = true;
    this.isUnScheduled = false;
    this.isAddBatch = true;
    this.program.type = 'scheduled'
  }

  selectUnScheduled() {
    this.isSingleEvent = false;
    this.isScheduled = false;
    this.isUnScheduled = true;
    this.isAddBatch = false;
    this.program.type = 'unScheduled'
  }
  addBatch() {

    this.batches.push(this.batch);
    this.batch = new Batch;
    console.log('program batches', this.batches);
  }

  // add(event: MatChipInputEvent): void {
  //   const input = event.input;
  //   const value = event.value;
  //   if ((value || '').trim()) {
  //     this.tag.push(
  //       {
  //         _id: '',
  //         name: value.trim()
  //       }
  //     );
  //     console.log('tagsssss', this.tag);
  //   }
  //   if (input) {
  //     input.value = '';
  //   }

  //   this.fruitCtrl.setValue(null);
  //   console.log('tagsssss', this.tag);
  // }

  remove(fruit, indx): void {
    this.tag.splice(indx, 1);
    console.log('tagsssss', this.tag);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tag.push(event.option.value);
  }



  selectEvent(item) {
    this.tag.push(item)
    console.log('selectEvent', item);
    console.log('intrests', this.tag);
  }

  onChangeSearch(key: string) {
    this.isLoading = true
    this.ngxLoader.start();
    this.tags = []
    this.apiservice.searchTag(key).subscribe((res: any) => {
      this.tags = res;
      console.log('searchTag list categories', this.tags);
      this.ngxLoader.stop()
      this.isLoading = false
    });

    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    console.log('onFocused', e)
    // do something when input is focused
  }

  publishProgram() {

    this.program.capacity.min = this.minCapacity
    this.program.capacity.max = this.maxCapacity
    this.program.ageGroup.from = this.minAge
    this.program.ageGroup.to = this.maxAge
    this.ngxLoader.start();
    this.program.userId = this.userData.id;
    this.program.bookingCancelledIn = this.bookingCancelledIn;
    this.program.time.from = new Date(this.fromTime);
    this.program.time.to = new Date(this.toTime);
    this.program.date.from = moment(this.fromDate).format('MM-DD-YYYY')
    this.program.date.to = moment(this.toDate).format("MM-DD-YYYY")
    this.program.isFree = this.isFree;
    this.program.adultAssistanceIsRequried = this.adultAssistanceIsRequried;
    this.program.batches = this.batches;
    this.program.tags = this.tag;
    console.log('progam date', this.program.date);
    console.log('progam time', this.program.time);
    console.log('program data before upload', this.program);

    this.apiservice.addProgram(this.program).subscribe(res => {
      this.response = res
      this.ngxLoader.stop();
      if (this.response.isSuccess === true) {
        this.snack.open('Program Added successfully', 'OK', { duration: 5000 });
        this.router.navigate(["/program/list"]);
      } else {
        this.router.navigate(["/program/list"]);
        this.snack.open('Program Added successfully', 'OK', { duration: 5000 });
      }
    });
    this.ngxLoader.stop();
  }

  ngOnInit() {
    this.addProgramForm = new FormGroup({
      description: new FormControl('', [Validators.required]),
      // rememberMe: new FormControl(false)
    });
  }
  selectPlan(plan) {
    console.log('plan', plan)
    this.selectedPlan = plan
    console.log('selectedPlan', this.selectedPlan)
  }

  onImageDrop = (event) => {
    this.UploadImage(event)
  }

  UploadImage(files) {

    this.fileData = files[0];
    this.formData.append('image', this.fileData);
    console.log('formdata', this.formData);
    this.apiservice.getPicUrl(this.formData).subscribe((res: any) => {
      if (res.isSuccess) {
        this.program.timelinePics = []
        this.program.timelinePics.push(res.data)
      }
    })
    this.isLoading = false
  }
}

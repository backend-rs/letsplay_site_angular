import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-batch-popup',
  templateUrl: './add-batch-popup.component.html',
  styleUrls: ['./add-batch-popup.component.css']
})
export class AddBatchPopupComponent implements OnInit {
  public itemForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddBatchPopupComponent>,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    // this.buildItemForm(this.data.payload)
  }
  buildItemForm(item) {
    this.itemForm = this.fb.group({
      name: [item.name || '', Validators.required],
      age: [item.age || ''],
      // email: [item.email || ''],
      // company: [item.company || ''],
      // phone: [item.phone || ''],
      // address: [item.address || ''],
      // balance: [item.balance || ''],
      // isActive: [item.isActive || false]
    })
  }
  close() {
    this.dialogRef.close()

  }

  submit() {
    this.dialogRef.close()
    // or
    // this.dialogRef.close(this.itemForm.value)
  }
}

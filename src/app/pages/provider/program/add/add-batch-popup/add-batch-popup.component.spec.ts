import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBatchPopupComponent } from './add-batch-popup.component';

describe('AddBatchPopupComponent', () => {
  let component: AddBatchPopupComponent;
  let fixture: ComponentFixture<AddBatchPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBatchPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBatchPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

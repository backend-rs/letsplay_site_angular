import { NgModule } from '@angular/core';
import { ProgramRoutingModule } from './program.routing';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component'
import { AddComponent } from './add/add.component'
import { DetailComponent } from './detail/detail.component'
import { CoreModule } from '../../../core/core.module';
import { Ng5SliderModule } from 'ng5-slider';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatSliderModule, MatIconModule, MatStepperModule, MatMenuModule, MatTableModule } from '@angular/material';
import { AddBatchPopupComponent } from '../program/add/add-batch-popup/add-batch-popup.component';
import { ModalComponent } from '../program/setting/modal/modal.component';
// import { ProgramComponent } from './program.component';
import { SettingComponent } from './setting/setting.component'
import { CustomFormsModule } from 'ng2-validation';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { ChartsModule } from 'ng2-charts';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  entryComponents: [AddBatchPopupComponent, ModalComponent],
  declarations: [
    HomeComponent,
    ListComponent,
    AddComponent,
    DetailComponent,
    AddBatchPopupComponent,
    ModalComponent,
    SettingComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    CustomFormsModule,
    ReactiveFormsModule,
    CoreModule,
    MatSliderModule,
    Ng5SliderModule,
    NgxUiLoaderModule,
    MatIconModule,
    CommonModule,
    MatStepperModule,
    MatMenuModule,
    ProgramRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ChartsModule,
    AutocompleteLibModule,
    MatTableModule,
    InfiniteScrollModule,
    // RouterModule.forChild(ProgramRoutes)
  ],
  exports: [
    HomeComponent,
    ListComponent,
    AddComponent,
    DetailComponent,
    SettingComponent
    // ProgramComponent
  ]
})
export class ProgramModule { }

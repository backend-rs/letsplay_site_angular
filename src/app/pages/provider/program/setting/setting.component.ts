import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MatDialog, MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { ModalComponent } from './modal/modal.component';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CustomValidators } from 'ng2-validation';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderComponent } from 'src/app/core/components/header/header.component';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserDataService } from 'src/app/core/services/user-data.service';
import { User } from '../../../../core/models';
import { ToastyService } from 'ng2-toasty';
import { Claim } from 'src/app/core/models/claim.model';
declare var $: any;
@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})

export class SettingComponent implements OnInit {
  @ViewChild(HeaderComponent, { static: true }) headerComponent: HeaderComponent;
  user = new User;
  message: string = 'Updated Successfully';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  resetPasswordForm: FormGroup;
  isVerification = false;
  isSecurity = true;
  answer: string = '';
  isClaimStatus = false;
  verification = '';
  security = 'active';
  claimStatus = '';
  userData: any = {};
  isAgree: Boolean = false;
  isAnswerVerify: Boolean = false;
  isOldQuestion: Boolean = false;
  resetPasswordResponse: any = {};

  resetPasswordData: any = {
    oldPassword: '',
    newPassword: '',
  }
  claimRequestList: any = new Claim;


  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private ngxLoader: NgxUiLoaderService,
    private snack: MatSnackBar,
    private router: Router,
    private auth: AuthService,
    private activatedRoute: ActivatedRoute,
    private apiservice: ApiService,
    private userdataservice: UserDataService,
    private toasty: ToastyService,
  ) {
    // this.user = this.auth.currentUser();
    // if (this.user.securityQuestion != "" && this.user.securityQuestion != undefined && this.user.securityQuestion != null) {
    //   this.isOldQuestion = true
    // }
    // this.auth.userChanges.subscribe(user => this.user = user)
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);

  }

  openPopUp(value): void {
    // const dialogRef = this.dialog.open(ModalComponent, {
    //   width: '500px',
    //   data: { name: value }
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   // this.animal = result;
    // });

  }
  submit() {
    if (!this.isOldQuestion) {
      if (!this.isAgree) {
        let msg = 'please mark check box first';
        // this.toasty.error(msg);
        alert(msg)
        return
      }
      this.user.answer = this.answer
      this.ngxLoader.start();
      this.apiservice.updateProviderById(this.userData.id, this.user).subscribe((res: any) => {
        this.ngxLoader.stop();
        this.getUser(this.userData.id)

        // $('#SequrityModal').hide();
        // this.router.navigate(['/program/setting']);
      });
    }
    else {
      this.verifyAns()
    }


  }
  verifyAns() {

    let verifyAnsBody: any = {
      answer: this.answer

    }
    if (!this.isAgree) {
      let msg = 'please mark check box first';
      // this.toasty.error(msg);
      alert(msg)
      return
    }
    this.ngxLoader.start();
    this.apiservice.verifyAns(this.userData.id, verifyAnsBody).subscribe((res: any) => {
      if (res.isSuccess) {
        this.isOldQuestion = false;
      }
      else {
        alert('Wrong Answer')
        this.isOldQuestion = false;
      }
      this.ngxLoader.stop();
    });
  }

  onSecurity() {
    this.isSecurity = true;
    this.isVerification = false;
    this.isClaimStatus = false;
    this.security = 'active';
    this.verification = '';
    this.claimStatus = '';

  }
  onVerification() {
    this.isSecurity = false;
    this.isVerification = true;
    this.isClaimStatus = false;

    this.verification = 'active';
    this.security = '';
    this.claimStatus = '';
  }
  onClaimStatus() {
    this.isSecurity = false;
    this.isVerification = false;
    this.isClaimStatus = true;

    this.verification = '';
    this.security = '';
    this.claimStatus = 'active';
  }

  getUser(id) {
    this.ngxLoader.start();
    this.apiservice.getUserById(id).subscribe((res: any) => {
      this.user = res
      if (this.user.securityQuestion != "" && this.user.securityQuestion != undefined && this.user.securityQuestion != null) {
        this.isOldQuestion = true
      }
      this.ngxLoader.stop();
      this.router.navigate(['/program/setting']);
    });
  }

  resetPassword(id) {
    this.ngxLoader.start();
    this.apiservice.resetPassword(id, this.resetPasswordData).subscribe(res => {
      this.resetPasswordResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', res);

      if (this.resetPasswordResponse.isSuccess === true) {
        this.snack.open(this.resetPasswordResponse.message, 'OK', { duration: 5000 });
        this.router.navigate(['/login']);
      } else {
        if (this.userData === null || this.userData === undefined) {
          this.router.navigate(['/login']);
          let msg = 'Please Login First!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
        else {
          if (this.resetPasswordResponse.error === 'Old Password Not Match') {
            this.snack.open(this.resetPasswordResponse.error, 'OK', { duration: 5000 });
          }
          else {
            let msg = 'Something Went Wrong!';
            this.snack.open(msg, 'OK', { duration: 5000 });
          }
        }
      }
    });
  }
  ClaimRquestListByProvider() {
    this.ngxLoader.start();
    this.apiservice.ClaimRquestListByProvider(this.userData.id).subscribe(res => {
      this.claimRequestList = res;
      this.ngxLoader.stop();
      console.log('claim req list', res);
    });
    this.ngxLoader.stop();
  }
  ngOnInit() {
    this.ClaimRquestListByProvider();
    this.getUser(this.userData.id)

    let newPassword = new FormControl('', [Validators.required]);
    let confirmPassword = new FormControl('', [Validators.required,
    CustomValidators.equalTo(newPassword)]);
    this.resetPasswordForm = new FormGroup({
      oldPassword: new FormControl('', [Validators.required]),
      newPassword: newPassword,
      confirmPassword: confirmPassword
    });
  }

}

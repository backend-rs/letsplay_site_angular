import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { User } from 'src/app/core/models/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HeaderComponent } from 'src/app/core/components/header/header.component';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { MapService } from 'src/app/services/map.service';
import { Program } from 'src/app/core/models';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userUpdateForm: FormGroup;
  lat: number = 51.673858;
  lng: number = 7.815982;

  message: string = 'Updated Successfully';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  isName = false;
  isAbout = false;
  isInformation = false;
  isLogin = false;
  providerRole: boolean = false;
  userData: any = {};
  otherUserId: any = {};
  user = new User;
  profileProgressResponse: any = {};
  updateResponse: any = {};

  providerImgURL: any;
  formData = new FormData();
  fileData: File = null;
  imagePath;
  msg: string;
  @ViewChild(HeaderComponent, { static: true }) headerComponent: HeaderComponent;
  constructor(private router: Router,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private map: MapService,
    private ngxLoader: NgxUiLoaderService) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);
    this.otherUserId = JSON.parse(localStorage.getItem('userId'));
    if (this.userData) {
      this.isLogin = true;
      if (this.userData.role === "provider") {
        this.providerRole = true;
      }
    }

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;

  }




  // markers: any[] = [
  //   {
  //     lat: 51.673858,
  //     lng: 7.815982,
  //     label: 'Technology (A)',
  //     mapType: 'satellite',
  //     icon: 'assets/user_group.svg',
  //     info: 'The Local School of Technology 1',
  //     draggable: true
  //   },
  //   {
  //     lat: 51.673858,
  //     lng: 7.815982,
  //     label: 'Technology (A)',
  //     info: 'The Local School of Technology 1',
  //     draggable: true
  //   },
  //   {
  //     lat: 51.373858,
  //     lng: 7.215982,
  //     label: 'Technology (B)',
  //     info: 'The Local School of Technology 2',
  //     draggable: false
  //   },
  //   {
  //     lat: 51.323858,
  //     lng: 7.255982,
  //     label: 'Technology (C)',
  //     info: 'The Local School of Technology 3',
  //     draggable: false
  //   },
  //   {
  //     lat: 51.293858,
  //     lng: 7.285982,
  //     label: 'Technology (D)',
  //     info: 'The Local School of Technology 4',
  //     draggable: false
  //   },
  //   {
  //     lat: 51.353858,
  //     lng: 7.305982,
  //     label: 'Technology (E)',
  //     info: 'The Local School of Technology 5',
  //     draggable: false
  //   },
  //   {
  //     lat: 51.723858,
  //     lng: 7.895982,
  //     label: 'Technology (F)',
  //     info: 'The Local School of Technology 6',
  //     draggable: true
  //   }
  // ]
  uploadCoverPic(event, id) {

    this.fileData = event.target.files[0];
    this.formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.providerImgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

    this.ngxLoader.start();
    this.apiservice.uploadProviderBanner(id, this.formData).subscribe(res => {
      this.getProviderById();
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }

  profileEdit() {
    this.router.navigate(['/edit-profile']);
  }
  openPopUp() { }
  closePopup() {
    this.isName = false;
    this.isAbout = false;
    this.isInformation = false;
  }

  updateProviderById(id) {
    this.closePopup();
    console.log('provider data before update', this.user);
    this.ngxLoader.start();
    this.apiservice.updateProviderById(id, this.user).subscribe((res) => {
      this.updateResponse = res;
      this.ngxLoader.stop();
      console.log('update provider data', this.updateResponse);
      if (this.updateResponse.isSuccess === true) {
        this.headerComponent.getProfileProgress();
        this.headerComponent.getUserById();
        this.snack.open(this.message, 'OK', { duration: 4000 });
        this.router.navigate(['/profile']);
      } else {
        this.ngxLoader.stop();
        this.snack.open(this.updateResponse.error, 'OK', { duration: 4000 });
      }
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }

  getProviderById() {
    if (this.otherUserId) {
      this.ngxLoader.start();
      this.apiservice.getProviderById(this.otherUserId).subscribe((res) => {
        this.user = res;
        console.log('provider data', this.user);
        this.ngxLoader.stop();
      });
      this.ngxLoader.stop();
    }
    else {
      this.ngxLoader.start();
      this.apiservice.getProviderById(this.userData.id).subscribe((res) => {
        this.user = res;
        console.log('provider data', this.user);
        this.ngxLoader.stop();
      });
      this.ngxLoader.stop();
    }
  }

  getProfileProgress() {
    this.ngxLoader.start();
    this.apiservice.getProfileProgress(this.userData.id, this.userData.role).subscribe(res => {
      this.profileProgressResponse = res;
      console.log('got profileProgress Data', this.profileProgressResponse);
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }

  ngOnInit() {
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    this.getProfileProgress();
    this.getProviderById();

    this.userUpdateForm = new FormGroup({
      firstName: new FormControl('', []),
      about: new FormControl('', []),
      addressLine1: new FormControl('', []),
      addressLine2: new FormControl('', []),
      email: new FormControl('', []),
      phoneNumber: new FormControl('', []),
      website: new FormControl('', []),
      facebook: new FormControl('', []),
      twitter: new FormControl('', []),
      instagram: new FormControl('', []),


    });
  }
  ngOnDestroy() {
    localStorage.removeItem('userId');
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { CoreModule } from 'src/app/core/core.module';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { AgmCoreModule } from '@agm/core';
const routes: Routes = [
  {
    path: '', component: ProfileComponent, children: [

    ]
  }
]


@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    CustomFormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCayIBisLl_xmSOmS3g524FAzEI-ZhT1sc'
    }),
    CoreModule,
    NgxUiLoaderModule,
    RouterModule.forChild(routes)
  ]
})
export class ProfileModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { ParentProfileComponent } from './parent-profile.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatChipsModule, MatAutocompleteModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { CustomFormsModule } from 'ng2-validation';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
const routes: Routes = [
  {
    path: '', component: ParentProfileComponent, children: [

    ]
  }
]


@NgModule({
  declarations: [ParentProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    CustomFormsModule,
    ReactiveFormsModule,
    CoreModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    AutocompleteLibModule,
    NgxUiLoaderModule,
    MatTooltipModule,
    RouterModule.forChild(routes)
  ],
})
export class ProfileModule { }

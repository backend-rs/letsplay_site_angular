import { Component, OnInit, ViewChild } from '@angular/core';
import { UserDataService } from 'src/app/core/services/user-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/services/api.service.service';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatSnackBar, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { Router } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CustomValidators } from 'ng2-validation';
import { Child } from '../../../core/models/child.model'
import { User } from '../../../core/models/user.model'
import { HeaderComponent } from 'src/app/core/components/header/header.component';


@Component({
  selector: 'parent-profile',
  templateUrl: './parent-profile.component.html',
  styleUrls: ['./parent-profile.component.css']
})
export class ParentProfileComponent implements OnInit {
  updateForm: FormGroup;
  resetPasswordForm: FormGroup;
  addChildForm: FormGroup;
  editChildForm: FormGroup;
  addGuardianForm: FormGroup;
  tellFriendForm: FormGroup;
  giveFeedbackForm: FormGroup;
  kid = new Child;
  user = new User;
  kids: Child[] = [];
  isSideBar: Boolean = true;
  msg: string;
  parentImgResponse: any;
  childImgResponse: any;
  tellFriendResponse: any;
  getTagResponse: any;
  giveFeedbackResponse: any;
  UpdateChildImgResponse: any;
  favourites: any = [];
  activeDeactiveResponse: any;
  getGuardianResponse: any;
  addGuardianResponse: any;
  profileProgressResponse: any = {};
  profileProgress: 0

  fileData: File = null;
  imagePath;
  parentImgURL: any;
  childImgURL: any;
  updateChildImgURL: any
  // formData = new FormData();
  isProfile = true; profile = 'active';
  isSetting = false; setting = '';
  isGuardian = false; guardian = '';
  isInvite = false;
  isChildren = false; children = '';
  isAddChild = false;
  isEditChildBtn = false;
  isAddChildBtn = false
  isFavourite = false; favourite = '';
  isNotification = false; notification = '';
  isFriend = false; friend = '';
  isFeedback = false; feedback = '';
  // ---------------autucomplete-------------  
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  addGuardianData: any = {
    firstName: '',
    email: '',
    personalNote: '',
    parentId: '',
  };
  categoryIds: [] = []
  separatorKeysCodes: number[] = [ENTER, COMMA];

  @ViewChild(HeaderComponent, { static: true }) headerComponent: HeaderComponent;

  message: string = 'Updated Successfully';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  updateResponse: any = {};
  userData: any = {};
  getChildResponse: any;
  resetPasswordResponse: any = {};
  editChild: any;
  getTagByCategoryResponse: any = {};
  getCategoryResponse: any;
  keyword = 'name';
  SelectedCategories: any = [];
  childImageURl: ''
  resetPasswordData: any = {
    oldPassword: '',
    newPassword: '',
  }
  tellFriendData: any = {
    parentName: '',
    fullName: '',
    email: '',
    personalNote: '',
  }
  giveFeedbackData: any = {
    id: '',
    name: '',
    email: '',
    feedback: '',
  }
  // ------------------------------------
  isLoading: Boolean = false
  tags: any = [];
  constructor(private userdataservice: UserDataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private router: Router,
    // private confirmDialogService: ConfirmDialogService,
    private ngxLoader: NgxUiLoaderService) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;



  }
  // showDialog() {
  //   this.confirmDialogService.confirmThis("Are you sure to delete?", function () {
  //     alert("Yes clicked");
  //   }, function () {
  //     alert("No clicked");
  //   })
  // }

  sideBar() {
    this.isSideBar = !this.isSideBar;
  }
  back() {

    this.router.navigate(['parent/search']);

  }
  refreshPage() {
    this.ngxLoader.start();
    this.kid = new Child;
    this.kid.interestInfo = [];
    this.tags = [];
    this.ngxLoader.stop();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.kid.interestInfo.push(event.option.value);
  }

  remove(indx): void {
    this.kid.interestInfo.splice(indx, 1);
    console.log(this.kid.interestInfo);
  }


  selectEvent(item) {
    this.kid.interestInfo.push(item)
    console.log('selectEvent', item);
    console.log('intrests', this.kid.interestInfo);
  }

  onChangeSearch(key: string) {
    this.isLoading = true
    this.ngxLoader.start();
    this.tags = []
    this.apiservice.searchTag(key).subscribe((res: any) => {
      this.tags = res;
      console.log('searchTag list categories', this.tags);
      this.ngxLoader.stop()
      this.isLoading = false
    });

    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    console.log('onFocused', e)
    // do something when input is focused
  }

  onProfile() {
    this.isProfile = true; this.profile = 'active';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = '';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
    return this.getParentById();

  }

  onGuardian(id) {

    this.ngxLoader.start();
    this.apiservice.getGuardianByParentId(id).subscribe(res => {
      this.getGuardianResponse = res;
      this.ngxLoader.stop();
      console.log('guardian list>>', this.getGuardianResponse);
    });
    this.ngxLoader.stop();

    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = true; this.guardian = 'active';
    this.isChildren = false; this.children = '';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
  }

  // ------------------guardian invite-----------------
  onGuardianAdd() {
    this.isGuardian = false;
    this.isInvite = true;
  }
  // --------------------------------------------------

  onChildren(id) {
    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = true; this.children = 'active';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;

    this.ngxLoader.start();
    this.apiservice.getChildByParentId(id).subscribe(res => {
      this.kids = res;
      console.log('kids', this.kids)
      this.ngxLoader.stop();
      console.log('get child res from model', res);
      if (this.userData === null || this.userData === undefined) {
        this.router.navigate(['/login']);
        let msg = 'Please Login First!';
        this.snack.open(msg, 'OK', { duration: 5000 });
      } this.ngxLoader.stop();

    });
    this.ngxLoader.stop();
  }
  onAddChild() {
    this.isAddChildBtn = true;
    this.isChildren = false;
    this.isAddChild = true;
    this.isEditChildBtn = false;
    this.kid = new Child;
    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = 'active';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isEditChildBtn = false;

  }

  onEditChild(child) {
    this.kid = child;
    this.isChildren = false;
    this.isAddChild = true;
    this.isAddChildBtn = false;
    this.isEditChildBtn = true
    console.log('kid detail', this.kid);
  }
  getFav(id) {
    this.apiservice.getFavouriteByParentId(id).subscribe(res => {
      this.favourites = res;
      this.ngxLoader.stop();
    });

    this.ngxLoader.stop();

    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = '';
    this.isFavourite = true; this.favourite = 'active';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
  }

  onNotification() {
    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = '';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = true; this.notification = 'active';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
  }



  onFriend() {
    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = '';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = true; this.friend = 'active';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
  }

  activeDeactiveUser(id, isActivated) {
    this.ngxLoader.start();
    this.apiservice.activeDeactiveUser(id, !isActivated).subscribe(res => {
      this.activeDeactiveResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', this.activeDeactiveResponse);
      if (this.activeDeactiveResponse.isSuccess === true && this.activeDeactiveResponse.data.isActivated === false) {
        let msg = 'acount Deactivated!';
        this.snack.open(msg, 'OK', { duration: 4000 });
        return this.getParentById();
      } else {
        if (this.activeDeactiveResponse.isSuccess === true && this.activeDeactiveResponse.data.isActivated === true) {
          let msg = 'acount Activated!';
          this.snack.open(msg, 'OK', { duration: 4000 });
          return this.getParentById();
        } else {
          let msg = 'somthing went wrong!';
          this.snack.open(msg, 'OK', { duration: 4000 });
        }
      }
    });
    this.ngxLoader.stop();
  }

  tellFriend(parentName) {
    this.tellFriendData.parentName = parentName;
    this.ngxLoader.start();
    this.apiservice.tellFriend(this.tellFriendData).subscribe(res => {
      this.tellFriendResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', this.tellFriendResponse);
      if (this.tellFriendResponse.isSuccess === true) {
        this.snack.open(this.tellFriendResponse.message, 'OK', { duration: 4000 });
      } else {
        this.snack.open(this.tellFriendResponse.error, 'OK', { duration: 4000 });

      }
    });
    this.ngxLoader.stop();

  }

  onFeedback() {
    this.isProfile = false; this.profile = '';
    this.isSetting = false; this.setting = '';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = '';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = true; this.feedback = 'active';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
  }
  giveFeedback(id) {
    this.giveFeedbackData.id = id;
    this.ngxLoader.start();
    this.apiservice.giveFeedback(this.giveFeedbackData).subscribe(res => {
      this.giveFeedbackResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', this.giveFeedbackResponse);
      if (this.giveFeedbackResponse.isSuccess === true) {
        let msg = 'Thankyou For Feedback!';
        this.snack.open(msg, 'OK', { duration: 4000 });
      } else {
        let msg = 'something went wrong, please try again Later!'
        this.snack.open(msg, 'OK', { duration: 4000 });

      }
    });
    this.ngxLoader.stop();
  }

  onSetting() {
    this.isProfile = false; this.profile = '';
    this.isSetting = true; this.setting = 'active';
    this.isGuardian = false; this.guardian = '';
    this.isChildren = false; this.children = '';
    this.isFavourite = false; this.favourite = '';
    this.isNotification = false; this.notification = '';
    this.isFriend = false; this.friend = '';
    this.isFeedback = false; this.feedback = '';
    this.isInvite = false; this.isAddChild = false; this.isEditChildBtn = false;
  }

  // remove(indx): void {

  //   this.kid.interestInfo.splice(indx, 1);

  // }

  parentImageSelect(event, id) {
    let formData = new FormData();
    this.fileData = event.target.files[0];
    formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.parentImgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

    this.ngxLoader.start();
    this.apiservice.uploadUserImage(id, formData).subscribe(res => {
      this.parentImgResponse = res;
      this.user.avatarImages = this.parentImgResponse.data.avatarImages
      this.ngxLoader.stop();
      console.log('parent IMAGE Response from server = >>> ', this.parentImgResponse);
      if (this.parentImgResponse.isSuccess === true) {
      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(msg, 'OK', { duration: 4000 });

      }
    });
    this.ngxLoader.stop();
  }

  childImageSelect(event) {
    let formData = new FormData();
    this.fileData = event.target.files[0];
    formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.childImgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

    this.ngxLoader.start();
    this.apiservice.getPicUrl(formData).subscribe(res => {
      this.childImgResponse = res;
      this.childImageURl = res.data
      this.kid.avtar = this.childImageURl;
      this.ngxLoader.stop();
      console.log('child IMAGE Response from server = >>> ', this.childImgResponse);
      if (this.childImgResponse.isSuccess === true) {
      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(msg, 'OK', { duration: 4000 });

      }
    });
  }


  updateParent(parent) {
    this.user.avatarImages
    this.ngxLoader.start();
    this.apiservice.updateParent(parent._id, parent).subscribe(res => {
      this.getProfileProgress();
      this.headerComponent.getProfileProgress();
      this.headerComponent.getUserById();
      this.updateResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', res);

      if (this.updateResponse.isSuccess === true) {
        this.snack.open(this.message, 'OK', { duration: 5000 });
      } else {
        if (this.userData === null || this.userData === undefined) {
          this.router.navigate(['/login']);
          let msg = 'Please Login First!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
        else {
          let msg = 'Something Went Wrong!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
      }
    });

  }

  resetPassword(id) {
    this.ngxLoader.start();
    this.apiservice.resetPassword(id, this.resetPasswordData).subscribe(res => {
      this.resetPasswordResponse = res;
      this.ngxLoader.stop();
      console.log('res from model', res);

      if (this.resetPasswordResponse.isSuccess === true) {
        this.snack.open(this.resetPasswordResponse.message, 'OK', { duration: 5000 });
        this.router.navigate(['/login']);
      } else {
        if (this.userData === null || this.userData === undefined) {
          this.router.navigate(['/login']);
          let msg = 'Please Login First!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
        else {
          if (this.resetPasswordResponse.error === 'Old Password Not Match') {
            this.snack.open(this.resetPasswordResponse.error, 'OK', { duration: 5000 });
          }
          else {
            let msg = 'Something Went Wrong!';
            this.snack.open(msg, 'OK', { duration: 5000 });
          }
        }
      }
    });
  }

  addGuardian(id) {
    this.addGuardianData.parentId = id;
    this.ngxLoader.start();
    this.apiservice.addGuardian(this.addGuardianData).subscribe(res => {
      this.addGuardianResponse = res;
      this.ngxLoader.stop();
      console.log('ADD GUARDIAN  res from model', this.addGuardianResponse);

      if (this.addGuardianResponse.isSuccess === true) {
        this.onGuardian(id);
        this.snack.open(this.addGuardianResponse.message, 'OK', { duration: 5000 });
      } else {
        this.snack.open(this.addGuardianResponse.error, 'OK', { duration: 5000 });

      }
    });
    this.ngxLoader.stop();
    // this.snack.open(msg, 'OK', { duration: 5000 });

  }

  addChild(userId) {
    this.kid.parentId = userId;
    if (this.childImageURl != "" && this.childImageURl != undefined) {
      this.kid.avtar = this.childImageURl
    }
    if (this.kid.name === '') {
      this.snack.open(this.getChildResponse.error, 'OK', { duration: 5000 });
    }
    else {
      this.ngxLoader.start();
      this.apiservice.addChild(this.kid).subscribe(res => {
        this.getChildResponse = res;
        this.getProfileProgress();
        this.headerComponent.getProfileProgress();
        this.headerComponent.getUserById();
        this.ngxLoader.stop();
        console.log('get child res from model', res);


        if (this.getChildResponse.isSuccess === true) {
          this.ngxLoader.start();
          this.apiservice.getGuardianByParentId(userId).subscribe(res => {
            this.getGuardianResponse = res;
            this.onChildren(userId);
            this.ngxLoader.stop();
            console.log('guardian list>>', this.getGuardianResponse);
          });
          this.ngxLoader.stop();
          this.snack.open(this.getChildResponse.message, 'OK', { duration: 5000 });
        }

        else {
          if (this.userData === null || this.userData === undefined) {
            this.router.navigate(['/login']);
            let msg = 'Please Login First!';
            this.snack.open(msg, 'OK', { duration: 5000 });
          }
          else {
            let msg = 'Something Went Wrong!';
            this.snack.open(this.getChildResponse.error, 'OK', { duration: 5000 });
          }
        }
      });
    }
  }

  updateChild(child, userId) {
    this.ngxLoader.start();
    if (this.childImageURl != "" && this.childImageURl != undefined) {
      this.kid.avtar = this.childImageURl
    }
    this.apiservice.updateChild(child._id, child).subscribe(res => {
      this.getProfileProgress();
      this.headerComponent.getProfileProgress();
      this.headerComponent.getUserById();
      this.getChildResponse = res;
      this.ngxLoader.stop();
      console.log('get update child res from model', res);

      if (this.getChildResponse.isSuccess === true) {
        // this.onChildren(parentId);
        let msg = 'Child Updated Successfully!';
        this.snack.open(msg, 'OK', { duration: 5000 });
        this.onChildren(userId);
      }
      else {
        console.log('err', this.getChildResponse.error)
        if (this.userData === null || this.userData === undefined) {
          this.router.navigate(['/login']);
          let msg = 'Please Login First!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
        else {

          let msg = 'Something Went Wrong!';
          this.snack.open(msg, 'OK', { duration: 5000 });
        }
      }
    })
  }
  getParentById() {
    this.apiservice.getParentById(this.userData.id).subscribe(res => {
      console.log('got parent Data by Id', res);
      this.user = res;

    });
  }
  getProfileProgress() {
    this.ngxLoader.start();
    this.apiservice.getProfileProgress(this.userData.id, this.userData.role).subscribe(res => {
      console.log('got profileProgress Data', this.profileProgressResponse);
      this.profileProgressResponse = res;
      this.profileProgress = this.profileProgressResponse.data.profileProgress
      this.ngxLoader.stop();
    });
    this.ngxLoader.stop();
  }
  removeFav(favId) {
    this.ngxLoader.start();
    this.apiservice.deleteFavProgram(favId).subscribe((res: any) => {
      this.getFav(this.userData.id)
      console.log('deleted from fav program >>', res);
    });
    this.ngxLoader.stop();
  }
  ngOnInit() {
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    this.getProfileProgress();
    this.headerComponent.getProfileProgress();
    this.headerComponent.getUserById();
    this.getParentById();
    this.user.avatarImages = this.userData.avatarImages;

    this.updateForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.email]),
      addressLine1: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required]),
    });

    let newPassword = new FormControl('', [Validators.required]);
    let confirmPassword = new FormControl('', [Validators.required,
    CustomValidators.equalTo(newPassword)]);
    this.resetPasswordForm = new FormGroup({
      oldPassword: new FormControl('', [Validators.required]),
      newPassword: newPassword,
      confirmPassword: confirmPassword
    });

    this.addGuardianForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      personalNote: new FormControl('', [Validators.required]),
    });

    this.tellFriendForm = new FormGroup({
      parentName: new FormControl('', []),
      fullName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      personalNote: new FormControl('', [Validators.required]),
    });

    this.giveFeedbackForm = new FormGroup({
      name: new FormControl('', []),
      email: new FormControl('', []),
      feedback: new FormControl('', [Validators.required]),
    });
  }

}

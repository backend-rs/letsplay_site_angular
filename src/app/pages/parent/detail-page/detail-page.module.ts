import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';
import { DetailPageComponent } from './detail-page.component';
import { FormsModule } from '@angular/forms';
const routes: Routes = [
  {
    path: '', component: DetailPageComponent, children: [

    ]
  }
]


@NgModule({
  declarations: [DetailPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    RouterModule.forChild(routes)
  ]
})
export class ProfileModule { }

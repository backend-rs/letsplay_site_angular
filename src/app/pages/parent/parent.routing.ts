import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginParentComponent } from './login-parent/login-parent.component'
import { ParentProfileComponent } from './parent-profile/parent-profile.component';
import { DetailPageComponent } from './detail-page/detail-page.component';
export const routes: Routes = [
    // { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
        path: 'login-parent', component: LoginParentComponent,
        data: { title: 'login-parent', breadcrumb: 'login-parent' }
    },
    {
        path: 'parent-profile', component: ParentProfileComponent,
        data: { title: 'parent-profile', breadcrumb: 'Account' }
    },
    {
        path: 'detail-page', component: DetailPageComponent,
        data: { title: 'detail', breadcrumb: 'detail' }
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ParentRoutingModule { }




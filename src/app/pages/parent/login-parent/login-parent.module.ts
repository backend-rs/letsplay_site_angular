import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginParentComponent } from './login-parent.component';
import { RouterModule, Routes } from '@angular/router';
import { MatIconModule, MatStepperModule, MatSliderModule, MatChipsModule, MatAutocompleteModule, MatTooltipModule, MatFormFieldModule } from '@angular/material';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { CustomFormsModule } from 'ng2-validation';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
const routes: Routes = [
  {
    path: '', component: LoginParentComponent, children: [

    ]
  }
]
@NgModule({
  declarations: [LoginParentComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    FormsModule,
    CustomFormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatStepperModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    NgxUiLoaderModule,
    MatSliderModule,
    AutocompleteLibModule,
    // Ng5SliderModule,
    RouterModule.forChild(routes)
  ]
})
export class LoginParentModule { }

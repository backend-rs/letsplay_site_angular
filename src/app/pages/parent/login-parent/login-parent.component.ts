import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/internal/operators/startWith';
import { map } from 'rxjs/internal/operators/map';
import { ENTER, COMMA } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-login-parent',
  templateUrl: './login-parent.component.html',
  styleUrls: ['./login-parent.component.css']
})
export class LoginParentComponent implements OnInit {
  addChildForm: FormGroup;
  getChildResponse: any;
  getTagByCategoryResponse: any = {};
  getCategoryResponse: any;
  getTagResponse: any;
  userData: any = {};
  // selected = '';

  keyword = 'name';
  addChildData: any = {
    name: '',
    sex: '',
    dob: '',
    age: '',
    schoolInfo: '',
    relationToChild: '',
    parentId: '',
    interestInfo: [],
  };
  categoryIds = [];


  // ---------------autucomplete-------------  
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;


  separatorKeysCodes: number[] = [ENTER, COMMA];

  fruitCtrl = new FormControl();

  filteredFruits: Observable<any[]>;
  filteredValues: Observable<any[]>;

  categories: any = [];

  SelectedCategories: any = [];
  @ViewChild('fruitInput', { static: true }) fruitInput: ElementRef;


  message: string = 'child added Successfully';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  constructor(private router: Router,
    private apiservice: ApiService,
    private ngxLoader: NgxUiLoaderService,
    private snack: MatSnackBar) {
    var retrievedObject = localStorage.getItem('userData');
    this.userData = JSON.parse(retrievedObject);

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;

    // this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
    //   startWith(null),
    //   map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));

  }
  logo() {
    this.router.navigate(['/search']);
  }
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.addChildData.interestInfo.push(
        // value.trim()
        {
          _id: '',
          name: value.trim()
        }
      );
      console.log('tagsssss', this.addChildData.interestInfo);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.fruitCtrl.setValue(null);
    console.log('tagsssss', this.addChildData.interestInfo);
  }

  remove(fruit, indx): void {
    this.addChildData.interestInfo.splice(indx, 1);
    console.log('tagsssss', this.addChildData.interestInfo);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.addChildData.interestInfo.push(event.option.value);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }
  // private _filter(value: any): string[] {
  //   return this.allFruits.filter(fruit => fruit._id === value._id);
  // }
  openSearch() {
    this.router.navigate(['/search']);
    console.log('addchild data', this.addChildData);
  }
  // onIntrest(intrest, isChecked: boolean) {
  //   if (isChecked) {
  //     this.categoryIds.push(intrest._id);
  //     console.log('category id', this.categoryIds);
  //   } else {
  //     const index = this.categoryIds.indexOf(intrest._id);
  //     this.categoryIds.splice(index, 1);
  //     console.log(' categoryId', this.categoryIds);


  //   }

  // }

  // getTagByCategoryId() {
  //   var categoryID = JSON.stringify(this.categoryIds);
  //   console.log('stringify categoryIds Array before request', categoryID),

  //     this.apiservice.getTagByCategoryId(categoryID).subscribe(res => {
  //       this.getTagByCategoryResponse = res;
  //       console.log('gettag by cat id res>>', this.getTagByCategoryResponse);
  //     });
  // }

  // getTag() {

  //   this.apiservice.getTag().subscribe(res => {
  //     this.getTagResponse = res;
  //     console.log('get tag  res>>', this.getTagResponse);
  //   });
  // }


  // onTags(info, isChecked: boolean) {
  //   if (isChecked) {
  //     this.addChildData.interestinfo.push(info);
  //     console.log('kidsInfo', this.addChildData.interestinfo);
  //   } else {
  //     const index = this.addChildData.interestinfo.indexOf(info);
  //     this.addChildData.interestinfo.splice(index, 1);
  //     console.log('kidsInfo', this.addChildData.interestinfo);

  //   }
  // }


  addChild(id) {
    this.addChildData.parentId = id;
    console.log('child data before upload', this.addChildData);
    this.ngxLoader.start();
    this.apiservice.addChild(this.addChildData).subscribe(res => {
      this.getChildResponse = res;
      this.ngxLoader.stop();
      if (this.getChildResponse.isSuccess === true) {
        this.router.navigate(['/search']);
        console.log('get child res from model', res);

        this.snack.open(this.message, 'OK', { duration: 5000 });
      } else {
        if (this.getChildResponse.error === 'child already exits with this name') {
          this.snack.open(this.getChildResponse.error, 'OK', { duration: 5000 });

          this.router.navigateByUrl('/parent', { skipLocationChange: true }).then(() => {
            this.router.navigate(['parent/login-parent']);
          });
        } else {
          this.snack.open(this.getChildResponse.error, 'OK', { duration: 5000 });
          this.router.navigateByUrl('/parent', { skipLocationChange: true }).then(() => {
            this.router.navigate(['parent/login-parent']);
          });
        }
      }
    });
    this.ngxLoader.stop();
  }


  searchCategory(key) {
    this.apiservice.searchCategory(key).subscribe(res => {
      this.getCategoryResponse = res;
      this.categories = this.getCategoryResponse.data;
      console.log('category list categories', this.categories);
    });

  }
  selectEvent(item) {
    this.addChildData.interestInfo.push(item)
    console.log('selectEvent', item);
    console.log('intrest array', this.addChildData.interestInfo);

    // do something with selected item
  }

  onChangeSearch(val: string) {
    this.searchCategory(val)
    console.log('onChangeSearch', val)
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    console.log('onFocused', e)
    // do something when input is focused
  }

  ngOnInit() {
    if (this.userData === null || this.userData === undefined) {
      this.router.navigate(['/login']);
    }
    // this.getCategory();
    // this.getTag();


    this.addChildForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      // email: new FormControl('', [Validators.email]),
      sex: new FormControl('', []),
      age: new FormControl('', [Validators.required]),
      schoolInfo: new FormControl('', []),
      dob: new FormControl('', []),
      relationToChild: new FormControl('', []),
      intrestes: new FormControl('', [])

    });

  }

}

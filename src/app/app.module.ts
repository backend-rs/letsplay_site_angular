import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { AppComponent } from '../app/app.component';
// import { LoginComponent } from './components/login/login.component';
import { LocalStorageService } from '../app/core/services';
import { LandingComponent } from './pages/landing/landing.component';
// import { AssociatesComponent } from './components/associates/associates.component';
import { ProgramComponent } from './pages/provider/program/program.component';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule, MatFormFieldModule, MatCheckboxModule } from '@angular/material';
import { ParentComponent } from './pages/parent/parent.component';
import { TermConditionComponent } from './pages/term-condition/term-condition.component';
import { Ng5SliderModule } from 'ng5-slider';
import { ProgramDetailComponent } from './pages/program-detail/program-detail.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { SearchComponent } from './pages/search/search.component';

const components = [
  AppComponent,
  // LoginComponent,
  LandingComponent
];
// guards
@NgModule({
  declarations: [
    ...components,
    LandingComponent,
    ProgramComponent,
    ParentComponent,
    TermConditionComponent,
    ProgramDetailComponent,
    // SearchComponent,
    // ForgotPasswordComponent,
    // ProfileComponent,
    // EditProfileComponent,
    // LoginParentComponent,
    // AssociatesComponent,
    // SettingComponent,

    // SignUpComponent,
    // ProgramListComponent,
    // AddProgramComponent,
    // ProgramComponent,
    // HomeComponent,
  ],

  entryComponents: [
    ...components,
  ],

  imports: [

    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatCheckboxModule,
    AppRoutingModule,
    NgxUiLoaderModule,
    CoreModule,
    Ng5SliderModule,
    AutocompleteLibModule,

  ],

  providers: [
    LocalStorageService
    // ...services
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from '../app/pages/landing/landing.component';
import { ProgramComponent } from './pages/provider/program/program.component';
import { ParentComponent } from './pages/parent/parent.component';
import { TermConditionComponent } from './pages/term-condition/term-condition.component';
import { ProgramDetailComponent } from './pages/program-detail/program-detail.component';
// main routes
const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: 'landing', component: LandingComponent },
  { path: 'term-condition', component: TermConditionComponent },
  { path: 'program-detail', component: ProgramDetailComponent },
  { path: 'login', loadChildren: '../app/pages/login/login.module#LoginModule' },
  { path: 'forgot-password', loadChildren: '../app/pages/forgot-password/forgot-password.module#ForgotPasswordModule' },
  { path: 'sign-up', loadChildren: '../app/pages/sign-up/sign-up.module#SignUpModule' },
  { path: 'search', loadChildren: '../app/pages/search/search.module#SearchModule' },

  {
    path: '', component: ProgramComponent, children: [
      {
        path: 'program',
        loadChildren: () => import('.//pages/provider/program/program.module').then(m => m.ProgramModule),
        // data: { title: 'Dashboard', breadcrumb: 'DASHBOARD' }
      },
      {
        path: 'profile',
        loadChildren: () => import('.//pages/provider/profile/profile.module').then(m => m.ProfileModule),
        // data: { title: 'Dashboard', breadcrumb: 'DASHBOARD' }
      }, {
        path: 'edit-profile',
        loadChildren: () => import('.//pages/provider/edit-profile/edit-profile.module').then(m => m.EditProfileModule),
        // data: { title: 'Dashboard', breadcrumb: 'DASHBOARD' }
      },
    ]
  },
  {
    path: '', component: ParentComponent, children: [
      {
        path: 'parent',
        loadChildren: () => import('.//pages/parent/parent.module').then(m => m.ParentModule),
        // data: { title: 'Dashboard', breadcrumb: 'DASHBOARD' }
      },
    ]
  },

  { path: '**', redirectTo: 'landing' },
];



// const routes: Routes = [
//   { path: '', redirectTo: 'landing', pathMatch: 'full' },
//   { path: 'landing', component: LandingComponent },
//   { path: 'login', loadChildren: '../app/components/login/login.module#LoginModule' },
//   { path: 'login-parent', loadChildren: '../app/components/login-parent/login-parent.module#LoginParentModule' },
//   { path: 'home', loadChildren: '../app/components/home/home.module#HomeModule' },
//   { path: 'program', loadChildren: '../app/components/program/program.module#ProgramModule' },
//   { path: 'add-program', loadChildren: '../app/components/add-program/add-program.module#AddProgramModule' },
//   { path: 'associates', loadChildren: '../app/components/associates/associates.module#AssociatesModule' },
//   { path: 'program-list', loadChildren: '../app/components/program-list/program-list.module#ProgramListModule' },
//   { path: 'sign-up', loadChildren: '../app/components/sign-up/sign-up.module#SignUpModule' },
//   { path: 'setting', loadChildren: '../app/components/setting/setting.module#SettingModule' },
//   { path: 'edit-profile', loadChildren: '../app/components/edit-profile/edit-profile.module#EditProfileModule' },
//   { path: 'profile', loadChildren: '../app/components/profile/profile.module#ProfileModule' },
//   { path: '**', redirectTo: 'landing' }
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
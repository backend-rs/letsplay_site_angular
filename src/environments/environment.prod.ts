export const environment = {
    production: true,
    apiUrls: {
        reports: 'http://93.188.167.68:6700/api',
        master: 'http://93.188.167.68:6100/api',
    },
    name: 'prod'
};

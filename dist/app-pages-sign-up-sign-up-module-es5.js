function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-pages-sign-up-sign-up-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesSignUpSignUpComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<body>\n\n\n    <div class=\"otp_outer\">\n        <div class=\"container-fluid\">\n            <div class=\"row align-items-center\">\n                <div class=\"col-md-6 sign_outer\">\n                    <div class=\"otp_form\">\n                        <div class=\"otp_logo\">\n                            <a href=\"#\">\n                                <img src=\"assets/logo-1.png\" alt=\"Logo Image\">\n                            </a>\n                        </div>\n                        <div class=\"sing_text\">\n                            <img src=\"assets/hand_icon.svg\" alt=\"Hand icon\">\n                            <h1>Welcome to Letsplay</h1>\n                            <h3>We need some of your details to setup an account for you</h3>\n                        </div>\n                        <div class=\"tab_outer\">\n                            <ul class=\"nav nav-tabs\" role=\"tablist\">\n                                <li class=\"nav-item\" (click)=\"selectParent()\">\n                                    <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#parent\" role=\"tab\">\n                                        <svg class=\"svg_img\" width=\"23\" height=\"19\" viewBox=\"0 0 23 19\" fill=\"none\"\n                                            xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path\n                                                d=\"M18.5 18.4999H22.5V14.1769C22.4999 13.9771 22.4399 13.7819 22.3278 13.6165C22.2157 13.4511 22.0566 13.3231 21.871 13.2489L18.129 11.7489C17.9438 11.6749 17.7849 11.5472 17.6728 11.3822C17.5607 11.2172 17.5005 11.0224 17.5 10.8229V9.94492C18.1065 9.59687 18.6107 9.09523 18.9617 8.49049C19.3128 7.88574 19.4985 7.19919 19.5 6.49992V4.49992C19.5002 3.7977 19.3154 3.10781 18.9644 2.49961C18.6134 1.89141 18.1085 1.38633 17.5004 1.03515C16.8923 0.683967 16.2024 0.499063 15.5002 0.499023C14.798 0.498984 14.1081 0.68381 13.5 1.03492\"\n                                                stroke=\"#748494\" stroke-miterlimit=\"10\" stroke-linecap=\"square\" />\n                                            <path\n                                                d=\"M13.871 14.249L10.129 12.749C9.94375 12.675 9.78488 12.5473 9.67279 12.3823C9.5607 12.2172 9.50053 12.0225 9.5 11.823V11.372C12.77 10.935 13.5 9.5 13.5 9.5C13.5 9.5 11.5 7.5 11.5 5.5C11.5 4.43913 11.0786 3.42172 10.3284 2.67157C9.57828 1.92143 8.56087 1.5 7.5 1.5C6.43913 1.5 5.42172 1.92143 4.67157 2.67157C3.92143 3.42172 3.5 4.43913 3.5 5.5C3.5 7.5 1.5 9.5 1.5 9.5C1.5 9.5 2.23 10.935 5.5 11.372V11.823C5.49987 12.0228 5.43989 12.218 5.32777 12.3834C5.21566 12.5488 5.05656 12.6769 4.871 12.751L1.129 14.251C0.943754 14.325 0.784876 14.4527 0.672788 14.6177C0.5607 14.7828 0.500525 14.9775 0.5 15.177V18.5H14.5V15.177C14.4999 14.9772 14.4399 14.782 14.3278 14.6166C14.2157 14.4512 14.0566 14.3231 13.871 14.249Z\"\n                                                stroke=\"#748494\" stroke-miterlimit=\"10\" stroke-linecap=\"square\" />\n                                        </svg>\n                                        <span> I’m a Parent </span>\n                                    </a>\n                                </li>\n                                <li class=\"nav-item\" (click)=\"selectProvider()\">\n                                    <a class=\"nav-link\" data-toggle=\"tab\" href=\"#provider\" role=\"tab\">\n                                        <svg class=\"svg_img\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\"\n                                            xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path d=\"M5.5 23.5H1.5V13.5H5.5\" stroke=\"#F15C20\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                            <path d=\"M9.5 16.5H11.5\" stroke=\"#F15C20\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                            <path d=\"M9.5 19.5H11.5\" stroke=\"#F15C20\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                            <path d=\"M9.5 13.5H11.5\" stroke=\"#F15C20\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                            <path d=\"M13.5 6.5V2.5L23.5 5.5V23.5H5.5V9.5H15.5V23.5\" stroke=\"#F15C20\"\n                                                stroke-miterlimit=\"10\" stroke-linecap=\"square\" />\n                                            <path d=\"M19.5 19.5V9.5\" stroke=\"#F15C20\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                        </svg>\n                                        <span> I’m a Provider </span> </a>\n                                </li>\n                            </ul>\n                            <div class=\"tab-content\">\n                                <div class=\"tab-pane active\" id=\"parent\" role=\"tabpanel\">\n                                    <div class=\"form\" [formGroup]=\"parentForm\">\n\n                                        <div class=\"form-group\">\n                                            <label>Parent Name</label>\n                                            <div class=\"input_outer\">\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"\"\n                                                    [formControl]=\"parentForm.controls['name']\"\n                                                    [(ngModel)]=\"userData.firstName\">\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"parentForm.controls['name'].hasError('required') && parentForm.controls['name'].touched\">\n                                                    CAN'T LEAVE IT BLANK</p>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Email</label>\n                                            <div class=\"input_outer\">\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"Type\"\n                                                    [formControl]=\"parentForm.controls['email']\"\n                                                    [(ngModel)]=\"userData.email\">\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"parentForm.controls['email'].hasError('required') && parentForm.controls['email'].touched\">\n                                                    CAN'T LEAVE IT BLANK</p>\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"parentForm.controls['email'].hasError('email') && parentForm.controls['email'].touched\">\n                                                    INVALID EMAIL ADDRESS</p>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Select Password</label>\n                                            <div class=\"input_outer password\">\n                                                <input type=\"password\" class=\"form-control\" placeholder=\"Type\"\n                                                    placeholder=\"Select a Strong Password\"\n                                                    [formControl]=\"parentForm.controls['password']\"\n                                                    [(ngModel)]=\"userData.password\" [type]=\"hide ? 'password' : 'text'\">\n                                                <mat-icon class=\"field-icon\" *ngIf=\"hide\" (click)=\"onPassword()\"><svg\n                                                        width=\"17\" height=\"13\" viewBox=\"0 0 17 13\" fill=\"none\"\n                                                        xmlns=\"http://www.w3.org/2000/svg\">\n                                                        <path\n                                                            d=\"M0.875 6.375C0.875 6.375 3.875 1.125 8.375 1.125C12.875 1.125 15.875 6.375 15.875 6.375C15.875 6.375 12.875 11.625 8.375 11.625C3.875 11.625 0.875 6.375 0.875 6.375Z\"\n                                                            stroke=\"#C5CED6\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                        <path\n                                                            d=\"M8.375 8.625C9.61764 8.625 10.625 7.61764 10.625 6.375C10.625 5.13236 9.61764 4.125 8.375 4.125C7.13236 4.125 6.125 5.13236 6.125 6.375C6.125 7.61764 7.13236 8.625 8.375 8.625Z\"\n                                                            stroke=\"#C5CED6\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                    </svg></mat-icon>\n                                                <mat-icon class=\"field-icon\" *ngIf=\"!hide\" (click)=\"onPassword()\"><svg\n                                                        width=\"17\" height=\"13\" viewBox=\"0 0 17 13\" fill=\"none\"\n                                                        xmlns=\"http://www.w3.org/2000/svg\">\n                                                        <path\n                                                            d=\"M0.875 6.375C0.875 6.375 3.875 1.125 8.375 1.125C12.875 1.125 15.875 6.375 15.875 6.375C15.875 6.375 12.875 11.625 8.375 11.625C3.875 11.625 0.875 6.375 0.875 6.375Z\"\n                                                            stroke=\"url(#paint0_linear)\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                        <path\n                                                            d=\"M8.375 8.625C9.61764 8.625 10.625 7.61764 10.625 6.375C10.625 5.13236 9.61764 4.125 8.375 4.125C7.13236 4.125 6.125 5.13236 6.125 6.375C6.125 7.61764 7.13236 8.625 8.375 8.625Z\"\n                                                            stroke=\"url(#paint1_linear)\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                        <defs>\n                                                            <linearGradient id=\"paint0_linear\" x1=\"-11.5163\" y1=\"11.625\"\n                                                                x2=\"13.788\" y2=\"11.625\" gradientUnits=\"userSpaceOnUse\">\n                                                                <stop offset=\"0.166667\" stop-color=\"#B21F6B\" />\n                                                                <stop offset=\"1\" stop-color=\"#F15A29\" />\n                                                            </linearGradient>\n                                                            <linearGradient id=\"paint1_linear\" x1=\"2.40761\" y1=\"8.625\"\n                                                                x2=\"9.99891\" y2=\"8.625\" gradientUnits=\"userSpaceOnUse\">\n                                                                <stop offset=\"0.166667\" stop-color=\"#B21F6B\" />\n                                                                <stop offset=\"1\" stop-color=\"#F15A29\" />\n                                                            </linearGradient>\n                                                        </defs>\n                                                    </svg></mat-icon>\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"parentForm.controls['password'].hasError('required') && parentForm.controls['password'].touched\">\n                                                    CAN'T LEAVE IT BLANK</p>\n\n                                            </div>\n                                        </div>\n                                        <div class=\"row forgot_password\">\n                                            <div class=\"col-12 forgot_text\">\n                                                <p> By clicking sign up I hereby agree with Letsplay’s\n                                                    <a [routerLink]=\"['/term-condition']\">Terms & Conditions</a></p>\n                                            </div>\n                                        </div>\n                                        <div class=\"sign_in_btn\">\n                                            <button class=\"SignBtn\" (click)=\"signup()\"\n                                                [disabled]=\"parentForm.invalid\">Sign up</button>\n                                            <button class=\"SignBtn cancel\" routerLink=\"/login\">Cancel</button>\n                                        </div>\n                                    </div>\n                                    <div class=\"row forgot_password\">\n                                        <div class=\"col-12 forgot_text\">\n                                            <p>Already a member? <a routerLink=\"/login\">SIGN In</a></p>\n                                        </div>\n                                    </div>\n                                </div>\n\n\n\n\n                                <div class=\"tab-pane\" id=\"provider\" role=\"tabpanel\">\n                                    <div class=\"form\" [formGroup]=\"providerForm\">\n                                        <div class=\"form-group\">\n                                            <label>Provider Type</label>\n                                            <div class=\"input_outer\">\n                                                <div class=\"custom_select\">\n                                                    <select [formControl]=\"providerForm.controls['type']\"\n                                                        [(ngModel)]=\"providerData.type\">\n                                                        <option value=\"Solo\" selected=\"Solo\">Solo</option>\n                                                        <option value=\"Agency\">Agency</option>\n                                                    </select>\n                                                </div>\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"providerForm.controls['type'].hasError('required') && providerForm.controls['type'].touched\">\n                                                    CAN'T LEAVE IT BLANK</p>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Provider Name</label>\n                                            <div class=\"input_outer\">\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"\"\n                                                    [formControl]=\"providerForm.controls['name']\"\n                                                    [(ngModel)]=\"providerData.firstName\">\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"providerForm.controls['name'].hasError('required') && providerForm.controls['name'].touched\">\n                                                    CAN'T LEAVE IT BLANK</p>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Email</label>\n                                            <div class=\"input_outer\">\n                                                <input type=\"text\" class=\"form-control\" placeholder=\"Type\"\n                                                    [formControl]=\"providerForm.controls['email']\"\n                                                    [(ngModel)]=\"providerData.email\">\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"providerForm.controls['email'].hasError('required') && providerForm.controls['email'].touched\">\n                                                    CAN'T LEAVE IT BLANK</p>\n                                                <p class=\"cant_blank\"\n                                                    *ngIf=\"providerForm.controls['email'].hasError('email') && providerForm.controls['email'].touched\">\n                                                    INVALID EMAIL ADDRESS </p>\n                                            </div>\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label>Select Password</label>\n                                            <div class=\"input_outer password\">\n                                                <input type=\"password\" class=\"form-control\" placeholder=\"Type\"\n                                                    [formControl]=\"providerForm.controls['password']\"\n                                                    [(ngModel)]=\"providerData.password\"\n                                                    [type]=\"hide ? 'password' : 'text'\">\n                                                <mat-icon class=\"field-icon\" *ngIf=\"hide\" (click)=\"onPassword()\"><svg\n                                                        width=\"17\" height=\"13\" viewBox=\"0 0 17 13\" fill=\"none\"\n                                                        xmlns=\"http://www.w3.org/2000/svg\">\n                                                        <path\n                                                            d=\"M0.875 6.375C0.875 6.375 3.875 1.125 8.375 1.125C12.875 1.125 15.875 6.375 15.875 6.375C15.875 6.375 12.875 11.625 8.375 11.625C3.875 11.625 0.875 6.375 0.875 6.375Z\"\n                                                            stroke=\"#C5CED6\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                        <path\n                                                            d=\"M8.375 8.625C9.61764 8.625 10.625 7.61764 10.625 6.375C10.625 5.13236 9.61764 4.125 8.375 4.125C7.13236 4.125 6.125 5.13236 6.125 6.375C6.125 7.61764 7.13236 8.625 8.375 8.625Z\"\n                                                            stroke=\"#C5CED6\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                    </svg></mat-icon>\n                                                <mat-icon class=\"field-icon\" *ngIf=\"!hide\" (click)=\"onPassword()\"><svg\n                                                        width=\"17\" height=\"13\" viewBox=\"0 0 17 13\" fill=\"none\"\n                                                        xmlns=\"http://www.w3.org/2000/svg\">\n                                                        <path\n                                                            d=\"M0.875 6.375C0.875 6.375 3.875 1.125 8.375 1.125C12.875 1.125 15.875 6.375 15.875 6.375C15.875 6.375 12.875 11.625 8.375 11.625C3.875 11.625 0.875 6.375 0.875 6.375Z\"\n                                                            stroke=\"url(#paint0_linear)\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                        <path\n                                                            d=\"M8.375 8.625C9.61764 8.625 10.625 7.61764 10.625 6.375C10.625 5.13236 9.61764 4.125 8.375 4.125C7.13236 4.125 6.125 5.13236 6.125 6.375C6.125 7.61764 7.13236 8.625 8.375 8.625Z\"\n                                                            stroke=\"url(#paint1_linear)\" stroke-miterlimit=\"10\"\n                                                            stroke-linecap=\"square\" />\n                                                        <defs>\n                                                            <linearGradient id=\"paint0_linear\" x1=\"-11.5163\" y1=\"11.625\"\n                                                                x2=\"13.788\" y2=\"11.625\" gradientUnits=\"userSpaceOnUse\">\n                                                                <stop offset=\"0.166667\" stop-color=\"#B21F6B\" />\n                                                                <stop offset=\"1\" stop-color=\"#F15A29\" />\n                                                            </linearGradient>\n                                                            <linearGradient id=\"paint1_linear\" x1=\"2.40761\" y1=\"8.625\"\n                                                                x2=\"9.99891\" y2=\"8.625\" gradientUnits=\"userSpaceOnUse\">\n                                                                <stop offset=\"0.166667\" stop-color=\"#B21F6B\" />\n                                                                <stop offset=\"1\" stop-color=\"#F15A29\" />\n                                                            </linearGradient>\n                                                        </defs>\n                                                    </svg></mat-icon>\n                                                <small\n                                                    *ngIf=\"providerForm.controls['password'].hasError('required') && providerForm.controls['password'].touched\"\n                                                    class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n\n                                            </div>\n                                        </div>\n                                        <div class=\"row forgot_password\">\n                                            <div class=\"col-12 forgot_text\">\n                                                <p> By clicking sign up I hereby agree with Letsplay’s <a class=\"cursor\"\n                                                        [routerLink]=\"['/term-condition']\">Terms & Conditions</a></p>\n                                            </div>\n                                        </div>\n                                        <div class=\"sign_in_btn\">\n                                            <button class=\"SignBtn\" (click)=\"signup()\"\n                                                [disabled]=\"providerForm.invalid\">Sign up</button>\n                                            <button class=\"SignBtn cancel\" routerLink=\"/login\">Cancel</button>\n                                        </div>\n                                    </div>\n                                    <div class=\"row forgot_password\">\n                                        <div class=\"col-12 forgot_text\">\n                                            <p>Already a member? <a routerLink=\"/login\">SIGN In</a></p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"copyright_text\">\n                            <p>© 2020 Letsplay Holdings Inc.</p>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-md-6\">\n                    <div class=\"otp_img\">\n                        <img *ngIf=\"this.userData.role === 'parent'\" src=\"assets/sign_up_img.jpg\" alt=\"Sign Up Image\"\n                            class=\"img-fluid\">\n                        <img *ngIf=\"this.userData.role === 'provider'\" src=\"assets/Mask Group.png\" alt=\"Sign Up Image\"\n                            class=\"img-fluid\">\n                        <div class=\"otp_img_overlay\"></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</body>\n\n<ngx-ui-loader></ngx-ui-loader>";
    /***/
  },

  /***/
  "./src/app/pages/sign-up/sign-up.component.css":
  /*!*****************************************************!*\
    !*** ./src/app/pages/sign-up/sign-up.component.css ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesSignUpSignUpComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".otp_form {\n    max-width: 440px;\n    margin: 0 auto;\n}\n.otp_form .otp_logo{\n    position: absolute;\n    top: 40px;\n    }\n.otp_form .form {\n    margin-top: 65px;\n}\n.otp_form .form .form-group {\n    margin: 0 0 10px 0;\n}\n.otp_form .form .form-group.email_text h2 {\n    font-family: 'Patrick Hand';\n    font-size: 20px;\n    line-height: 27px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n.otp_form .form .form-group .input_outer {\n    position: relative;\n}\n.otp_form .form .form-group .input_outer:focus::after,\n.otp_form .form .form-group .input_outer:focus-within::after {\n    opacity: 1;\n}\n.otp_form .form .form-group .input_outer.password::after{\n    content:none;\n}\n.otp_form .form .form-group .input_outer.password .password_view{\nposition: absolute;\nright: 9px;\ntop: 50%;\ntransform: translateY(-50%);\n}\n.ch-radio input[type=\"checkbox\"]:checked+span:before,\n.ch-radio input[type=\"checkbox\"]:checked~span:before,\n.ch-radio input[type=\"radio\"]:checked+span:before,\n.ch-radio input[type=\"radio\"]:checked~span:before {\n    border-color: #27AE60;\n    background-color: #27AE60;\n}\n.sign_in_btn {\n    text-align: center;\n    margin:40px 0 0 0;\n}\n.SignBtn {\n    background: #F15C20;\n    border: 1px solid #F15C20;\n    box-sizing: border-box;\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    min-width: 205px;\n    min-height: 56px;\n    line-height: 56px;\n    text-align: center;\n    color: #ffffff;\n    font-weight: 600;\n    font-size: 13px;\n    margin: 0 auto;\n    text-transform: uppercase;\n}\n.SignBtn:hover {\n    background: #c74009;\n}\n.SignBtn.cancel {\n    background: transparent;\n    border: 1px solid #F15C20;\n    color: #F15C20;\n    margin: 0 auto 0 16px;\n}\n.SignBtn.cancel:hover {\n    background: #F15C20;\n    color: #ffffff;\n}\n.otp_img {\n    position: relative;\n    overflow: hidden;\n}\n.otp_img_overlay {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    left: 0;\n    top: 0px;\n    background: linear-gradient(214.99deg, rgba(255, 255, 255, 0.4) 0%, rgba(255, 255, 255, 0) 100%);\n}\n.otp_outer {\n    position: relative;\n}\n.otp_outer .email_register {\n    position: inherit;\n}\n.go_back_page {\n    position: absolute;\n    bottom: 27px;\n}\n.go_back_page p {\n    font-size: 11px;\n    line-height: 15px;\n    letter-spacing: 0.015em;\n    text-transform: uppercase;\n    color: #748494;\n}\n.go_back_page p a {\n    color: #F15C20;\n}\n.sing_text {\n    margin-top: 84px;\n}\n.sing_text h1 {\n    font-family: 'Patrick Hand';\n    font-size: 36px;\n    line-height: 37px;\n    color: #777CEA;\n    margin-top: 31px;\n}\n.sing_text h3 {\n    font-family: 'Patrick Hand';\n    font-size: 20px;\n    line-height: 27px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin-top: 10px;\n}\n.otp_outer .sign_outer {\n    position: inherit;\n}\n.forgot_text p {\n    font-size: 11px;\n    line-height: 15px;\n    letter-spacing: 0.015em;\n    text-transform: uppercase;\n    color: #748494;\n}\n.forgot_text p a {\n    color: #F15C20;\n}\n.forgot_text.right {\n    text-align: right;\n}\n.forgot_password {\n    margin-top: 44px;\n    padding: 0 5px;\n    text-align: center;\n}\n.copyright_text {\n    position: absolute;\n    bottom: 24px;\n}\n.copyright_text p {\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n.tab_outer {\n    margin-top: 60px;\n}\n.tab_outer .nav-tabs {\n    border: none;\n}\n.tab_outer .nav-tabs .nav-item {\n    margin: 0 22px 0 0;\n}\n.tab_outer .nav-tabs .nav-link {\n    text-align: center;\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 4px;\n    padding: 19px 0;\n    background-color: #fff;\n    min-width: 198px;\n    font-weight: 600;\n    font-size: 13px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    height: 100%;\n}\n.tab_outer .nav-tabs .nav-link span{\n    width: 100%;\n    display: inline-block;\n    margin-top: 9px;\n}\n.tab_outer .nav-tabs .nav-link .svg_img path{\nstroke: #748494;\n}\n.tab_outer .nav-tabs .nav-link.active {\n    color: #F15C20;\n    border: 1px solid #F15C20;\n}\n.tab_outer .nav-tabs .nav-link.active .svg_img path{\n    stroke: #F15C20;\n}\n.otp_form .form .form-group .input_outer .cant_blank{\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #F15C20;\n    margin: 4px 0 0 15px;\n}\nmat-icon.field-icon {\n    float: right;\n    margin-right: 5px;\n    margin-top: -37px;\n    position: relative;\n    z-index: 2;\n    \n}\n.form-error-msg {\n    color: red;\n}\ninput.ng-invalid.ng-touched {\n    border: 1px solid red;\n}\nselect.ng-invalid.ng-touched {\n    border: 1px solid red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2lnbi11cC9zaWduLXVwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsY0FBYztBQUNsQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVDtBQUVKO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCO0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7QUFHQTs7SUFFSSxVQUFVO0FBQ2Q7QUFFQTtJQUNJLFlBQVk7QUFDaEI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixVQUFVO0FBQ1YsUUFBUTtBQUVSLDJCQUEyQjtBQUMzQjtBQUVBOzs7O0lBSUkscUJBQXFCO0lBQ3JCLHlCQUF5QjtBQUM3QjtBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGlCQUFpQjtBQUNyQjtBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsY0FBYztJQUNkLHlCQUF5QjtBQUM3QjtBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCO0FBRUE7SUFDSSx1QkFBdUI7SUFDdkIseUJBQXlCO0lBQ3pCLGNBQWM7SUFDZCxxQkFBcUI7QUFDekI7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixjQUFjO0FBQ2xCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWixPQUFPO0lBQ1AsUUFBUTtJQUNSLGdHQUFnRztBQUNwRztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0FBQ2hCO0FBRUE7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2Qix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjtBQUVBO0lBQ0ksY0FBYztBQUNsQjtBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSwyQkFBMkI7SUFDM0IsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSwyQkFBMkI7SUFDM0IsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjtBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBRUE7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2Qix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjtBQUVBO0lBQ0ksY0FBYztBQUNsQjtBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFlBQVk7QUFDaEI7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7QUFDbEI7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUVBO0lBQ0ksWUFBWTtBQUNoQjtBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFdBQVc7SUFDWCxxQkFBcUI7SUFDckIsZUFBZTtBQUNuQjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7SUFDSSxjQUFjO0lBQ2QseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSxlQUFlO0FBQ25CO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsY0FBYztJQUNkLG9CQUFvQjtBQUN4QjtBQUVFO0lBQ0UsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFVBQVU7O0FBRWQ7QUFDQTtJQUNJLFVBQVU7QUFDZDtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxxQkFBcUI7QUFDekIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zaWduLXVwL3NpZ24tdXAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdHBfZm9ybSB7XG4gICAgbWF4LXdpZHRoOiA0NDBweDtcbiAgICBtYXJnaW46IDAgYXV0bztcbn1cbi5vdHBfZm9ybSAub3RwX2xvZ297XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNDBweDtcbiAgICB9XG5cbi5vdHBfZm9ybSAuZm9ybSB7XG4gICAgbWFyZ2luLXRvcDogNjVweDtcbn1cblxuLm90cF9mb3JtIC5mb3JtIC5mb3JtLWdyb3VwIHtcbiAgICBtYXJnaW46IDAgMCAxMHB4IDA7XG59XG5cbi5vdHBfZm9ybSAuZm9ybSAuZm9ybS1ncm91cC5lbWFpbF90ZXh0IGgyIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyN3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cblxuLm90cF9mb3JtIC5mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlcjpmb2N1czo6YWZ0ZXIsXG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyOmZvY3VzLXdpdGhpbjo6YWZ0ZXIge1xuICAgIG9wYWNpdHk6IDE7XG59XG5cbi5vdHBfZm9ybSAuZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXIucGFzc3dvcmQ6OmFmdGVye1xuICAgIGNvbnRlbnQ6bm9uZTtcbn1cbi5vdHBfZm9ybSAuZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXIucGFzc3dvcmQgLnBhc3N3b3JkX3ZpZXd7XG5wb3NpdGlvbjogYWJzb2x1dGU7XG5yaWdodDogOXB4O1xudG9wOiA1MCU7XG4td2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbnRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cblxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkK3NwYW46YmVmb3JlLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkfnNwYW46YmVmb3JlLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkK3NwYW46YmVmb3JlLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkfnNwYW46YmVmb3JlIHtcbiAgICBib3JkZXItY29sb3I6ICMyN0FFNjA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI3QUU2MDtcbn1cblxuLnNpZ25faW5fYnRuIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOjQwcHggMCAwIDA7XG59XG5cbi5TaWduQnRuIHtcbiAgICBiYWNrZ3JvdW5kOiAjRjE1QzIwO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMTVDMjA7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBtaW4td2lkdGg6IDIwNXB4O1xuICAgIG1pbi1oZWlnaHQ6IDU2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDU2cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5TaWduQnRuOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjYzc0MDA5O1xufVxuXG4uU2lnbkJ0bi5jYW5jZWwge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMTVDMjA7XG4gICAgY29sb3I6ICNGMTVDMjA7XG4gICAgbWFyZ2luOiAwIGF1dG8gMCAxNnB4O1xufVxuXG4uU2lnbkJ0bi5jYW5jZWw6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICNGMTVDMjA7XG4gICAgY29sb3I6ICNmZmZmZmY7XG59XG5cbi5vdHBfaW1nIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLm90cF9pbWdfb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMHB4O1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgyMTQuOTlkZWcsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC40KSAwJSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwKSAxMDAlKTtcbn1cblxuLm90cF9vdXRlciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ub3RwX291dGVyIC5lbWFpbF9yZWdpc3RlciB7XG4gICAgcG9zaXRpb246IGluaGVyaXQ7XG59XG5cbi5nb19iYWNrX3BhZ2Uge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDI3cHg7XG59XG5cbi5nb19iYWNrX3BhZ2UgcCB7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAxNWVtO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5nb19iYWNrX3BhZ2UgcCBhIHtcbiAgICBjb2xvcjogI0YxNUMyMDtcbn1cblxuLnNpbmdfdGV4dCB7XG4gICAgbWFyZ2luLXRvcDogODRweDtcbn1cblxuLnNpbmdfdGV4dCBoMSB7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xuICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICBsaW5lLWhlaWdodDogMzdweDtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICBtYXJnaW4tdG9wOiAzMXB4O1xufVxuXG4uc2luZ190ZXh0IGgzIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyN3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5vdHBfb3V0ZXIgLnNpZ25fb3V0ZXIge1xuICAgIHBvc2l0aW9uOiBpbmhlcml0O1xufVxuXG4uZm9yZ290X3RleHQgcCB7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAxNWVtO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5mb3Jnb3RfdGV4dCBwIGEge1xuICAgIGNvbG9yOiAjRjE1QzIwO1xufVxuXG4uZm9yZ290X3RleHQucmlnaHQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uZm9yZ290X3Bhc3N3b3JkIHtcbiAgICBtYXJnaW4tdG9wOiA0NHB4O1xuICAgIHBhZGRpbmc6IDAgNXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNvcHlyaWdodF90ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAyNHB4O1xufVxuXG4uY29weXJpZ2h0X3RleHQgcCB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4udGFiX291dGVyIHtcbiAgICBtYXJnaW4tdG9wOiA2MHB4O1xufVxuXG4udGFiX291dGVyIC5uYXYtdGFicyB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4udGFiX291dGVyIC5uYXYtdGFicyAubmF2LWl0ZW0ge1xuICAgIG1hcmdpbjogMCAyMnB4IDAgMDtcbn1cblxuLnRhYl9vdXRlciAubmF2LXRhYnMgLm5hdi1saW5rIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0U1RTlFQztcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBwYWRkaW5nOiAxOXB4IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBtaW4td2lkdGg6IDE5OHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIGhlaWdodDogMTAwJTtcbn1cbi50YWJfb3V0ZXIgLm5hdi10YWJzIC5uYXYtbGluayBzcGFue1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tdG9wOiA5cHg7XG59XG4udGFiX291dGVyIC5uYXYtdGFicyAubmF2LWxpbmsgLnN2Z19pbWcgcGF0aHtcbnN0cm9rZTogIzc0ODQ5NDtcbn1cbi50YWJfb3V0ZXIgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmUge1xuICAgIGNvbG9yOiAjRjE1QzIwO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMTVDMjA7XG59XG4udGFiX291dGVyIC5uYXYtdGFicyAubmF2LWxpbmsuYWN0aXZlIC5zdmdfaW1nIHBhdGh7XG4gICAgc3Ryb2tlOiAjRjE1QzIwO1xufVxuLm90cF9mb3JtIC5mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlciAuY2FudF9ibGFua3tcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMTRweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAjRjE1QzIwO1xuICAgIG1hcmdpbjogNHB4IDAgMCAxNXB4O1xufVxuXG4gIG1hdC1pY29uLmZpZWxkLWljb24ge1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICBtYXJnaW4tdG9wOiAtMzdweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMjtcbiAgICBcbn1cbi5mb3JtLWVycm9yLW1zZyB7XG4gICAgY29sb3I6IHJlZDtcbn1cbmlucHV0Lm5nLWludmFsaWQubmctdG91Y2hlZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xufVxuc2VsZWN0Lm5nLWludmFsaWQubmctdG91Y2hlZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/sign-up/sign-up.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/pages/sign-up/sign-up.component.ts ***!
    \****************************************************/

  /*! exports provided: SignUpComponent */

  /***/
  function srcAppPagesSignUpSignUpComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignUpComponent", function () {
      return SignUpComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/core/services/api.service.service */
    "./src/app/core/services/api.service.service.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");

    var SignUpComponent = /*#__PURE__*/function () {
      function SignUpComponent(router, apiservice, snack, ngxLoader) {
        _classCallCheck(this, SignUpComponent);

        this.router = router;
        this.apiservice = apiservice;
        this.snack = snack;
        this.ngxLoader = ngxLoader;
        this.userData = {
          firstName: '',
          email: '',
          password: '',
          role: 'parent'
        };
        this.providerData = {
          firstName: '',
          email: '',
          type: '',
          password: '',
          role: 'provider'
        };
        this.message = 'Registered Successfully!';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        this.hide = true;
        var config = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarConfig"]();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
      }

      _createClass(SignUpComponent, [{
        key: "onPassword",
        value: function onPassword() {
          this.hide = !this.hide;
        }
      }, {
        key: "selectParent",
        value: function selectParent() {
          this.userData.role = "parent";
        }
      }, {
        key: "selectProvider",
        value: function selectProvider() {
          this.userData.role = "provider";
        }
      }, {
        key: "signup",
        value: function signup() {
          var _this = this;

          if (this.userData.role === "parent") {
            this.ngxLoader.start();
            this.apiservice.addUser(this.userData).subscribe(function (res) {
              console.log('res from use model ', res);
              _this.response = res;

              _this.ngxLoader.stop();

              if (_this.response.isSuccess === true) {
                _this.snack.open(_this.message, 'OK', {
                  duration: 7000
                });

                _this.router.navigate(['/login']);
              } else {
                if (_this.response.isSuccess === false && _this.response.error === 'Email already resgister') {
                  var msg = 'Email already registered!';

                  _this.snack.open(msg, 'OK', {
                    duration: 7000
                  });
                } else {
                  var _msg = 'Something Went Wrong!';

                  _this.snack.open(_msg, 'OK', {
                    duration: 7000
                  });
                }
              }
            });
          }

          if (this.userData.role === "provider") {
            this.ngxLoader.start();
            this.apiservice.addUser(this.providerData).subscribe(function (res) {
              console.log('res from use model ', res);
              _this.response = res;

              _this.ngxLoader.stop();

              if (_this.response.isSuccess === true) {
                _this.snack.open(_this.message, 'OK', {
                  duration: 7000
                });

                _this.router.navigate(['/login']);
              } else {
                if (_this.response.isSuccess === false && _this.response.error === 'Email already resgister') {
                  var msg = 'Email already registered!';

                  _this.snack.open(msg, 'OK', {
                    duration: 7000
                  });
                } else {
                  var _msg2 = 'Something Went Wrong!';

                  _this.snack.open(_msg2, 'OK', {
                    duration: 7000
                  });
                }
              }
            });
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.providerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
          });
          this.parentForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
          });
        }
      }]);

      return SignUpComponent;
    }();

    SignUpComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"]
      }, {
        type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__["NgxUiLoaderService"]
      }];
    };

    SignUpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sign-up',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sign-up.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sign-up.component.css */
      "./src/app/pages/sign-up/sign-up.component.css"))["default"]]
    })], SignUpComponent);
    /***/
  },

  /***/
  "./src/app/pages/sign-up/sign-up.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/sign-up/sign-up.module.ts ***!
    \*************************************************/

  /*! exports provided: SignUpModule */

  /***/
  function srcAppPagesSignUpSignUpModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignUpModule", function () {
      return SignUpModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/core/core.module */
    "./src/app/core/core.module.ts");
    /* harmony import */


    var _sign_up_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./sign-up.component */
    "./src/app/pages/sign-up/sign-up.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");

    var routes = [{
      path: '',
      component: _sign_up_component__WEBPACK_IMPORTED_MODULE_5__["SignUpComponent"],
      children: []
    }];

    var SignUpModule = function SignUpModule() {
      _classCallCheck(this, SignUpModule);
    };

    SignUpModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_sign_up_component__WEBPACK_IMPORTED_MODULE_5__["SignUpComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], src_app_core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)]
    })], SignUpModule);
    /***/
  }
}]);
//# sourceMappingURL=app-pages-sign-up-sign-up-module-es5.js.map
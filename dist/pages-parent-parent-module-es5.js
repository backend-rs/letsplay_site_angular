function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-parent-parent-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/detail-page/detail-page.component.html":
  /*!***********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/detail-page/detail-page.component.html ***!
    \***********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesParentDetailPageDetailPageComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<body>\n  <app-header></app-header>\n  <div class=\"main_outer\">\n    <div class=\"program_outer\">\n      <div class=\"container-fluid\">\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <div class=\"program_images\">\n              <div class=\"program_box\">\n                <img src=\"assets/program_img1.jpg\" alt=\"Program Image\">\n              </div>\n              <div class=\"program_box\">\n                <img src=\"assets/program_img2.jpg\" alt=\"Program Image\">\n              </div>\n              <div class=\"program_box\">\n                <img src=\"assets/program_img3.jpg\" alt=\"Program Image\">\n              </div>\n              <div class=\"program_box\">\n                <img src=\"assets/program_img1.jpg\" alt=\"Program Image\">\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"batch_outer detailed2\">\n\n          <div class=\"row program_text\">\n            <div class=\"col-12\">\n              <div class=\"CategoryText\">\n                <h5>Category</h5>\n                <h3>Soccer Multi Class Session Micro\n                </h3>\n                <div class=\"shareIcon\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/share_orange.svg\" alt=\"share icon\">\n                  </a>\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/bookmark_orange.svg\" alt=\"share icon\">\n                  </a>\n                </div>\n                <div class=\"session_details\">\n                  <p><img src=\"assets/loction_pin.svg\" alt=\"Loction Pin\"> Jersey City, NJ </p>\n                  <p><img src=\"assets/calendar.svg\" alt=\"Loction Pin\"> Mar 09-13,2020 </p>\n                  <p><img src=\"assets/user_group.svg\" alt=\"Loction Pin\"> 12-16 Yrs. </p>\n                  <p><img src=\"assets/watch.svg\" alt=\"Loction Pin\"> 09:00 – 11:00 </p>\n                </div>\n                <div class=\"insured_outer\">\n                  <p><img src=\"assets/tick_img.png\" alt=\"Loction Pin\"> Verified </p>\n                  <p><img src=\"assets/reward_img2.png\" alt=\"Loction Pin\"> Top Ratted in STEM </p>\n                  <p><img src=\"assets/reward_img.png\" alt=\"Loction Pin\"> Certified Provider </p>\n                  <p><img src=\"assets/insured.png\" alt=\"Loction Pin\"> Fully Insured </p>\n                </div>\n\n                <div class=\"completion_score logout\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/logout_icon.svg\" alt=\"Logout Image\">\n                  </a>\n                </div>\n\n              </div>\n            </div>\n          </div>\n\n\n          <div class=\"row detail_page\">\n            <div class=\"col-md-8\">\n              <div class=\"batch_left\">\n                <h2>Batches</h2>\n                <div class=\"listing_outer\">\n                  <div class=\"batch_box\">\n                    <div class=\"batch_title\">\n                      <p> Tile of the batch</p>\n                    </div>\n                    <div class=\"batch_list_outer\">\n                      <div class=\"batch_listing\">\n                        <label>Time</label>\n                        <p>09:00 – 11:00</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>Dates</label>\n                        <p>09-13 Mar, 2020</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>instructor</label>\n                        <div class=\"batch_profile\">\n                          <img src=\"assets/profile2.png\" alt=\"Profile Image\">\n                        </div>\n                        <p>Janet Doe</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>number of seats</label>\n                        <p>20</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>Location</label>\n                        <p>Fake ave 123</p>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"batch_box\">\n                    <div class=\"batch_title\">\n                      <p> Tile of the batch</p>\n                    </div>\n                    <div class=\"batch_list_outer\">\n                      <div class=\"batch_listing\">\n                        <label>Time</label>\n                        <p>09:00 – 11:00</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>Dates</label>\n                        <p>09-13 Mar, 2020</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>instructor</label>\n                        <div class=\"batch_profile\">\n                          <img src=\"assets/profile2.png\" alt=\"Profile Image\">\n                        </div>\n                        <p>Janet Doe</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>number of seats</label>\n                        <p>20</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>Location</label>\n                        <p>Fake ave 123</p>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"edit_icon\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/edit.svg\" alt=\"Edit Image\">\n                  </a>\n                </div>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Description</h2>\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n                  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\n                  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum\n                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui\n                  officia deserunt mollit anim id est laborum.</p>\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n                  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\n                  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum\n                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui\n                  officia deserunt mollit anim id est laborum.</p>\n                <div class=\"edit_icon\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/edit.svg\" alt=\"Edit Image\">\n                  </a>\n                </div>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Cancellation Policy</h2>\n                <p>\n                  <label>Booking can’t be cancelled before</label>\n                  24 Hours</p>\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n                  et dolore magna aliqua. Ut enim ad minim veniam,</p>\n                <div class=\"edit_icon\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/edit.svg\" alt=\"Edit Image\">\n                  </a>\n                </div>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Special Instructions</h2>\n                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n                  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\n                  aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum\n                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui\n                  officia deserunt mollit anim id est laborum.</p>\n                <div class=\"edit_icon\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/edit.svg\" alt=\"Edit Image\">\n                  </a>\n                </div>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Reviews <a href=\"javascript:;\" class=\"view_all\">View all</a> </h2>\n                <div class=\"reviews_outer\">\n                  <div class=\"media align-items-center\">\n                    <div class=\"media-left\">\n                      <img src=\"assets/review_img1.png\" alt=\"Review\">\n                    </div>\n                    <div class=\"media-body\">\n                      <label>John Doe</label>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                        labore et dolore magna aliqua.</p>\n                    </div>\n                  </div>\n                  <div class=\"media align-items-center\">\n                    <div class=\"media-left\">\n                      <img src=\"assets/review_img2.png\" alt=\"Review\">\n                    </div>\n                    <div class=\"media-body\">\n                      <label>James Do</label>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                        labore et dolore magna aliqua.</p>\n                    </div>\n                  </div>\n                  <div class=\"media align-items-center\">\n                    <div class=\"media-left\">\n                      <img src=\"assets/review_img3.png\" alt=\"Review\">\n                    </div>\n                    <div class=\"media-body\">\n                      <label>Jane Doe</label>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                        labore et dolore magna aliqua.</p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n            </div>\n            <div class=\"col-md-4\">\n              <div class=\"batch_right border_none active_edit\">\n                <h2>Active</h2>\n                <div class=\"price_child\">\n                  <h3>$200</h3>\n                  <p>/ per child</p>\n                </div>\n                <div class=\"batch_right_form\">\n                  <div class=\"form-group date_time\">\n                    <label>Date</label>\n                    <div class=\"input_outer\">\n                      <input type=\"date\">\n                      <div class=\"arrow_img\">\n                        <img src=\"assets/arrow-right.svg\" alt=\"Arrow Image\">\n                      </div>\n                      <input type=\"date\" class=\"right_input\">\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Days</label>\n                    <div class=\"input_outer\">\n                      <input type=\"text\" placeholder=\"\" class=\"form-control\">\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Number of seats</label>\n                    <div class=\"input_outer\">\n                      <input type=\"text\" placeholder=\"\" class=\"form-control\">\n                    </div>\n                  </div>\n                  <div class=\"form-group date_time\">\n                    <label>Time</label>\n                    <div class=\"input_outer\">\n                      <input type=\"time\">\n                      <div class=\"arrow_img\">\n                        <img src=\"assets/arrow-right.svg\" alt=\"Arrow Image\">\n                      </div>\n                      <input type=\"time\" class=\"right_input\">\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Location</label>\n                    <div class=\"input_outer\">\n                      <input type=\"text\" placeholder=\"\" class=\"form-control\">\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Age Group</label>\n                    <img src=\"assets/age_group.jpg\" alt=\"Age Group Image\" class=\"fluid-img\">\n                  </div>\n                  <div class=\"form-group\">\n                    <label>Adult assistance is required</label>\n                    <div class=\"adult_assistance\">\n                      <label class=\"switch notification\">\n                        <input type=\"checkbox\">\n                        <span class=\"slider round\"></span>\n                      </label>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <button type=\"button\" class=\"udpate_btn\">Update</button>\n                  </div>\n                </div>\n                <div class=\"edit_icon\">\n                  <a href=\"javascript:;\">\n                    <img src=\"assets/edit.svg\" alt=\"Edit Image\">\n                  </a>\n                </div>\n              </div>\n\n            </div>\n          </div>\n\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n\n  <app-footer></app-footer>\n</body>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/login-parent/login-parent.component.html":
  /*!*************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/login-parent/login-parent.component.html ***!
    \*************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesParentLoginParentLoginParentComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<body>\n  <div class=\"step_header\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <div (click)=\"logo()\"><img src=\"assets/logo-1.png\" alt=\"Logo Image\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"step_outer\">\n\n\n    <mat-horizontal-stepper [linear]=\"true\">\n\n      <!-- --------------1st step------------------ -->\n\n      <mat-step>\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-12\">\n              <h2>Step-1</h2>\n              <h3>Lets get you onboard</h3>\n              <p>Your location will help us to find the best activities near by you</p>\n              <div class=\"location_input\">\n                <div class=\"form-group\">\n                  <label>Location</label>\n                  <input type=\"text\" placeholder=\"New York, NY\" class=\"form-control loction\">\n                </div>\n              </div>\n              <div class=\"btns_next\">\n                <button matStepperNext class=\"button next\">Next</button>\n                <button matStepperNext class=\"button skip\">Skip</button>\n              </div>\n            </div>\n          </div>\n        </div>\n\n      </mat-step>\n\n      <!-- ---------------------------2nd step-------------------------------- -->\n\n      <mat-step [formGroup]=\"addChildForm\">\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-12\">\n              <h2>Step-2</h2>\n              <h3>Lets add your kid’s information</h3>\n              <p>Your kid’s information will help us to customized activities according to their age group</p>\n              <div class=\"location_input\">\n                <div class=\"form-group\">\n                  <label>Name</label>\n                  <input type=\"text\" placeholder=\"Type\" class=\"form-control\"\n                    [formControl]=\"addChildForm.controls['name']\" [(ngModel)]=\"addChildData.name\">\n                  <small\n                    *ngIf=\" addChildForm.controls['name'].hasError('required') && addChildForm.controls['name'].touched\"\n                    class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n\n                </div>\n\n                <div class=\"form-group\">\n                  <label>Age</label>\n                  <input type=\"text\" placeholder=\"Type\" class=\"form-control\"\n                    [formControl]=\"addChildForm.controls['age']\" [(ngModel)]=\"addChildData.age\">\n\n                  <small\n                    *ngIf=\" addChildForm.controls['age'].hasError('required') && addChildForm.controls['age'].touched\"\n                    class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                </div>\n\n                <div class=\"form-group\">\n                  <label>date of Birth (Optional)</label>\n                  <input type=\"text\" placeholder=\"MM/DD/YYYY\" class=\"form-control\"\n                    [formControl]=\"addChildForm.controls['dob']\" [(ngModel)]=\"addChildData.dob\">\n                </div>\n\n                <div class=\"form-group\">\n                  <!-- <div class=\"add_another\"><span class=\"icon icon-plus\"></span>Add another Child</div> -->\n                </div>\n\n              </div>\n\n              <div class=\"btns_next\">\n                <button matStepperNext class=\"button next\" [disabled]=\"addChildForm.invalid\">Next</button>\n                <button (click)=\"openSearch()\" class=\"button skip\">Skip</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </mat-step>\n\n      <!-- -----------------------------3rd step---------------------------------------- -->\n      <mat-step>\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-12\">\n              <h2>Step-3</h2>\n              <h3>Lets add your kid’s interests</h3>\n              <p>Your kid’s interest will help us to narrow down the activities</p>\n              <div class=\"location_input interest\">\n\n                <div class=\"form-group\">\n                  <label>Intrests</label>\n                  <div class=\"input_outer\">\n                    <div class=\"ng-autocomplete\">\n                      <ng-autocomplete [data]=\"categories\" [searchKeyword]=\"keyword\" (selected)='selectEvent($event)'\n                        (inputChanged)='onChangeSearch($event)' (inputFocused)='onFocused($event)'\n                        [itemTemplate]=\"itemTemplate\" [notFoundTemplate]=\"notFoundTemplate\">\n                      </ng-autocomplete>\n\n                      <ng-template #itemTemplate let-item>\n                        <a [innerHTML]=\"item.name\"></a>\n                      </ng-template>\n\n                      <ng-template #notFoundTemplate let-notFound>\n                        <div *ngIf=\"categories.length\" [innerHTML]=\"notFound\"></div>\n                      </ng-template>\n                    </div>\n\n\n                  </div>\n                  <br>\n                  <mat-form-field>\n                    <mat-chip-list>\n                      <mat-chip *ngFor=\"let category of addChildData.interestInfo;let indx=index;\"\n                        [selectable]=\"selectable\" [removable]=\"removable\" (removed)=\"remove(category)\">\n                        {{category.name}}\n                        <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\n                      </mat-chip>\n                    </mat-chip-list>\n                  </mat-form-field>\n\n                </div>\n\n\n                <!-- <div class=\"kids_interest\">\n\n                  <label class=\"ch-radio cursor\" *ngFor=\" let intrest of getCategoryResponse.data\">\n                    <input style=\"display: none;\" name=\"hiring\" type=\"checkbox\"\n                      (change)=\"onIntrest(intrest, $event.target.checked)\">\n                    <span>\n                      {{intrest.name}}\n                    </span>\n                  </label>\n                </div> -->\n\n              </div>\n              <div class=\"btns_next\">\n                <button matStepperNext (click)=\"addChild(userData.id)\" class=\"button next\">Next</button>\n                <button matStepperNext (click)=\"addChild(userData.id)\" class=\"button skip\">Skip</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </mat-step>\n\n      <!-- --------------------4th step--------------------------- -->\n      <!-- \n      <mat-step [formGroup]=\"addChildForm\">\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-12\">\n              <h2>Step-4</h2>\n              <h3>Lets add your kid’s information</h3>\n              <p>Your kid’s information will help us to customized activities according to their age group</p>\n              <div class=\"location_input interest\">\n\n                <div class=\"kids_interest\" *ngIf=\"getTagByCategoryResponse\">\n                  <div class=\"interest_sub\" *ngFor=\"let category of getTagByCategoryResponse.data\">\n                    <a class=\"selected interest\">{{category.name}}</a>\n                    <div class=\"sub_inner\">\n                      <label class=\"ch-radio cursor\" *ngFor=\" let info of kidsInfo[0]\">\n                        <input style=\"display: none;\" name=\"hiring\" type=\"checkbox\"\n                          (change)=\"onTags(info, $event.target.checked)\">\n                        <span>\n                          {{info}}\n                        </span>\n                      </label>\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"kids_interest\" *ngIf=\"!getTagByCategoryResponse\">\n                  <div class=\"interest_sub\">\n                    <a class=\"selected interest\"></a>\n                    <div class=\"sub_inner\">NO DATA\n                    </div>\n                  </div>\n                </div>\n\n              </div>\n              <div class=\"btns_next\">\n                <button (click)=\"addChild(userData.data.id)\" class=\"button next\">Let’s Browse</button>\n                <button (click)=\"addChild(userData.data.id)\" class=\"button skip\">Skip</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </mat-step> -->\n    </mat-horizontal-stepper>\n    <div class=\"step_footer\">\n      <div class=\"container\">\n        <p>© 2020 Letsplay Holdings Inc.</p>\n      </div>\n    </div>\n  </div>\n\n</body>\n<ngx-ui-loader></ngx-ui-loader>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/parent-profile/parent-profile.component.html":
  /*!*****************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/parent-profile/parent-profile.component.html ***!
    \*****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesParentParentProfileParentProfileComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-header></app-header>\n\n<body>\n\n  <section class=\"main_outer\">\n\n\n    <div class=\"content_outer\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <app-breadcrumb></app-breadcrumb>\n            <div class=\"page_heading\">\n              <h2>My Profile</h2>\n            </div>\n          </div>\n        </div>\n        <button class=\"my_class\" (click)=\"sideBar()\">\n          <span class=\"icon icon-menu\"></span>\n        </button>\n\n        <div class=\"MainContent\">\n          <div class=\"side_bar\" [ngClass]=\"isSideBar ? 'side_bar_show' : 'side_bar_hide'\">\n            <div class=\"progress\">\n              <div class=\"progress-bar\" [style.width.%]=\"profileProgress\">\n                <div class=\"progress-value\">{{profileProgress}}%</div>\n              </div>\n            </div>\n            <div class=\"complete_score\">\n              <p>Profile completion score<span class=\"icon-information\"></span></p>\n            </div>\n            <div class=\"side_menu\">\n              <ul class=\"cursor\">\n                <li>\n                  <a [class.active]=\"profile\" (click)=\"onProfile()\">My Profile</a>\n                </li>\n                <li>\n                  <a [class.active]=\"setting\" (click)=\"onSetting()\">Account Settings</a>\n                </li>\n                <li>\n                  <a [class.active]=\"guardian\" (click)=\"onGuardian(userData.id)\">Guardians</a>\n                </li>\n                <li>\n                  <a [class.active]=\"children\" (click)=\"onChildren(userData.id)\">Children</a>\n                </li>\n                <li>\n                  <a [class.active]=\"favourite\" (click)=\"getFav(userData.id)\">Saved Listings</a>\n                </li>\n                <li>\n                  <a [class.active]=\"notification\" (click)=\"onNotification()\">Notifications</a>\n                </li>\n                <li>\n                  <a [class.active]=\"friend\" (click)=\"onFriend()\">Tell a Friend</a>\n                </li>\n                <li>\n                  <a [class.active]=\"feedback\" (click)=\"onFeedback()\">Give Feedback</a>\n                </li>\n              </ul>\n            </div>\n          </div>\n\n\n          <div class=\"content_right\">\n            <!-- ----------------------------my profile------------------------------- -->\n\n            <div *ngIf=\"isProfile\" class=\"personal_details\">\n              <h2>Personal Details</h2>\n              <div class=\"row\">\n                <div class=\"col-md-8\">\n                  <form [formGroup]=\"updateForm\" class=\"details_form\">\n                    <div class=\"form-group\">\n                      <label>Full Name</label>\n                      <input type=\"search\" placeholder=\"Type\" class=\"form-control\"\n                        [formControl]=\"updateForm.controls['firstName']\" [(ngModel)]=\"user.firstName\" name=\"firstName\">\n                      <small\n                        *ngIf=\"updateForm.controls['firstName'].hasError('required') && updateForm.controls['firstName'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Email Address</label>\n                      <input type=\"search\" placeholder=\"\" class=\"form-control\"\n                        [formControl]=\"updateForm.controls['email']\" [(ngModel)]=\"user.email\" name=\"email\" readonly>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Address</label>\n                      <input type=\"search\" placeholder=\"Type\" class=\"form-control\"\n                        [formControl]=\"updateForm.controls['addressLine1']\" [(ngModel)]=\"user.addressLine1\"\n                        name=\"addressLine1\">\n                      <small\n                        *ngIf=\" updateForm.controls['addressLine1'].hasError('required') && updateForm.controls['addressLine1'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Mobile Number</label>\n                      <input type=\"search\" placeholder=\"e.g. (555) 555 5555\" class=\"form-control\"\n                        [formControl]=\"updateForm.controls['phoneNumber']\" [(ngModel)]=\"user.phoneNumber\"\n                        name=\"phoneNumber\">\n                      <small\n                        *ngIf=\" updateForm.controls['phoneNumber'].hasError('required') && updateForm.controls['phoneNumber'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n                  </form>\n                </div>\n\n                <div class=\"col-md-4\">\n                  <div class=\"profile_img\">\n                    <label class=\"cursor\">\n                      <input style=\"display: none;\" #file accept=\"image/*\"\n                        (change)=\"parentImageSelect($event,userData.id)\" type=\"file\" name=\"image\" />\n                      <a *ngIf=\"!user.avatarImages\">\n                        <span class=\"icon icon-edit_profile\"></span>\n                        Upload Photo\n                      </a>\n                      <img *ngIf=\"user.avatarImages\" src=\"{{user.avatarImages}}\" alt=\"Profile image\">\n                    </label>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a (click)=\"back()\" class=\"back_arrow cursor\"><span class=\"icon-left-arrow\"></span>Back</a>\n                  <button (click)=\"updateParent(user)\" [disabled]=\"updateForm.invalid\"\n                    class=\"btn_style save_btn cursor\">Save</button>\n                </div>\n              </div>\n\n\n            </div>\n            <!-- -------------------------------------------------------------------- -->\n\n\n            <!-- ---------------account setting------------------------- -->\n            <div *ngIf=\"isSetting\" class=\"personal_details\">\n              <h2>Change Password</h2>\n              <div class=\"row\">\n                <div class=\"col-md-8\">\n                  <form class=\"details_form\" [formGroup]=\"resetPasswordForm\">\n                    <div class=\"form-group\">\n                      <label>Current Password</label>\n                      <div class=\"input_outer\">\n                        <input type=\"password\" placeholder=\"Type\" class=\"form-control\"\n                          [formControl]=\"resetPasswordForm.controls['oldPassword']\"\n                          [(ngModel)]=\"resetPasswordData.oldPassword\">\n                        <small\n                          *ngIf=\" resetPasswordForm.controls['oldPassword'].hasError('required') && resetPasswordForm.controls['oldPassword'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>New Password</label>\n                      <div class=\"input_outer\">\n                        <input type=\"password\" placeholder=\"Type\" class=\"form-control\"\n                          [formControl]=\"resetPasswordForm.controls['newPassword']\"\n                          [(ngModel)]=\"resetPasswordData.newPassword\">\n                        <small\n                          *ngIf=\" resetPasswordForm.controls['newPassword'].hasError('required') && resetPasswordForm.controls['newPassword'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Confirm New Password</label>\n                      <div class=\"input_outer\">\n                        <input type=\"password\" placeholder=\"Type\" class=\"form-control\"\n                          [formControl]=\"resetPasswordForm.controls['confirmPassword']\">\n                        <small *ngIf=\"resetPasswordForm.controls['confirmPassword'].hasError('equalTo')\"\n                          class=\"form-error-msg\"> PASSWORD DO NOT MATCH</small>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group delete_account\">\n                      <label>\n                        <a *ngIf=\"user.isActivated\" class=\"cursor\" data-toggle=\"modal\" data-target=\"#confirmModal\">\n                          Deactivate my account\n                        </a>\n                        <a *ngIf=\"!user.isActivated\" class=\"cursor\" data-toggle=\"modal\" data-target=\"#confirmModal\">\n                          Activate my account\n                        </a>\n                      </label>\n\n                    </div>\n                  </form>\n                </div>\n              </div>\n\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a href=\"javascript:;\" class=\"back_arrow\"><span class=\"icon-left-arrow\"></span>Back</a>\n                  <button (click)=\"resetPassword(userData.id)\" [disabled]=\"resetPasswordForm.invalid\"\n                    class=\"btn_style save_btn cursor\">Save</button>\n                </div>\n              </div>\n            </div>\n\n            <!-- --------------------------------------------------------- -->\n\n            <!-- ------------------guardians---------------------- -->\n            <div *ngIf=\"isGuardian\" class=\"personal_details\">\n              <h2>Guardian Information</h2>\n              <div class=\"row margin_top\">\n                <div class=\"col-md-8\">\n                  <div class=\"profile_list\" *ngIf=\"getGuardianResponse\">\n                    <div class=\"list_box\" *ngFor=\"let guardian of getGuardianResponse.data\">\n                      <div class=\"profile_img\">\n                        <img src=\"assets/profile2.png\" alt=\"Profile Image\">\n                      </div>\n                      <div class=\"profile_text\">\n                        <h3>{{guardian.user.firstName}}</h3>\n                        <p>{{guardian.user.sex}}</p>\n                      </div>\n                    </div>\n                    <div class=\"list_box\" *ngIf=\"!getGuardianResponse\">\n                      <div class=\"profile_img\">\n                        <img src=\"assets/profile3.png\" alt=\"Profile Image\">\n                      </div>\n                      <div class=\"profile_text\">\n                        <h3>NO DATA</h3>\n                        <p></p>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col-md-4\">\n                  <a (click)=\"onGuardianAdd()\" class=\"btn_style save_btn cursor\">ADD</a>\n                </div>\n              </div>\n\n\n            </div>\n\n            <!-- ---------------------------------------------------------- -->\n\n            <!-- ----------------------------------guardians information------------------ -->\n            <div *ngIf=\"isInvite\" class=\"personal_details\">\n              <h2>Guardian Information</h2>\n              <div class=\"row\">\n                <div class=\"col-md-8\">\n                  <form class=\"details_form\" [formGroup]=\"addGuardianForm\">\n                    <div class=\"form-group\">\n                      <label>Full Name</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"Type\" class=\"form-control\"\n                          [formControl]=\"addGuardianForm.controls['firstName']\" [(ngModel)]=\"addGuardianData.firstName\">\n                        <small\n                          *ngIf=\" addGuardianForm.controls['firstName'].hasError('required') && addGuardianForm.controls['firstName'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label>Email Address</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"example@domain.com\" class=\"form-control\"\n                          [formControl]=\"addGuardianForm.controls['email']\" [(ngModel)]=\"addGuardianData.email\">\n                        <small\n                          *ngIf=\" addGuardianForm.controls['email'].hasError('required') && addGuardianForm.controls['email'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                        <small\n                          *ngIf=\" addGuardianForm.controls['email'].hasError('email') && addGuardianForm.controls['email'].touched\"\n                          class=\"form-error-msg\">INVALID EMAIL ADDRESS</small>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label>add a personal note</label>\n                      <textarea placeholder=\"Type\" class=\"form-control\"\n                        [formControl]=\"addGuardianForm.controls['personalNote']\"\n                        [(ngModel)]=\"addGuardianData.personalNote\">Type</textarea>\n                      <small\n                        *ngIf=\" addGuardianForm.controls['personalNote'].hasError('required') && addGuardianForm.controls['personalNote'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n                  </form>\n                </div>\n              </div>\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a (click)=\"onGuardian(userData.id)\" class=\"back_arrow cursor\"><span\n                      class=\"icon-left-arrow\"></span>Back</a>\n                  <button (click)=\"addGuardian(userData.id)\" [disabled]=\"addGuardianForm.invalid\"\n                    class=\"btn_style save_btn cursor\">SAVE</button>\n                </div>\n              </div>\n            </div>\n\n\n            <!-- ---------------------------------------------------------------------------------- -->\n\n            <!-- ----------------------children--------------------- -->\n            <div *ngIf=\"isChildren\" class=\"personal_details\">\n              <h2>Children Information</h2>\n              <div class=\"row margin_top\">\n                <div class=\"col-md-8\">\n                  <div class=\"profile_list\" *ngIf=\" kids\">\n                    <div class=\"list_box\" *ngFor=\"let kid of kids\">\n                      <div class=\"profile_img cursor\" (click)=\"onEditChild(kid)\">\n                        <img src=\"{{kid.avtar}}\" alt=\"Profile Image\">\n                      </div>\n                      <div class=\"profile_text\">\n                        <h3>{{kid.name}}</h3>\n                        <p *ngIf=\"kid.sex==='male'\">SON</p>\n                        <p *ngIf=\"kid.sex==='female'\">DAUGHTER</p>\n\n                      </div>\n                    </div>\n                  </div>\n\n                  <div class=\"profile_list\" *ngIf=\"!kids\">\n                    <div class=\"list_box\">\n                      <div class=\"profile_img\">\n                      </div>\n                      <div class=\"profile_text\">\n                        <h3>NO DATA</h3>\n                      </div>\n                    </div>\n                  </div>\n\n                </div>\n                <div class=\"col-md-4\">\n                  <a (click)=\"onAddChild()\" class=\"btn_style save_btn cursor\">Add</a>\n                </div>\n              </div>\n\n\n            </div>\n\n            <!-- ------------------------------------------------------------- -->\n\n            <!-- ------------------------------add child----------------------------- -->\n            <div *ngIf=\"isAddChild\" class=\"personal_details\">\n              <h2>Children</h2>\n              <div class=\"row margin_top\">\n                <div class=\"col-md-8\">\n                  <form class=\"details_form\">\n\n                    <div class=\"form-group\">\n                      <label>Full Name</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"e.g. John Doe\" class=\"form-control\" [(ngModel)]=\"kid.name\"\n                          name=\"FullName\">\n\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Gender</label>\n                      <div class=\"custom_select\">\n                        <select [(ngModel)]=\"kid.sex\" name=\"sex\">\n                          <option *ngIf=\"kid.sex==='male'\" selected>SON</option>\n                          <option *ngIf=\"kid.sex==='female'\" selected>DAUGHTER</option>\n                          <option value=\"male\">SON</option>\n                          <option value=\"female\">DAUGHTER</option>\n                        </select>\n\n                      </div>\n\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Date of Birth</label>\n                      <div class=\"input_outer\">\n                        <input type=\"dob\" placeholder=\"MM/DD/YYYY\" class=\"form-control\" [(ngModel)]=\"kid.dob\"\n                          name=\"dob\">\n\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>School</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"Type\" class=\"form-control\" [(ngModel)]=\"kid.schoolInfo\"\n                          name=\"schoolInfo\">\n\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Relationship to child</label>\n                      <div class=\"custom_select\">\n                        <select [(ngModel)]=\"kid.relationToChild\" name=\"relationToChild\">\n                          <option selected>{{kid.relationToChild}}</option>\n                          <option selected value=\"Father\">Father</option>\n                          <option value=\"Mother\">Mother</option>\n                        </select>\n                      </div>\n\n                    </div>\n                    <br>\n                    <div class=\"form-group\">\n                      <label>Intrests</label>\n                      <div class=\"input_outer\">\n                        <div class=\"ng-autocomplete\">\n                          <ngx-ui-loader></ngx-ui-loader>\n                          <ng-autocomplete [data]=\"tags\" [searchKeyword]=\"keyword\" (selected)='selectEvent($event)'\n                            (inputChanged)='onChangeSearch($event)' (inputFocused)='onFocused($event)'\n                            [itemTemplate]=\"itemTemplate\" [notFoundTemplate]=\"notFoundTemplate\">\n                          </ng-autocomplete>\n\n                          <ng-template #itemTemplate let-item>\n                            <a [innerHTML]=\"item.name\"></a>\n                          </ng-template>\n\n                          <ng-template #notFoundTemplate let-notFound>\n                            <div *ngIf=\"tags.length\" [innerHTML]=\"notFound\"></div>\n                          </ng-template>\n                        </div>\n                      </div>\n                      <br>\n                      <mat-form-field class=\"demo-chip-list\">\n                        <mat-chip-list>\n                          <mat-chip *ngFor=\"let t of kid.interestInfo;let indx=index;\" [selectable]=\"selectable\"\n                            [removable]=\"removable\" (removed)=\"remove(t)\">\n                            {{t.name}}\n                            <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\n                          </mat-chip>\n                        </mat-chip-list>\n                      </mat-form-field>\n\n                    </div>\n\n                    <div class=\"form-group\">\n                      <a *ngIf=\"isAddChildBtn\" (click)=\"refreshPage()\" class=\"add_child cursor\"><span\n                          class=\"icon icon-plus\"></span>Add\n                        another\n                        Child</a>\n                      <a *ngIf=\"isEditChildBtn\" (click)=\"onAddChild()\" class=\"add_child cursor\"><span\n                          class=\"icon icon-plus\"></span>Add\n                        another\n                        Child</a>\n                    </div>\n                  </form>\n                </div>\n\n                <div class=\"col-md-4\">\n                  <div class=\"profile_img edit\">\n                    <label class=\"cursor\">\n                      <input style=\"display: none;\" #file accept=\"image/*\" (change)=\"childImageSelect($event)\"\n                        type=\"file\" name=\"image\" />\n                      <a *ngIf=\"!kid.avtar\">\n                        <span class=\"icon icon-edit_profile\"></span>\n                        Upload Photo\n                      </a>\n                      <img *ngIf=\"kid.avtar\" src=\"{{kid.avtar}}\">\n                    </label>\n\n                  </div>\n                </div>\n              </div>\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a (click)=\"onChildren(userData.id)\" class=\"back_arrow cursor\"><span\n                      class=\"icon-left-arrow\"></span>Back</a>\n                  <button *ngIf=\"isAddChildBtn\" (click)=\"addChild(userData.id)\" [disabled]=\"!kid.name\"\n                    class=\"btn_style save_btn cursor\">Save</button>\n                  <button *ngIf=\"isEditChildBtn\" (click)=\"updateChild(kid,userData.id)\"\n                    class=\"btn_style save_btn cursor\">Save</button>\n                </div>\n              </div>\n            </div>\n\n            <!-- ---------------------------------------------------------------------------------------- -->\n\n            <!-- ----------------------------------edit child----------------------------------->\n            <!-- <div *ngIf=\"isEditChild\" class=\"personal_details\">\n              <h2>Children</h2>\n              <div class=\"row margin_top\">\n                <div class=\"col-md-8\">\n                  <form [formGroup]=\"editChildForm\" class=\"details_form\">\n                    <div class=\"form-group\">\n                      <div class=\"media account_name align-items-center\">\n                        <div class=\"media-left\">\n                          <img *ngIf=\"editChild.avtar\" src=\"{{editChild.avtar}}\" alt=\"Children Image\">\n                          <img *ngIf=\"!editChild.avtar\" src=\"assets/userIMAGE.png\">\n                        </div>\n                        <div class=\"media-body\">\n                          <h4>{{editChild.name}}</h4>\n                          <p>{{editChild.age}} Years Old</p>\n                        </div>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Full Name</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"e.g. John Doe\" class=\"form-control\"\n                          [formControl]=\"editChildForm.controls['name']\" [(ngModel)]=\"editChild.name\">\n                        <small\n                          *ngIf=\" editChildForm.controls['name'].hasError('required') && editChildForm.controls['name'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Gender</label>\n                      <div class=\"custom_select\">\n                        <select [formControl]=\"editChildForm.controls['sex']\" [(ngModel)]=\"editChild.sex\">\n                          <option selected>{{editChild.sex}}</option>\n                          <option value=\"SON\">SON</option>\n                          <option value=\"DAUGHTER\">DAUGHTER</option>\n                        </select>\n                      </div><small\n                        *ngIf=\" editChildForm.controls['sex'].hasError('required') && editChildForm.controls['sex'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Date of Birth</label>\n                      <div class=\"input_outer\">\n                        <input type=\"dob\" placeholder=\"MM/DD/YYYY\" class=\"form-control\"\n                          [formControl]=\"editChildForm.controls['dob']\" [(ngModel)]=\"editChild.dob\">\n                        <small\n                          *ngIf=\" editChildForm.controls['dob'].hasError('required') && editChildForm.controls['dob'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>School</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"Type\" class=\"form-control\"\n                          [formControl]=\"editChildForm.controls['schoolInfo']\" [(ngModel)]=\"editChild.schoolInfo\">\n                        <small\n                          *ngIf=\" editChildForm.controls['schoolInfo'].hasError('required') && editChildForm.controls['schoolInfo'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Relationship to child</label>\n                      <div class=\"custom_select\">\n                        <select [formControl]=\"editChildForm.controls['relationToChild']\"\n                          [(ngModel)]=\"editChild.relationToChild\">\n                          <option selected>{{editChild.relationToChild}}</option>\n                          <option value=\"Father\">Father</option>\n                          <option value=\"Mother\">Mother</option>\n                        </select>\n\n                      </div><small\n                        *ngIf=\" editChildForm.controls['relationToChild'].hasError('required') && editChildForm.controls['relationToChild'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <label>Interests</label>\n                      <div class=\"input_outer\">\n                        <div class=\"ng-autocomplete\">\n                          <ng-autocomplete [data]=\"categories\" [searchKeyword]=\"keyword\"\n                            (selected)='selectEvent($event)' (inputChanged)='onChangeSearch($event)'\n                            (inputFocused)='onFocused($event)' [itemTemplate]=\"itemTemplate\"\n                            [notFoundTemplate]=\"notFoundTemplate\">\n                          </ng-autocomplete>\n\n                          <ng-template #itemTemplate let-item>\n                            <a [innerHTML]=\"item.name\"></a>\n                          </ng-template>\n\n                          <ng-template #notFoundTemplate let-notFound>\n                            <div *ngIf=\"categories.length\" [innerHTML]=\"notFound\"></div>\n                          </ng-template>\n                        </div>\n                        <br>\n                        <mat-form-field class=\"demo-chip-list\">\n                          <mat-chip-list>\n                            <mat-chip *ngFor=\"let category of editChild.interestInfo;let indx=index;\"\n                              [selectable]=\"selectable\" [removable]=\"removable\" (removed)=\"remove(category)\">\n                              {{category.name}}\n                              <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\n                            </mat-chip>\n                          </mat-chip-list>\n\n\n                        </mat-form-field>\n\n                      </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                      <a (click)=\"onAddChild()\" class=\"add_child cursor\"><span class=\"icon icon-plus\"></span>Add another\n                        Child</a>\n                    </div>\n                  </form>\n                </div>\n\n\n                <div class=\"col-md-4\">\n                  <div class=\"profile_img edit\">\n                    <label class=\"cursor\">\n                      <input style=\"display: none;\" #file accept=\"image/*\" (change)=\"UpdateChildImageSelect($event)\"\n                        type=\"file\" name=\"myfile\" />\n\n                      <a *ngIf=\"!editChild.avtar\">\n                        <span class=\"icon icon-edit_profile\"></span>\n                        Upload Photo\n                      </a>\n                      <img *ngIf=\"editChild.avtar\" src=\"{{editChild.avtar}}\">\n                    </label>\n                  </div>\n                  <div class=\"select_avtar\">\n                    <p>OR</p>\n                    <label class=\"cursor\">\n                      <a>Select Avatar\n                        <input style=\"display: none;\" #file accept=\"image/*\" (change)=\"UpdateChildImageSelect($event)\"\n                          type=\"file\" name=\"myfile\" />\n                      </a>\n                    </label>\n                  </div>\n                </div>\n\n              </div>\n\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a (click)=\"onChildren(userData.data.id)\" class=\"back_arrow cursor\"><span\n                      class=\"icon-left-arrow\"></span>Back</a>\n                  <button (click)=\"updateChild(editChild._id,userData.data.id)\" [disabled]=\"editChildForm.invalid\"\n                    class=\"btn_style save_btn cursor\">Save</button>\n                </div>\n              </div>\n            </div> -->\n\n            <!-- ----------------------------------------------------------------------------- -->\n\n            <!-- -----------------------saved listing----------------------------------- -->\n            <div *ngIf=\"isFavourite\" class=\"personal_details\">\n              <h2>Saved activities</h2>\n\n              <div class=\"details_outer\">\n                <p style=\"font-family: 'Open Sans';\n                font-weight: 800 ; text-align: center ;    color: #748494;\" *ngIf='!favourites.length'>\n                  Saved List Empty!\n                </p>\n                <div class=\"row row-0\" *ngFor=\"let favourite of favourites\">\n\n                  <div class=\"col-10\">\n                    <div class=\"media activity align-items-center\">\n                      <div class=\"media-left\">\n                        <img src=\"assets/activity1.png\" alt=\"Activity\">\n                        <div class=\"share_icon\">\n                          <a data-toggle=\"modal\" data-target=\"#SearchModal\">\n\n                            <img src=\"assets/share.svg\" alt=\"Share Icon\">\n\n\n\n                          </a>\n                          <div>\n\n                            <a (click)=\"removeFav(favourite.program._id)\" class=\"bookmark_icon cursor\"><img\n                                src=\"assets/bookmarkOrange.svg\" alt=\"Menu Image\"></a>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"media-body\">\n                        <!-- <h3>{{favourite.program.code}}</h3> -->\n                        <h2>{{favourite.program.name}}</h2>\n                        <div class=\"list_outer row row-0\">\n                          <div class=\"list col-12\">\n                            <p><img src=\"assets/local.svg\" alt=\"School image\">{{favourite?.name}}</p>\n                          </div>\n                          <div class=\"list col-md-4\">\n                            <p><img src=\"assets/user_group.svg\" alt=\"School image\">\n                              {{favourite.program.date?.from | date:\"dd:MM:yyyy\"}} –\n                              {{favourite.program.date?.to | date:\"dd:MM:yyyy\"}}\n                            </p>\n                          </div>\n                          <div class=\"list col-md-4\">\n                            <p><img src=\"assets/calendar.svg\" alt=\"School image\">\n                              {{favourite?.location}}\n                            </p>\n                          </div>\n                          <div class=\"list col-md-4\">\n                            <p><img src=\"assets/loction_pin.svg\" alt=\"School image\">\n                              {{favourite.program.location}}\n                            </p>\n                          </div>\n                        </div>\n                        <div class=\"recomend_images\">\n                          <a href=\"javascript:;\">\n                            <img src=\"assets/tick_img.png\" alt=\"Recomend images\">\n                          </a>\n                          <a href=\"javascript:;\">\n                            <img src=\"assets/reward_img2.png\" alt=\"Recomend images\">\n                          </a>\n                          <a href=\"javascript:;\">\n                            <img src=\"assets/reward_img.png\" alt=\"Recomend images\">\n                          </a>\n                        </div>\n                        <div class=\"recomend_btn\">\n                          <!-- <a href=\"javascript:;\">Recommended</a> -->\n                        </div>\n                      </div>\n                      <div class=\"rating_box\">\n                        <a href=\"\">\n                          <img src=\"assets/star.svg\" alt=\"Star\">\n                          5.0 (15)\n                        </a>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <!-- \n                <div class=\"row bottom_margin\">\n                  <div class=\"col-12\">\n                    <a href=\"javascript:;\" class=\"btn_style save_btn cursor\">Add</a>\n                  </div>\n                </div> -->\n              </div>\n\n            </div>\n\n\n            <!-- ------------------------------------------------------------------------- -->\n\n            <!-- -------------------------------------------notification----------------------------- -->\n            <div *ngIf=\"isNotification\" class=\"personal_details\">\n              <h2>Notifications</h2>\n              <div class=\"row\">\n                <div class=\"col-md-12\">\n                  <form>\n                    <div class=\"notifications\">\n                      <h3>Show Notifications</h3>\n                      <label class=\"switch notification\">\n                        <input type=\"checkbox\" checked>\n                        <span class=\"slider round\"></span>\n                      </label>\n                    </div>\n                    <div class=\"check_list\">\n                      <label class=\"ch-radio\">\n                        <input name=\"hiring\" type=\"checkbox\" checked>\n                        <span>\n                          <i class=\"ch-icon icon-tick\"></i>All\n                        </span>\n                      </label>\n                      <label class=\"ch-radio\">\n                        <input name=\"hiring\" type=\"checkbox\">\n                        <span>\n                          <i class=\"ch-icon icon-tick\"></i>Lorem Ipsum\n                        </span>\n                      </label>\n                      <label class=\"ch-radio\">\n                        <input name=\"hiring\" type=\"checkbox\">\n                        <span>\n                          <i class=\"ch-icon icon-tick\"></i>Lorem Ipsum\n                        </span>\n                      </label>\n                      <label class=\"ch-radio\">\n                        <input name=\"hiring\" type=\"checkbox\">\n                        <span>\n                          <i class=\"ch-icon icon-tick\"></i>Lorem Ipsum\n                        </span>\n                      </label>\n                    </div>\n                  </form>\n                </div>\n              </div>\n\n            </div>\n\n            <!-- ----------------------------------------------------------------------------------------- -->\n\n            <!-- -------------------------------------------tell a friend----------------------------- -->\n            <div *ngIf=\"isFriend\" class=\"personal_details\">\n              <h2>Tell a Friend</h2>\n              <div class=\"row\">\n                <div class=\"col-md-8\">\n                  <form class=\"details_form\" [formGroup]=\"tellFriendForm\">\n                    <div class=\"form-group\">\n                      <label>Full Name</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"Type\" class=\"form-control\"\n                          [formControl]=\"tellFriendForm.controls['fullName']\" [(ngModel)]=\"tellFriendData.fullName\">\n                        <small\n                          *ngIf=\" tellFriendForm.controls['fullName'].hasError('required') && tellFriendForm.controls['fullName'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label>Email Address</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"example@domain.com\" class=\"form-control\"\n                          [formControl]=\"tellFriendForm.controls['email']\" [(ngModel)]=\"tellFriendData.email\">\n                        <small\n                          *ngIf=\" tellFriendForm.controls['email'].hasError('required') && tellFriendForm.controls['email'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                        <small\n                          *ngIf=\" tellFriendForm.controls['email'].hasError('email') && tellFriendForm.controls['email'].touched\"\n                          class=\"form-error-msg\">INVALID EMAIL ADDRESS</small>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label>add a personal note</label>\n                      <textarea class=\"form-control\" [formControl]=\"tellFriendForm.controls['personalNote']\"\n                        [(ngModel)]=\"tellFriendData.personalNote\">Type</textarea>\n                      <div class=\"textarea_length\">\n                        <!-- <p>(0 of 3000)</p> -->\n                      </div>\n                      <small\n                        *ngIf=\" tellFriendForm.controls['personalNote'].hasError('required') && tellFriendForm.controls['personalNote'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n                  </form>\n                </div>\n              </div>\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a (click)=\"back()\" class=\"back_arrow cursor\"><span class=\"icon-left-arrow \"></span>Back</a>\n                  <button class=\"btn_style save_btn cursor\" (click)=\"tellFriend(userData.firstName)\"\n                    [disabled]=\"tellFriendForm.invalid\">Send</button>\n                </div>\n              </div>\n            </div>\n\n\n            <!-- ----------------------------------------------------------------------------------------- -->\n\n            <!-- -------------------------------------------feedback----------------------------- -->\n            <div *ngIf=\"isFeedback\" class=\"personal_details\">\n              <h2>Give Feedback</h2>\n              <div class=\"row\">\n                <div class=\"col-md-8\">\n                  <form class=\"details_form\" [formGroup]=\"giveFeedbackForm\">\n                    <!-- <div class=\"form-group\">\n                      <label>Full Name</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"\" value=\"John Doe\" class=\"form-control\"\n                          [formControl]=\"giveFeedbackForm.controls['name']\" [(ngModel)]=\"giveFeedbackData.name\">\n                        <small\n                          *ngIf=\" giveFeedbackForm.controls['name'].hasError('required') && giveFeedbackForm.controls['name'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label>Email Address</label>\n                      <div class=\"input_outer\">\n                        <input type=\"text\" placeholder=\"example@domain.com\" class=\"form-control\"\n                          [formControl]=\"giveFeedbackForm.controls['email']\" [(ngModel)]=\"giveFeedbackData.email\">\n                        <small\n                          *ngIf=\" giveFeedbackForm.controls['email'].hasError('required') && giveFeedbackForm.controls['email'].touched\"\n                          class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                        <small\n                          *ngIf=\" giveFeedbackForm.controls['email'].hasError('email') && giveFeedbackForm.controls['email'].touched\"\n                          class=\"form-error-msg\">INVALID EMAIL ADDRESS</small>\n                      </div>\n                    </div> -->\n                    <div class=\"form-group\">\n                      <label>Feedback</label>\n                      <textarea class=\"form-control\" [formControl]=\"giveFeedbackForm.controls['feedback']\"\n                        [(ngModel)]=\"giveFeedbackData.feedback\">Type</textarea>\n                      <div class=\"textarea_length\">\n                        <!-- <p>(0 of 3000)</p> -->\n                      </div>\n                      <small\n                        *ngIf=\" giveFeedbackForm.controls['feedback'].hasError('required') && giveFeedbackForm.controls['feedback'].touched\"\n                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                    </div>\n                  </form>\n                </div>\n              </div>\n              <div class=\"row bottom_arrow\">\n                <div class=\"col-12\">\n                  <a (click)=\"back()\" class=\"back_arrow cursor\"><span class=\"icon-left-arrow\"></span>Back</a>\n                  <button class=\"btn_style save_btn cursor\" (click)=\"giveFeedback(userData.id)\"\n                    [disabled]=\"giveFeedbackForm.invalid\">Send</button>\n                </div>\n              </div>\n            </div>\n\n            <!-- ----------------------------------------------------------------------------------------- -->\n          </div>\n\n\n        </div>\n      </div>\n\n    </div>\n\n  </section>\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"confirmModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n    aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <!-- <h5 class=\"modal-title\" id=\"exampleModalLabel\">YOUR ACCOUNT IS ACTIVE NOW</h5> -->\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          Do you really want to Activate or Deativate account ?\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">CANCEL</button>\n          <button type=\"button\" (click)=\"activeDeactiveUser(userData.id,user.isActivated)\" data-dismiss=\"modal\"\n            class=\"btn btn-primary\">CONFIRM</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <!--share Modal -->\n  <div class=\"modal fade search_modal password\" id=\"SearchModal\" data-backdrop=\"static\" data-keyboard=\"false\"\n    tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\" class=\"icon-close\"></span>\n        </button>\n        <div class=\"modal-body\">\n          <h2>Share</h2>\n          <div class=\"share_option\">\n            <ul>\n              <li>\n                <a href=\"javascript:;\">Facebook</a>\n              </li>\n              <li>\n                <a href=\"javascript:;\">Messenger</a>\n              </li>\n              <li>\n                <a href=\"javascript:;\">Email</a>\n              </li>\n              <li>\n                <a href=\"javascript:;\">Whatsapp</a>\n              </li>\n              <li>\n                <a href=\"javascript:;\">Copylink</a>\n              </li>\n            </ul>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n  <app-footer></app-footer>\n\n</body>\n<!-- <app-confirm-dialog></app-confirm-dialog> -->\n<ngx-ui-loader></ngx-ui-loader>";
    /***/
  },

  /***/
  "./src/app/pages/parent/detail-page/detail-page.component.css":
  /*!********************************************************************!*\
    !*** ./src/app/pages/parent/detail-page/detail-page.component.css ***!
    \********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesParentDetailPageDetailPageComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".program_outer {\n    padding: 50px 95px 30px 95px;\n}\n\n.program_images {\n    width: 100%;\n    display: flex;\n}\n\n.program_images .program_box {\n    width: 100%;\n    max-width: 350px;\n    margin-right: 5px;\n}\n\n.program_images .program_box:last-child {\n    margin-right: 0;\n}\n\n.program_images .program_box img {\n    max-width: 100%;\n}\n\n.program_text {\n    padding: 35px 0 31px 0;\n}\n\n.program_text .CategoryText {\n    position: relative;\n}\n\n.program_text .CategoryText .edit_icon {\n    right: 37%;\n}\n\n.program_text .CategoryText h5 {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    color: #777CEA;\n    text-transform: uppercase;\n}\n\n.program_text .CategoryText .shareIcon {\n    display: inline-block;\n}\n\n.program_text .CategoryText .shareIcon a {\n    display: inline-block;\n    margin: 0 0 0 11px;\n    vertical-align: super;\n}\n\n.program_text .CategoryText .learn_code {\n    width: 60px;\n    height: 60px;\n    border-radius: 6px;\n    overflow: hidden;\n    position: absolute;\n    top: 0;\n    right: 0;\n}\n\n.program_text .CategoryText .learn_code img {\n    max-width: 100%;\n    height: 100%;\n}\n\n.program_text .CategoryText h3 {\n    font-size: 36px;\n    line-height: 37px;\n    color: #777CEA;\n    font-family: 'Patrick Hand';\n    margin: 2px 0 0 0;\n    position: relative;\n    display: inline-block;\n}\n\n.program_text .CategoryText h3 .edit_name {\n    position: absolute;\n    right: -36px;\n    top: 0;\n}\n\n.session_details {\n    margin-top: 3px;\n}\n\n.session_details p {\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    display: inline-block;\n    margin: 0 28px 0 0;\n}\n\n.session_details p img {\n    margin-right: 6px;\n    vertical-align: middle;\n}\n\n.insured_outer {\n    border-top: 1px solid #E5E9EC;\n    border-bottom: 1px solid #E5E9EC;\n    padding: 13px 0;\n    margin-top: 29px;\n    max-width: 742px;\n}\n\n.insured_outer p {\n    display: inline-block;\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 0 35px 0 0;\n}\n\n.insured_outer p img {\n    max-width: 27px;\n    margin-right: 12px;\n}\n\n.batch_outer .batch_left {\n    background: #FFFFFF;\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\n    border-radius: 2px;\n    padding: 30px 35px;\n    position: relative;\n}\n\n.batch_outer .batch_left h2 {\n    font-family: 'Patrick Hand';\n    font-size: 26px;\n    line-height: 18px;\n    letter-spacing: 0.005em;\n    color: #777CEA;\n}\n\n.batch_box {\n    border: 1px solid #E5E9EC;\n    border-radius: 4px;\n    padding: 16px 28px;\n    margin-top: 11px;\n}\n\n.batch_box .batch_title p {\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n}\n\n.listing_outer {\n    margin-top: 29px;\n}\n\n.batch_outer .batch_right {\n    background: #FFFFFF;\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 2px;\n    padding: 21px;\n    margin-bottom: 21px;\n    position: relative;\n}\n\n.batch_outer .batch_right:last-child {\n    margin: 0;\n}\n\n.batch_outer .batch_right img {\n    max-width: 100%;\n}\n\n.batch_outer .batch_right.border_none {\n    border: none;\n    text-align: center;\n    padding: 32px;\n}\n\n.batch_outer .batch_right h2 {\n    font-family: 'Patrick Hand';\n    font-size: 26px;\n    line-height: 18px;\n    letter-spacing: 0.005em;\n    color: #777CEA;\n    margin-top: 16px;\n}\n\n.batch_outer .batch_right P {\n    font-size: 12px;\n    line-height: 18px;\n    text-align: center;\n    color: #748494;\n    margin: 26px 0 0 0;\n}\n\n.batch_outer .batch_right a.business_btn {\n    min-height: 52px;\n    line-height: 52px;\n    width: 100%;\n    max-width: 100%;\n    text-align: center;\n    background-color: #f15c20;\n    color: #fff;\n    font-size: 18px;\n    letter-spacing: 0.005em;\n    padding: 0 10px;\n    border-radius: 4px;\n    display: inline-block;\n    margin-top: 32px;\n    transition: 0.8s;\n}\n\n.batch_outer .batch_right a.business_btn:hover {\n    background: #c74009;\n}\n\n.batch_list_outer {\n    display: flex;\n}\n\n.batch_list_outer .batch_listing {\n    display: inline-block;\n    margin: 0 15px 0 0;\n}\n\n.batch_list_outer .batch_listing label {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    color: #777CEA;\n    text-transform: uppercase;\n    width: 100%;\n}\n\n.batch_list_outer .batch_listing .batch_profile {\n    width: 22px;\n    height: 22px;\n    border-radius: 4px;\n    display: inline-block;\n    margin-right: 14px;\n    overflow: hidden;\n    vertical-align: middle;\n}\n\n.batch_list_outer .batch_listing .batch_profile img {\n    max-width: 100%;\n    height: 100%;\n}\n\n.batch_list_outer .batch_listing p {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    display: inline-block;\n    color: #748494;\n}\n\n.description_text {\n    background: #FFFFFF;\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\n    border-radius: 2px;\n    padding: 30px 35px;\n    margin-top: 21px;\n    position: relative;\n}\n\n.edit_icon {\n    position: absolute;\n    right: 30px;\n    top: 24px;\n}\n\n.description_text h2 {\n    font-family: 'Patrick Hand';\n    font-size: 26px;\n    line-height: 18px;\n    letter-spacing: 0.005em;\n    color: #777CEA;\n}\n\n.description_text p {\n    font-size: 13px;\n    line-height: 19px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin-top: 22px;\n}\n\n.description_text label {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #777CEA;\n    width: 100%;\n    display: inline-block\n}\n\n.description_text h2 .view_all {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    text-transform: uppercase;\n    font-family: 'Open Sans';\n    color: #748494;\n    float: right;\n}\n\n.reviews_outer {\n    margin-top: 36px;\n}\n\n.reviews_outer .media {\n    margin: 0 0 15px 0;\n}\n\n.reviews_outer .media .media-left {\n    width: 50px;\n    height: 50px;\n    border-radius: 4px;\n    overflow: hidden;\n}\n\n.reviews_outer .media .media-left img {\n    max-width: 100%;\n    height: 100%;\n}\n\n.reviews_outer .media .media-body {\n    padding: 0 0 0 12px;\n}\n\n.reviews_outer .media .media-body label {\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n}\n\n.reviews_outer .media .media-body p {\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 0;\n}\n\n.menu_control {\n    position: absolute;\n    top: 0;\n    right: 0;\n    text-align: center;\n}\n\n/* start program detail2 page css */\n\n.batch_outer.detailed2 {\n    padding: 30px 0 0 0;\n}\n\n.batch_outer.detailed2 .program_text {\n    padding: 0 0 31px 0;\n}\n\n.learn_code.left img {\n    max-width: 100px;\n}\n\n.description_text .des_media {\n    margin: 0;\n}\n\n.description_text .des_media:last-child {\n    margin: 0;\n}\n\n.description_text .des_media .media {\n    margin-top: 17px;\n}\n\n.description_text .des_media .media-body {\n    padding: 0 0 0 20px;\n}\n\n.description_text .des_media .media-body p {\n    margin: 0;\n}\n\n.subscription_text {\n    margin-top: 56px;\n}\n\n.subscription_text .subscription_box {\n    display: inline-block;\n    vertical-align: top;\n    margin: 0 161px 0 0;\n}\n\n.subscription_text .subscription_box:last-child {\n    margin: 0;\n}\n\n.social_media {\n    margin: 9px 0 0 0;\n}\n\n.social_media a .icon {\n    color: #748494;\n    margin: 0 6px 0 0;\n    font-size: 19px;\n}\n\n.featured_text .rating_box {\n    position: absolute;\n    right: 0;\n    bottom: 13px;\n    top: auto;\n}\n\n.featured_text .feature_box p {\n    margin: 0 16px 0 0;\n}\n\n.featured_text p {\n    color: #777CEA;\n}\n\n.featured_img .coding_text {\n    position: absolute;\n    bottom: 9px;\n    left: 9px;\n}\n\n.featured_img .coding_text h3 {\n    min-width: 75px;\n    min-height: 21px;\n    line-height: 21px;\n    background: #F15C20;\n    border-radius: 10.5px;\n    display: inline-block;\n    color: #fff;\n    font-size: 10px;\n    letter-spacing: 0.01em;\n    text-align: center;\n    text-transform: uppercase;\n}\n\n.featured_text .featured_btn {\n    border: 1px solid #F15C20;\n    color: #F15C20;\n}\n\n.featured_text .feature_box {\n    margin: 5px 0 12px 0;\n}\n\n/* program edit page css */\n\n.batch_outer .batch_right.active_edit {\n    padding: 30px;\n    text-align: left;\n}\n\n.batch_outer .batch_right.active_edit h2 {\n    margin: 0;\n}\n\n.batch_outer .batch_right .price_child {\n    margin-top: 8px;\n}\n\n.batch_outer .batch_right .price_child h3 {\n    font-weight: 600;\n    font-size: 15px;\n    line-height: 18px;\n    text-align: justify;\n    letter-spacing: 0.01em;\n    color: #748494;\n    display: inline-block;\n}\n\n.batch_outer .batch_right .price_child p {\n    font-size: 12px;\n    line-height: 16px;\n    text-align: justify;\n    color: #748494;\n    display: inline-block;\n    margin: 0;\n}\n\n.batch_right_form {\n    margin-top: 24px;\n}\n\n.batch_right_form .form-group {\n    margin-bottom: 2px;\n}\n\n.batch_right_form .adult_assistance {\n    width: 100%;\n    display: inline-block;\n    margin-top: 5px;\n}\n\n.batch_right_form .form-group.date_time .input_outer {\n    width: 100%;\n    border-radius: 4px;\n    border: 1px solid #E5E9EC;\n    min-height: 52px;\n}\n\n.batch_right_form .form-group.date_time .input_outer input {\n    max-width: 45%;\n    border: none;\n    display: inline-block;\n}\n\n.batch_right_form .form-group.date_time .input_outer input.right_input {\n    text-align: center;\n}\n\n.batch_right_form .form-group.date_time .input_outer .arrow_img {\n    display: inline-block;\n}\n\n.batch_right_form .form-group.date_time .input_outer input::-webkit-calendar-picker-indicator {\n    display: none;\n    -webkit-appearance: none;\n}\n\n.batch_right_form .form-group .udpate_btn {\n    width: 100%;\n    max-width: 352px;\n    background: #F15C20;\n    font-family: 'Patrick Hand';\n    box-sizing: border-box;\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\n    min-height: 52px;\n    line-height: 52px;\n    border-radius: 4px;\n    border: none;\n    box-shadow: none;\n    font-size: 26px;\n    color: #fff;\n    transition: 0.8s;\n}\n\n.batch_right_form .form-group .udpate_btn:hover {\n    background: #c74009;\n}\n\n.edit_icon {\n    position: absolute;\n    right: 30px;\n    top: 24px;\n}\n\n.description_text .map_img {\n    border: 1px solid #E5E9EC;\n    border-radius: 2px;\n    padding: 24px;\n}\n\n.description_text .des_media .media-body.social {\n    padding: 0 0 0 40px;\n}\n\n.completion_score {\n    position: absolute;\n    right: 0;\n    top: 0;\n}\n\n.completion_score p {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    text-transform: uppercase;\n    color: #748494;\n    display: inline-block;\n}\n\n.completion_score a {\n    margin: 0 6px 0 0;\n}\n\n.completion_score .profile_score {\n    display: inline-block;\n}\n\n.description_media .edit_icon {\n    top: 0;\n}\n\n.completion_score.logout {\n    top: 20px;\n}\n\n/* ------------style.css-------------- */\n\n.form-group label {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #777CEA;\n    padding: 0 0 0 15px;\n}\n\n.form-group input {\n    width: 100%;\n    border-radius: 4px;\n    border: 1px solid #E5E9EC;\n    min-height: 52px;\n    height: auto;\n    font-size: 13px;\n    color: #748494;\n    transition: 0.8s;\n    padding: 14px;\n}\n\n/* Rounded sliders */\n\n.switch.notification .slider.round {\n    border-radius: 34px;\n}\n\n.switch.notification .slider.round:before {\n    border-radius: 50%;\n}\n\n/* ---------------------------------- */\n\n@media(max-width:991px) {\n    .program_outer{\n        padding: 34px 15px;\n    }\n    .program_text .CategoryText h3 {\n        font-size: 26px;\n    }\n    .batch_outer .batch_left {\n        padding: 30px 17px;\n    }\n    .batch_outer .batch_right.active_edit {\n        padding:30px 15px;\n    }\n    .batch_list_outer{\n        display: inline-block;\n    }\n    .batch_right_form .form-group .udpate_btn {\n        min-height: 42px;\n        line-height: 42px;\n        font-size: 18px;\n    }\n    .detail_page .col-md-8 {\n        flex: 0 0 59.666667%;\n        max-width: 59.666667%;\n    }\n    .detail_page .col-md-4 {\n        flex: 0 0 39.333333%;\n        max-width: 39.333333%;\n    }\n}\n\n@media(max-width:767px) {\n    .program_outer {\n        padding: 35px 10px;\n    }\n\n    .program_text .CategoryText h3 {\n        font-size: 20px;\n        line-height: 26px;\n        margin: 0px 0 0 0;\n    }\n\n    .batch_list_outer {\n        display: inline-block;\n    }\n\n    .insured_outer p {\n        font-size: 12px;\n        margin: 0 11px 10px 0;\n    }\n\n    .insured_outer p img {\n        max-width: 27px;\n    }\n    .session_details{\n        margin-top: 14px;\n    }\n    .session_details p{\n        margin: 0 17px 7px 0;\n        font-size: 12px;\n    }\n    .batch_outer .batch_left{\n        padding: 30px 12px;\n    }\n    .batch_box{\n        padding: 16px;\n    }\n    .description_text{\n        padding: 16px; \n    }\n    .description_text h2 {\n        font-size: 22px;\n    }\n    .edit_icon {\n        right: 17px;\n        top: 17px;\n    }\n    .batch_outer .batch_right.active_edit {\n        margin-top: 24px;\n        padding: 16px;\n    }\n    .program_text .CategoryText .shareIcon {\n        margin: 0 0 0 12px;\n        vertical-align: middle;\n    }\n    .program_text .CategoryText .shareIcon a {\n        margin: 0 12px 0 0;\n    }\n    .program_text .CategoryText .shareIcon a:last-child{\n        margin: 0;\n    }\n}\n\n@media(max-width:480px) {\n    .program_text .CategoryText .shareIcon {\n        width: 100%;\n        margin: 5px 0 0 0;\n    }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFyZW50L2RldGFpbC1wYWdlL2RldGFpbC1wYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw0QkFBNEI7QUFDaEM7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0kscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQixxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixRQUFRO0FBQ1o7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLDJCQUEyQjtJQUMzQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osTUFBTTtBQUNWOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxxQkFBcUI7SUFDckIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLDZCQUE2QjtJQUM3QixnQ0FBZ0M7SUFDaEMsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsMENBQTBDO0lBQzFDLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksMkJBQTJCO0lBQzNCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFNBQVM7QUFDYjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGFBQWE7QUFDakI7O0FBRUE7SUFDSSwyQkFBMkI7SUFDM0IsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixzQkFBc0I7SUFDdEIsY0FBYztJQUNkLHlCQUF5QjtJQUN6QixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixxQkFBcUI7SUFDckIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQiwwQ0FBMEM7SUFDMUMsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxTQUFTO0FBQ2I7O0FBRUE7SUFDSSwyQkFBMkI7SUFDM0IsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsY0FBYztJQUNkLFdBQVc7SUFDWDtBQUNKOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6Qix3QkFBd0I7SUFDeEIsY0FBYztJQUNkLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLFNBQVM7QUFDYjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sUUFBUTtJQUNSLGtCQUFrQjtBQUN0Qjs7QUFFQSxtQ0FBbUM7O0FBQ25DO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksU0FBUztBQUNiOztBQUVBO0lBQ0ksU0FBUztBQUNiOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksU0FBUztBQUNiOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFlBQVk7SUFDWixTQUFTO0FBQ2I7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQixXQUFXO0lBQ1gsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxvQkFBb0I7QUFDeEI7O0FBRUEsMEJBQTBCOztBQUUxQjtJQUNJLGFBQWE7SUFDYixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLHFCQUFxQjtJQUNyQixTQUFTO0FBQ2I7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gscUJBQXFCO0lBQ3JCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsWUFBWTtJQUNaLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGFBQWE7SUFDYix3QkFBd0I7QUFDNUI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0Isc0JBQXNCO0lBQ3RCLDJDQUEyQztJQUMzQyxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxTQUFTO0FBQ2I7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLE1BQU07QUFDVjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIsY0FBYztJQUNkLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLE1BQU07QUFDVjs7QUFFQTtJQUNJLFNBQVM7QUFDYjs7QUFFQSx3Q0FBd0M7O0FBQ3hDO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGNBQWM7SUFDZCxtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGVBQWU7SUFDZixjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGFBQWE7QUFDakI7O0FBRUEsb0JBQW9COztBQUNwQjtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQSx1Q0FBdUM7O0FBQ3ZDO0lBQ0k7UUFDSSxrQkFBa0I7SUFDdEI7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDQTtRQUNJLGtCQUFrQjtJQUN0QjtJQUNBO1FBQ0ksaUJBQWlCO0lBQ3JCO0lBQ0E7UUFDSSxxQkFBcUI7SUFDekI7SUFDQTtRQUNJLGdCQUFnQjtRQUNoQixpQkFBaUI7UUFDakIsZUFBZTtJQUNuQjtJQUNBO1FBRUksb0JBQW9CO1FBQ3BCLHFCQUFxQjtJQUN6QjtJQUNBO1FBRUksb0JBQW9CO1FBQ3BCLHFCQUFxQjtJQUN6QjtBQUNKOztBQUNBO0lBQ0k7UUFDSSxrQkFBa0I7SUFDdEI7O0lBRUE7UUFDSSxlQUFlO1FBQ2YsaUJBQWlCO1FBQ2pCLGlCQUFpQjtJQUNyQjs7SUFFQTtRQUNJLHFCQUFxQjtJQUN6Qjs7SUFFQTtRQUNJLGVBQWU7UUFDZixxQkFBcUI7SUFDekI7O0lBRUE7UUFDSSxlQUFlO0lBQ25CO0lBQ0E7UUFDSSxnQkFBZ0I7SUFDcEI7SUFDQTtRQUNJLG9CQUFvQjtRQUNwQixlQUFlO0lBQ25CO0lBQ0E7UUFDSSxrQkFBa0I7SUFDdEI7SUFDQTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDQTtRQUNJLFdBQVc7UUFDWCxTQUFTO0lBQ2I7SUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO0lBQ2pCO0lBQ0E7UUFDSSxrQkFBa0I7UUFDbEIsc0JBQXNCO0lBQzFCO0lBQ0E7UUFDSSxrQkFBa0I7SUFDdEI7SUFDQTtRQUNJLFNBQVM7SUFDYjtBQUNKOztBQUNBO0lBQ0k7UUFDSSxXQUFXO1FBQ1gsaUJBQWlCO0lBQ3JCO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wYXJlbnQvZGV0YWlsLXBhZ2UvZGV0YWlsLXBhZ2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9ncmFtX291dGVyIHtcbiAgICBwYWRkaW5nOiA1MHB4IDk1cHggMzBweCA5NXB4O1xufVxuXG4ucHJvZ3JhbV9pbWFnZXMge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5wcm9ncmFtX2ltYWdlcyAucHJvZ3JhbV9ib3gge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1heC13aWR0aDogMzUwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5wcm9ncmFtX2ltYWdlcyAucHJvZ3JhbV9ib3g6bGFzdC1jaGlsZCB7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xufVxuXG4ucHJvZ3JhbV9pbWFnZXMgLnByb2dyYW1fYm94IGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xufVxuXG4ucHJvZ3JhbV90ZXh0IHtcbiAgICBwYWRkaW5nOiAzNXB4IDAgMzFweCAwO1xufVxuXG4ucHJvZ3JhbV90ZXh0IC5DYXRlZ29yeVRleHQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0IC5lZGl0X2ljb24ge1xuICAgIHJpZ2h0OiAzNyU7XG59XG5cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCBoNSB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgY29sb3I6ICM3NzdDRUE7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0IC5zaGFyZUljb24ge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0IC5zaGFyZUljb24gYSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCAwIDAgMTFweDtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogc3VwZXI7XG59XG5cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCAubGVhcm5fY29kZSB7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgcmlnaHQ6IDA7XG59XG5cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCAubGVhcm5fY29kZSBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCBoMyB7XG4gICAgZm9udC1zaXplOiAzNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzN3B4O1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBtYXJnaW46IDJweCAwIDAgMDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4ucHJvZ3JhbV90ZXh0IC5DYXRlZ29yeVRleHQgaDMgLmVkaXRfbmFtZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAtMzZweDtcbiAgICB0b3A6IDA7XG59XG5cbi5zZXNzaW9uX2RldGFpbHMge1xuICAgIG1hcmdpbi10b3A6IDNweDtcbn1cblxuLnNlc3Npb25fZGV0YWlscyBwIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCAyOHB4IDAgMDtcbn1cblxuLnNlc3Npb25fZGV0YWlscyBwIGltZyB7XG4gICAgbWFyZ2luLXJpZ2h0OiA2cHg7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxuLmluc3VyZWRfb3V0ZXIge1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIHBhZGRpbmc6IDEzcHggMDtcbiAgICBtYXJnaW4tdG9wOiAyOXB4O1xuICAgIG1heC13aWR0aDogNzQycHg7XG59XG5cbi5pbnN1cmVkX291dGVyIHAge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiAwIDM1cHggMCAwO1xufVxuXG4uaW5zdXJlZF9vdXRlciBwIGltZyB7XG4gICAgbWF4LXdpZHRoOiAyN3B4O1xuICAgIG1hcmdpbi1yaWdodDogMTJweDtcbn1cblxuLmJhdGNoX291dGVyIC5iYXRjaF9sZWZ0IHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgcGFkZGluZzogMzBweCAzNXB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmJhdGNoX291dGVyIC5iYXRjaF9sZWZ0IGgyIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwNWVtO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uYmF0Y2hfYm94IHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBwYWRkaW5nOiAxNnB4IDI4cHg7XG4gICAgbWFyZ2luLXRvcDogMTFweDtcbn1cblxuLmJhdGNoX2JveCAuYmF0Y2hfdGl0bGUgcCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5saXN0aW5nX291dGVyIHtcbiAgICBtYXJnaW4tdG9wOiAyOXB4O1xufVxuXG4uYmF0Y2hfb3V0ZXIgLmJhdGNoX3JpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgcGFkZGluZzogMjFweDtcbiAgICBtYXJnaW4tYm90dG9tOiAyMXB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodDpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQuYm9yZGVyX25vbmUge1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMzJweDtcbn1cblxuLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodCBoMiB7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xuICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDVlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xufVxuXG4uYmF0Y2hfb3V0ZXIgLmJhdGNoX3JpZ2h0IFAge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiAyNnB4IDAgMCAwO1xufVxuXG4uYmF0Y2hfb3V0ZXIgLmJhdGNoX3JpZ2h0IGEuYnVzaW5lc3NfYnRuIHtcbiAgICBtaW4taGVpZ2h0OiA1MnB4O1xuICAgIGxpbmUtaGVpZ2h0OiA1MnB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YxNWMyMDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDA1ZW07XG4gICAgcGFkZGluZzogMCAxMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXRvcDogMzJweDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4uYmF0Y2hfb3V0ZXIgLmJhdGNoX3JpZ2h0IGEuYnVzaW5lc3NfYnRuOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjYzc0MDA5O1xufVxuXG4uYmF0Y2hfbGlzdF9vdXRlciB7XG4gICAgZGlzcGxheTogZmxleDtcbn1cblxuLmJhdGNoX2xpc3Rfb3V0ZXIgLmJhdGNoX2xpc3Rpbmcge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDAgMTVweCAwIDA7XG59XG5cbi5iYXRjaF9saXN0X291dGVyIC5iYXRjaF9saXN0aW5nIGxhYmVsIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMWVtO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5iYXRjaF9saXN0X291dGVyIC5iYXRjaF9saXN0aW5nIC5iYXRjaF9wcm9maWxlIHtcbiAgICB3aWR0aDogMjJweDtcbiAgICBoZWlnaHQ6IDIycHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tcmlnaHQ6IDE0cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG4uYmF0Y2hfbGlzdF9vdXRlciAuYmF0Y2hfbGlzdGluZyAuYmF0Y2hfcHJvZmlsZSBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5iYXRjaF9saXN0X291dGVyIC5iYXRjaF9saXN0aW5nIHAge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLmRlc2NyaXB0aW9uX3RleHQge1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBwYWRkaW5nOiAzMHB4IDM1cHg7XG4gICAgbWFyZ2luLXRvcDogMjFweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5lZGl0X2ljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMzBweDtcbiAgICB0b3A6IDI0cHg7XG59XG5cbi5kZXNjcmlwdGlvbl90ZXh0IGgyIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwNWVtO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uZGVzY3JpcHRpb25fdGV4dCBwIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luLXRvcDogMjJweDtcbn1cblxuLmRlc2NyaXB0aW9uX3RleHQgbGFiZWwge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NzdDRUE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrXG59XG5cbi5kZXNjcmlwdGlvbl90ZXh0IGgyIC52aWV3X2FsbCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDFlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJztcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbi5yZXZpZXdzX291dGVyIHtcbiAgICBtYXJnaW4tdG9wOiAzNnB4O1xufVxuXG4ucmV2aWV3c19vdXRlciAubWVkaWEge1xuICAgIG1hcmdpbjogMCAwIDE1cHggMDtcbn1cblxuLnJldmlld3Nfb3V0ZXIgLm1lZGlhIC5tZWRpYS1sZWZ0IHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5yZXZpZXdzX291dGVyIC5tZWRpYSAubWVkaWEtbGVmdCBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5yZXZpZXdzX291dGVyIC5tZWRpYSAubWVkaWEtYm9keSB7XG4gICAgcGFkZGluZzogMCAwIDAgMTJweDtcbn1cblxuLnJldmlld3Nfb3V0ZXIgLm1lZGlhIC5tZWRpYS1ib2R5IGxhYmVsIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLnJldmlld3Nfb3V0ZXIgLm1lZGlhIC5tZWRpYS1ib2R5IHAge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5tZW51X2NvbnRyb2wge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vKiBzdGFydCBwcm9ncmFtIGRldGFpbDIgcGFnZSBjc3MgKi9cbi5iYXRjaF9vdXRlci5kZXRhaWxlZDIge1xuICAgIHBhZGRpbmc6IDMwcHggMCAwIDA7XG59XG5cbi5iYXRjaF9vdXRlci5kZXRhaWxlZDIgLnByb2dyYW1fdGV4dCB7XG4gICAgcGFkZGluZzogMCAwIDMxcHggMDtcbn1cblxuLmxlYXJuX2NvZGUubGVmdCBpbWcge1xuICAgIG1heC13aWR0aDogMTAwcHg7XG59XG5cbi5kZXNjcmlwdGlvbl90ZXh0IC5kZXNfbWVkaWEge1xuICAgIG1hcmdpbjogMDtcbn1cblxuLmRlc2NyaXB0aW9uX3RleHQgLmRlc19tZWRpYTpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5kZXNjcmlwdGlvbl90ZXh0IC5kZXNfbWVkaWEgLm1lZGlhIHtcbiAgICBtYXJnaW4tdG9wOiAxN3B4O1xufVxuXG4uZGVzY3JpcHRpb25fdGV4dCAuZGVzX21lZGlhIC5tZWRpYS1ib2R5IHtcbiAgICBwYWRkaW5nOiAwIDAgMCAyMHB4O1xufVxuXG4uZGVzY3JpcHRpb25fdGV4dCAuZGVzX21lZGlhIC5tZWRpYS1ib2R5IHAge1xuICAgIG1hcmdpbjogMDtcbn1cblxuLnN1YnNjcmlwdGlvbl90ZXh0IHtcbiAgICBtYXJnaW4tdG9wOiA1NnB4O1xufVxuXG4uc3Vic2NyaXB0aW9uX3RleHQgLnN1YnNjcmlwdGlvbl9ib3gge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIG1hcmdpbjogMCAxNjFweCAwIDA7XG59XG5cbi5zdWJzY3JpcHRpb25fdGV4dCAuc3Vic2NyaXB0aW9uX2JveDpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5zb2NpYWxfbWVkaWEge1xuICAgIG1hcmdpbjogOXB4IDAgMCAwO1xufVxuXG4uc29jaWFsX21lZGlhIGEgLmljb24ge1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbjogMCA2cHggMCAwO1xuICAgIGZvbnQtc2l6ZTogMTlweDtcbn1cblxuLmZlYXR1cmVkX3RleHQgLnJhdGluZ19ib3gge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDEzcHg7XG4gICAgdG9wOiBhdXRvO1xufVxuXG4uZmVhdHVyZWRfdGV4dCAuZmVhdHVyZV9ib3ggcCB7XG4gICAgbWFyZ2luOiAwIDE2cHggMCAwO1xufVxuXG4uZmVhdHVyZWRfdGV4dCBwIHtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmZlYXR1cmVkX2ltZyAuY29kaW5nX3RleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDlweDtcbiAgICBsZWZ0OiA5cHg7XG59XG5cbi5mZWF0dXJlZF9pbWcgLmNvZGluZ190ZXh0IGgzIHtcbiAgICBtaW4td2lkdGg6IDc1cHg7XG4gICAgbWluLWhlaWdodDogMjFweDtcbiAgICBsaW5lLWhlaWdodDogMjFweDtcbiAgICBiYWNrZ3JvdW5kOiAjRjE1QzIwO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwLjVweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAxZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5mZWF0dXJlZF90ZXh0IC5mZWF0dXJlZF9idG4ge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMTVDMjA7XG4gICAgY29sb3I6ICNGMTVDMjA7XG59XG5cbi5mZWF0dXJlZF90ZXh0IC5mZWF0dXJlX2JveCB7XG4gICAgbWFyZ2luOiA1cHggMCAxMnB4IDA7XG59XG5cbi8qIHByb2dyYW0gZWRpdCBwYWdlIGNzcyAqL1xuXG4uYmF0Y2hfb3V0ZXIgLmJhdGNoX3JpZ2h0LmFjdGl2ZV9lZGl0IHtcbiAgICBwYWRkaW5nOiAzMHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQuYWN0aXZlX2VkaXQgaDIge1xuICAgIG1hcmdpbjogMDtcbn1cblxuLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodCAucHJpY2VfY2hpbGQge1xuICAgIG1hcmdpbi10b3A6IDhweDtcbn1cblxuLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodCAucHJpY2VfY2hpbGQgaDMge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQgLnByaWNlX2NoaWxkIHAge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDA7XG59XG5cbi5iYXRjaF9yaWdodF9mb3JtIHtcbiAgICBtYXJnaW4tdG9wOiAyNHB4O1xufVxuXG4uYmF0Y2hfcmlnaHRfZm9ybSAuZm9ybS1ncm91cCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuXG4uYmF0Y2hfcmlnaHRfZm9ybSAuYWR1bHRfYXNzaXN0YW5jZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmJhdGNoX3JpZ2h0X2Zvcm0gLmZvcm0tZ3JvdXAuZGF0ZV90aW1lIC5pbnB1dF9vdXRlciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgbWluLWhlaWdodDogNTJweDtcbn1cblxuLmJhdGNoX3JpZ2h0X2Zvcm0gLmZvcm0tZ3JvdXAuZGF0ZV90aW1lIC5pbnB1dF9vdXRlciBpbnB1dCB7XG4gICAgbWF4LXdpZHRoOiA0NSU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLmJhdGNoX3JpZ2h0X2Zvcm0gLmZvcm0tZ3JvdXAuZGF0ZV90aW1lIC5pbnB1dF9vdXRlciBpbnB1dC5yaWdodF9pbnB1dCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYmF0Y2hfcmlnaHRfZm9ybSAuZm9ybS1ncm91cC5kYXRlX3RpbWUgLmlucHV0X291dGVyIC5hcnJvd19pbWcge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLmJhdGNoX3JpZ2h0X2Zvcm0gLmZvcm0tZ3JvdXAuZGF0ZV90aW1lIC5pbnB1dF9vdXRlciBpbnB1dDo6LXdlYmtpdC1jYWxlbmRhci1waWNrZXItaW5kaWNhdG9yIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbn1cblxuLmJhdGNoX3JpZ2h0X2Zvcm0gLmZvcm0tZ3JvdXAgLnVkcGF0ZV9idG4ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1heC13aWR0aDogMzUycHg7XG4gICAgYmFja2dyb3VuZDogI0YxNUMyMDtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICAgIG1pbi1oZWlnaHQ6IDUycHg7XG4gICAgbGluZS1oZWlnaHQ6IDUycHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4uYmF0Y2hfcmlnaHRfZm9ybSAuZm9ybS1ncm91cCAudWRwYXRlX2J0bjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2M3NDAwOTtcbn1cblxuLmVkaXRfaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAzMHB4O1xuICAgIHRvcDogMjRweDtcbn1cblxuLmRlc2NyaXB0aW9uX3RleHQgLm1hcF9pbWcge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIHBhZGRpbmc6IDI0cHg7XG59XG5cbi5kZXNjcmlwdGlvbl90ZXh0IC5kZXNfbWVkaWEgLm1lZGlhLWJvZHkuc29jaWFsIHtcbiAgICBwYWRkaW5nOiAwIDAgMCA0MHB4O1xufVxuXG4uY29tcGxldGlvbl9zY29yZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwO1xuICAgIHRvcDogMDtcbn1cblxuLmNvbXBsZXRpb25fc2NvcmUgcCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDFlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuLmNvbXBsZXRpb25fc2NvcmUgYSB7XG4gICAgbWFyZ2luOiAwIDZweCAwIDA7XG59XG5cbi5jb21wbGV0aW9uX3Njb3JlIC5wcm9maWxlX3Njb3JlIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5kZXNjcmlwdGlvbl9tZWRpYSAuZWRpdF9pY29uIHtcbiAgICB0b3A6IDA7XG59XG5cbi5jb21wbGV0aW9uX3Njb3JlLmxvZ291dCB7XG4gICAgdG9wOiAyMHB4O1xufVxuXG4vKiAtLS0tLS0tLS0tLS1zdHlsZS5jc3MtLS0tLS0tLS0tLS0tLSAqL1xuLmZvcm0tZ3JvdXAgbGFiZWwge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NzdDRUE7XG4gICAgcGFkZGluZzogMCAwIDAgMTVweDtcbn1cblxuLmZvcm0tZ3JvdXAgaW5wdXQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIG1pbi1oZWlnaHQ6IDUycHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xuICAgIHBhZGRpbmc6IDE0cHg7XG59XG5cbi8qIFJvdW5kZWQgc2xpZGVycyAqL1xuLnN3aXRjaC5ub3RpZmljYXRpb24gLnNsaWRlci5yb3VuZCB7XG4gICAgYm9yZGVyLXJhZGl1czogMzRweDtcbn1cblxuLnN3aXRjaC5ub3RpZmljYXRpb24gLnNsaWRlci5yb3VuZDpiZWZvcmUge1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAqL1xuQG1lZGlhKG1heC13aWR0aDo5OTFweCkge1xuICAgIC5wcm9ncmFtX291dGVye1xuICAgICAgICBwYWRkaW5nOiAzNHB4IDE1cHg7XG4gICAgfVxuICAgIC5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCBoMyB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICB9XG4gICAgLmJhdGNoX291dGVyIC5iYXRjaF9sZWZ0IHtcbiAgICAgICAgcGFkZGluZzogMzBweCAxN3B4O1xuICAgIH1cbiAgICAuYmF0Y2hfb3V0ZXIgLmJhdGNoX3JpZ2h0LmFjdGl2ZV9lZGl0IHtcbiAgICAgICAgcGFkZGluZzozMHB4IDE1cHg7XG4gICAgfVxuICAgIC5iYXRjaF9saXN0X291dGVye1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgfVxuICAgIC5iYXRjaF9yaWdodF9mb3JtIC5mb3JtLWdyb3VwIC51ZHBhdGVfYnRuIHtcbiAgICAgICAgbWluLWhlaWdodDogNDJweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQycHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB9XG4gICAgLmRldGFpbF9wYWdlIC5jb2wtbWQtOCB7XG4gICAgICAgIC1tcy1mbGV4OiAwIDAgNTkuNjY2NjY3JTtcbiAgICAgICAgZmxleDogMCAwIDU5LjY2NjY2NyU7XG4gICAgICAgIG1heC13aWR0aDogNTkuNjY2NjY3JTtcbiAgICB9XG4gICAgLmRldGFpbF9wYWdlIC5jb2wtbWQtNCB7XG4gICAgICAgIC1tcy1mbGV4OiAwIDAgMzkuMzMzMzMzJTtcbiAgICAgICAgZmxleDogMCAwIDM5LjMzMzMzMyU7XG4gICAgICAgIG1heC13aWR0aDogMzkuMzMzMzMzJTtcbiAgICB9XG59XG5AbWVkaWEobWF4LXdpZHRoOjc2N3B4KSB7XG4gICAgLnByb2dyYW1fb3V0ZXIge1xuICAgICAgICBwYWRkaW5nOiAzNXB4IDEwcHg7XG4gICAgfVxuXG4gICAgLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0IGgzIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMjZweDtcbiAgICAgICAgbWFyZ2luOiAwcHggMCAwIDA7XG4gICAgfVxuXG4gICAgLmJhdGNoX2xpc3Rfb3V0ZXIge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgfVxuXG4gICAgLmluc3VyZWRfb3V0ZXIgcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgbWFyZ2luOiAwIDExcHggMTBweCAwO1xuICAgIH1cblxuICAgIC5pbnN1cmVkX291dGVyIHAgaW1nIHtcbiAgICAgICAgbWF4LXdpZHRoOiAyN3B4O1xuICAgIH1cbiAgICAuc2Vzc2lvbl9kZXRhaWxze1xuICAgICAgICBtYXJnaW4tdG9wOiAxNHB4O1xuICAgIH1cbiAgICAuc2Vzc2lvbl9kZXRhaWxzIHB7XG4gICAgICAgIG1hcmdpbjogMCAxN3B4IDdweCAwO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgfVxuICAgIC5iYXRjaF9vdXRlciAuYmF0Y2hfbGVmdHtcbiAgICAgICAgcGFkZGluZzogMzBweCAxMnB4O1xuICAgIH1cbiAgICAuYmF0Y2hfYm94e1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgIH1cbiAgICAuZGVzY3JpcHRpb25fdGV4dHtcbiAgICAgICAgcGFkZGluZzogMTZweDsgXG4gICAgfVxuICAgIC5kZXNjcmlwdGlvbl90ZXh0IGgyIHtcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgIH1cbiAgICAuZWRpdF9pY29uIHtcbiAgICAgICAgcmlnaHQ6IDE3cHg7XG4gICAgICAgIHRvcDogMTdweDtcbiAgICB9XG4gICAgLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodC5hY3RpdmVfZWRpdCB7XG4gICAgICAgIG1hcmdpbi10b3A6IDI0cHg7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgfVxuICAgIC5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCAuc2hhcmVJY29uIHtcbiAgICAgICAgbWFyZ2luOiAwIDAgMCAxMnB4O1xuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgIH1cbiAgICAucHJvZ3JhbV90ZXh0IC5DYXRlZ29yeVRleHQgLnNoYXJlSWNvbiBhIHtcbiAgICAgICAgbWFyZ2luOiAwIDEycHggMCAwO1xuICAgIH1cbiAgICAucHJvZ3JhbV90ZXh0IC5DYXRlZ29yeVRleHQgLnNoYXJlSWNvbiBhOmxhc3QtY2hpbGR7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG59XG5AbWVkaWEobWF4LXdpZHRoOjQ4MHB4KSB7XG4gICAgLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0IC5zaGFyZUljb24ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWFyZ2luOiA1cHggMCAwIDA7XG4gICAgfVxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/parent/detail-page/detail-page.component.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/pages/parent/detail-page/detail-page.component.ts ***!
    \*******************************************************************/

  /*! exports provided: DetailPageComponent */

  /***/
  function srcAppPagesParentDetailPageDetailPageComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DetailPageComponent", function () {
      return DetailPageComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var DetailPageComponent = /*#__PURE__*/function () {
      function DetailPageComponent() {
        _classCallCheck(this, DetailPageComponent);
      }

      _createClass(DetailPageComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return DetailPageComponent;
    }();

    DetailPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'detail-page',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./detail-page.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/detail-page/detail-page.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./detail-page.component.css */
      "./src/app/pages/parent/detail-page/detail-page.component.css"))["default"]]
    })], DetailPageComponent);
    /***/
  },

  /***/
  "./src/app/pages/parent/login-parent/login-parent.component.css":
  /*!**********************************************************************!*\
    !*** ./src/app/pages/parent/login-parent/login-parent.component.css ***!
    \**********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesParentLoginParentLoginParentComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".step_header{\n    margin-top: 50px;\n}\n.step_outer{\n    margin-top: 80px;\n    margin-bottom: 30px;\n}\n.step_outer h2{\n    font-family: 'Patrick Hand';\n    font-size: 32px;\n    line-height: 43px;\n    color: #748494;\n}\n.step_outer h3{\n    font-family: 'Patrick Hand';\n    font-size: 26px;\n    line-height: 35px;\n    color: #777CEA;\n    margin: 2px 0 0 0;\n}\n.step_outer p{\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: -0.01em;\n    color: #748494;\n}\n.location_input{\n    max-width: 470px;\n    display: inline-block;\n    margin: 85px 0 0 0;\n    width: 100%;\n}\n.location_input label{\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    text-transform: uppercase;\n    color: #777CEA;\n}\n.location_input .form-control{\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 4px;\n    min-height: 50px;\n    padding: 0 16px;\n    font-size: 13px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n.location_input .form-group .mat-form-field{\n    /* display: inline-block; */\n    /* width: 100%; */\n    /* position: relative; */\n    /* border: 1px solid #E5E9EC;\n    border-radius: 4px;\n    min-height: 50px; */\n    /* padding: 0 16px; */\n    /* font-size: 13px; */\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n.location_input .form-group .mat-form-field::after{\n    content: '';\n    background-color: #f9fafa;\n    height: 3px;\n    width: 100%;\n    position: absolute;\n    bottom: 19px;\n    left: 0;\n}\n.location_input .mat-standard-chip.mat-chip-with-trailing-icon{\n    background: #F15C20;\n    border-radius: 40px;\n    min-width: 119px;\n    line-height: 30px;\n    min-height: 30px;\n    font-size: 12px;\n    display: inline-block;\n    text-align: left;\n    color: #FFFFFF;\n    margin-right: 2px;\n    transition: 0.8s;\n    margin: 0 0 11px 11px;\n    padding: 0 15px;\n}\n.location_input .mat-standard-chip .mat-chip-remove.mat-icon{\nfont-size: 0;\nopacity: 1;\n}\n.location_input .mat-standard-chip .mat-chip-remove.mat-icon::after{\n    content: \"\\e900\";\n    font-family: 'icomoon' !important;\n    position: absolute;\n    right: 9px;\n    top: 7px;\n    font-size: 14px;\n    color:#fff;\n}\n.location_input .mat-form-field-appearance-legacy .mat-form-field-wrapper{\npadding-bottom: 0;\n}\n.input_outer .mat-form-field-wrapper {\n    padding-bottom: 16px;\n}\n.btns_next .button{\nmin-width: 223px;\n min-height: 55px;\n line-height: 55px;\n border: 1px solid #F15C20;\n border-radius: 4px;\n display: inline-block;\n text-align: center;\n font-weight: 600;\n font-size: 13px;\n transition: 0.8s;\n margin: 0 17px 0 0;\n}\n.btns_next .button:last-child{\n    margin:0;\n}\n.btns_next .button.next{\n background-color: #F15C20;\n color: #fff;\n}\n.btns_next .button.skip{\n    background-color: #FFFFFF;\n    color: #998c87;\n}\n.btns_next .button.skip:hover{\n    background-color: #F15C20;\n    color: #fff;\n}\n.step_outer .btns_next {\n    margin-top: 142px;\n}\n.step_footer{\n    margin-top: 90px;\n}\n.step_footer p{\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: -0.01em;\n    color: #748494;\n}\n.location_input .form-group .add_another{\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n.location_input .form-group .add_another .icon{\n  color: #777CEA;\n  margin: 0 12px 0 0;\n  font-size: 18px;\n  margin-left: 15px;\n  vertical-align: middle;\n}\n.location_input.interest{\n max-width: 590px;\n}\n.kids_interest .ch-radio{\n    overflow:hidden;\n    \n}\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n    height: 0px !important; \n}\n.kids_interest .ch-radio span ,a{\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    font-weight: 600;\n    background: #FFFFFF;\n    border: 1px solid #E5E9EC;\n    border-radius: 20px;\n    display: inline-block;\n    padding: 0 15px;\n    min-height: 28px;\n    line-height: 28px;\n    text-align: center;\n    transition: 0.8s;\n    margin: 0 4px 6px 0;\n    text-transform: capitalize;\n}\n.kids_interest .ch-radio span:before{\n content: none;\n}\n.ch-radio input[type=\"checkbox\"]:checked + span,\n.ch-radio input[type=\"checkbox\"]:checked ~ span,\n.ch-radio input[type=\"radio\"]:checked + span,\n.ch-radio input[type=\"radio\"]:checked ~ span{\n    border: 1px solid #F15C20;\n    color: #F15C20;\n}\n.kids_interest a:hover,\n.kids_interest a.selected\n{\n    border: 1px solid #F15C20;\n    color: #F15C20;\n}\n.kids_interest a.selected.interest{\n    background-color: #F15C20;\n    color: #FFFFFF;\n}\n.kids_interest .interest_sub{\n    margin: 0 0 31px 0;\n}\n.kids_interest .interest_sub:last-child{\n    margin:0;\n}\n.sub_inner {\n    margin: 8px 0 0 0;\n}\n/* very important to hide mat-step header */\n::ng-deep .mat-horizontal-stepper-header-container {\n    display: none !important;\n  }\n:host ::ng-deep .mat-horizontal-content-container {\n    padding:0 !important;\n    background: #f9fafa;\n}\n/* VALIDATION ERROR  */\n.form-error-msg {\n    color: red;\n}\ninput.ng-invalid.ng-touched {\n    border: 1px solid red;\n}\nselect.ng-invalid.ng-touched {\n    border: 1px solid red;\n}\n.autocomplete-container{\n    box-shadow:none;\n}\n.autocomplete-container .input-container input {\n    border: 1px solid #E5E9EC;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFyZW50L2xvZ2luLXBhcmVudC9sb2dpbi1wYXJlbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksMkJBQTJCO0lBQzNCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztBQUNsQjtBQUNBO0lBQ0ksMkJBQTJCO0lBQzNCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIsV0FBVztBQUNmO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLGNBQWM7QUFDbEI7QUFDQTtJQUNJLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSwyQkFBMkI7SUFDM0IsaUJBQWlCO0lBQ2pCLHdCQUF3QjtJQUN4Qjs7dUJBRW1CO0lBQ25CLHFCQUFxQjtJQUNyQixxQkFBcUI7SUFDckIsdUJBQXVCO0lBQ3ZCLGNBQWM7QUFDbEI7QUFDQTtJQUNJLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIsV0FBVztJQUNYLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE9BQU87QUFDWDtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixxQkFBcUI7SUFDckIsZUFBZTtBQUNuQjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFVBQVU7QUFDVjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGlDQUFpQztJQUNqQyxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFFBQVE7SUFDUixlQUFlO0lBQ2YsVUFBVTtBQUNkO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFFQTtJQUNJLG9CQUFvQjtBQUN4QjtBQUVBO0FBQ0EsZ0JBQWdCO0NBQ2YsZ0JBQWdCO0NBQ2hCLGlCQUFpQjtDQUNqQix5QkFBeUI7Q0FDekIsa0JBQWtCO0NBQ2xCLHFCQUFxQjtDQUNyQixrQkFBa0I7Q0FDbEIsZ0JBQWdCO0NBQ2hCLGVBQWU7Q0FDZixnQkFBZ0I7Q0FDaEIsa0JBQWtCO0FBQ25CO0FBQ0E7SUFDSSxRQUFRO0FBQ1o7QUFDQTtDQUNDLHlCQUF5QjtDQUN6QixXQUFXO0FBQ1o7QUFDQTtJQUNJLHlCQUF5QjtJQUN6QixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsV0FBVztBQUNmO0FBQ0E7SUFDSSxpQkFBaUI7QUFDckI7QUFDQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7QUFDbEI7QUFDQTtFQUNFLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixzQkFBc0I7QUFDeEI7QUFDQTtDQUNDLGdCQUFnQjtBQUNqQjtBQUNBO0lBQ0ksZUFBZTs7QUFFbkI7QUFDQTtJQUNJLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsMEJBQTBCO0FBQzlCO0FBQ0E7Q0FDQyxhQUFhO0FBQ2Q7QUFDQTs7OztJQUlJLHlCQUF5QjtJQUN6QixjQUFjO0FBQ2xCO0FBRUE7OztJQUdJLHlCQUF5QjtJQUN6QixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsY0FBYztBQUNsQjtBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxRQUFRO0FBQ1o7QUFDQTtJQUNJLGlCQUFpQjtBQUNyQjtBQUVBLDJDQUEyQztBQUUzQztJQUNJLHdCQUF3QjtFQUMxQjtBQUNBO0lBQ0Usb0JBQW9CO0lBQ3BCLG1CQUFtQjtBQUN2QjtBQUdBLHNCQUFzQjtBQUN0QjtJQUNJLFVBQVU7QUFDZDtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxxQkFBcUI7QUFDekI7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLHlCQUF5QjtBQUM3QiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BhcmVudC9sb2dpbi1wYXJlbnQvbG9naW4tcGFyZW50LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RlcF9oZWFkZXJ7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbn1cbi5zdGVwX291dGVye1xuICAgIG1hcmdpbi10b3A6IDgwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cbi5zdGVwX291dGVyIGgye1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBmb250LXNpemU6IDMycHg7XG4gICAgbGluZS1oZWlnaHQ6IDQzcHg7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG4uc3RlcF9vdXRlciBoM3tcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xuICAgIG1hcmdpbjogMnB4IDAgMCAwO1xufVxuLnN0ZXBfb3V0ZXIgcHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG4ubG9jYXRpb25faW5wdXR7XG4gICAgbWF4LXdpZHRoOiA0NzBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luOiA4NXB4IDAgMCAwO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmxvY2F0aW9uX2lucHV0IGxhYmVse1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAxZW07XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cbi5sb2NhdGlvbl9pbnB1dCAuZm9ybS1jb250cm9se1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgbWluLWhlaWdodDogNTBweDtcbiAgICBwYWRkaW5nOiAwIDE2cHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuLmxvY2F0aW9uX2lucHV0IC5mb3JtLWdyb3VwIC5tYXQtZm9ybS1maWVsZHtcbiAgICAvKiBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7ICovXG4gICAgLyogd2lkdGg6IDEwMCU7ICovXG4gICAgLyogcG9zaXRpb246IHJlbGF0aXZlOyAqL1xuICAgIC8qIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIG1pbi1oZWlnaHQ6IDUwcHg7ICovXG4gICAgLyogcGFkZGluZzogMCAxNnB4OyAqL1xuICAgIC8qIGZvbnQtc2l6ZTogMTNweDsgKi9cbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cbi5sb2NhdGlvbl9pbnB1dCAuZm9ybS1ncm91cCAubWF0LWZvcm0tZmllbGQ6OmFmdGVye1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWZhZmE7XG4gICAgaGVpZ2h0OiAzcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMTlweDtcbiAgICBsZWZ0OiAwO1xufVxuLmxvY2F0aW9uX2lucHV0IC5tYXQtc3RhbmRhcmQtY2hpcC5tYXQtY2hpcC13aXRoLXRyYWlsaW5nLWljb257XG4gICAgYmFja2dyb3VuZDogI0YxNUMyMDtcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICAgIG1pbi13aWR0aDogMTE5cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgbWluLWhlaWdodDogMzBweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gICAgdHJhbnNpdGlvbjogMC44cztcbiAgICBtYXJnaW46IDAgMCAxMXB4IDExcHg7XG4gICAgcGFkZGluZzogMCAxNXB4O1xufVxuLmxvY2F0aW9uX2lucHV0IC5tYXQtc3RhbmRhcmQtY2hpcCAubWF0LWNoaXAtcmVtb3ZlLm1hdC1pY29ue1xuZm9udC1zaXplOiAwO1xub3BhY2l0eTogMTtcbn1cbi5sb2NhdGlvbl9pbnB1dCAubWF0LXN0YW5kYXJkLWNoaXAgLm1hdC1jaGlwLXJlbW92ZS5tYXQtaWNvbjo6YWZ0ZXJ7XG4gICAgY29udGVudDogXCJcXGU5MDBcIjtcbiAgICBmb250LWZhbWlseTogJ2ljb21vb24nICFpbXBvcnRhbnQ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiA5cHg7XG4gICAgdG9wOiA3cHg7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiNmZmY7XG59XG4ubG9jYXRpb25faW5wdXQgLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtbGVnYWN5IC5tYXQtZm9ybS1maWVsZC13cmFwcGVye1xucGFkZGluZy1ib3R0b206IDA7XG59XG5cbi5pbnB1dF9vdXRlciAubWF0LWZvcm0tZmllbGQtd3JhcHBlciB7XG4gICAgcGFkZGluZy1ib3R0b206IDE2cHg7XG59XG5cbi5idG5zX25leHQgLmJ1dHRvbntcbm1pbi13aWR0aDogMjIzcHg7XG4gbWluLWhlaWdodDogNTVweDtcbiBsaW5lLWhlaWdodDogNTVweDtcbiBib3JkZXI6IDFweCBzb2xpZCAjRjE1QzIwO1xuIGJvcmRlci1yYWRpdXM6IDRweDtcbiBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gdGV4dC1hbGlnbjogY2VudGVyO1xuIGZvbnQtd2VpZ2h0OiA2MDA7XG4gZm9udC1zaXplOiAxM3B4O1xuIHRyYW5zaXRpb246IDAuOHM7XG4gbWFyZ2luOiAwIDE3cHggMCAwO1xufVxuLmJ0bnNfbmV4dCAuYnV0dG9uOmxhc3QtY2hpbGR7XG4gICAgbWFyZ2luOjA7XG59XG4uYnRuc19uZXh0IC5idXR0b24ubmV4dHtcbiBiYWNrZ3JvdW5kLWNvbG9yOiAjRjE1QzIwO1xuIGNvbG9yOiAjZmZmO1xufVxuLmJ0bnNfbmV4dCAuYnV0dG9uLnNraXB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgICBjb2xvcjogIzk5OGM4Nztcbn1cbi5idG5zX25leHQgLmJ1dHRvbi5za2lwOmhvdmVye1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMTVDMjA7XG4gICAgY29sb3I6ICNmZmY7XG59XG4uc3RlcF9vdXRlciAuYnRuc19uZXh0IHtcbiAgICBtYXJnaW4tdG9wOiAxNDJweDtcbn1cbi5zdGVwX2Zvb3RlcntcbiAgICBtYXJnaW4tdG9wOiA5MHB4O1xufVxuLnN0ZXBfZm9vdGVyIHB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4wMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuLmxvY2F0aW9uX2lucHV0IC5mb3JtLWdyb3VwIC5hZGRfYW5vdGhlcntcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cbi5sb2NhdGlvbl9pbnB1dCAuZm9ybS1ncm91cCAuYWRkX2Fub3RoZXIgLmljb257XG4gIGNvbG9yOiAjNzc3Q0VBO1xuICBtYXJnaW46IDAgMTJweCAwIDA7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG4ubG9jYXRpb25faW5wdXQuaW50ZXJlc3R7XG4gbWF4LXdpZHRoOiA1OTBweDtcbn1cbi5raWRzX2ludGVyZXN0IC5jaC1yYWRpb3tcbiAgICBvdmVyZmxvdzpoaWRkZW47XG4gICAgXG59XG4ubWF0LWZvcm0tZmllbGQtYXBwZWFyYW5jZS1sZWdhY3kgLm1hdC1mb3JtLWZpZWxkLXVuZGVybGluZSB7XG4gICAgaGVpZ2h0OiAwcHggIWltcG9ydGFudDsgXG59XG4ua2lkc19pbnRlcmVzdCAuY2gtcmFkaW8gc3BhbiAsYXtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcGFkZGluZzogMCAxNXB4O1xuICAgIG1pbi1oZWlnaHQ6IDI4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRyYW5zaXRpb246IDAuOHM7XG4gICAgbWFyZ2luOiAwIDRweCA2cHggMDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cbi5raWRzX2ludGVyZXN0IC5jaC1yYWRpbyBzcGFuOmJlZm9yZXtcbiBjb250ZW50OiBub25lO1xufVxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkICsgc3Bhbixcbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB+IHNwYW4sXG4uY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgKyBzcGFuLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkIH4gc3BhbntcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRjE1QzIwO1xuICAgIGNvbG9yOiAjRjE1QzIwO1xufVxuXG4ua2lkc19pbnRlcmVzdCBhOmhvdmVyLFxuLmtpZHNfaW50ZXJlc3QgYS5zZWxlY3RlZFxue1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMTVDMjA7XG4gICAgY29sb3I6ICNGMTVDMjA7XG59XG4ua2lkc19pbnRlcmVzdCBhLnNlbGVjdGVkLmludGVyZXN0e1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMTVDMjA7XG4gICAgY29sb3I6ICNGRkZGRkY7XG59XG4ua2lkc19pbnRlcmVzdCAuaW50ZXJlc3Rfc3Vie1xuICAgIG1hcmdpbjogMCAwIDMxcHggMDtcbn1cbi5raWRzX2ludGVyZXN0IC5pbnRlcmVzdF9zdWI6bGFzdC1jaGlsZHtcbiAgICBtYXJnaW46MDtcbn1cbi5zdWJfaW5uZXIge1xuICAgIG1hcmdpbjogOHB4IDAgMCAwO1xufVxuXG4vKiB2ZXJ5IGltcG9ydGFudCB0byBoaWRlIG1hdC1zdGVwIGhlYWRlciAqL1xuXG46Om5nLWRlZXAgLm1hdC1ob3Jpem9udGFsLXN0ZXBwZXItaGVhZGVyLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICB9XG4gIDpob3N0IDo6bmctZGVlcCAubWF0LWhvcml6b250YWwtY29udGVudC1jb250YWluZXIge1xuICAgIHBhZGRpbmc6MCAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQ6ICNmOWZhZmE7XG59XG5cblxuLyogVkFMSURBVElPTiBFUlJPUiAgKi9cbi5mb3JtLWVycm9yLW1zZyB7XG4gICAgY29sb3I6IHJlZDtcbn1cbmlucHV0Lm5nLWludmFsaWQubmctdG91Y2hlZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xufVxuc2VsZWN0Lm5nLWludmFsaWQubmctdG91Y2hlZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xufVxuLmF1dG9jb21wbGV0ZS1jb250YWluZXJ7XG4gICAgYm94LXNoYWRvdzpub25lO1xufVxuLmF1dG9jb21wbGV0ZS1jb250YWluZXIgLmlucHV0LWNvbnRhaW5lciBpbnB1dCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0U1RTlFQztcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/parent/login-parent/login-parent.component.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/pages/parent/login-parent/login-parent.component.ts ***!
    \*********************************************************************/

  /*! exports provided: LoginParentComponent */

  /***/
  function srcAppPagesParentLoginParentLoginParentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginParentComponent", function () {
      return LoginParentComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/core/services/api.service.service */
    "./src/app/core/services/api.service.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/cdk/keycodes */
    "./node_modules/@angular/cdk/esm2015/keycodes.js");

    var LoginParentComponent = /*#__PURE__*/function () {
      function LoginParentComponent(router, apiservice, ngxLoader, snack) {
        _classCallCheck(this, LoginParentComponent);

        this.router = router;
        this.apiservice = apiservice;
        this.ngxLoader = ngxLoader;
        this.snack = snack;
        this.getTagByCategoryResponse = {};
        this.userData = {}; // selected = '';

        this.keyword = 'name';
        this.addChildData = {
          name: '',
          sex: '',
          dob: '',
          age: '',
          schoolInfo: '',
          relationToChild: '',
          parentId: '',
          interestInfo: []
        };
        this.categoryIds = []; // ---------------autucomplete-------------  

        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = false;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__["COMMA"]];
        this.fruitCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.categories = [];
        this.SelectedCategories = [];
        this.message = 'child added Successfully';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        var retrievedObject = localStorage.getItem('userData');
        this.userData = JSON.parse(retrievedObject);
        var config = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSnackBarConfig"]();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0; // this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
        //   startWith(null),
        //   map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
      }

      _createClass(LoginParentComponent, [{
        key: "logo",
        value: function logo() {
          this.router.navigate(['/search']);
        }
      }, {
        key: "add",
        value: function add(event) {
          var input = event.input;
          var value = event.value; // Add our fruit

          if ((value || '').trim()) {
            this.addChildData.interestInfo.push( // value.trim()
            {
              _id: '',
              name: value.trim()
            });
            console.log('tagsssss', this.addChildData.interestInfo);
          } // Reset the input value


          if (input) {
            input.value = '';
          }

          this.fruitCtrl.setValue(null);
          console.log('tagsssss', this.addChildData.interestInfo);
        }
      }, {
        key: "remove",
        value: function remove(fruit, indx) {
          this.addChildData.interestInfo.splice(indx, 1);
          console.log('tagsssss', this.addChildData.interestInfo);
        }
      }, {
        key: "selected",
        value: function selected(event) {
          this.addChildData.interestInfo.push(event.option.value);
          this.fruitInput.nativeElement.value = '';
          this.fruitCtrl.setValue(null);
        } // private _filter(value: any): string[] {
        //   return this.allFruits.filter(fruit => fruit._id === value._id);
        // }

      }, {
        key: "openSearch",
        value: function openSearch() {
          this.router.navigate(['/search']);
          console.log('addchild data', this.addChildData);
        } // onIntrest(intrest, isChecked: boolean) {
        //   if (isChecked) {
        //     this.categoryIds.push(intrest._id);
        //     console.log('category id', this.categoryIds);
        //   } else {
        //     const index = this.categoryIds.indexOf(intrest._id);
        //     this.categoryIds.splice(index, 1);
        //     console.log(' categoryId', this.categoryIds);
        //   }
        // }
        // getTagByCategoryId() {
        //   var categoryID = JSON.stringify(this.categoryIds);
        //   console.log('stringify categoryIds Array before request', categoryID),
        //     this.apiservice.getTagByCategoryId(categoryID).subscribe(res => {
        //       this.getTagByCategoryResponse = res;
        //       console.log('gettag by cat id res>>', this.getTagByCategoryResponse);
        //     });
        // }
        // getTag() {
        //   this.apiservice.getTag().subscribe(res => {
        //     this.getTagResponse = res;
        //     console.log('get tag  res>>', this.getTagResponse);
        //   });
        // }
        // onTags(info, isChecked: boolean) {
        //   if (isChecked) {
        //     this.addChildData.interestinfo.push(info);
        //     console.log('kidsInfo', this.addChildData.interestinfo);
        //   } else {
        //     const index = this.addChildData.interestinfo.indexOf(info);
        //     this.addChildData.interestinfo.splice(index, 1);
        //     console.log('kidsInfo', this.addChildData.interestinfo);
        //   }
        // }

      }, {
        key: "addChild",
        value: function addChild(id) {
          var _this = this;

          this.addChildData.parentId = id;
          console.log('child data before upload', this.addChildData);
          this.ngxLoader.start();
          this.apiservice.addChild(this.addChildData).subscribe(function (res) {
            _this.getChildResponse = res;

            _this.ngxLoader.stop();

            if (_this.getChildResponse.isSuccess === true) {
              _this.router.navigate(['/search']);

              console.log('get child res from model', res);

              _this.snack.open(_this.message, 'OK', {
                duration: 5000
              });
            } else {
              if (_this.getChildResponse.error === 'child already exits with this name') {
                _this.snack.open(_this.getChildResponse.error, 'OK', {
                  duration: 5000
                });

                _this.router.navigateByUrl('/parent', {
                  skipLocationChange: true
                }).then(function () {
                  _this.router.navigate(['parent/login-parent']);
                });
              } else {
                _this.snack.open(_this.getChildResponse.error, 'OK', {
                  duration: 5000
                });

                _this.router.navigateByUrl('/parent', {
                  skipLocationChange: true
                }).then(function () {
                  _this.router.navigate(['parent/login-parent']);
                });
              }
            }
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "searchCategory",
        value: function searchCategory(key) {
          var _this2 = this;

          this.apiservice.searchCategory(key).subscribe(function (res) {
            _this2.getCategoryResponse = res;
            _this2.categories = _this2.getCategoryResponse.data;
            console.log('category list categories', _this2.categories);
          });
        }
      }, {
        key: "selectEvent",
        value: function selectEvent(item) {
          this.addChildData.interestInfo.push(item);
          console.log('selectEvent', item);
          console.log('intrest array', this.addChildData.interestInfo); // do something with selected item
        }
      }, {
        key: "onChangeSearch",
        value: function onChangeSearch(val) {
          this.searchCategory(val);
          console.log('onChangeSearch', val); // fetch remote data from here
          // And reassign the 'data' which is binded to 'data' property.
        }
      }, {
        key: "onFocused",
        value: function onFocused(e) {
          console.log('onFocused', e); // do something when input is focused
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          if (this.userData === null || this.userData === undefined) {
            this.router.navigate(['/login']);
          } // this.getCategory();
          // this.getTag();


          this.addChildForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            // email: new FormControl('', [Validators.email]),
            sex: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', []),
            age: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]),
            schoolInfo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', []),
            dob: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', []),
            relationToChild: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', []),
            intrestes: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [])
          });
        }
      }]);

      return LoginParentComponent;
    }();

    LoginParentComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]
      }, {
        type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_5__["NgxUiLoaderService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSnackBar"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fruitInput', {
      "static": true
    })], LoginParentComponent.prototype, "fruitInput", void 0);
    LoginParentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login-parent',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login-parent.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/login-parent/login-parent.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login-parent.component.css */
      "./src/app/pages/parent/login-parent/login-parent.component.css"))["default"]]
    })], LoginParentComponent);
    /***/
  },

  /***/
  "./src/app/pages/parent/parent-profile/parent-profile.component.css":
  /*!**************************************************************************!*\
    !*** ./src/app/pages/parent/parent-profile/parent-profile.component.css ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesParentParentProfileParentProfileComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "[class^=\"icon-\"],\n[class*=\" icon-\"] {\n    /* use !important to prevent issues with browser extensions that change fonts */\n    font-family: 'icomoon' !important;\n    speak: never;\n    font-style: normal;\n    font-weight: normal;\n    font-variant: normal;\n    text-transform: none;\n    line-height: 1;\n\n    /* Better Font Rendering =========== */\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n}\n\n.icon-close:before {\n    content: \"\\e900\";\n}\n\n.icon-home:before {\n    content: \"\\e901\";\n}\n\n.icon-information:before {\n    content: \"\\e902\";\n}\n\n.icon-left-arrow:before {\n    content: \"\\e903\";\n}\n\n.icon-edit_profile:before {\n    content: \"\\e904\";\n}\n\n.icon-plus:before {\n    content: \"\\e905\";\n}\n\n.icon-checklist:before {\n    content: \"\\e907\";\n}\n\n.icon-grid:before {\n    content: \"\\e906\";\n}\n\n.icon-tick:before {\n    content: \"\\e90b\";\n}\n\nbody {\n    margin: 0;\n    padding: 0;\n    height: 100%;\n    width: 100%;\n    background-color: #f9fafa;\n    font-family: 'Open Sans';\n    font-weight: 400;\n}\n\nbody ul,\nli,\na {\n    list-style: none;\n    padding: 0;\n    margin: 0;\n    text-decoration: none;\n}\n\nbody h1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n    margin: 0;\n}\n\nbody a:hover {\n    text-decoration: none;\n}\n\n.row-0 {\n    margin-right: 0;\n    margin-left: 0;\n}\n\n.row-0 [class*=\"col-\"] {\n    padding-left: 0;\n    padding-right: 0;\n}\n\n.container {\n    max-width: 1160px;\n}\n\n.overlay {\n    background-color: rgba(0, 0, 0, 0.4);\n    position: absolute;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n}\n\ninput.form-control,\nbutton {\n    box-shadow: none;\n    outline: none;\n}\n\ninput.form-control:focus {\n    box-shadow: none;\n    outline: none;\n}\n\nbutton:focus {\n    box-shadow: none;\n    outline: none;\n}\n\n.btn_style {\n    width: 120px;\n    min-height: 36px;\n    line-height: 36px;\n    background-color: #F15C20;\n    border: 1px solid #F15C20;\n    box-sizing: border-box;\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    display: inline-block;\n    text-align: center;\n    font-family: 'Open Sans';\n    font-size: 13px;\n    letter-spacing: 0.02em;\n    text-transform: uppercase;\n    font-weight: 600;\n    color: #fff;\n    transition: 0.8s;\n}\n\n.btn_style:hover {\n    color: #fff;\n    background-color: #F15C20;\n}\n\n.custom_select {\n    height: auto;\n    display: inline-block;\n    border-radius: 3px;\n    min-width: 200px;\n    min-height: 52px;\n    line-height: 52px;\n    border: 1px solid #E5E9EC;\n    margin: 0;\n    padding: 0;\n    width: 100%;\n}\n\nheader {\n    background-color: #fff;\n    padding: 19px 35px;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\nheader .logo {\n    max-width: 136px;\n}\n\nheader .logo img {\n    max-width: 100%;\n}\n\n.content_outer {\n    padding-top: 99px;\n    padding-bottom: 36px;\n}\n\n.breadcrump_top a {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    color: #777CEA;\n}\n\n.breadcrump_top a span {\n    color: #748494;\n    font-size: 14px;\n    margin-right: 3px;\n}\n\n.page_heading {\n    margin-top: 11px;\n}\n\n.page_heading h2 {\n    font-family: 'Patrick Hand';\n    font-style: normal;\n    font-weight: normal;\n    font-size: 28px;\n    line-height: 37px;\n    color: #748494;\n    background: linear-gradient(to right, #748494, #748494);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n}\n\n.side_bar .progress-title {\n    font-size: 16px;\n    font-weight: 700;\n    color: #333;\n    margin: 0 0 20px;\n}\n\n.side_bar .progress {\n    height: 8px;\n    background: #e5e9ec;\n    border-radius: 0;\n    box-shadow: none;\n    margin-top: 46px;\n    overflow: visible;\n}\n\n.side_bar .progress .progress-bar {\n    position: relative;\n    -webkit-animation: animate-positive 2s;\n    animation: animate-positive 2s;\n    overflow: visible;\n    width:0%;\n     background:#F15C20;\n}\n\n.side_bar .progress .progress-value {\n    display: block;\n    font-size: 15px;\n    font-weight: 600;\n    color: #F15C20;\n    position: absolute;\n    top: -22px;\n    left: 0;\n}\n\n@-webkit-keyframes animate-positive {\n    0% {\n        width: 0;\n    }\n}\n\n@keyframes animate-positive {\n    0% {\n        width: 0;\n    }\n}\n\n.side_bar {\n    width: 100%;\n    max-width: 245px;\n    display: inline-block;\n    padding-right: 65px;\n}\n\n.side_bar .complete_score {\n    margin-top: 7px;\n}\n\n.side_bar .complete_score p {\n    font-style: normal;\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #748494;\n}\n\n.side_bar .complete_score p span {\n    color: #F15C20;\n    float: right;\n    font-size: 14px;\n}\n\n/* side menu css start */\n\n.side_menu {\n    margin-top: 33px;\n}\n\n.side_menu ul li a {\n    font-family: 'Patrick Hand';\n    font-weight: normal;\n    font-size: 20px;\n    line-height: 25px;\n    color: #748494;\n    padding: 10px 16px;\n    display: inline-block;\n    width: 100%;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\n.side_menu ul li a {\n    position: relative;\n}\n\n.side_menu ul li a::after {\n    content: '';\n    position: absolute;\n    left: 0;\n    top: 50%;\n    width: 4px;\n    height: 20px;\n    background-color: transparent;\n    transform: translateY(-50%);\n    border-radius: 0 4px 4px 0;\n    transition: 0.8s;\n}\n\n.side_menu ul li a.active {\n    background-color: #fff;\n}\n\n.side_menu ul li a:hover {\n    background-color: #fff;\n}\n\n.side_menu ul li a:hover::after {\n    background-color: #777CEA;\n}\n\n.side_menu ul li a.active::after {\n    background-color: #777CEA;\n}\n\n/* start content_right css */\n\n.MainContent {\n    display: flex;\n}\n\n.content_right {\n    background: #FFFFFF;\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\n    border-radius: 4px;\n    padding: 58px 34px 38px 34px;\n    width: 100%;\n    float: right;\n    max-width: 955px;\n}\n\n.content_right h2 {\n    font-family: 'Patrick Hand';\n    font-style: normal;\n    font-weight: normal;\n    font-size: 26px;\n    line-height: 32px;\n    color: #748494;\n    margin: 0;\n    background: linear-gradient(to right, #748494, #748494);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n}\n\n.details_form {\n    display: inline-block;\n    max-width: 440px;\n    width: 100%;\n    margin-top: 15px;\n}\n\n.details_form .form-group {\n    margin-bottom: 4px;\n    position: relative;\n}\n\n.details_form .form-group.delete_account a {\n    color: #777CEA;\n}\n\n.details_form .form-group.delete_account {\n    margin-top: 40px;\n}\n\n.form-group label {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #777CEA;\n    padding: 0 0 0 15px;\n}\n\n.form-group .form-control,\n.form-group input {\n    width: 100%;\n    border-radius: 4px;\n    border: 1px solid #E5E9EC;\n    min-height: 52px;\n    height: auto;\n    font-size: 13px;\n    color: #748494;\n    transition: 0.8s;\n    padding: 14px;\n}\n\n.form-group textarea.form-control {\n    min-height: 180px;\n    resize: none;\n}\n\n.details_form .form-group .input_outer {\n    position: relative;\n}\n\n.details_form .form-group .input_outer::after {\n    position: absolute;\n    /* content: \"\\e900\"; */\n    font-family: 'icomoon' !important;\n    font-size: 18px;\n    top: 50%;\n    transform: translateY(-50%);\n    right: 9px;\n    color: #F15C20;\n    opacity: 0;\n}\n\n.details_form .form-group .input_outer:focus::after,\n.details_form .form-group .input_outer:focus-within::after {\n    opacity: 1;\n}\n\n.form-group .form-control:focus,\n.form-group input:focus {\n    outline: none;\n    box-shadow: none;\n    border: 1px solid #666;\n}\n\n.profile_img {\n    max-width: 180px;\n    max-height: 150px;\n    width: 100%;\n    height: 100%;\n    float: right;\n}\n\n.profile_img img {\n    max-width: 100%;\n    min-height: 150px;\n    -o-object-fit: cover;\n       object-fit: cover;\n}\n\n.profile_img.edit {\n    background: #E5E9EC;\n    border-radius: 4px;\n    text-align: center;\n    position: relative;\n}\n\n.profile_img a {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 18px;\n    color: #748494;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n}\n\n.profile_img .icon-edit_profile {\n    display: inline-block;\n    width: 100%;\n    font-size: 20px;\n    margin-bottom: 10px;\n}\n\n.back_arrow {\n    font-size: 13px;\n    line-height: 16px;\n    text-align: justify;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.bottom_arrow {\n    padding: 11px 0;\n    background: rgba(255, 255, 255, 0.1);\n    box-shadow: inset 0px 1px 0px rgba(0, 0, 0, 0.1);\n    width: 100%;\n    margin-top: 148px;\n}\n\n.back_arrow span {\n    vertical-align: middle;\n    margin-right: 14px;\n}\n\n.btn_style.save_btn {\n    float: right;\n}\n\nfooter {\n    padding-bottom: 26px;\n}\n\n.footer_inner {\n    padding: 20px 25px;\n    background-color: #FFFFFF;\n}\n\n.footer_logo {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_logo a img {\n    max-width: 78px;\n}\n\n.footer_menu {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_menu ul {\n    margin-top: 7px;\n}\n\n.footer_menu ul li {\n    display: inline-block;\n    margin: 0 27px 0 0;\n}\n\n.footer_menu ul a {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.footer_menu ul a:hover {\n    color: #777CEA;\n}\n\n.copyright_text {\n    margin-top: 13px;\n}\n\n.copyright_text p {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    margin: 0;\n    color: #748494;\n}\n\n/* guardins page css */\n\n.profile_list .list_box {\n    display: inline-block;\n    width: 100%;\n    max-width: 120px;\n    margin-right: 13px;\n    margin-bottom: 20px; \n}\n\n.personal_details .margin_top {\n    margin-top: 38px;\n}\n\n.profile_list .list_box .profile_img {\n    max-width: 136px;\n    max-height: 100px;\n    height: 100%;\n    overflow: hidden;\n    width: 100%;\n    float: left;\n}\n\n.profile_list .list_box .profile_img img {\n    max-width: 100%;\n    width: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n}\n\n.profile_list .list_box .profile_text {\n    margin-top: 5px;\n    display: inline-block;\n    width: 100%;\n}\n\n.profile_list .list_box .profile_text h3 {\n    font-weight: 600;\n    font-size: 14px;\n    line-height: 18px;\n    color: #748494;\n}\n\n.profile_list .list_box .profile_text p {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #777CEA;\n}\n\n.activity .media-left {\n    max-width: 230px;\n    max-height: 200px;\n    position: relative;\n}\n\n.activity .media-left .share_icon {\n    position: absolute;\n    top: 12px;\n    right: 14px;\n}\n\n.activity .media-left img {\n    max-width: 100%;\n    height: 100%;\n    width: 100%;\n}\n\n.activity .media-body {\n    padding-left: 26px;\n}\n\n.list p {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.list p img {\n    margin: 0 8px 0 0;\n    vertical-align: middle;\n}\n\n.activity .media-body h3 {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    text-transform: uppercase;\n    color: #777CEA;\n}\n\n.activity .media-body h2 {\n    font-size: 22px;\n    line-height: 30px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n    margin: 0;\n    font-family: 'Patrick Hand';\n}\n\n.recomend_images{\n    margin-top: 10px;\n}\n\n.recomend_images a {\n    margin-right: 10px;\n}\n\n.recomend_images a:last-child {\n    margin: 0;\n}\n\n.recomend_images img {\n    max-width: 21px;\n}\n\n.recomend_btn {\n    margin-top: 28px;\n}\n\n.recomend_btn a {\n    min-width: 104px;\n    min-height: 20px;\n    font-size: 12px;\n    color: #F15C20;\n    line-height: 20px;\n    border: 1px solid #F15C20;\n    border-radius: 3px;\n    padding: 0 8px;\n    transition: 0.8s;\n}\n\n.recomend_btn a:hover {\n    background-color: #F15C20;\n    color: #fff;\n}\n\n.details_outer {\n    margin-top: 40px;\n}\n\n.details_outer .row {\n    margin-bottom: 10px;\n    background: #FFFFFF;\n}\n\n.details_outer .list{\n    margin-top: 9px;\n}\n\n.details_outer .list_outer {\n    border: none;\n    border-radius: 0;\n    margin-bottom: 0;\n}\n\n.activity {\n    border: 1px solid #E5E9EC;\n    background-color: #fff;\n    border-radius: 4px;\n}\n\n.rating_box {\n    position: absolute;\n    top: 9px;\n    right: 20px;\n}\n\n.rating_box a {\n    font-size: 12px;\n    line-height: 16px;\n    text-align: justify;\n    color: #748494;\n}\n\n.row.bottom_margin {\n    margin-top: 26px;\n}\n\n.form-group .add_child {\n    font-style: normal;\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 13px 0 0 17px;\n    display: inline-block;\n}\n\n.form-group .add_child .icon-plus {\n    color: #777CEA;\n    font-size: 20px;\n    margin-right: 8px;\n    vertical-align: middle;\n}\n\n.media.account_name {\n    background: #FFFFFF;\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\n    border-radius: 4px;\n    padding: 21px 15px;\n    margin-bottom: 6px;\n}\n\n.media.account_name .media-left {\n    max-width: 46px;\n    max-height: 46px;\n    border-radius: 5px;\n}\n\n.media.account_name .media-left img {\n    max-width: 100%;\n    height: 100%;\n    width: 100%;\n}\n\n.media.account_name .media-body {\n    padding-left: 24px;\n}\n\n.media.account_name .media-body h4 {\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n}\n\n.media.account_name .media-body p {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 3px 0 0 0;\n}\n\n.form-group.add_interests .input_outer {\n    border: 1px solid #E5E9EC;\n    padding: 14px;\n}\n\n.form-group.add_interests .input_outer a {\n    background: #777CEA;\n    border-radius: 40px;\n    min-width: 119px;\n    line-height: 30px;\n    min-height: 30px;\n    font-size: 12px;\n    display: inline-block;\n    text-align: center;\n    color: #FFFFFF;\n    margin-right: 2px;\n    transition: 0.8s;\n}\n\n.form-group.add_interests .input_outer a:hover {\n    background-color: #296eb9;\n}\n\n.form-group.add_interests .input_outer::after {\n    content: none;\n}\n\n.form-group.add_interests .input_outer .icon-close {\n    vertical-align: middle;\n    margin-right: 13px;\n    font-size: 15px;\n    float: right;\n    margin-top: 7px;\n}\n\n/* start search page css */\n\n.search_portion {\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n    background: #FFFFFF;\n    display: inline-block;\n    padding: 10px 45px;\n    width: 100%;\n}\n\n.left_search {\n    float: left;\n}\n\n.left_search input::-webkit-input-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-moz-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-ms-input-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-moz-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-webkit-placeholder {\n    color: #E5E9EC;\n}\n\n.right_search {\n    float: right;\n    padding-top: 6px;\n}\n\n.right_search a {\n    display: inline-block;\n    color: #C5CED6;\n    font-size: 20px;\n    margin: 0 0 0 11px;\n}\n\n.right_search a.active {\n    color: #F15C20;\n}\n\n.switch.notification {\n    position: relative;\n    display: inline-block;\n    width: 28px;\n    height: 17px;\n    margin: 0 0 0 12px;\n}\n\n.switch.notification input {\n    opacity: 0;\n    width: 0;\n    height: 0;\n}\n\n.switch.notification .slider {\n    position: absolute;\n    cursor: pointer;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #ccc;\n    transition: .4s;\n}\n\n.switch.notification .slider:before {\n    position: absolute;\n    content: \"\";\n    height: 12px;\n    width: 12px;\n    left: 4px;\n    bottom: 3px;\n    background-color: white;\n    transition: .4s;\n}\n\n.switch.notification input:checked+.slider {\n    background-color: #F15C20;\n}\n\n.switch.notification input:focus+.slider {\n    box-shadow: 0 0 1px #F15C20;\n}\n\n.switch.notification input:checked+.slider:before {\n    transform: translateX(9px);\n}\n\n/* Rounded sliders */\n\n.switch.notification .slider.round {\n    border-radius: 34px;\n}\n\n.switch.notification .slider.round:before {\n    border-radius: 50%;\n}\n\n.category_list {\n    display: inline-block;\n    width: 100%;\n    padding: 10px 46px 17px;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\n.category_list li {\n    display: inline-block;\n\n}\n\n.category_list li a {\n    font-size: 13px;\n    color: #748494;\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 2px;\n    padding: 0 15px;\n    min-height: 36px;\n    line-height: 36px;\n    display: inline-block;\n    margin-right: 7px;\n    transition: 0.8s;\n}\n\n.category_list li a:hover,\n.category_list li a.active {\n    background-color: #ffb206;\n    color: #fff;\n    border-color: #ffb206;\n}\n\n.search_activity {\n    padding: 0 0 0 46px;\n}\n\n.search_activity .list_left {\n    padding-top: 20px;\n    padding-right: 20px;\n}\n\n.search_activity .activity {\n    margin-bottom: 8px;\n    position: relative;\n}\n\n.search_activity .activity:last-child {\n    margin: 0;\n}\n\n.search_activity .map_section img {\n    max-width: 100%;\n}\n\n.select_avtar p {\n    font-weight: 600;\n    font-size: 10px;\n    text-transform: uppercase;\n    color: #C5CED6;\n    margin: 14px 0 0 0;\n}\n\n.select_avtar {\n    text-align: center;\n    width: 100%;\n    float: right;\n    max-width: 180px;\n}\n\n.select_avtar a {\n    font-style: normal;\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    color: #777CEA;\n}\n\n.select_avtar a img {\n    margin: 0 0 0 5px;\n}\n\nheader .navbar {\n    padding: 0;\n}\n\nheader .navbar-light .navbar-nav .nav-link {\n    font-size: 13px;\n    font-weight: normal;\n    line-height: 16px;\n    color: #748494;\n    padding: 9px 0;\n    margin-right: 25px;\n}\n\nheader .navbar-nav .btn_style {\n    font-family: 'Open Sans';\n    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    font-weight: 600;\n    font-size: 13px;\n    color: #fff;\n}\n\n/* start activity_category css */\n\n.activity_category {\n    padding: 140px 0 54px 0;\n}\n\n.activity_category h2 {\n    font-family: Nanum Pen;\n    font-weight: normal;\n    font-size: 28px;\n    line-height: 18px;\n    color: #3D3789;\n}\n\n.activity_category_list {\n    padding-top: 48px;\n}\n\n.activity_category .category_list_box {\n    max-height: 276px;\n    border-radius: 4px;\n}\n\n.category_list_box {\n    overflow: hidden;\n    position: relative;\n}\n\n.activity_category .category_list_box img {\n    max-width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    border-radius: 4px;\n}\n\n.category_list_box .category_title {\n    position: absolute;\n    bottom: 23px;\n    left: 33px;\n}\n\n.category_list_box .category_title p {\n    font-family: Nanum Pen;\n    font-weight: normal;\n    font-size: 28px;\n    line-height: 18px;\n    color: #FFFFFF;\n}\n\n.category_list_box .category_title {\n    position: absolute;\n    bottom: 23px;\n    left: 33px;\n    z-index: 1;\n}\n\n/* start notifications page css */\n\n.notifications h3 {\n    font-weight: 600;\n    font-size: 15px;\n    line-height: 18px;\n    color: #777CEA;\n}\n\n.notifications .switch.notification {\n    margin: 14px 0 0 0;\n}\n\n.notifications .switch.notification input:checked+.slider {\n    background-color: #777CEA;\n}\n\n.ch-radio {\n    display: inline-block;\n    overflow: hidden;\n    cursor: pointer;\n    position: relative;\n    transition: all 0.4s;\n}\n\n.ch-radio * {\n    transition: all 0.4s;\n}\n\n.ch-radio input[type=\"checkbox\"],\n.ch-radio input[type=\"radio\"] {\n    position: absolute;\n    top: 0;\n    left: 0;\n    opacity: 0;\n}\n\n.ch-radio input[type=\"checkbox\"]:checked+span:before,\n.ch-radio input[type=\"checkbox\"]:checked~span:before,\n.ch-radio input[type=\"radio\"]:checked+span:before,\n.ch-radio input[type=\"radio\"]:checked~span:before {\n    border-color: #4b97d7;\n    background-color: #4b97d7;\n}\n\n.ch-radio input[type=\"checkbox\"]:checked+span .ch-icon,\n.ch-radio input[type=\"checkbox\"]:checked~span .ch-icon,\n.ch-radio input[type=\"radio\"]:checked+span .ch-icon,\n.ch-radio input[type=\"radio\"]:checked~span .ch-icon {\n    opacity: 1;\n}\n\n.ch-radio span {\n    display: block;\n    overflow: hidden;\n    line-height: 16px;\n    position: relative;\n    padding-left: 30px;\n    font-size: 13px;\n    font-weight: normal;\n    color: #748494;\n}\n\n.ch-radio span:before {\n    content: \"\";\n    background-color: transparent;\n    position: absolute;\n    top: 1px;\n    left: 1px;\n    width: 15px;\n    height: 15px;\n    transition: inherit;\n    border: 1px solid #C5CED6;\n    border-radius: 1px;\n}\n\n.ch-radio span .ch-icon {\n    color: #FFFFFF;\n    position: absolute;\n    top: 0;\n    left: 5px;\n    font-size: 9px;\n    opacity: 0;\n    transform: translateY(0);\n    line-height: 17px;\n}\n\n.check_list {\n    margin-top: 23px;\n}\n\n.check_list .ch-radio {\n    display: inline-block;\n    margin-right: 44px;\n}\n\n.textarea_length {\n    position: absolute;\n    bottom: -4px;\n    right: 12px;\n}\n\n.textarea_length p {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 18px;\n    color: rgba(0, 0, 0, 0.3);\n}\n\n/* VALIDATION ERROR  */\n\n.form-error-msg {\n    color: red;\n}\n\ninput.ng-invalid.ng-touched {\n    border: 1px solid red;\n}\n\nselect.ng-invalid.ng-touched {\n    border: 1px solid red;\n}\n\n/* matform chips css & ng-autocomplete*/\n\n.form-group .input_outer .ng-autocomplete{\n    width: 438px !important;\n}\n\n.form-group .input_outer .mat-form-field {\n    \n    width: 100%;\n}\n\n.form-group .input_outer .mat-chip.mat-standard-chip {\n    background: #F15C20;\n    border-radius: 40px;\n    min-width: 119px;\n    line-height: 30px;\n    min-height: 30px;\n    font-size: 12px;\n    display: inline-block;\n    text-align: left;\n    color: #FFFFFF;\n    margin-right: 2px;\n    transition: 0.8s;\n    margin: 0 0 0 11px;\n    padding: 0 15px;\n}\n\n.form-group .input_outer .mat-standard-chip .mat-chip-remove.mat-icon {\n    font-size: 0;\n    opacity: 1;\n    width: 0;\n    height: auto;\n    right: 27px;\n}\n\n.form-group .input_outer .mat-standard-chip .mat-chip-remove.mat-icon:before {\n    content: \"\\e900\";\n    font-family: 'icomoon' !important;\n    font-size: 15px;\n    color: #FFFFFF;\n}\n\n.details_form .mat-form-field-infix {\n    border-top: none;\n    padding: 0;\n}\n\n.details_form .mat-form-field-appearance-legacy .mat-form-field-underline {\n    height: 0;\n    /* background-color: #f00; */\n}\n\n.mat-form-field-infix {\n    border: none;\n    padding: 0;\n}\n\n.demo-chip-list.mat-form-field-appearance-legacy .mat-form-field-underline {\n    height: 0;\n}\n\n.mat-form-field-appearance-legacy .mat-form-field-wrapper {\n    padding: 0 !important;\n}\n\n.form-group .input_outer .demo-chip-list {\n    position: relative;\n}\n\n.form-group .input_outer .demo-chip-list .mat-form-field-wrapper {\n    padding-bottom: 0 !important;\n}\n\n.form-group .input_outer .demo-chip-list::after {\n    content: '';\n    background-color: #fff;\n    position: absolute;\n    bottom: 18px;\n    height: 4px;\n    width: 100%;\n}\n\n.my_class {\n    display: none;\n}\n\n.profile_img{\n    background: #E5E9EC;\n    border-radius: 4px;\n    text-align: center;\n    position: relative;\n}\n\n.notifications {\n    margin-top: 28px;\n}\n\n@media(max-width:767px) {\n    .my_class {\n        display: block;\n        position: absolute;\n        right: 17px;\n        margin: -29px 0 0 0;\n        background-color: #F15C20;\n        color: #fff;\n        border: none;\n        box-shadow: none;\n        min-width: 47px;\n        font-size: 25px;\n        min-height: 16px;\n        line-height: 36px;\n    }\n    .btn_style {\n        width: 100px;\n        min-height: 26px;\n        line-height: 26px;\n    }\n    .side_bar{\n        position: fixed;\n        top: 0;\n        left: -100%;\n        background-color: #FFFFFF;\n        height: 100vh;\n        width: 100%;\n        max-width: 310px;\n        transition: 0.5s;\n        display: block !important;\n        z-index: 1;\n        padding:0 26px;\n    }\n    .side_bar.side_bar_hide{\n        left: 0;\n        display: block !important;\n        box-shadow: 0 10px 16px 11px rgba(24, 87, 171, 0.08);\n    }\n    .MainContent{\n        width: 100%;\n    }\n    .page_heading h2{\n        font-size: 21px;\n    }\n    .content_right h2{\n        font-size: 22px;\n    }\n    .profile_list{\n        text-align: center;\n    }\n    .profile_img{\n        min-height: 150px;\n    }\n    .profile_img img{\n        min-height: 150px;\n    }\n    .btn_style.save_btn.cursor{\n        margin-top: 21px;\n    }\n    .bottom_arrow .btn_style.save_btn.cursor{\n        margin-top: 0;\n    }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFyZW50L3BhcmVudC1wcm9maWxlL3BhcmVudC1wcm9maWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksK0VBQStFO0lBQy9FLGlDQUFpQztJQUNqQyxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLGNBQWM7O0lBRWQsc0NBQXNDO0lBQ3RDLG1DQUFtQztJQUNuQyxrQ0FBa0M7QUFDdEM7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxTQUFTO0lBQ1QsVUFBVTtJQUNWLFlBQVk7SUFDWixXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLHdCQUF3QjtJQUN4QixnQkFBZ0I7QUFDcEI7O0FBRUE7OztJQUdJLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsU0FBUztJQUNULHFCQUFxQjtBQUN6Qjs7QUFFQTs7Ozs7O0lBTUksU0FBUztBQUNiOztBQUVBO0lBQ0kscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksb0NBQW9DO0lBQ3BDLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sUUFBUTtJQUNSLFNBQVM7SUFDVCxPQUFPO0lBQ1AsV0FBVztJQUNYLFlBQVk7QUFDaEI7O0FBRUE7O0lBRUksZ0JBQWdCO0lBQ2hCLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLHdCQUF3QjtJQUN4QixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixnQkFBZ0I7SUFDaEIsV0FBVztJQUVYLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCx5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsU0FBUztJQUNULFVBQVU7SUFDVixXQUFXO0FBQ2Y7O0FBR0E7SUFDSSxzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLHNDQUFzQztBQUMxQzs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsdURBQXVEO0lBQ3ZELDZCQUE2QjtJQUM3QixvQ0FBb0M7QUFDeEM7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixzQ0FBc0M7SUFDdEMsOEJBQThCO0lBQzlCLGlCQUFpQjtJQUNqQixRQUFRO0tBQ1Asa0JBQWtCO0FBQ3ZCOztBQUVBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsT0FBTztBQUNYOztBQUVBO0lBQ0k7UUFDSSxRQUFRO0lBQ1o7QUFDSjs7QUFFQTtJQUNJO1FBQ0ksUUFBUTtJQUNaO0FBQ0o7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLHFCQUFxQjtJQUNyQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztJQUNkLFlBQVk7SUFDWixlQUFlO0FBQ25COztBQUVBLHdCQUF3Qjs7QUFDeEI7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSwyQkFBMkI7SUFDM0IsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsV0FBVztJQUNYLHNDQUFzQztBQUMxQzs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtJQUNaLDZCQUE2QjtJQUM3QiwyQkFBMkI7SUFDM0IsMEJBQTBCO0lBQzFCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQSw0QkFBNEI7O0FBQzVCO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQiwwQ0FBMEM7SUFDMUMsa0JBQWtCO0lBQ2xCLDRCQUE0QjtJQUM1QixXQUFXO0lBQ1gsWUFBWTtJQUNaLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLFNBQVM7SUFDVCx1REFBdUQ7SUFDdkQsNkJBQTZCO0lBQzdCLG9DQUFvQztBQUN4Qzs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGNBQWM7SUFDZCxtQkFBbUI7QUFDdkI7O0FBRUE7O0lBRUksV0FBVztJQUNYLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixlQUFlO0lBQ2YsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsc0JBQXNCO0lBQ3RCLGlDQUFpQztJQUNqQyxlQUFlO0lBQ2YsUUFBUTtJQUVSLDJCQUEyQjtJQUMzQixVQUFVO0lBQ1YsY0FBYztJQUNkLFVBQVU7QUFDZDs7QUFFQTs7SUFFSSxVQUFVO0FBQ2Q7O0FBRUE7O0lBRUksYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxZQUFZO0lBQ1osWUFBWTtBQUNoQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsb0JBQWlCO09BQWpCLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxnQ0FBZ0M7QUFDcEM7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLGVBQWU7SUFDZixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixvQ0FBb0M7SUFDcEMsZ0RBQWdEO0lBQ2hELFdBQVc7SUFDWCxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxzQkFBc0I7SUFDdEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxjQUFjO0FBQ2xCOztBQUVBLHNCQUFzQjs7QUFDdEI7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsV0FBVztJQUNYLG9CQUFpQjtPQUFqQixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxTQUFTO0lBQ1QsMkJBQTJCO0FBQy9COztBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksU0FBUztBQUNiOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixtQkFBbUI7QUFDdkI7O0FBQ0E7SUFDSSxlQUFlO0FBQ25COztBQUdBO0lBQ0ksWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLHFCQUFxQjtJQUNyQixxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsMENBQTBDO0lBQzFDLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGlCQUFpQjtJQUVqQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSx5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsWUFBWTtJQUNaLGVBQWU7QUFDbkI7O0FBRUEsMEJBQTBCOztBQUMxQjtJQUNJLHNDQUFzQztJQUN0QyxtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxXQUFXO0FBQ2Y7O0FBSUE7SUFDSSxjQUFjO0FBQ2xCOztBQUZBO0lBQ0ksY0FBYztBQUNsQjs7QUFGQTtJQUNJLGNBQWM7QUFDbEI7O0FBRkE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGNBQWM7SUFDZCxlQUFlO0lBQ2Ysa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsUUFBUTtJQUNSLFNBQVM7QUFDYjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsTUFBTTtJQUNOLE9BQU87SUFDUCxRQUFRO0lBQ1IsU0FBUztJQUNULHNCQUFzQjtJQUV0QixlQUFlO0FBQ25COztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1osV0FBVztJQUNYLFNBQVM7SUFDVCxXQUFXO0lBQ1gsdUJBQXVCO0lBRXZCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSx5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSwyQkFBMkI7QUFDL0I7O0FBRUE7SUFHSSwwQkFBMEI7QUFDOUI7O0FBRUEsb0JBQW9COztBQUNwQjtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0lBQ1gsdUJBQXVCO0lBQ3ZCLHNDQUFzQztBQUMxQzs7QUFFQTtJQUNJLHFCQUFxQjs7QUFFekI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsY0FBYztJQUNkLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLHFCQUFxQjtJQUNyQixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCOztBQUVBOztJQUVJLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZix5QkFBeUI7SUFDekIsY0FBYztJQUNkLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksVUFBVTtBQUNkOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGNBQWM7SUFDZCxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsNENBQTRDO0lBQzVDLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLFdBQVc7QUFDZjs7QUFHQSxnQ0FBZ0M7O0FBQ2hDO0lBQ0ksdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixZQUFZO0lBQ1osb0JBQWlCO09BQWpCLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFVBQVU7QUFDZDs7QUFFQTtJQUNJLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixVQUFVO0lBQ1YsVUFBVTtBQUNkOztBQUVBLGlDQUFpQzs7QUFDakM7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0kseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCO0lBRWxCLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUVJLG9CQUFvQjtBQUN4Qjs7QUFFQTs7SUFFSSxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxVQUFVO0FBQ2Q7O0FBRUE7Ozs7SUFJSSxxQkFBcUI7SUFDckIseUJBQXlCO0FBQzdCOztBQUVBOzs7O0lBSUksVUFBVTtBQUNkOztBQUVBO0lBQ0ksY0FBYztJQUNkLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsNkJBQTZCO0lBQzdCLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsU0FBUztJQUNULFdBQVc7SUFDWCxZQUFZO0lBRVosbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixTQUFTO0lBQ1QsY0FBYztJQUNkLFVBQVU7SUFFVix3QkFBd0I7SUFDeEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osV0FBVztBQUNmOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIseUJBQXlCO0FBQzdCOztBQUlBLHNCQUFzQjs7QUFDdEI7SUFDSSxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxxQkFBcUI7QUFDekI7O0FBTUEsdUNBQXVDOztBQUN2QztJQUNJLHVCQUF1QjtBQUMzQjs7QUFFQTs7SUFFSSxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksWUFBWTtJQUNaLFVBQVU7SUFDVixRQUFRO0lBQ1IsWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixpQ0FBaUM7SUFDakMsZUFBZTtJQUNmLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsVUFBVTtBQUNkOztBQUVBO0lBQ0ksU0FBUztJQUNULDRCQUE0QjtBQUNoQzs7QUFFQTtJQUNJLFlBQVk7SUFDWixVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSw0QkFBNEI7QUFDaEM7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osV0FBVztJQUNYLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGFBQWE7QUFDakI7O0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixrQkFBa0I7QUFDdEI7O0FBQ0E7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSTtRQUNJLGNBQWM7UUFDZCxrQkFBa0I7UUFDbEIsV0FBVztRQUNYLG1CQUFtQjtRQUNuQix5QkFBeUI7UUFDekIsV0FBVztRQUNYLFlBQVk7UUFDWixnQkFBZ0I7UUFDaEIsZUFBZTtRQUNmLGVBQWU7UUFDZixnQkFBZ0I7UUFDaEIsaUJBQWlCO0lBQ3JCO0lBQ0E7UUFDSSxZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLGlCQUFpQjtJQUNyQjtJQUNBO1FBQ0ksZUFBZTtRQUNmLE1BQU07UUFDTixXQUFXO1FBQ1gseUJBQXlCO1FBQ3pCLGFBQWE7UUFDYixXQUFXO1FBQ1gsZ0JBQWdCO1FBRWhCLGdCQUFnQjtRQUNoQix5QkFBeUI7UUFDekIsVUFBVTtRQUNWLGNBQWM7SUFDbEI7SUFDQTtRQUNJLE9BQU87UUFDUCx5QkFBeUI7UUFFekIsb0RBQW9EO0lBQ3hEO0lBQ0E7UUFDSSxXQUFXO0lBQ2Y7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDQTtRQUNJLGtCQUFrQjtJQUN0QjtJQUNBO1FBQ0ksaUJBQWlCO0lBQ3JCO0lBQ0E7UUFDSSxpQkFBaUI7SUFDckI7SUFDQTtRQUNJLGdCQUFnQjtJQUNwQjtJQUNBO1FBQ0ksYUFBYTtJQUNqQjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGFyZW50L3BhcmVudC1wcm9maWxlL3BhcmVudC1wcm9maWxlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJbY2xhc3NePVwiaWNvbi1cIl0sXG5bY2xhc3MqPVwiIGljb24tXCJdIHtcbiAgICAvKiB1c2UgIWltcG9ydGFudCB0byBwcmV2ZW50IGlzc3VlcyB3aXRoIGJyb3dzZXIgZXh0ZW5zaW9ucyB0aGF0IGNoYW5nZSBmb250cyAqL1xuICAgIGZvbnQtZmFtaWx5OiAnaWNvbW9vbicgIWltcG9ydGFudDtcbiAgICBzcGVhazogbmV2ZXI7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC12YXJpYW50OiBub3JtYWw7XG4gICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG5cbiAgICAvKiBCZXR0ZXIgRm9udCBSZW5kZXJpbmcgPT09PT09PT09PT0gKi9cbiAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgICAtbW96LW9zeC1mb250LXNtb290aGluZzogZ3JheXNjYWxlO1xufVxuXG4uaWNvbi1jbG9zZTpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxlOTAwXCI7XG59XG5cbi5pY29uLWhvbWU6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlxcZTkwMVwiO1xufVxuXG4uaWNvbi1pbmZvcm1hdGlvbjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxlOTAyXCI7XG59XG5cbi5pY29uLWxlZnQtYXJyb3c6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlxcZTkwM1wiO1xufVxuXG4uaWNvbi1lZGl0X3Byb2ZpbGU6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlxcZTkwNFwiO1xufVxuXG4uaWNvbi1wbHVzOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcXGU5MDVcIjtcbn1cblxuLmljb24tY2hlY2tsaXN0OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcXGU5MDdcIjtcbn1cblxuLmljb24tZ3JpZDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxlOTA2XCI7XG59XG5cbi5pY29uLXRpY2s6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlxcZTkwYlwiO1xufVxuXG5ib2R5IHtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZmFmYTtcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2Fucyc7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuYm9keSB1bCxcbmxpLFxuYSB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmJvZHkgaDEsXG5oMixcbmgzLFxuaDQsXG5oNSxcbmg2IHtcbiAgICBtYXJnaW46IDA7XG59XG5cbmJvZHkgYTpob3ZlciB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4ucm93LTAge1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBtYXJnaW4tbGVmdDogMDtcbn1cblxuLnJvdy0wIFtjbGFzcyo9XCJjb2wtXCJdIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgcGFkZGluZy1yaWdodDogMDtcbn1cblxuLmNvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAxMTYwcHg7XG59XG5cbi5vdmVybGF5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbmlucHV0LmZvcm0tY29udHJvbCxcbmJ1dHRvbiB7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBvdXRsaW5lOiBub25lO1xufVxuXG5pbnB1dC5mb3JtLWNvbnRyb2w6Zm9jdXMge1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgb3V0bGluZTogbm9uZTtcbn1cblxuYnV0dG9uOmZvY3VzIHtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIG91dGxpbmU6IG5vbmU7XG59XG5cbi5idG5fc3R5bGUge1xuICAgIHdpZHRoOiAxMjBweDtcbiAgICBtaW4taGVpZ2h0OiAzNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzNnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMTVDMjA7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0YxNUMyMDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggMnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMmVtO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IDAuOHM7XG4gICAgdHJhbnNpdGlvbjogMC44cztcbn1cblxuLmJ0bl9zdHlsZTpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0YxNUMyMDtcbn1cblxuLmN1c3RvbV9zZWxlY3Qge1xuICAgIGhlaWdodDogYXV0bztcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIG1pbi13aWR0aDogMjAwcHg7XG4gICAgbWluLWhlaWdodDogNTJweDtcbiAgICBsaW5lLWhlaWdodDogNTJweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5cbmhlYWRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBwYWRkaW5nOiAxOXB4IDM1cHg7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMHB4IC0xcHggMHB4ICNFNUU5RUM7XG59XG5cbmhlYWRlciAubG9nbyB7XG4gICAgbWF4LXdpZHRoOiAxMzZweDtcbn1cblxuaGVhZGVyIC5sb2dvIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xufVxuXG4uY29udGVudF9vdXRlciB7XG4gICAgcGFkZGluZy10b3A6IDk5cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDM2cHg7XG59XG5cbi5icmVhZGNydW1wX3RvcCBhIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmJyZWFkY3J1bXBfdG9wIGEgc3BhbiB7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIG1hcmdpbi1yaWdodDogM3B4O1xufVxuXG4ucGFnZV9oZWFkaW5nIHtcbiAgICBtYXJnaW4tdG9wOiAxMXB4O1xufVxuXG4ucGFnZV9oZWFkaW5nIGgyIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAyOHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzN3B4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzc0ODQ5NCwgIzc0ODQ5NCk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4uc2lkZV9iYXIgLnByb2dyZXNzLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBjb2xvcjogIzMzMztcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xufVxuXG4uc2lkZV9iYXIgLnByb2dyZXNzIHtcbiAgICBoZWlnaHQ6IDhweDtcbiAgICBiYWNrZ3JvdW5kOiAjZTVlOWVjO1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtYXJnaW4tdG9wOiA0NnB4O1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xufVxuXG4uc2lkZV9iYXIgLnByb2dyZXNzIC5wcm9ncmVzcy1iYXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogYW5pbWF0ZS1wb3NpdGl2ZSAycztcbiAgICBhbmltYXRpb246IGFuaW1hdGUtcG9zaXRpdmUgMnM7XG4gICAgb3ZlcmZsb3c6IHZpc2libGU7XG4gICAgd2lkdGg6MCU7XG4gICAgIGJhY2tncm91bmQ6I0YxNUMyMDtcbn1cblxuLnNpZGVfYmFyIC5wcm9ncmVzcyAucHJvZ3Jlc3MtdmFsdWUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGNvbG9yOiAjRjE1QzIwO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC0yMnB4O1xuICAgIGxlZnQ6IDA7XG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBhbmltYXRlLXBvc2l0aXZlIHtcbiAgICAwJSB7XG4gICAgICAgIHdpZHRoOiAwO1xuICAgIH1cbn1cblxuQGtleWZyYW1lcyBhbmltYXRlLXBvc2l0aXZlIHtcbiAgICAwJSB7XG4gICAgICAgIHdpZHRoOiAwO1xuICAgIH1cbn1cblxuLnNpZGVfYmFyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXgtd2lkdGg6IDI0NXB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nLXJpZ2h0OiA2NXB4O1xufVxuXG4uc2lkZV9iYXIgLmNvbXBsZXRlX3Njb3JlIHtcbiAgICBtYXJnaW4tdG9wOiA3cHg7XG59XG5cbi5zaWRlX2JhciAuY29tcGxldGVfc2NvcmUgcCB7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5zaWRlX2JhciAuY29tcGxldGVfc2NvcmUgcCBzcGFuIHtcbiAgICBjb2xvcjogI0YxNUMyMDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4vKiBzaWRlIG1lbnUgY3NzIHN0YXJ0ICovXG4uc2lkZV9tZW51IHtcbiAgICBtYXJnaW4tdG9wOiAzM3B4O1xufVxuXG4uc2lkZV9tZW51IHVsIGxpIGEge1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMjVweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAtMXB4IDBweCAjRTVFOUVDO1xufVxuXG4uc2lkZV9tZW51IHVsIGxpIGEge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnNpZGVfbWVudSB1bCBsaSBhOjphZnRlciB7XG4gICAgY29udGVudDogJyc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgdG9wOiA1MCU7XG4gICAgd2lkdGg6IDRweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xuICAgIHRyYW5zaXRpb246IDAuOHM7XG59XG5cbi5zaWRlX21lbnUgdWwgbGkgYS5hY3RpdmUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG5cbi5zaWRlX21lbnUgdWwgbGkgYTpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLnNpZGVfbWVudSB1bCBsaSBhOmhvdmVyOjphZnRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc3N0NFQTtcbn1cblxuLnNpZGVfbWVudSB1bCBsaSBhLmFjdGl2ZTo6YWZ0ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM3NzdDRUE7XG59XG5cbi8qIHN0YXJ0IGNvbnRlbnRfcmlnaHQgY3NzICovXG4uTWFpbkNvbnRlbnQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5jb250ZW50X3JpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgcGFkZGluZzogNThweCAzNHB4IDM4cHggMzRweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWF4LXdpZHRoOiA5NTVweDtcbn1cblxuLmNvbnRlbnRfcmlnaHQgaDIge1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDI2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiAwO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzc0ODQ5NCwgIzc0ODQ5NCk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4uZGV0YWlsc19mb3JtIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWF4LXdpZHRoOiA0NDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwIHtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwLmRlbGV0ZV9hY2NvdW50IGEge1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwLmRlbGV0ZV9hY2NvdW50IHtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG4uZm9ybS1ncm91cCBsYWJlbCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICBwYWRkaW5nOiAwIDAgMCAxNXB4O1xufVxuXG4uZm9ybS1ncm91cCAuZm9ybS1jb250cm9sLFxuLmZvcm0tZ3JvdXAgaW5wdXQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIG1pbi1oZWlnaHQ6IDUycHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xuICAgIHBhZGRpbmc6IDE0cHg7XG59XG5cbi5mb3JtLWdyb3VwIHRleHRhcmVhLmZvcm0tY29udHJvbCB7XG4gICAgbWluLWhlaWdodDogMTgwcHg7XG4gICAgcmVzaXplOiBub25lO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlcjo6YWZ0ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAvKiBjb250ZW50OiBcIlxcZTkwMFwiOyAqL1xuICAgIGZvbnQtZmFtaWx5OiAnaWNvbW9vbicgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgdG9wOiA1MCU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIHJpZ2h0OiA5cHg7XG4gICAgY29sb3I6ICNGMTVDMjA7XG4gICAgb3BhY2l0eTogMDtcbn1cblxuLmRldGFpbHNfZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXI6Zm9jdXM6OmFmdGVyLFxuLmRldGFpbHNfZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXI6Zm9jdXMtd2l0aGluOjphZnRlciB7XG4gICAgb3BhY2l0eTogMTtcbn1cblxuLmZvcm0tZ3JvdXAgLmZvcm0tY29udHJvbDpmb2N1cyxcbi5mb3JtLWdyb3VwIGlucHV0OmZvY3VzIHtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzY2Njtcbn1cblxuLnByb2ZpbGVfaW1nIHtcbiAgICBtYXgtd2lkdGg6IDE4MHB4O1xuICAgIG1heC1oZWlnaHQ6IDE1MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbi5wcm9maWxlX2ltZyBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBtaW4taGVpZ2h0OiAxNTBweDtcbiAgICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLnByb2ZpbGVfaW1nLmVkaXQge1xuICAgIGJhY2tncm91bmQ6ICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5wcm9maWxlX2ltZyBhIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xufVxuXG4ucHJvZmlsZV9pbWcgLmljb24tZWRpdF9wcm9maWxlIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5iYWNrX2Fycm93IHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLmJvdHRvbV9hcnJvdyB7XG4gICAgcGFkZGluZzogMTFweCAwO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcbiAgICBib3gtc2hhZG93OiBpbnNldCAwcHggMXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLXRvcDogMTQ4cHg7XG59XG5cbi5iYWNrX2Fycm93IHNwYW4ge1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNHB4O1xufVxuXG4uYnRuX3N0eWxlLnNhdmVfYnRuIHtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbmZvb3RlciB7XG4gICAgcGFkZGluZy1ib3R0b206IDI2cHg7XG59XG5cbi5mb290ZXJfaW5uZXIge1xuICAgIHBhZGRpbmc6IDIwcHggMjVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xufVxuXG4uZm9vdGVyX2xvZ28ge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmZvb3Rlcl9sb2dvIGEgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDc4cHg7XG59XG5cbi5mb290ZXJfbWVudSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9vdGVyX21lbnUgdWwge1xuICAgIG1hcmdpbi10b3A6IDdweDtcbn1cblxuLmZvb3Rlcl9tZW51IHVsIGxpIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luOiAwIDI3cHggMCAwO1xufVxuXG4uZm9vdGVyX21lbnUgdWwgYSB7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5mb290ZXJfbWVudSB1bCBhOmhvdmVyIHtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmNvcHlyaWdodF90ZXh0IHtcbiAgICBtYXJnaW4tdG9wOiAxM3B4O1xufVxuXG4uY29weXJpZ2h0X3RleHQgcCB7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbWFyZ2luOiAwO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4vKiBndWFyZGlucyBwYWdlIGNzcyAqL1xuLnByb2ZpbGVfbGlzdCAubGlzdF9ib3gge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXgtd2lkdGg6IDEyMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTNweDtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4OyBcbn1cblxuLnBlcnNvbmFsX2RldGFpbHMgLm1hcmdpbl90b3Age1xuICAgIG1hcmdpbi10b3A6IDM4cHg7XG59XG5cbi5wcm9maWxlX2xpc3QgLmxpc3RfYm94IC5wcm9maWxlX2ltZyB7XG4gICAgbWF4LXdpZHRoOiAxMzZweDtcbiAgICBtYXgtaGVpZ2h0OiAxMDBweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmbG9hdDogbGVmdDtcbn1cblxuLnByb2ZpbGVfbGlzdCAubGlzdF9ib3ggLnByb2ZpbGVfaW1nIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4ucHJvZmlsZV9saXN0IC5saXN0X2JveCAucHJvZmlsZV90ZXh0IHtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ucHJvZmlsZV9saXN0IC5saXN0X2JveCAucHJvZmlsZV90ZXh0IGgzIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLnByb2ZpbGVfbGlzdCAubGlzdF9ib3ggLnByb2ZpbGVfdGV4dCBwIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMTRweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uYWN0aXZpdHkgLm1lZGlhLWxlZnQge1xuICAgIG1heC13aWR0aDogMjMwcHg7XG4gICAgbWF4LWhlaWdodDogMjAwcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uYWN0aXZpdHkgLm1lZGlhLWxlZnQgLnNoYXJlX2ljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDEycHg7XG4gICAgcmlnaHQ6IDE0cHg7XG59XG5cbi5hY3Rpdml0eSAubWVkaWEtbGVmdCBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5hY3Rpdml0eSAubWVkaWEtYm9keSB7XG4gICAgcGFkZGluZy1sZWZ0OiAyNnB4O1xufVxuXG4ubGlzdCBwIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLmxpc3QgcCBpbWcge1xuICAgIG1hcmdpbjogMCA4cHggMCAwO1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbi5hY3Rpdml0eSAubWVkaWEtYm9keSBoMyB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDFlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uYWN0aXZpdHkgLm1lZGlhLWJvZHkgaDIge1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xufVxuLnJlY29tZW5kX2ltYWdlc3tcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLnJlY29tZW5kX2ltYWdlcyBhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5yZWNvbWVuZF9pbWFnZXMgYTpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5yZWNvbWVuZF9pbWFnZXMgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDIxcHg7XG59XG5cbi5yZWNvbWVuZF9idG4ge1xuICAgIG1hcmdpbi10b3A6IDI4cHg7XG59XG5cbi5yZWNvbWVuZF9idG4gYSB7XG4gICAgbWluLXdpZHRoOiAxMDRweDtcbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBjb2xvcjogI0YxNUMyMDtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRjE1QzIwO1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBwYWRkaW5nOiAwIDhweDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4ucmVjb21lbmRfYnRuIGE6aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMTVDMjA7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5kZXRhaWxzX291dGVyIHtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG4uZGV0YWlsc19vdXRlciAucm93IHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG59XG4uZGV0YWlsc19vdXRlciAubGlzdHtcbiAgICBtYXJnaW4tdG9wOiA5cHg7XG59XG5cblxuLmRldGFpbHNfb3V0ZXIgLmxpc3Rfb3V0ZXIge1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5hY3Rpdml0eSB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0U1RTlFQztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLnJhdGluZ19ib3gge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDlweDtcbiAgICByaWdodDogMjBweDtcbn1cblxuLnJhdGluZ19ib3ggYSB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5yb3cuYm90dG9tX21hcmdpbiB7XG4gICAgbWFyZ2luLXRvcDogMjZweDtcbn1cblxuLmZvcm0tZ3JvdXAgLmFkZF9jaGlsZCB7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbjogMTNweCAwIDAgMTdweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5mb3JtLWdyb3VwIC5hZGRfY2hpbGQgLmljb24tcGx1cyB7XG4gICAgY29sb3I6ICM3NzdDRUE7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbi1yaWdodDogOHB4O1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbi5tZWRpYS5hY2NvdW50X25hbWUge1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBwYWRkaW5nOiAyMXB4IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNnB4O1xufVxuXG4ubWVkaWEuYWNjb3VudF9uYW1lIC5tZWRpYS1sZWZ0IHtcbiAgICBtYXgtd2lkdGg6IDQ2cHg7XG4gICAgbWF4LWhlaWdodDogNDZweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5tZWRpYS5hY2NvdW50X25hbWUgLm1lZGlhLWxlZnQgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ubWVkaWEuYWNjb3VudF9uYW1lIC5tZWRpYS1ib2R5IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDI0cHg7XG59XG5cbi5tZWRpYS5hY2NvdW50X25hbWUgLm1lZGlhLWJvZHkgaDQge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4ubWVkaWEuYWNjb3VudF9uYW1lIC5tZWRpYS1ib2R5IHAge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbjogM3B4IDAgMCAwO1xufVxuXG4uZm9ybS1ncm91cC5hZGRfaW50ZXJlc3RzIC5pbnB1dF9vdXRlciB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0U1RTlFQztcbiAgICBwYWRkaW5nOiAxNHB4O1xufVxuXG4uZm9ybS1ncm91cC5hZGRfaW50ZXJlc3RzIC5pbnB1dF9vdXRlciBhIHtcbiAgICBiYWNrZ3JvdW5kOiAjNzc3Q0VBO1xuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgbWluLXdpZHRoOiAxMTlweDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBtaW4taGVpZ2h0OiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogMC44cztcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4uZm9ybS1ncm91cC5hZGRfaW50ZXJlc3RzIC5pbnB1dF9vdXRlciBhOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjk2ZWI5O1xufVxuXG4uZm9ybS1ncm91cC5hZGRfaW50ZXJlc3RzIC5pbnB1dF9vdXRlcjo6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IG5vbmU7XG59XG5cbi5mb3JtLWdyb3VwLmFkZF9pbnRlcmVzdHMgLmlucHV0X291dGVyIC5pY29uLWNsb3NlIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgIG1hcmdpbi1yaWdodDogMTNweDtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbi10b3A6IDdweDtcbn1cblxuLyogc3RhcnQgc2VhcmNoIHBhZ2UgY3NzICovXG4uc2VhcmNoX3BvcnRpb24ge1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAtMXB4IDBweCAjRTVFOUVDO1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBhZGRpbmc6IDEwcHggNDVweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmxlZnRfc2VhcmNoIHtcbiAgICBmbG9hdDogbGVmdDtcbn1cblxuXG5cbi5sZWZ0X3NlYXJjaCBpbnB1dDo6cGxhY2Vob2xkZXIge1xuICAgIGNvbG9yOiAjRTVFOUVDO1xufVxuXG4ubGVmdF9zZWFyY2ggaW5wdXQ6Oi1tb3otcGxhY2Vob2xkZXIge1xuICAgIGNvbG9yOiAjRTVFOUVDO1xufVxuXG4ubGVmdF9zZWFyY2ggaW5wdXQ6Oi13ZWJraXQtcGxhY2Vob2xkZXIge1xuICAgIGNvbG9yOiAjRTVFOUVDO1xufVxuXG4ucmlnaHRfc2VhcmNoIHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgcGFkZGluZy10b3A6IDZweDtcbn1cblxuLnJpZ2h0X3NlYXJjaCBhIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgY29sb3I6ICNDNUNFRDY7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbjogMCAwIDAgMTFweDtcbn1cblxuLnJpZ2h0X3NlYXJjaCBhLmFjdGl2ZSB7XG4gICAgY29sb3I6ICNGMTVDMjA7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAyOHB4O1xuICAgIGhlaWdodDogMTdweDtcbiAgICBtYXJnaW46IDAgMCAwIDEycHg7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIGlucHV0IHtcbiAgICBvcGFjaXR5OiAwO1xuICAgIHdpZHRoOiAwO1xuICAgIGhlaWdodDogMDtcbn1cblxuLnN3aXRjaC5ub3RpZmljYXRpb24gLnNsaWRlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2NjYztcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC40cztcbiAgICB0cmFuc2l0aW9uOiAuNHM7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIC5zbGlkZXI6YmVmb3JlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBoZWlnaHQ6IDEycHg7XG4gICAgd2lkdGg6IDEycHg7XG4gICAgbGVmdDogNHB4O1xuICAgIGJvdHRvbTogM3B4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLjRzO1xuICAgIHRyYW5zaXRpb246IC40cztcbn1cblxuLnN3aXRjaC5ub3RpZmljYXRpb24gaW5wdXQ6Y2hlY2tlZCsuc2xpZGVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjE1QzIwO1xufVxuXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiBpbnB1dDpmb2N1cysuc2xpZGVyIHtcbiAgICBib3gtc2hhZG93OiAwIDAgMXB4ICNGMTVDMjA7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIGlucHV0OmNoZWNrZWQrLnNsaWRlcjpiZWZvcmUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDlweCk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWCg5cHgpO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCg5cHgpO1xufVxuXG4vKiBSb3VuZGVkIHNsaWRlcnMgKi9cbi5zd2l0Y2gubm90aWZpY2F0aW9uIC5zbGlkZXIucm91bmQge1xuICAgIGJvcmRlci1yYWRpdXM6IDM0cHg7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIC5zbGlkZXIucm91bmQ6YmVmb3JlIHtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5jYXRlZ29yeV9saXN0IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMTBweCA0NnB4IDE3cHg7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMHB4IC0xcHggMHB4ICNFNUU5RUM7XG59XG5cbi5jYXRlZ29yeV9saXN0IGxpIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG5cbn1cblxuLmNhdGVnb3J5X2xpc3QgbGkgYSB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgcGFkZGluZzogMCAxNXB4O1xuICAgIG1pbi1oZWlnaHQ6IDM2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDM2cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogN3B4O1xuICAgIHRyYW5zaXRpb246IDAuOHM7XG59XG5cbi5jYXRlZ29yeV9saXN0IGxpIGE6aG92ZXIsXG4uY2F0ZWdvcnlfbGlzdCBsaSBhLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYjIwNjtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXItY29sb3I6ICNmZmIyMDY7XG59XG5cbi5zZWFyY2hfYWN0aXZpdHkge1xuICAgIHBhZGRpbmc6IDAgMCAwIDQ2cHg7XG59XG5cbi5zZWFyY2hfYWN0aXZpdHkgLmxpc3RfbGVmdCB7XG4gICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLnNlYXJjaF9hY3Rpdml0eSAuYWN0aXZpdHkge1xuICAgIG1hcmdpbi1ib3R0b206IDhweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5zZWFyY2hfYWN0aXZpdHkgLmFjdGl2aXR5Omxhc3QtY2hpbGQge1xuICAgIG1hcmdpbjogMDtcbn1cblxuLnNlYXJjaF9hY3Rpdml0eSAubWFwX3NlY3Rpb24gaW1nIHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbi5zZWxlY3RfYXZ0YXIgcCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogI0M1Q0VENjtcbiAgICBtYXJnaW46IDE0cHggMCAwIDA7XG59XG5cbi5zZWxlY3RfYXZ0YXIge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWF4LXdpZHRoOiAxODBweDtcbn1cblxuLnNlbGVjdF9hdnRhciBhIHtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5zZWxlY3RfYXZ0YXIgYSBpbWcge1xuICAgIG1hcmdpbjogMCAwIDAgNXB4O1xufVxuXG5oZWFkZXIgLm5hdmJhciB7XG4gICAgcGFkZGluZzogMDtcbn1cblxuaGVhZGVyIC5uYXZiYXItbGlnaHQgLm5hdmJhci1uYXYgLm5hdi1saW5rIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBwYWRkaW5nOiA5cHggMDtcbiAgICBtYXJnaW4tcmlnaHQ6IDI1cHg7XG59XG5cbmhlYWRlciAubmF2YmFyLW5hdiAuYnRuX3N0eWxlIHtcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2Fucyc7XG4gICAgYm94LXNoYWRvdzogMHB4IDZweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGNvbG9yOiAjZmZmO1xufVxuXG5cbi8qIHN0YXJ0IGFjdGl2aXR5X2NhdGVnb3J5IGNzcyAqL1xuLmFjdGl2aXR5X2NhdGVnb3J5IHtcbiAgICBwYWRkaW5nOiAxNDBweCAwIDU0cHggMDtcbn1cblxuLmFjdGl2aXR5X2NhdGVnb3J5IGgyIHtcbiAgICBmb250LWZhbWlseTogTmFudW0gUGVuO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAyOHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiAjM0QzNzg5O1xufVxuXG4uYWN0aXZpdHlfY2F0ZWdvcnlfbGlzdCB7XG4gICAgcGFkZGluZy10b3A6IDQ4cHg7XG59XG5cbi5hY3Rpdml0eV9jYXRlZ29yeSAuY2F0ZWdvcnlfbGlzdF9ib3gge1xuICAgIG1heC1oZWlnaHQ6IDI3NnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmNhdGVnb3J5X2xpc3RfYm94IHtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmFjdGl2aXR5X2NhdGVnb3J5IC5jYXRlZ29yeV9saXN0X2JveCBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuXG4uY2F0ZWdvcnlfbGlzdF9ib3ggLmNhdGVnb3J5X3RpdGxlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAyM3B4O1xuICAgIGxlZnQ6IDMzcHg7XG59XG5cbi5jYXRlZ29yeV9saXN0X2JveCAuY2F0ZWdvcnlfdGl0bGUgcCB7XG4gICAgZm9udC1mYW1pbHk6IE5hbnVtIFBlbjtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbn1cblxuLmNhdGVnb3J5X2xpc3RfYm94IC5jYXRlZ29yeV90aXRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMjNweDtcbiAgICBsZWZ0OiAzM3B4O1xuICAgIHotaW5kZXg6IDE7XG59XG5cbi8qIHN0YXJ0IG5vdGlmaWNhdGlvbnMgcGFnZSBjc3MgKi9cbi5ub3RpZmljYXRpb25zIGgzIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLm5vdGlmaWNhdGlvbnMgLnN3aXRjaC5ub3RpZmljYXRpb24ge1xuICAgIG1hcmdpbjogMTRweCAwIDAgMDtcbn1cblxuLm5vdGlmaWNhdGlvbnMgLnN3aXRjaC5ub3RpZmljYXRpb24gaW5wdXQ6Y2hlY2tlZCsuc2xpZGVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uY2gtcmFkaW8ge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC40cztcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC40cztcbn1cblxuLmNoLXJhZGlvICoge1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNHM7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNHM7XG59XG5cbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0sXG4uY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgb3BhY2l0eTogMDtcbn1cblxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkK3NwYW46YmVmb3JlLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkfnNwYW46YmVmb3JlLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkK3NwYW46YmVmb3JlLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkfnNwYW46YmVmb3JlIHtcbiAgICBib3JkZXItY29sb3I6ICM0Yjk3ZDc7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzRiOTdkNztcbn1cblxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkK3NwYW4gLmNoLWljb24sXG4uY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWR+c3BhbiAuY2gtaWNvbixcbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwicmFkaW9cIl06Y2hlY2tlZCtzcGFuIC5jaC1pY29uLFxuLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkfnNwYW4gLmNoLWljb24ge1xuICAgIG9wYWNpdHk6IDE7XG59XG5cbi5jaC1yYWRpbyBzcGFuIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBwYWRkaW5nLWxlZnQ6IDMwcHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5jaC1yYWRpbyBzcGFuOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxcHg7XG4gICAgbGVmdDogMXB4O1xuICAgIHdpZHRoOiAxNXB4O1xuICAgIGhlaWdodDogMTVweDtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGluaGVyaXQ7XG4gICAgdHJhbnNpdGlvbjogaW5oZXJpdDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQzVDRUQ2O1xuICAgIGJvcmRlci1yYWRpdXM6IDFweDtcbn1cblxuLmNoLXJhZGlvIHNwYW4gLmNoLWljb24ge1xuICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogNXB4O1xuICAgIGZvbnQtc2l6ZTogOXB4O1xuICAgIG9wYWNpdHk6IDA7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDApO1xuICAgIGxpbmUtaGVpZ2h0OiAxN3B4O1xufVxuXG4uY2hlY2tfbGlzdCB7XG4gICAgbWFyZ2luLXRvcDogMjNweDtcbn1cblxuLmNoZWNrX2xpc3QgLmNoLXJhZGlvIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXJpZ2h0OiA0NHB4O1xufVxuXG4udGV4dGFyZWFfbGVuZ3RoIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAtNHB4O1xuICAgIHJpZ2h0OiAxMnB4O1xufVxuXG4udGV4dGFyZWFfbGVuZ3RoIHAge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG59XG5cblxuXG4vKiBWQUxJREFUSU9OIEVSUk9SICAqL1xuLmZvcm0tZXJyb3ItbXNnIHtcbiAgICBjb2xvcjogcmVkO1xufVxuXG5pbnB1dC5uZy1pbnZhbGlkLm5nLXRvdWNoZWQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbn1cblxuc2VsZWN0Lm5nLWludmFsaWQubmctdG91Y2hlZCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xufVxuXG5cblxuXG5cbi8qIG1hdGZvcm0gY2hpcHMgY3NzICYgbmctYXV0b2NvbXBsZXRlKi9cbi5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlciAubmctYXV0b2NvbXBsZXRle1xuICAgIHdpZHRoOiA0MzhweCAhaW1wb3J0YW50O1xufVxuXG4uZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXIgLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyIC5tYXQtY2hpcC5tYXQtc3RhbmRhcmQtY2hpcCB7XG4gICAgYmFja2dyb3VuZDogI0YxNUMyMDtcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICAgIG1pbi13aWR0aDogMTE5cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgbWluLWhlaWdodDogMzBweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gICAgdHJhbnNpdGlvbjogMC44cztcbiAgICBtYXJnaW46IDAgMCAwIDExcHg7XG4gICAgcGFkZGluZzogMCAxNXB4O1xufVxuXG4uZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXIgLm1hdC1zdGFuZGFyZC1jaGlwIC5tYXQtY2hpcC1yZW1vdmUubWF0LWljb24ge1xuICAgIGZvbnQtc2l6ZTogMDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHdpZHRoOiAwO1xuICAgIGhlaWdodDogYXV0bztcbiAgICByaWdodDogMjdweDtcbn1cblxuLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyIC5tYXQtc3RhbmRhcmQtY2hpcCAubWF0LWNoaXAtcmVtb3ZlLm1hdC1pY29uOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcXGU5MDBcIjtcbiAgICBmb250LWZhbWlseTogJ2ljb21vb24nICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGNvbG9yOiAjRkZGRkZGO1xufVxuXG4uZGV0YWlsc19mb3JtIC5tYXQtZm9ybS1maWVsZC1pbmZpeCB7XG4gICAgYm9yZGVyLXRvcDogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG4uZGV0YWlsc19mb3JtIC5tYXQtZm9ybS1maWVsZC1hcHBlYXJhbmNlLWxlZ2FjeSAubWF0LWZvcm0tZmllbGQtdW5kZXJsaW5lIHtcbiAgICBoZWlnaHQ6IDA7XG4gICAgLyogYmFja2dyb3VuZC1jb2xvcjogI2YwMDsgKi9cbn1cblxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgcGFkZGluZzogMDtcbn1cblxuLmRlbW8tY2hpcC1saXN0Lm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtbGVnYWN5IC5tYXQtZm9ybS1maWVsZC11bmRlcmxpbmUge1xuICAgIGhlaWdodDogMDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtbGVnYWN5IC5tYXQtZm9ybS1maWVsZC13cmFwcGVyIHtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlciAuZGVtby1jaGlwLWxpc3Qge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyIC5kZW1vLWNoaXAtbGlzdCAubWF0LWZvcm0tZmllbGQtd3JhcHBlciB7XG4gICAgcGFkZGluZy1ib3R0b206IDAgIWltcG9ydGFudDtcbn1cblxuLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyIC5kZW1vLWNoaXAtbGlzdDo6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMThweDtcbiAgICBoZWlnaHQ6IDRweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm15X2NsYXNzIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuLnByb2ZpbGVfaW1ne1xuICAgIGJhY2tncm91bmQ6ICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubm90aWZpY2F0aW9ucyB7XG4gICAgbWFyZ2luLXRvcDogMjhweDtcbn1cblxuQG1lZGlhKG1heC13aWR0aDo3NjdweCkge1xuICAgIC5teV9jbGFzcyB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxN3B4O1xuICAgICAgICBtYXJnaW46IC0yOXB4IDAgMCAwO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjE1QzIwO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgICBtaW4td2lkdGg6IDQ3cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgICAgbWluLWhlaWdodDogMTZweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDM2cHg7XG4gICAgfVxuICAgIC5idG5fc3R5bGUge1xuICAgICAgICB3aWR0aDogMTAwcHg7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDI2cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNnB4O1xuICAgIH1cbiAgICAuc2lkZV9iYXJ7XG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBsZWZ0OiAtMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbiAgICAgICAgaGVpZ2h0OiAxMDB2aDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1heC13aWR0aDogMzEwcHg7XG4gICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogMC41cztcbiAgICAgICAgdHJhbnNpdGlvbjogMC41cztcbiAgICAgICAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcbiAgICAgICAgei1pbmRleDogMTtcbiAgICAgICAgcGFkZGluZzowIDI2cHg7XG4gICAgfVxuICAgIC5zaWRlX2Jhci5zaWRlX2Jhcl9oaWRle1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xuICAgICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMTBweCAxNnB4IDExcHggcmdiYSgyNCwgODcsIDE3MSwgMC4wOCk7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCAxNnB4IDExcHggcmdiYSgyNCwgODcsIDE3MSwgMC4wOCk7XG4gICAgfVxuICAgIC5NYWluQ29udGVudHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC5wYWdlX2hlYWRpbmcgaDJ7XG4gICAgICAgIGZvbnQtc2l6ZTogMjFweDtcbiAgICB9XG4gICAgLmNvbnRlbnRfcmlnaHQgaDJ7XG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICB9XG4gICAgLnByb2ZpbGVfbGlzdHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgICAucHJvZmlsZV9pbWd7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDE1MHB4O1xuICAgIH1cbiAgICAucHJvZmlsZV9pbWcgaW1ne1xuICAgICAgICBtaW4taGVpZ2h0OiAxNTBweDtcbiAgICB9XG4gICAgLmJ0bl9zdHlsZS5zYXZlX2J0bi5jdXJzb3J7XG4gICAgICAgIG1hcmdpbi10b3A6IDIxcHg7XG4gICAgfVxuICAgIC5ib3R0b21fYXJyb3cgLmJ0bl9zdHlsZS5zYXZlX2J0bi5jdXJzb3J7XG4gICAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgfVxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/parent/parent-profile/parent-profile.component.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/pages/parent/parent-profile/parent-profile.component.ts ***!
    \*************************************************************************/

  /*! exports provided: ParentProfileComponent */

  /***/
  function srcAppPagesParentParentProfileParentProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ParentProfileComponent", function () {
      return ParentProfileComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_core_services_user_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/core/services/user-data.service */
    "./src/app/core/services/user-data.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/core/services/api.service.service */
    "./src/app/core/services/api.service.service.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/cdk/keycodes */
    "./node_modules/@angular/cdk/esm2015/keycodes.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");
    /* harmony import */


    var ng2_validation__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ng2-validation */
    "./node_modules/ng2-validation/dist/index.js");
    /* harmony import */


    var ng2_validation__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_9__);
    /* harmony import */


    var _core_models_child_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../core/models/child.model */
    "./src/app/core/models/child.model.ts");
    /* harmony import */


    var _core_models_user_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../core/models/user.model */
    "./src/app/core/models/user.model.ts");
    /* harmony import */


    var src_app_core_components_header_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! src/app/core/components/header/header.component */
    "./src/app/core/components/header/header.component.ts");

    var ParentProfileComponent = /*#__PURE__*/function () {
      function ParentProfileComponent(userdataservice, apiservice, snack, router, // private confirmDialogService: ConfirmDialogService,
      ngxLoader) {
        _classCallCheck(this, ParentProfileComponent);

        this.userdataservice = userdataservice;
        this.apiservice = apiservice;
        this.snack = snack;
        this.router = router;
        this.ngxLoader = ngxLoader;
        this.kid = new _core_models_child_model__WEBPACK_IMPORTED_MODULE_10__["Child"]();
        this.user = new _core_models_user_model__WEBPACK_IMPORTED_MODULE_11__["User"]();
        this.kids = [];
        this.isSideBar = true;
        this.favourites = [];
        this.profileProgressResponse = {};
        this.fileData = null; // formData = new FormData();

        this.isProfile = true;
        this.profile = 'active';
        this.isSetting = false;
        this.setting = '';
        this.isGuardian = false;
        this.guardian = '';
        this.isInvite = false;
        this.isChildren = false;
        this.children = '';
        this.isAddChild = false;
        this.isEditChildBtn = false;
        this.isAddChildBtn = false;
        this.isFavourite = false;
        this.favourite = '';
        this.isNotification = false;
        this.notification = '';
        this.isFriend = false;
        this.friend = '';
        this.isFeedback = false;
        this.feedback = ''; // ---------------autucomplete-------------  

        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = false;
        this.addGuardianData = {
          firstName: '',
          email: '',
          personalNote: '',
          parentId: ''
        };
        this.categoryIds = [];
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_7__["COMMA"]];
        this.message = 'Updated Successfully';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        this.updateResponse = {};
        this.userData = {};
        this.resetPasswordResponse = {};
        this.getTagByCategoryResponse = {};
        this.keyword = 'name';
        this.SelectedCategories = [];
        this.resetPasswordData = {
          oldPassword: '',
          newPassword: ''
        };
        this.tellFriendData = {
          parentName: '',
          fullName: '',
          email: '',
          personalNote: ''
        };
        this.giveFeedbackData = {
          id: '',
          name: '',
          email: '',
          feedback: ''
        }; // ------------------------------------

        this.isLoading = false;
        this.tags = [];
        var retrievedObject = localStorage.getItem('userData');
        this.userData = JSON.parse(retrievedObject);
        var config = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarConfig"]();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
      } // showDialog() {
      //   this.confirmDialogService.confirmThis("Are you sure to delete?", function () {
      //     alert("Yes clicked");
      //   }, function () {
      //     alert("No clicked");
      //   })
      // }


      _createClass(ParentProfileComponent, [{
        key: "sideBar",
        value: function sideBar() {
          this.isSideBar = !this.isSideBar;
        }
      }, {
        key: "back",
        value: function back() {
          this.router.navigate(['parent/search']);
        }
      }, {
        key: "refreshPage",
        value: function refreshPage() {
          this.ngxLoader.start();
          this.kid = new _core_models_child_model__WEBPACK_IMPORTED_MODULE_10__["Child"]();
          this.kid.interestInfo = [];
          this.tags = [];
          this.ngxLoader.stop();
        }
      }, {
        key: "selected",
        value: function selected(event) {
          this.kid.interestInfo.push(event.option.value);
        }
      }, {
        key: "remove",
        value: function remove(indx) {
          this.kid.interestInfo.splice(indx, 1);
          console.log(this.kid.interestInfo);
        }
      }, {
        key: "selectEvent",
        value: function selectEvent(item) {
          this.kid.interestInfo.push(item);
          console.log('selectEvent', item);
          console.log('intrests', this.kid.interestInfo);
        }
      }, {
        key: "onChangeSearch",
        value: function onChangeSearch(key) {
          var _this3 = this;

          this.isLoading = true;
          this.ngxLoader.start();
          this.tags = [];
          this.apiservice.searchTag(key).subscribe(function (res) {
            _this3.tags = res;
            console.log('searchTag list categories', _this3.tags);

            _this3.ngxLoader.stop();

            _this3.isLoading = false;
          }); // fetch remote data from here
          // And reassign the 'data' which is binded to 'data' property.
        }
      }, {
        key: "onFocused",
        value: function onFocused(e) {
          console.log('onFocused', e); // do something when input is focused
        }
      }, {
        key: "onProfile",
        value: function onProfile() {
          this.isProfile = true;
          this.profile = 'active';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
          return this.getParentById();
        }
      }, {
        key: "onGuardian",
        value: function onGuardian(id) {
          var _this4 = this;

          this.ngxLoader.start();
          this.apiservice.getGuardianByParentId(id).subscribe(function (res) {
            _this4.getGuardianResponse = res;

            _this4.ngxLoader.stop();

            console.log('guardian list>>', _this4.getGuardianResponse);
          });
          this.ngxLoader.stop();
          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = true;
          this.guardian = 'active';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
        } // ------------------guardian invite-----------------

      }, {
        key: "onGuardianAdd",
        value: function onGuardianAdd() {
          this.isGuardian = false;
          this.isInvite = true;
        } // --------------------------------------------------

      }, {
        key: "onChildren",
        value: function onChildren(id) {
          var _this5 = this;

          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = true;
          this.children = 'active';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
          this.ngxLoader.start();
          this.apiservice.getChildByParentId(id).subscribe(function (res) {
            _this5.kids = res;
            console.log('kids', _this5.kids);

            _this5.ngxLoader.stop();

            console.log('get child res from model', res);

            if (_this5.userData === null || _this5.userData === undefined) {
              _this5.router.navigate(['/login']);

              var msg = 'Please Login First!';

              _this5.snack.open(msg, 'OK', {
                duration: 5000
              });
            }

            _this5.ngxLoader.stop();
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "onAddChild",
        value: function onAddChild() {
          this.isAddChildBtn = true;
          this.isChildren = false;
          this.isAddChild = true;
          this.isEditChildBtn = false;
          this.kid = new _core_models_child_model__WEBPACK_IMPORTED_MODULE_10__["Child"]();
          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = 'active';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isEditChildBtn = false;
        }
      }, {
        key: "onEditChild",
        value: function onEditChild(child) {
          this.kid = child;
          this.isChildren = false;
          this.isAddChild = true;
          this.isAddChildBtn = false;
          this.isEditChildBtn = true;
          console.log('kid detail', this.kid);
        }
      }, {
        key: "getFav",
        value: function getFav(id) {
          var _this6 = this;

          this.apiservice.getFavouriteByParentId(id).subscribe(function (res) {
            _this6.favourites = res;

            _this6.ngxLoader.stop();
          });
          this.ngxLoader.stop();
          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = true;
          this.favourite = 'active';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
        }
      }, {
        key: "onNotification",
        value: function onNotification() {
          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = true;
          this.notification = 'active';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
        }
      }, {
        key: "onFriend",
        value: function onFriend() {
          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = true;
          this.friend = 'active';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
        }
      }, {
        key: "activeDeactiveUser",
        value: function activeDeactiveUser(id, isActivated) {
          var _this7 = this;

          this.ngxLoader.start();
          this.apiservice.activeDeactiveUser(id, !isActivated).subscribe(function (res) {
            _this7.activeDeactiveResponse = res;

            _this7.ngxLoader.stop();

            console.log('res from model', _this7.activeDeactiveResponse);

            if (_this7.activeDeactiveResponse.isSuccess === true && _this7.activeDeactiveResponse.data.isActivated === false) {
              var msg = 'acount Deactivated!';

              _this7.snack.open(msg, 'OK', {
                duration: 4000
              });

              return _this7.getParentById();
            } else {
              if (_this7.activeDeactiveResponse.isSuccess === true && _this7.activeDeactiveResponse.data.isActivated === true) {
                var _msg = 'acount Activated!';

                _this7.snack.open(_msg, 'OK', {
                  duration: 4000
                });

                return _this7.getParentById();
              } else {
                var _msg2 = 'somthing went wrong!';

                _this7.snack.open(_msg2, 'OK', {
                  duration: 4000
                });
              }
            }
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "tellFriend",
        value: function tellFriend(parentName) {
          var _this8 = this;

          this.tellFriendData.parentName = parentName;
          this.ngxLoader.start();
          this.apiservice.tellFriend(this.tellFriendData).subscribe(function (res) {
            _this8.tellFriendResponse = res;

            _this8.ngxLoader.stop();

            console.log('res from model', _this8.tellFriendResponse);

            if (_this8.tellFriendResponse.isSuccess === true) {
              _this8.snack.open(_this8.tellFriendResponse.message, 'OK', {
                duration: 4000
              });
            } else {
              _this8.snack.open(_this8.tellFriendResponse.error, 'OK', {
                duration: 4000
              });
            }
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "onFeedback",
        value: function onFeedback() {
          this.isProfile = false;
          this.profile = '';
          this.isSetting = false;
          this.setting = '';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = true;
          this.feedback = 'active';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
        }
      }, {
        key: "giveFeedback",
        value: function giveFeedback(id) {
          var _this9 = this;

          this.giveFeedbackData.id = id;
          this.ngxLoader.start();
          this.apiservice.giveFeedback(this.giveFeedbackData).subscribe(function (res) {
            _this9.giveFeedbackResponse = res;

            _this9.ngxLoader.stop();

            console.log('res from model', _this9.giveFeedbackResponse);

            if (_this9.giveFeedbackResponse.isSuccess === true) {
              var msg = 'Thankyou For Feedback!';

              _this9.snack.open(msg, 'OK', {
                duration: 4000
              });
            } else {
              var _msg3 = 'something went wrong, please try again Later!';

              _this9.snack.open(_msg3, 'OK', {
                duration: 4000
              });
            }
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "onSetting",
        value: function onSetting() {
          this.isProfile = false;
          this.profile = '';
          this.isSetting = true;
          this.setting = 'active';
          this.isGuardian = false;
          this.guardian = '';
          this.isChildren = false;
          this.children = '';
          this.isFavourite = false;
          this.favourite = '';
          this.isNotification = false;
          this.notification = '';
          this.isFriend = false;
          this.friend = '';
          this.isFeedback = false;
          this.feedback = '';
          this.isInvite = false;
          this.isAddChild = false;
          this.isEditChildBtn = false;
        } // remove(indx): void {
        //   this.kid.interestInfo.splice(indx, 1);
        // }

      }, {
        key: "parentImageSelect",
        value: function parentImageSelect(event, id) {
          var _this10 = this;

          var formData = new FormData();
          this.fileData = event.target.files[0];
          formData.append('image', this.fileData); // --------------------preview image before upload ------------------------

          if (event.target.files.length === 0) return;
          var reader = new FileReader();
          this.imagePath = event.target.files;
          reader.readAsDataURL(event.target.files[0]);

          reader.onload = function (_event) {
            _this10.parentImgURL = reader.result;
          };

          var mimeType = event.target.files[0].type;

          if (mimeType.match(/image\/*/) == null) {
            this.msg = " only images are supported";
            return;
          } // -------------------------------------------------------------------------------


          this.ngxLoader.start();
          this.apiservice.uploadUserImage(id, formData).subscribe(function (res) {
            _this10.parentImgResponse = res;
            _this10.user.avatarImages = _this10.parentImgResponse.data.avatarImages;

            _this10.ngxLoader.stop();

            console.log('parent IMAGE Response from server = >>> ', _this10.parentImgResponse);

            if (_this10.parentImgResponse.isSuccess === true) {} else {
              var msg = "Something Went Wrong!";

              _this10.snack.open(msg, 'OK', {
                duration: 4000
              });
            }
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "childImageSelect",
        value: function childImageSelect(event) {
          var _this11 = this;

          var formData = new FormData();
          this.fileData = event.target.files[0];
          formData.append('image', this.fileData); // --------------------preview image before upload ------------------------

          if (event.target.files.length === 0) return;
          var reader = new FileReader();
          this.imagePath = event.target.files;
          reader.readAsDataURL(event.target.files[0]);

          reader.onload = function (_event) {
            _this11.childImgURL = reader.result;
          };

          var mimeType = event.target.files[0].type;

          if (mimeType.match(/image\/*/) == null) {
            this.msg = " only images are supported";
            return;
          } // -------------------------------------------------------------------------------


          this.ngxLoader.start();
          this.apiservice.getPicUrl(formData).subscribe(function (res) {
            _this11.childImgResponse = res;
            _this11.childImageURl = res.data;
            _this11.kid.avtar = _this11.childImageURl;

            _this11.ngxLoader.stop();

            console.log('child IMAGE Response from server = >>> ', _this11.childImgResponse);

            if (_this11.childImgResponse.isSuccess === true) {} else {
              var msg = "Something Went Wrong!";

              _this11.snack.open(msg, 'OK', {
                duration: 4000
              });
            }
          });
        }
      }, {
        key: "updateParent",
        value: function updateParent(parent) {
          var _this12 = this;

          this.user.avatarImages;
          this.ngxLoader.start();
          this.apiservice.updateParent(parent._id, parent).subscribe(function (res) {
            _this12.getProfileProgress();

            _this12.headerComponent.getProfileProgress();

            _this12.headerComponent.getUserById();

            _this12.updateResponse = res;

            _this12.ngxLoader.stop();

            console.log('res from model', res);

            if (_this12.updateResponse.isSuccess === true) {
              _this12.snack.open(_this12.message, 'OK', {
                duration: 5000
              });
            } else {
              if (_this12.userData === null || _this12.userData === undefined) {
                _this12.router.navigate(['/login']);

                var msg = 'Please Login First!';

                _this12.snack.open(msg, 'OK', {
                  duration: 5000
                });
              } else {
                var _msg4 = 'Something Went Wrong!';

                _this12.snack.open(_msg4, 'OK', {
                  duration: 5000
                });
              }
            }
          });
        }
      }, {
        key: "resetPassword",
        value: function resetPassword(id) {
          var _this13 = this;

          this.ngxLoader.start();
          this.apiservice.resetPassword(id, this.resetPasswordData).subscribe(function (res) {
            _this13.resetPasswordResponse = res;

            _this13.ngxLoader.stop();

            console.log('res from model', res);

            if (_this13.resetPasswordResponse.isSuccess === true) {
              _this13.snack.open(_this13.resetPasswordResponse.message, 'OK', {
                duration: 5000
              });

              _this13.router.navigate(['/login']);
            } else {
              if (_this13.userData === null || _this13.userData === undefined) {
                _this13.router.navigate(['/login']);

                var msg = 'Please Login First!';

                _this13.snack.open(msg, 'OK', {
                  duration: 5000
                });
              } else {
                if (_this13.resetPasswordResponse.error === 'Old Password Not Match') {
                  _this13.snack.open(_this13.resetPasswordResponse.error, 'OK', {
                    duration: 5000
                  });
                } else {
                  var _msg5 = 'Something Went Wrong!';

                  _this13.snack.open(_msg5, 'OK', {
                    duration: 5000
                  });
                }
              }
            }
          });
        }
      }, {
        key: "addGuardian",
        value: function addGuardian(id) {
          var _this14 = this;

          this.addGuardianData.parentId = id;
          this.ngxLoader.start();
          this.apiservice.addGuardian(this.addGuardianData).subscribe(function (res) {
            _this14.addGuardianResponse = res;

            _this14.ngxLoader.stop();

            console.log('ADD GUARDIAN  res from model', _this14.addGuardianResponse);

            if (_this14.addGuardianResponse.isSuccess === true) {
              _this14.onGuardian(id);

              _this14.snack.open(_this14.addGuardianResponse.message, 'OK', {
                duration: 5000
              });
            } else {
              _this14.snack.open(_this14.addGuardianResponse.error, 'OK', {
                duration: 5000
              });
            }
          });
          this.ngxLoader.stop(); // this.snack.open(msg, 'OK', { duration: 5000 });
        }
      }, {
        key: "addChild",
        value: function addChild(userId) {
          var _this15 = this;

          this.kid.parentId = userId;

          if (this.childImageURl != "" && this.childImageURl != undefined) {
            this.kid.avtar = this.childImageURl;
          }

          if (this.kid.name === '') {
            this.snack.open(this.getChildResponse.error, 'OK', {
              duration: 5000
            });
          } else {
            this.ngxLoader.start();
            this.apiservice.addChild(this.kid).subscribe(function (res) {
              _this15.getChildResponse = res;

              _this15.getProfileProgress();

              _this15.headerComponent.getProfileProgress();

              _this15.headerComponent.getUserById();

              _this15.ngxLoader.stop();

              console.log('get child res from model', res);

              if (_this15.getChildResponse.isSuccess === true) {
                _this15.ngxLoader.start();

                _this15.apiservice.getGuardianByParentId(userId).subscribe(function (res) {
                  _this15.getGuardianResponse = res;

                  _this15.onChildren(userId);

                  _this15.ngxLoader.stop();

                  console.log('guardian list>>', _this15.getGuardianResponse);
                });

                _this15.ngxLoader.stop();

                _this15.snack.open(_this15.getChildResponse.message, 'OK', {
                  duration: 5000
                });
              } else {
                if (_this15.userData === null || _this15.userData === undefined) {
                  _this15.router.navigate(['/login']);

                  var msg = 'Please Login First!';

                  _this15.snack.open(msg, 'OK', {
                    duration: 5000
                  });
                } else {
                  var _msg6 = 'Something Went Wrong!';

                  _this15.snack.open(_this15.getChildResponse.error, 'OK', {
                    duration: 5000
                  });
                }
              }
            });
          }
        }
      }, {
        key: "updateChild",
        value: function updateChild(child, userId) {
          var _this16 = this;

          this.ngxLoader.start();

          if (this.childImageURl != "" && this.childImageURl != undefined) {
            this.kid.avtar = this.childImageURl;
          }

          this.apiservice.updateChild(child._id, child).subscribe(function (res) {
            _this16.getProfileProgress();

            _this16.headerComponent.getProfileProgress();

            _this16.headerComponent.getUserById();

            _this16.getChildResponse = res;

            _this16.ngxLoader.stop();

            console.log('get update child res from model', res);

            if (_this16.getChildResponse.isSuccess === true) {
              // this.onChildren(parentId);
              var msg = 'Child Updated Successfully!';

              _this16.snack.open(msg, 'OK', {
                duration: 5000
              });

              _this16.onChildren(userId);
            } else {
              console.log('err', _this16.getChildResponse.error);

              if (_this16.userData === null || _this16.userData === undefined) {
                _this16.router.navigate(['/login']);

                var _msg7 = 'Please Login First!';

                _this16.snack.open(_msg7, 'OK', {
                  duration: 5000
                });
              } else {
                var _msg8 = 'Something Went Wrong!';

                _this16.snack.open(_msg8, 'OK', {
                  duration: 5000
                });
              }
            }
          });
        }
      }, {
        key: "getParentById",
        value: function getParentById() {
          var _this17 = this;

          this.apiservice.getParentById(this.userData.id).subscribe(function (res) {
            console.log('got parent Data by Id', res);
            _this17.user = res;
          });
        }
      }, {
        key: "getProfileProgress",
        value: function getProfileProgress() {
          var _this18 = this;

          this.ngxLoader.start();
          this.apiservice.getProfileProgress(this.userData.id, this.userData.role).subscribe(function (res) {
            console.log('got profileProgress Data', _this18.profileProgressResponse);
            _this18.profileProgressResponse = res;
            _this18.profileProgress = _this18.profileProgressResponse.data.profileProgress;

            _this18.ngxLoader.stop();
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "removeFav",
        value: function removeFav(favId) {
          var _this19 = this;

          this.ngxLoader.start();
          this.apiservice.deleteFavProgram(favId).subscribe(function (res) {
            _this19.getFav(_this19.userData.id);

            console.log('deleted from fav program >>', res);
          });
          this.ngxLoader.stop();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          if (this.userData === null || this.userData === undefined) {
            this.router.navigate(['/login']);
          }

          this.getProfileProgress();
          this.headerComponent.getProfileProgress();
          this.headerComponent.getUserById();
          this.getParentById();
          this.user.avatarImages = this.userData.avatarImages;
          this.updateForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]),
            addressLine1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            phoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
          });
          var newPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]);
          var confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_9__["CustomValidators"].equalTo(newPassword)]);
          this.resetPasswordForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            oldPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            newPassword: newPassword,
            confirmPassword: confirmPassword
          });
          this.addGuardianForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]),
            personalNote: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
          });
          this.tellFriendForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            parentName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []),
            fullName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]),
            personalNote: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
          });
          this.giveFeedbackForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', []),
            feedback: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
          });
        }
      }]);

      return ParentProfileComponent;
    }();

    ParentProfileComponent.ctorParameters = function () {
      return [{
        type: src_app_core_services_user_data_service__WEBPACK_IMPORTED_MODULE_2__["UserDataService"]
      }, {
        type: src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }, {
        type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_8__["NgxUiLoaderService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(src_app_core_components_header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"], {
      "static": true
    })], ParentProfileComponent.prototype, "headerComponent", void 0);
    ParentProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'parent-profile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./parent-profile.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/parent-profile/parent-profile.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./parent-profile.component.css */
      "./src/app/pages/parent/parent-profile/parent-profile.component.css"))["default"]]
    })], ParentProfileComponent);
    /***/
  },

  /***/
  "./src/app/pages/parent/parent.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/pages/parent/parent.module.ts ***!
    \***********************************************/

  /*! exports provided: ParentModule */

  /***/
  function srcAppPagesParentParentModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ParentModule", function () {
      return ParentModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _parent_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./parent.routing */
    "./src/app/pages/parent/parent.routing.ts");
    /* harmony import */


    var _core_core_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../core/core.module */
    "./src/app/core/core.module.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _login_parent_login_parent_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./login-parent/login-parent.component */
    "./src/app/pages/parent/login-parent/login-parent.component.ts");
    /* harmony import */


    var _parent_profile_parent_profile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./parent-profile/parent-profile.component */
    "./src/app/pages/parent/parent-profile/parent-profile.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");
    /* harmony import */


    var ng2_validation__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ng2-validation */
    "./node_modules/ng2-validation/dist/index.js");
    /* harmony import */


    var ng2_validation__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_10__);
    /* harmony import */


    var _detail_page_detail_page_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./detail-page/detail-page.component */
    "./src/app/pages/parent/detail-page/detail-page.component.ts");
    /* harmony import */


    var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! angular-ng-autocomplete */
    "./node_modules/angular-ng-autocomplete/fesm2015/angular-ng-autocomplete.js");
    /* harmony import */


    var _agm_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @agm/core */
    "./node_modules/@agm/core/fesm2015/agm-core.js");

    var ParentModule = function ParentModule() {
      _classCallCheck(this, ParentModule);
    };

    ParentModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      entryComponents: [],
      declarations: [_login_parent_login_parent_component__WEBPACK_IMPORTED_MODULE_6__["LoginParentComponent"], _parent_profile_parent_profile_component__WEBPACK_IMPORTED_MODULE_7__["ParentProfileComponent"], _detail_page_detail_page_component__WEBPACK_IMPORTED_MODULE_11__["DetailPageComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_3__["CoreModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSliderModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatChipsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocompleteModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_9__["NgxUiLoaderModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatStepperModule"], _parent_routing__WEBPACK_IMPORTED_MODULE_2__["ParentRoutingModule"], angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_12__["AutocompleteLibModule"], _agm_core__WEBPACK_IMPORTED_MODULE_13__["AgmCoreModule"].forRoot({
        apiKey: 'AIzaSyCayIBisLl_xmSOmS3g524FAzEI-ZhT1sc'
      })],
      exports: [_login_parent_login_parent_component__WEBPACK_IMPORTED_MODULE_6__["LoginParentComponent"], _parent_profile_parent_profile_component__WEBPACK_IMPORTED_MODULE_7__["ParentProfileComponent"], _detail_page_detail_page_component__WEBPACK_IMPORTED_MODULE_11__["DetailPageComponent"]]
    })], ParentModule);
    /***/
  },

  /***/
  "./src/app/pages/parent/parent.routing.ts":
  /*!************************************************!*\
    !*** ./src/app/pages/parent/parent.routing.ts ***!
    \************************************************/

  /*! exports provided: routes, ParentRoutingModule */

  /***/
  function srcAppPagesParentParentRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routes", function () {
      return routes;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ParentRoutingModule", function () {
      return ParentRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _login_parent_login_parent_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./login-parent/login-parent.component */
    "./src/app/pages/parent/login-parent/login-parent.component.ts");
    /* harmony import */


    var _parent_profile_parent_profile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./parent-profile/parent-profile.component */
    "./src/app/pages/parent/parent-profile/parent-profile.component.ts");
    /* harmony import */


    var _detail_page_detail_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./detail-page/detail-page.component */
    "./src/app/pages/parent/detail-page/detail-page.component.ts");

    var routes = [// { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
      path: 'login-parent',
      component: _login_parent_login_parent_component__WEBPACK_IMPORTED_MODULE_3__["LoginParentComponent"],
      data: {
        title: 'login-parent',
        breadcrumb: 'login-parent'
      }
    }, {
      path: 'parent-profile',
      component: _parent_profile_parent_profile_component__WEBPACK_IMPORTED_MODULE_4__["ParentProfileComponent"],
      data: {
        title: 'parent-profile',
        breadcrumb: 'Account'
      }
    }, {
      path: 'detail-page',
      component: _detail_page_detail_page_component__WEBPACK_IMPORTED_MODULE_5__["DetailPageComponent"],
      data: {
        title: 'detail',
        breadcrumb: 'detail'
      }
    }];

    var ParentRoutingModule = function ParentRoutingModule() {
      _classCallCheck(this, ParentRoutingModule);
    };

    ParentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ParentRoutingModule);
    /***/
  }
}]);
//# sourceMappingURL=pages-parent-parent-module-es5.js.map
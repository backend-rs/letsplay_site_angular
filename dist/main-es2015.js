(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../app/pages/forgot-password/forgot-password.module": [
		"./src/app/pages/forgot-password/forgot-password.module.ts",
		"default~app-pages-forgot-password-forgot-password-module~pages-parent-parent-module~pages-provider-e~a7dbd0f8",
		"app-pages-forgot-password-forgot-password-module"
	],
	"../app/pages/login/login.module": [
		"./src/app/pages/login/login.module.ts",
		"app-pages-login-login-module"
	],
	"../app/pages/search/search.module": [
		"./src/app/pages/search/search.module.ts",
		"default~app-pages-search-search-module~pages-parent-parent-module~pages-provider-profile-profile-module",
		"default~app-pages-search-search-module~pages-provider-program-program-module",
		"app-pages-search-search-module"
	],
	"../app/pages/sign-up/sign-up.module": [
		"./src/app/pages/sign-up/sign-up.module.ts",
		"app-pages-sign-up-sign-up-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <app-header></app-header> -->\r\n\r\n<ng2-toasty></ng2-toasty>\r\n<router-outlet></router-outlet>\r\n\r\n<!-- <app-footer> </app-footer> -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/breadcrumb/breadcrumb.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/breadcrumb/breadcrumb.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <div class=\"heading2\">\n    <img src=\"assets/home.png\"><label class=\"custom-label1\"> ></label> \n</div> -->\n<div class=\"breadcrump_top\">\n    <!-- <a href=\"\"><span class=\"icon-home\"></span></a> -->\n    <a href=\"\"><img src=\"assets/home.png\"></a>\n\n    <a href=\"\"> > {{routeName}}</a>\n  </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/carousel/carousel.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/carousel/carousel.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\n    <div id=\"dataInfo\">\n        <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">\n            <div class=\"carousel-inner\">\n                <div class=\"carousel-item active\">\n                    <div class=\"row\">\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image2.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n                            </div>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image3.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image4.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n                            </div>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image3.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"carousel-item\">\n                    <div class=\"row\">\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image3.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n\n                            </div>\n                        </div>\n\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image2.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image4.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n\n                            </div>\n                        </div>\n                        <div class=\"col-md-3\">\n                            <div class=\"card\">\n                                <img class=\"card-img-top img-fluid\" src=\"assets/image2.png\" width=\"100\" height=\"100\"\n                                    alt=\"Card image cap\">\n\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <a class=\"carousel-control-prev\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">\n                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n\n            </a>\n            <a class=\"carousel-control-next\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">\n                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n\n            </a>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/drag-drop/drag-drop.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/drag-drop/drag-drop.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"center\">\n    <ngx-file-drop dropZoneLabel=\"Drop files here\" (onFileDrop)=\"dropped($event)\" (onFileOver)=\"fileOver($event)\"\n        (onFileLeave)=\"fileLeave($event)\" [multiple]=\"isMultiple\">\n\n        <ng-template ngx-file-drop-content-tmp let-openFileSelector=\"openFileSelector\">\n            <span>Drag and Drop.</span>\n            <button type=\"button\" class=\"btn btn-Grade\" (click)=\"openFileSelector()\">Browse Files</button>\n        </ng-template>\n    </ngx-file-drop>\n    <div class=\"upload-table\">\n        <table class=\"table\">\n            <thead>\n                <tr *ngIf=\"images.length\">\n                    <th>Image</th>\n                    <th>Name</th>\n                    <th>Size</th>\n                    <th>Action</th>\n                </tr>\n            </thead>\n            <tbody class=\"upload-name-style\">\n                <tr *ngFor=\"let item of images; let i=index\">\n                    <td> <img src=\"{{item.url}}\" style=\"width: 50px; height: 50px\"></td>\n                    <td><strong>{{ item.name }}</strong></td>\n                    <td><strong>{{ item.size }}</strong></td>\n                    <td><button type=\"button\" (click)=\"remove(i)\">remove </button></td>\n                </tr>\n            </tbody>\n        </table>\n        <!-- <button *ngIf=\"images.length\" type=\"button\" class=\"btn btn-Grade\" (click)=\"upload()\">upload</button> -->\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/footer/footer.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/footer/footer.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<br>\r\n<footer>\r\n  <div class=\"container\">\r\n    <div class=\"footer_inner\">\r\n      <div class=\"row\">\r\n        <div class=\"col-12\">\r\n          <div class=\"footer_logo\">\r\n            <a href=\"landing\">\r\n              <img src=\"assets/newlogo.png\" alt=\"Logo\">\r\n            </a>\r\n          </div>\r\n          <div class=\"footer_menu\">\r\n            <ul>\r\n              <li class=\"cursor\">\r\n                <a [routerLink]=\"['/term-condition']\">About Us</a>\r\n              </li>\r\n              <li class=\"cursor\">\r\n                <a [routerLink]=\"['/term-condition']\">Terms & Conditions</a>\r\n              </li>\r\n              <li class=\"cursor\">\r\n                <a [routerLink]=\"['/term-condition']\">Privacy Policy</a>\r\n              </li>\r\n              <li class=\"cursor\">\r\n                <a [routerLink]=\"['/term-condition']\">Ambassador Policy</a>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n          <div class=\"copyright_text\">\r\n            <p>© 2019 Letsplay Holdings Inc. All Rights Reserved.</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/footer2/footer2.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/footer2/footer2.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer style=\"background-color:#fff;\">\n    <div class=\"container\">\n        <div class=\"footer_inner\">\n            <div class=\"row\">\n                <div class=\"col-12\">\n                    <div class=\"footer_logo\">\n                        <a href=\"javascript:;\">\n                            <img src=\"assets/newlogo.png\" alt=\"Logo\">\n                        </a>\n                    </div>\n                    <div class=\"footer_menu\">\n                        <ul>\n                            <li class=\"cursor\">\n                                <a [routerLink]=\"['/term-condition']\">About Us</a>\n                            </li>\n                            <li class=\"cursor\">\n                                <a [routerLink]=\"['/term-condition']\">Terms & Conditions</a>\n                            </li>\n                            <li class=\"cursor\">\n                                <a [routerLink]=\"['/term-condition']\">Privacy Policy</a>\n                            </li>\n                            <li class=\"cursor\">\n                                <a [routerLink]=\"['/term-condition']\">Ambassador Policy</a>\n                            </li>\n                        </ul>\n                    </div>\n                    <div class=\"copyright_text\">\n                        <p>© 2019 Letsplay Holdings Inc. All Rights Reserved.</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/header/header.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/header/header.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\r\n\r\n  <header>\r\n    <div class=\"container-fluid\">\r\n      <div class=\"row\">\r\n        <div class=\"col-12\">\r\n          <div class=\"logo\">\r\n            <a (click)=\"logo()\">\r\n              <img src=\"assets/logo-1.png\" alt=\"Logo Image\">\r\n\r\n            </a>\r\n          </div>\r\n          <div class=\"header_right row\">\r\n            <a [routerLink]=\"['/term-condition']\" class=\"help\"><img src=\"assets/help-icon-gray.svg\" alt=\"Help Icon\"></a>\r\n            <a class=\"progress_bar\">\r\n              <div class=\"progress\" id=\"progress\">\r\n                <span class=\"progress-left\">\r\n                  <span class=\"progress-bar\"></span>\r\n                </span>\r\n                <span class=\"progress-right\">\r\n                  <span class=\"progress-bar\"></span>\r\n                </span>\r\n                <div *ngIf=\"profileProgress\" class=\"progress-value\">\r\n                  {{profileProgress}}%\r\n                </div>\r\n              </div>\r\n            </a>\r\n\r\n\r\n            <a class=\"cursor\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\r\n              aria-expanded=\"false\">\r\n              <div class=\"head_profile\">\r\n                <img *ngIf=\"user.avatarImages\" src=\"{{user.avatarImages}}\" alt=\"Profile image\">\r\n                <img *ngIf=\"!user.avatarImages\" src=\"assets/userIMAGE.png\" alt=\"Profile image\">\r\n              </div>\r\n            </a>\r\n            <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n              <a class=\"dropdown-item cursor\" (click)=\"profile()\"><img src=\"assets/profile.png\"> Profile</a>\r\n              <a class=\"dropdown-item cursor\" (click)=\"logout()\"><img src=\"assets/logout.png\"> Logout</a>\r\n\r\n            </div>\r\n\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </header>\r\n</body>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/header2/header2.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/header2/header2.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\n    <nav class=\"navbar navbar-expand-lg navbar-light\">\n        <a class=\"navbar-brand\" href=\"#\"><img src=\"assets/logo-1.png\" alt=\"Logo\"></a>\n        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n            <ul class=\"navbar-nav ml-auto\">\n                <li class=\"nav-item\">\n                    <a class=\"nav-link cursor\" [routerLink]=\"['/term-condition']\">How it works</a>\n                </li>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link cursor\" [routerLink]=\"['/term-condition']\">FAQ</a>\n                </li>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link cursor\" [routerLink]=\"['/sign-up']\">Create an acount</a>\n                </li>\n                <li>\n                    <a [routerLink]=\"['/login']\" class=\"btn_style cursor\">Login</a>\n                </li>\n            </ul>\n        </div>\n    </nav>\n</header>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/nav/nav.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/nav/nav.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"navigation\">\n  <div class=\"container\">\n    <ul>\n      <li>\n        <a routerLink=\"/program/home\" [class.active]=\"dashboard\">Dashboard</a>\n      </li>\n      <li>\n        <a routerLink=\"/program/list\" [class.active]=\"program\">Programs</a>\n      </li>\n      <li>\n        <a routerLink=\"/profile\" [class.active]=\"profile\">Profile</a>\n      </li>\n      <li>\n        <a routerLink=\"/program/setting\" [class.active]=\"setting\">Settings</a>\n      </li>\n    </ul>\n  </div>\n</div>\n<div class=\"main_outer\"></div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/landing/landing.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/landing/landing.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\n\n  <app-header2 *ngIf=\"!isLogin\"></app-header2>\n  <app-header *ngIf=\"isLogin\"></app-header>\n  <section class=\"landing_banner\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <div class=\"best_activities\">\n            <h1>Find the best activities for<br> your kids...</h1>\n            <form class=\"banner_form\">\n              <div class=\"form-group\">\n                <input type=\"text\" placeholder=\"e.g. Summer Camp\" class=\"form-control camp_input\">\n              </div>\n              <div class=\"form-group\">\n                <input type=\"text\" placeholder=\"Pick a Date\" class=\"form-control date_input\">\n              </div>\n              <div class=\"form-group\">\n                <button (click)=\"search()\" class=\"banner_button\" type=\"submit\">Search</button>\n              </div>\n            </form>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"banner_overlay\"></div>\n  </section>\n\n  <section class=\"activity_category\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <h2 class=\"title\">Activity Categories</h2>\n        </div>\n      </div>\n\n      <div class=\"activity_category_list\">\n        <div class=\"row\">\n          <div class=\"col-md-8\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category1.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Dancing</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category2.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Visual Arts</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category3.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Painting</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category4.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Stem</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category5.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Robotics</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category6.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Reading</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category7.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Martial Arts</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n          <div class=\"col-md-4\">\n            <div class=\"category_list_box cursor\" [routerLink]=\"['/search']\">\n              <a href=\"javascritpt:;\">\n                <img src=\"assets/category8.jpg\" alt=\"Category image\">\n                <div class=\"category_title\">\n                  <p>Photography</p>\n                </div>\n              </a>\n              <div class=\"overlay\"></div>\n            </div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n  </section>\n\n  <section class=\"featured_activity\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <h2 class=\"title\">Featured Activities</h2>\n          <div class=\"custom_select featured\">\n            <select>\n              <option selected>Hightest Rated</option>\n              <option>Hightest Rated 1</option>\n              <option>Hightest Rated 2</option>\n              <option>Hightest Rated 3</option>\n            </select>\n\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-3\">\n          <div class=\"featured_box  cursor\" [routerLink]=\"['/program-detail']\">\n            <div class=\"featured_img\">\n              <img src=\"assets/featured1.jpg\" alt=\"Featured Image\">\n            </div>\n            <div class=\"featured_text\">\n              <h3>GYM</h3>\n              <p>Kinderbots (Robotics)</p>\n              <div class=\"rating_box\">\n                <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                <span>5.0 (15)</span>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-3\">\n          <div class=\"featured_box cursor\" [routerLink]=\"['/program-detail']\">\n            <div class=\"featured_img\">\n              <img src=\"assets/category3.jpg\" alt=\"Featured Image\">\n            </div>\n            <div class=\"featured_text\">\n              <h3>Arts & Crafts </h3>\n              <p>Art & Craft for Kids</p>\n              <div class=\"rating_box\">\n                <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                <span>5.0 (15)</span>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-3\">\n          <div class=\"featured_box cursor\" [routerLink]=\"['/program-detail']\">\n            <div class=\"featured_img\">\n              <img src=\"assets/category8.jpg\" alt=\"Featured Image\">\n            </div>\n            <div class=\"featured_text\">\n              <h3>Arts & Crafts</h3>\n              <p> Learn Photography</p>\n              <div class=\"rating_box\">\n                <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                <span>5.0 (15)</span>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-3\">\n          <div class=\"featured_box cursor\" [routerLink]=\"['/program-detail']\">\n            <div class=\"featured_img\">\n              <img src=\"assets/category4.jpg\" alt=\"Featured Image\">\n            </div>\n            <div class=\"featured_text\">\n              <h3>Coding</h3>\n              <p>Learn Web Development</p>\n              <div class=\"rating_box\">\n                <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                <span>5.0 (15)</span>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n\n  <section class=\"trending_discussions\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <h2 class=\"title\">Trending Discussions</h2>\n          <a href=\"javascritp:;\" class=\"view\">View All</a>\n          <div class=\"trending_outer\">\n            <div class=\"trending_box\">\n              <h3>Discussion Title</h3>\n              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n                dolore magna aliqua. ut labore et dolore magna aliqua.</p>\n            </div>\n            <div class=\"trending_box\">\n              <h3>Discussion Title</h3>\n              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n                dolore magna aliqua. ut labore et dolore magna aliqua.</p>\n            </div>\n            <div class=\"trending_box\">\n              <h3>Discussion Title</h3>\n              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n                dolore magna aliqua. ut labore et dolore magna aliqua.</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n\n  <section class=\"upto_date\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <div class=\"upto_inner\">\n            <h2 class=\"title\">Stay upto date</h2>\n            <p>Stay upto date and receive best promotions directly in your inbox. We promise not to spam you.</p>\n            <div class=\"input-group\">\n              <input type=\"text\" class=\"form-control\" placeholder=\"example@domain.com\">\n              <div class=\"input-group-append\">\n                <button class=\"Submit_btn\" type=\"button\">Submit</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n\n\n\n  <footer>\n    <div class=\"container\">\n      <div class=\"footer_inner\">\n        <div class=\"row align-items-center\">\n          <div class=\"col-12\">\n            <div class=\"footer_left\">\n              <div class=\"footer_logo\">\n                <a href=\"landing\">\n                  <img src=\"assets/white_logo.png\" alt=\"Logo\">\n                </a>\n              </div>\n              <div class=\"footer_menu\">\n                <ul>\n                  <li>\n                    <a [routerLink]=\"['/term-condition']\">About Us</a>\n                  </li>\n                  <li>\n                    <a [routerLink]=\"['/term-condition']\">Terms & Conditions</a>\n                  </li>\n                  <li>\n                    <a [routerLink]=\"['/term-condition']\">Privacy Policy</a>\n                  </li>\n                  <li>\n                    <a [routerLink]=\"['/term-condition']\">Ambassador Policy</a>\n                  </li>\n                </ul>\n              </div>\n              <div class=\"copyright_text\">\n                <p>© 2019 Letsplay Holdings Inc. All Rights Reserved.</p>\n              </div>\n            </div>\n            <div class=\"social_icon_footer\">\n              <ul>\n                <li>\n                  <a href=\"javascript:;\">\n                    <span class=\"icon icon-facebook\"></span>\n                  </a>\n                </li>\n                <li>\n                  <a href=\"javascript:;\">\n                    <span class=\"icon icon-instagram\"></span>\n                  </a>\n                </li>\n                <li>\n                  <a href=\"javascript:;\">\n                    <span class=\"icon icon-pinterest\"></span>\n                  </a>\n                </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </footer>\n</body>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/parent.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/parent.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/program-detail/program-detail.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/program-detail/program-detail.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\n\n  <app-header2 *ngIf=\"!isLogin\"></app-header2>\n  <app-header *ngIf=\"isLogin\"></app-header>\n  <app-nav *ngIf=\"providerRole\"></app-nav>\n\n  <section class=\"main_outer\">\n\n    <div class=\"program_outer\">\n      <div class=\"container-fluid\">\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <app-carousel></app-carousel>\n          </div>\n        </div>\n\n\n        <div class=\"program_text\">\n          <div class=\"row\">\n            <div class=\"col-md-8\">\n              <div class=\"CategoryText\">\n                <h5>Intrest</h5>\n                <mat-chip-list>\n                  <mat-chip *ngFor=\"let intrest of program.tags\">{{intrest.name}}</mat-chip>\n                </mat-chip-list>\n                <h3>{{program.name}}</h3>\n                <div class=\"session_details\">\n                  <p><img src=\"assets/pin_blue.svg\" alt=\"Loction Pin\">{{program.location}}</p>\n                  <p><img src=\"assets/calendar_blue.svg\"\n                      alt=\"Loction Pin\">{{program.date.from  | date:\"dd:MM:yyyy\"}}-{{program.date.to  | date:\"dd:MM:yyyy\"}}\n                  </p>\n                  <p><img src=\"assets/user_group_blue.svg\" alt=\"Loction Pin\">\n                    {{program.ageGroup.from}}-{{program.ageGroup.to}} Yrs. </p>\n                  <p><img src=\"assets/watch_blue.svg\"\n                      alt=\"Loction Pin\">{{program.time.from | date:\"shortTime\"}}-{{program.time.to | date:\"shortTime\"}}\n                  </p>\n                  <!-- <p><img src=\"assets/timer_blue.svg\" alt=\"Loction Pin\"> 30 Mins </p> -->\n                </div>\n                <div class=\"insured_outer\">\n                  <p><img src=\"assets/tick_img.png\" alt=\"Loction Pin\"> Verified </p>\n                  <p><img src=\"assets/medical.svg\" alt=\"Loction Pin\"> COVID-19 Cleared </p>\n                  <p><img src=\"assets/reward_img.png\" alt=\"Loction Pin\"> Certified Provider </p>\n                  <p><img src=\"assets/insured.png\" alt=\"Loction Pin\"> Fully Insured </p>\n                </div>\n\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"batch_outer\">\n          <div class=\"row\">\n            <div class=\"col-md-8\">\n              <div class=\"batch_left\">\n                <h2>Batches</h2>\n                <div class=\"listing_outer\">\n                  <div class=\"batch_box\" *ngFor=\"let batch of program.batches\">\n                    <div class=\"batch_title\">\n                      <p> Tile of the batch</p>\n                    </div>\n                    <div class=\"batch_list_outer\">\n                      <div class=\"batch_listing\">\n                        <label>Time</label>\n                        <p>{{batch.startTime | date:\"shortTime\"}}–{{batch.endTime | date:\"shortTime\"}}\n                        </p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>Dates</label>\n                        <p>{{batch.startDate | date:\"dd:MM:yyyy\"}}–{{batch.endDate | date:\"dd:MM:yyyy\"}}\n                        </p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>instructor</label>\n                        <div class=\"batch_profile\">\n                          <img src=\"assets/profile2.png\" alt=\"Profile Image\">\n                        </div>\n                        <p>{{batch.instructor}}</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>number of seats</label>\n                        <p>{{batch.numberOfSeats}}</p>\n                      </div>\n                      <div class=\"batch_listing\">\n                        <label>Location</label>\n                        <p>{{batch.location}}</p>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Description</h2>\n                <p>{{program.description}}</p>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Cancellation Policy</h2>\n                <p>\n                  <label>Booking can’t be cancelled before</label>\n                  {{program.bookingCancelledIn.days}} days {{program.bookingCancelledIn.hours}} Hours\n                </p>\n                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n                  et dolore magna aliqua. Ut enim ad minim veniam,</p> -->\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Special Instructions</h2>\n                <p>{{program.specialInstructions}}</p>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Reviews <a href=\"\" class=\"view_all\">View all</a> </h2>\n                <div class=\"reviews_outer\">\n                  <div class=\"media align-items-center\">\n                    <div class=\"media-left\">\n                      <img src=\"assets/review_img1.png\" alt=\"Review\">\n                    </div>\n                    <div class=\"media-body\">\n                      <label>John Doe</label>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                        labore et dolore magna aliqua.</p>\n                    </div>\n                  </div>\n                  <div class=\"media align-items-center\">\n                    <div class=\"media-left\">\n                      <img src=\"assets/review_img2.png\" alt=\"Review\">\n                    </div>\n                    <div class=\"media-body\">\n                      <label>James Do</label>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                        labore et dolore magna aliqua.</p>\n                    </div>\n                  </div>\n                  <div class=\"media align-items-center\">\n                    <div class=\"media-left\">\n                      <img src=\"assets/review_img3.png\" alt=\"Review\">\n                    </div>\n                    <div class=\"media-body\">\n                      <label>Jane Doe</label>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                        labore et dolore magna aliqua.</p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"description_text\">\n                <h2>Other Offerings <a href=\"\" class=\"view_all\">View all</a> </h2>\n              </div>\n\n              <div class=\"row\">\n\n                <div class=\"col-md-4\">\n                  <div class=\"featured_box\">\n                    <div class=\"featured_img\">\n                      <img src=\"assets/offeringImg1.png\" alt=\"Featured Image\">\n                      <div class=\"menu_control\">\n                        <a href=\"\"><img src=\"assets/menu.svg\" alt=\"Menu Image\"></a>\n                        <a href=\"\" class=\"bookmark_icon\"><img src=\"assets/bookmark_svg.svg\" alt=\"Menu Image\"></a>\n                      </div>\n                    </div>\n                    <div class=\"featured_text\">\n                      <a href=\"\" class=\"featured_btn\">Featured</a>\n                      <h3>coding</h3>\n                      <p>Kinderbots</p>\n                      <div class=\"feature_box\">\n                        <p><img src=\"assets/calendar_blue.svg\" alt=\"Loction Pin\"> Mar 09-13,2020 </p>\n                        <p><img src=\"assets/user_group_blue.svg\" alt=\"Loction Pin\"> 12-16 Yrs. </p>\n                      </div>\n                      <div class=\"rating_box\">\n                        <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                        <span>5.0 (15)</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"col-md-4\">\n                  <div class=\"featured_box\">\n                    <div class=\"featured_img\">\n                      <img src=\"assets/offeringImg2.png\" alt=\"Featured Image\">\n                      <div class=\"menu_control\">\n                        <a href=\"\"><img src=\"assets/menu.svg\" alt=\"Menu Image\"></a>\n                        <a href=\"\" class=\"bookmark_icon\"><img src=\"assets/bookmark_svg.svg\" alt=\"Menu Image\"></a>\n                      </div>\n                    </div>\n                    <div class=\"featured_text\">\n                      <a href=\"\" class=\"featured_btn\">Featured</a>\n                      <h3>reading</h3>\n                      <p>How to read as a kid</p>\n                      <div class=\"feature_box\">\n                        <p><img src=\"assets/calendar_blue.svg\" alt=\"Loction Pin\"> Mar 09-13,2020 </p>\n                        <p><img src=\"assets/user_group_blue.svg\" alt=\"Loction Pin\"> 12-16 Yrs. </p>\n                      </div>\n                      <div class=\"rating_box\">\n                        <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                        <span>5.0 (15)</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"col-md-4\">\n                  <div class=\"featured_box\">\n                    <div class=\"featured_img\">\n                      <img src=\"assets/offeringImg3.png\" alt=\"Featured Image\">\n                      <div class=\"menu_control\">\n                        <a href=\"\"><img src=\"assets/menu.svg\" alt=\"Menu Image\"></a>\n                        <a href=\"\" class=\"bookmark_icon\"><img src=\"assets/bookmark_svg.svg\" alt=\"Menu Image\"></a>\n                      </div>\n                    </div>\n                    <div class=\"featured_text\">\n                      <a href=\"\" class=\"featured_btn\">Featured</a>\n                      <h3>Stem education</h3>\n                      <p>Learn to Create Robots</p>\n                      <div class=\"feature_box\">\n                        <p><img src=\"assets/calendar_blue.svg\" alt=\"Loction Pin\"> Mar 09-13,2020 </p>\n                        <p><img src=\"assets/user_group_blue.svg\" alt=\"Loction Pin\"> 12-16 Yrs. </p>\n                      </div>\n                      <div class=\"rating_box\">\n                        <img src=\"assets/featured_star.svg\" alt=\"Star Image\">\n                        <span>5.0 (15)</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n              </div>\n\n            </div>\n            <div class=\"col-md-4\">\n              <div class=\"batch_right\">\n                <img src=\"assets/small_map.png\" alt=\"Map image\">\n                <!-- <div class=\"map_img\">\n                  <agm-map [latitude]=\"lat\" [longitude]=\"lng\">\n                    <agm-marker *ngFor=\"let item of markers\" [latitude]=\"item.lat\"\n                          [longitude]=\"item.lng\"\n                          [label]=\"{color: 'black',fontWeight: 'bold', text:item.label}\">\n                          <agm-info-window>\n                              <strong>{{item.info}}</strong>\n                          </agm-info-window>\n                      </agm-marker>\n                  </agm-map>\n                </div> -->\n              </div>\n              <div class=\"batch_right border_none\">\n                <img src=\"assets/business.svg\" alt=\"Business image\">\n                <h2>Is this your business?</h2>\n                <p>Claim your business to immediately update business information,\n                  respond to reviews, and more!</p>\n                <a (click)=\"claimBusiness()\" class=\"business_btn cursor\">Claim This Business</a>\n              </div>\n            </div>\n          </div>\n        </div>\n\n      </div>\n\n\n\n\n    </div>\n\n  </section>\n  <app-footer></app-footer>\n\n</body>\n<ngx-ui-loader></ngx-ui-loader>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/provider/program/program.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/provider/program/program.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/term-condition/term-condition.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/term-condition/term-condition.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body class=\"terms_body\">\n  <!-- <app-header2 *ngIf=\"!isLogin\"></app-header2> -->\n  <app-header *ngIf=\"isLogin\"></app-header>\n\n  <div class=\"main_outer\">\n    <div class=\"container\">\n      <div class=\"terms_outer\">\n        <div class=\"terms_top\">\n          <h5>Terms and Conditions</h5>\n          <h2>Welcome to Letsplay</h2>\n          <h3>Last modified: Mar 16, 2020 (view archived versions)</h3>\n          <p>These terms and conditions outline the rules and regulations for the use of LetsPlay's Website, located at\n            www.letsplay.com.</p>\n          <p> By accessing this website we assume you accept these terms and conditions. Do not continue to use LetsPlay\n            if you do not agree to take all of the terms and conditions stated on this page. Our Terms and Conditions\n            were created with the help of the Terms And Conditions Generator and the Free Terms & Conditions Generator.\n          </p>\n          <p> The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice\n            and all Agreements: \"Client\", \"You\" and \"Your\" refers to you, the person log on this website and compliant\n            to the Company’s terms and conditions. \"The Company\", \"Ourselves\", \"We\", \"Our\" and \"Us\", refers to our\n            Company. \"Party\", \"Parties\", or \"Us\", refers to both the Client and ourselves. All terms refer to the offer,\n            acceptance and consideration of payment necessary to undertake the process of our assistance to the Client\n            in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision\n            of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use\n            of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are\n            taken as interchangeable and therefore as referring to same.</p>\n        </div>\n\n        <div class=\"terms_content_outer\">\n          <div class=\"terms_box\">\n            <h2>Cookies</h2>\n            <p>We employ the use of cookies. By accessing LetsPlay, you agreed to use cookies in agreement with the\n              LetsPlay's Privacy Policy.</p>\n            <p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are\n              used by our website to enable the functionality of certain areas to make it easier for people visiting our\n              website. Some of our affiliate/advertising partners may also use cookies.</p>\n          </div>\n          <div class=\"terms_box\">\n            <h2>License</h2>\n            <p>Unless otherwise stated, LetsPlay and/or its licensors own the intellectual property rights for all\n              material on LetsPlay. All intellectual property rights are reserved. You may access this from LetsPlay for\n              your own personal use subjected to restrictions set in these terms and conditions.</p>\n            <p>You must not:</p>\n            <ul>\n              <li>Republish material from LetsPlay</li>\n              <li>Sell, rent or sub-license material from LetsPlay</li>\n              <li>Reproduce, duplicate or copy material from LetsPlay</li>\n              <li>Redistribute content from LetsPlay</li>\n              <li>This Agreement shall begin on the date hereof.</li>\n            </ul>\n            <p>This Agreement shall begin on the date hereof.</p>\n            <p>Parts of this website offer an opportunity for users to post and exchange opinions and information in\n              certain areas of the website. LetsPlay does not filter, edit, publish or review Comments prior to their\n              presence on the website. Comments do not reflect the views and opinions of LetsPlay,its agents and/or\n              affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To\n              the extent permitted by applicable laws, LetsPlay shall not be liable for the Comments or for any\n              liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or\n              appearance of the Comments on this website.</p>\n            <p>LetsPlay reserves the right to monitor all Comments and to remove any Comments which can be considered\n              inappropriate, offensive or causes breach of these Terms and Conditions.</p>\n            <p>You warrant and represent that:</p>\n            <ul>\n              <li>You are entitled to post the Comments on our website and have all necessary licenses and consents to\n                do so;</li>\n              <li>The Comments do not invade any intellectual property right, including without limitation copyright,\n                patent or trademark of any third party;</li>\n              <li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful\n                material which is an invasion of privacy</li>\n              <li>The Comments will not be used to solicit or promote business or custom or present commercial\n                activities or unlawful activity.</li>\n            </ul>\n          </div>\n          <div class=\"terms_box\">\n            <h2>Hyperlinking to our Content</h2>\n            <p>The following organizations may link to our Website without prior written approval:</p>\n            <ul>\n              <li>Government agencies;</li>\n              <li>Search engines;</li>\n              <li>News organizations;</li>\n              <li>Online directory distributors may link to our Website in the same manner as they hyperlink to the\n                Websites of other listed businesses; and</li>\n              <li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls,\n                and charity fundraising groups which may not hyperlink to our Web site.</li>\n            </ul>\n            <p>These organizations may link to our home page, to publications or to other Website information so long as\n              the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval\n              of the linking party and its products and/or services; and (c) fits within the context of the linking\n              party’s site.</p>\n            <p>We may consider and approve other link requests from the following types of organizations:</p>\n            <ul>\n              <li>commonly-known consumer and/or business information sources;</li>\n              <li>dot.com community sites;</li>\n              <li>associations or other groups representing charities;</li>\n              <li>online directory distributors;</li>\n              <li>internet portals;</li>\n              <li>accounting, law and consulting firms; and</li>\n              <li>educational institutions and trade associations.</li>\n            </ul>\n            <p>We will approve link requests from these organizations if we decide that: (a) the link would not make us\n              look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any\n              negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the\n              absence of LetsPlay; and (d) the link is in the context of general resource information.</p>\n            <p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b)\n              does not falsely imply sponsorship, endorsement or approval of the linking party and its products or\n              services; and (c) fits within the context of the linking party’s site.</p>\n            <p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our\n              website, you must inform us by sending an e-mail to LetsPlay. Please include your name, your organization\n              name, contact information as well as the URL of your site, a list of any URLs from which you intend to\n              link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks\n              for a response.</p>\n            <p>Approved organizations may hyperlink to our Website as follows:</p>\n            <ul>\n              <li>By use of our corporate name; or</li>\n              <li>By use of the uniform resource locator being linked to; or</li>\n              <li>By use of any other description of our Website being linked to that makes sense within the context and\n                format of content on the linking party’s site.</li>\n            </ul>\n            <p>No use of LetsPlay's logo or other artwork will be allowed for linking absent a trademark license\n              agreement.</p>\n          </div>\n\n          <div class=\"terms_box\">\n            <h2>iFrames</h2>\n            <p>Without prior approval and written permission, you may not create frames around our Webpages that alter\n              in any way the visual presentation or appearance of our Website.</p>\n          </div>\n\n          <div class=\"terms_box\">\n            <h2>Content Liability</h2>\n            <p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and\n              defend us against all claims that is rising on your Website. No link(s) should appear on any Website that\n              may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates\n              the infringement or other violation of, any third party rights.</p>\n          </div>\n\n          <div class=\"terms_box\">\n            <h2>Reservation of Rights</h2>\n            <p>We reserve the right to request that you remove all links or any particular link to our Website. You\n              approve to immediately remove all links to our Website upon request. We also reserve the right to amen\n              these terms and conditions and it’s linking policy at any time. By continuously linking to our Website,\n              you agree to be bound to and follow these linking terms and conditions.</p>\n          </div>\n\n          <div class=\"terms_box\">\n            <h2>Removal of links from our website</h2>\n            <p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform\n              us any moment. We will consider requests to remove links but we are not obligated to or so or to respond\n              to you directly.</p>\n            <p>We do not ensure that the information on this website is correct, we do not warrant its completeness or\n              accuracy; nor do we promise to ensure that the website remains available or that the material on the\n              website is kept up to date.</p>\n          </div>\n\n          <div class=\"terms_box\">\n            <h2>Disclaimer</h2>\n            <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and\n              conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>\n            <ul>\n              <li>limit or exclude our or your liability for death or personal injury;</li>\n              <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>\n              <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>\n              <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>\n            </ul>\n            <p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a)\n              are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer,\n              including liabilities arising in contract, in tort and for breach of statutory duty.</p>\n            <p>As long as the website and the information and services on the website are provided free of charge, we\n              will not be liable for any loss or damage of any nature.</p>\n          </div>\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n\n  <footer>\n    <div class=\"container\">\n      <div class=\"footer_inner\">\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <div class=\"footer_logo\">\n              <a href=\"javascript:;\">\n                <img src=\"assets/logo.png\" alt=\"Logo\">\n              </a>\n            </div>\n            <div class=\"footer_menu\">\n              <ul>\n                <li>\n                  <a href=\"javascript:;\">About Us</a>\n                </li>\n                <li>\n                  <a href=\"javascript:;\">Terms & Conditions</a>\n                </li>\n                <li>\n                  <a href=\"javascript:;\">Privacy Policy</a>\n                </li>\n                <li>\n                  <a href=\"javascript:;\">Ambassador Policy</a>\n                </li>\n              </ul>\n            </div>\n            <div class=\"copyright_text\">\n              <p>© 2019 Letsplay Holdings Inc. All Rights Reserved.</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </footer>\n\n\n</body>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_pages_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app/pages/landing/landing.component */ "./src/app/pages/landing/landing.component.ts");
/* harmony import */ var _pages_provider_program_program_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/provider/program/program.component */ "./src/app/pages/provider/program/program.component.ts");
/* harmony import */ var _pages_parent_parent_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/parent/parent.component */ "./src/app/pages/parent/parent.component.ts");
/* harmony import */ var _pages_term_condition_term_condition_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/term-condition/term-condition.component */ "./src/app/pages/term-condition/term-condition.component.ts");
/* harmony import */ var _pages_program_detail_program_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/program-detail/program-detail.component */ "./src/app/pages/program-detail/program-detail.component.ts");








// main routes
const routes = [
    { path: '', redirectTo: 'landing', pathMatch: 'full' },
    { path: 'landing', component: _app_pages_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__["LandingComponent"] },
    { path: 'term-condition', component: _pages_term_condition_term_condition_component__WEBPACK_IMPORTED_MODULE_6__["TermConditionComponent"] },
    { path: 'program-detail', component: _pages_program_detail_program_detail_component__WEBPACK_IMPORTED_MODULE_7__["ProgramDetailComponent"] },
    { path: 'login', loadChildren: '../app/pages/login/login.module#LoginModule' },
    { path: 'forgot-password', loadChildren: '../app/pages/forgot-password/forgot-password.module#ForgotPasswordModule' },
    { path: 'sign-up', loadChildren: '../app/pages/sign-up/sign-up.module#SignUpModule' },
    { path: 'search', loadChildren: '../app/pages/search/search.module#SearchModule' },
    {
        path: '', component: _pages_provider_program_program_component__WEBPACK_IMPORTED_MODULE_4__["ProgramComponent"], children: [
            {
                path: 'program',
                loadChildren: () => Promise.all(/*! import() | pages-provider-program-program-module */[__webpack_require__.e("default~app-pages-forgot-password-forgot-password-module~pages-parent-parent-module~pages-provider-e~a7dbd0f8"), __webpack_require__.e("default~app-pages-search-search-module~pages-provider-program-program-module"), __webpack_require__.e("pages-provider-program-program-module")]).then(__webpack_require__.bind(null, /*! .//pages/provider/program/program.module */ "./src/app/pages/provider/program/program.module.ts")).then(m => m.ProgramModule),
            },
            {
                path: 'profile',
                loadChildren: () => Promise.all(/*! import() | pages-provider-profile-profile-module */[__webpack_require__.e("default~app-pages-forgot-password-forgot-password-module~pages-parent-parent-module~pages-provider-e~a7dbd0f8"), __webpack_require__.e("default~app-pages-search-search-module~pages-parent-parent-module~pages-provider-profile-profile-module"), __webpack_require__.e("pages-provider-profile-profile-module")]).then(__webpack_require__.bind(null, /*! .//pages/provider/profile/profile.module */ "./src/app/pages/provider/profile/profile.module.ts")).then(m => m.ProfileModule),
            }, {
                path: 'edit-profile',
                loadChildren: () => Promise.all(/*! import() | pages-provider-edit-profile-edit-profile-module */[__webpack_require__.e("default~app-pages-forgot-password-forgot-password-module~pages-parent-parent-module~pages-provider-e~a7dbd0f8"), __webpack_require__.e("pages-provider-edit-profile-edit-profile-module")]).then(__webpack_require__.bind(null, /*! .//pages/provider/edit-profile/edit-profile.module */ "./src/app/pages/provider/edit-profile/edit-profile.module.ts")).then(m => m.EditProfileModule),
            },
        ]
    },
    {
        path: '', component: _pages_parent_parent_component__WEBPACK_IMPORTED_MODULE_5__["ParentComponent"], children: [
            {
                path: 'parent',
                loadChildren: () => Promise.all(/*! import() | pages-parent-parent-module */[__webpack_require__.e("default~app-pages-forgot-password-forgot-password-module~pages-parent-parent-module~pages-provider-e~a7dbd0f8"), __webpack_require__.e("default~app-pages-search-search-module~pages-parent-parent-module~pages-provider-profile-profile-module"), __webpack_require__.e("pages-parent-parent-module")]).then(__webpack_require__.bind(null, /*! .//pages/parent/parent.module */ "./src/app/pages/parent/parent.module.ts")).then(m => m.ParentModule),
            },
        ]
    },
    { path: '**', redirectTo: 'landing' },
];
// const routes: Routes = [
//   { path: '', redirectTo: 'landing', pathMatch: 'full' },
//   { path: 'landing', component: LandingComponent },
//   { path: 'login', loadChildren: '../app/components/login/login.module#LoginModule' },
//   { path: 'login-parent', loadChildren: '../app/components/login-parent/login-parent.module#LoginParentModule' },
//   { path: 'home', loadChildren: '../app/components/home/home.module#HomeModule' },
//   { path: 'program', loadChildren: '../app/components/program/program.module#ProgramModule' },
//   { path: 'add-program', loadChildren: '../app/components/add-program/add-program.module#AddProgramModule' },
//   { path: 'associates', loadChildren: '../app/components/associates/associates.module#AssociatesModule' },
//   { path: 'program-list', loadChildren: '../app/components/program-list/program-list.module#ProgramListModule' },
//   { path: 'sign-up', loadChildren: '../app/components/sign-up/sign-up.module#SignUpModule' },
//   { path: 'setting', loadChildren: '../app/components/setting/setting.module#SettingModule' },
//   { path: 'edit-profile', loadChildren: '../app/components/edit-profile/edit-profile.module#EditProfileModule' },
//   { path: 'profile', loadChildren: '../app/components/profile/profile.module#ProfileModule' },
//   { path: '**', redirectTo: 'landing' }
// ];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true, enableTracing: true })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n.content {\r\n  background-color: #eeeeee;\r\n  height: 90vh;\r\n  padding: 0 10px;\r\n  overflow-y: scroll;\r\n}\r\n\r\n.nav-container {\r\n  position: absolute;\r\n  top: 60px;\r\n  bottom: 0;\r\n  left: 0;\r\n  right: 0;\r\n}\r\n\r\nmat-sidenav-content {\r\n\r\n  flex: 1 1 auto;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxTQUFTO0VBQ1QsT0FBTztFQUNQLFFBQVE7QUFDVjs7QUFFQTs7RUFFRSxjQUFjO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmNvbnRlbnQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWU7XHJcbiAgaGVpZ2h0OiA5MHZoO1xyXG4gIHBhZGRpbmc6IDAgMTBweDtcclxuICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbn1cclxuXHJcbi5uYXYtY29udGFpbmVyIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA2MHB4O1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG5tYXQtc2lkZW5hdi1jb250ZW50IHtcclxuXHJcbiAgZmxleDogMSAxIGF1dG87XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var ng2_toasty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-toasty */ "./node_modules/ng2-toasty/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./core/services/auth.service */ "./src/app/core/services/auth.service.ts");






// import { MatSidenav } from '@angular/material';
let AppComponent = class AppComponent {
    constructor(router, toastyService, auth, toastyConfig) {
        this.router = router;
        this.toastyService = toastyService;
        this.auth = auth;
        this.toastyConfig = toastyConfig;
        this.toastyConfig.theme = 'material';
        this.toastyConfig.timeout = 5000;
        this.toastyConfig.showClose = true;
        this.toastyConfig.limit = 2;
        this.toastyConfig.position = 'top-right';
        if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].name && _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].name !== 'prod') {
            this.envName = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].name;
        }
    }
    ngOnInit() {
    }
    ngOnDestroy() {
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: ng2_toasty__WEBPACK_IMPORTED_MODULE_2__["ToastyService"] },
    { type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: ng2_toasty__WEBPACK_IMPORTED_MODULE_2__["ToastyConfig"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _app_app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../app/app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_core_services__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../app/core/services */ "./src/app/core/services/index.ts");
/* harmony import */ var _pages_landing_landing_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/landing/landing.component */ "./src/app/pages/landing/landing.component.ts");
/* harmony import */ var _pages_provider_program_program_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/provider/program/program.component */ "./src/app/pages/provider/program/program.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _pages_parent_parent_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pages/parent/parent.component */ "./src/app/pages/parent/parent.component.ts");
/* harmony import */ var _pages_term_condition_term_condition_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/term-condition/term-condition.component */ "./src/app/pages/term-condition/term-condition.component.ts");
/* harmony import */ var ng5_slider__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ng5-slider */ "./node_modules/ng5-slider/esm2015/ng5-slider.js");
/* harmony import */ var _pages_program_detail_program_detail_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/program-detail/program-detail.component */ "./src/app/pages/program-detail/program-detail.component.ts");
/* harmony import */ var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-ui-loader */ "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");
/* harmony import */ var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! angular-ng-autocomplete */ "./node_modules/angular-ng-autocomplete/fesm2015/angular-ng-autocomplete.js");









// import { LoginComponent } from './components/login/login.component';


// import { AssociatesComponent } from './components/associates/associates.component';









const components = [
    _app_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
    // LoginComponent,
    _pages_landing_landing_component__WEBPACK_IMPORTED_MODULE_10__["LandingComponent"]
];
// guards
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        declarations: [
            ...components,
            _pages_landing_landing_component__WEBPACK_IMPORTED_MODULE_10__["LandingComponent"],
            _pages_provider_program_program_component__WEBPACK_IMPORTED_MODULE_11__["ProgramComponent"],
            _pages_parent_parent_component__WEBPACK_IMPORTED_MODULE_14__["ParentComponent"],
            _pages_term_condition_term_condition_component__WEBPACK_IMPORTED_MODULE_15__["TermConditionComponent"],
            _pages_program_detail_program_detail_component__WEBPACK_IMPORTED_MODULE_17__["ProgramDetailComponent"],
        ],
        entryComponents: [
            ...components,
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClientModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatCheckboxModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            ngx_ui_loader__WEBPACK_IMPORTED_MODULE_18__["NgxUiLoaderModule"],
            _core_core_module__WEBPACK_IMPORTED_MODULE_7__["CoreModule"],
            ng5_slider__WEBPACK_IMPORTED_MODULE_16__["Ng5SliderModule"],
            angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_19__["AutocompleteLibModule"],
        ],
        providers: [
            _app_core_services__WEBPACK_IMPORTED_MODULE_9__["LocalStorageService"]
            // ...services
        ],
        bootstrap: [_app_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/core/components/breadcrumb/breadcrumb.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/core/components/breadcrumb/breadcrumb.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* .heading2{\n    margin-top: 2rem;\n} */\n\n\n/* label.custom-label1 {\n    text-transform: uppercase;\n    font-weight: 600;\n    font-size: 10px;\n    padding-left: 1%;\n    background: linear-gradient(to right, #cb3651, #f15a29);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n} */\n\n\n.breadcrump_top a {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    color: #777CEA;\n}\n\n\n.breadcrump_top a span {\n    color: #748494;\n    font-size: 14px;\n    margin-right: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztHQUVHOzs7QUFHSDs7Ozs7Ozs7R0FRRzs7O0FBR0g7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOzs7QUFFQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsaUJBQWlCO0FBQ3JCIiwiZmlsZSI6InNyYy9hcHAvY29yZS9jb21wb25lbnRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLmhlYWRpbmcye1xuICAgIG1hcmdpbi10b3A6IDJyZW07XG59ICovXG5cblxuLyogbGFiZWwuY3VzdG9tLWxhYmVsMSB7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDElO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2NiMzY1MSwgI2YxNWEyOSk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xufSAqL1xuXG5cbi5icmVhZGNydW1wX3RvcCBhIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmJyZWFkY3J1bXBfdG9wIGEgc3BhbiB7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIG1hcmdpbi1yaWdodDogM3B4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/core/components/breadcrumb/breadcrumb.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/core/components/breadcrumb/breadcrumb.component.ts ***!
  \********************************************************************/
/*! exports provided: BreadcrumbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumbComponent", function() { return BreadcrumbComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let BreadcrumbComponent = class BreadcrumbComponent {
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.activatedRoute.snapshot.data.breadcrumb;
        this.routeName = this.activatedRoute.snapshot.data.breadcrumb;
        // switch (this.router.url) {
        //   case '/program-list': this.routeName = 'programs'; break
        //   case '/program': this.routeName = 'programs'; break
        //   case '/setting': this.routeName = 'setting'; break
        //   case '/profile': this.routeName = 'profile'; break
        //   case '/associates': this.routeName = 'associates'; break
        //   default: this.routeName = ''
        // }
    }
};
BreadcrumbComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
BreadcrumbComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-breadcrumb',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./breadcrumb.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/breadcrumb/breadcrumb.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./breadcrumb.component.css */ "./src/app/core/components/breadcrumb/breadcrumb.component.css")).default]
    })
], BreadcrumbComponent);



/***/ }),

/***/ "./src/app/core/components/carousel/carousel.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/core/components/carousel/carousel.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".carousel-control-next {\n    right: -10%;\n  }\n  .carousel-control-prev {\n    left: -10%;\n  }\n  .carousel-control-next-icon, .carousel-control-prev-icon {\n    width: 36px;\n    height: 46px;\n    border-radius: 2px;\n    \n}\n  .carousel-control-next-icon,\n  .carousel-control-prev-icon {\n    background-color: rgba(0, 0, 0, 0.25);\n    background-size: 25%, 25%;\n  }\n  .carousel-control-prev {\n    left: -5%;\n}\n  .carousel-control-next {\n    right: -5%;\n\n}\n  .col-md-4 {\n    padding: 3px;\n}\n  .card {\n  border-radius: 0px;\n}\n  /* .carousel-control-next-icon {\n  background-image: url(../../../../assets/carouselLeftIcon.png);\n}\n.carousel-control-prev-icon {\n  background-image: url(../../../../assets/carouselRightIcon.png);\n} */\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2Nhcm91c2VsL2Nhcm91c2VsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0VBQ2I7RUFDQTtJQUNFLFVBQVU7RUFDWjtFQUNBO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7O0FBRXRCO0VBQ0U7O0lBRUUscUNBQXFDO0lBQ3JDLHlCQUF5QjtFQUMzQjtFQUNBO0lBQ0UsU0FBUztBQUNiO0VBQ0E7SUFDSSxVQUFVOztBQUVkO0VBQ0E7SUFDSSxZQUFZO0FBQ2hCO0VBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7RUFDQTs7Ozs7R0FLRyIsImZpbGUiOiJzcmMvYXBwL2NvcmUvY29tcG9uZW50cy9jYXJvdXNlbC9jYXJvdXNlbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcm91c2VsLWNvbnRyb2wtbmV4dCB7XG4gICAgcmlnaHQ6IC0xMCU7XG4gIH1cbiAgLmNhcm91c2VsLWNvbnRyb2wtcHJldiB7XG4gICAgbGVmdDogLTEwJTtcbiAgfVxuICAuY2Fyb3VzZWwtY29udHJvbC1uZXh0LWljb24sIC5jYXJvdXNlbC1jb250cm9sLXByZXYtaWNvbiB7XG4gICAgd2lkdGg6IDM2cHg7XG4gICAgaGVpZ2h0OiA0NnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBcbn1cbiAgLmNhcm91c2VsLWNvbnRyb2wtbmV4dC1pY29uLFxuICAuY2Fyb3VzZWwtY29udHJvbC1wcmV2LWljb24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gICAgYmFja2dyb3VuZC1zaXplOiAyNSUsIDI1JTtcbiAgfVxuICAuY2Fyb3VzZWwtY29udHJvbC1wcmV2IHtcbiAgICBsZWZ0OiAtNSU7XG59XG4uY2Fyb3VzZWwtY29udHJvbC1uZXh0IHtcbiAgICByaWdodDogLTUlO1xuXG59XG4uY29sLW1kLTQge1xuICAgIHBhZGRpbmc6IDNweDtcbn1cbi5jYXJkIHtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xufVxuLyogLmNhcm91c2VsLWNvbnRyb2wtbmV4dC1pY29uIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9jYXJvdXNlbExlZnRJY29uLnBuZyk7XG59XG4uY2Fyb3VzZWwtY29udHJvbC1wcmV2LWljb24ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL2Nhcm91c2VsUmlnaHRJY29uLnBuZyk7XG59ICovXG5cbiJdfQ== */");

/***/ }),

/***/ "./src/app/core/components/carousel/carousel.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/core/components/carousel/carousel.component.ts ***!
  \****************************************************************/
/*! exports provided: CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CarouselComponent = class CarouselComponent {
    constructor() { }
    ngOnInit() {
    }
};
CarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-carousel',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./carousel.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/carousel/carousel.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./carousel.component.css */ "./src/app/core/components/carousel/carousel.component.css")).default]
    })
], CarouselComponent);



/***/ }),

/***/ "./src/app/core/components/drag-drop/drag-drop.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/core/components/drag-drop/drag-drop.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n  .btn.btn-Grade{\n    border: none;\n    padding: 0 10px;\n    min-width: 120px;\n    min-height: 28px;\n    line-height: 28px;\n    float: right;\n    color: #fff;\n    background: #777CEA;\n    font-size: 12px;\n    font-weight: 500;\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    text-transform: uppercase;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2RyYWctZHJvcC9kcmFnLWRyb3AuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztFQUVFO0lBQ0UsWUFBWTtJQUNaLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osV0FBVztJQUNYLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLDJDQUEyQztJQUMzQyxrQkFBa0I7SUFDbEIseUJBQXlCO0FBQzdCIiwiZmlsZSI6InNyYy9hcHAvY29yZS9jb21wb25lbnRzL2RyYWctZHJvcC9kcmFnLWRyb3AuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG4gIC5idG4uYnRuLUdyYWRle1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBwYWRkaW5nOiAwIDEwcHg7XG4gICAgbWluLXdpZHRoOiAxMjBweDtcbiAgICBtaW4taGVpZ2h0OiAyOHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyOHB4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kOiAjNzc3Q0VBO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggMnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59Il19 */");

/***/ }),

/***/ "./src/app/core/components/drag-drop/drag-drop.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/components/drag-drop/drag-drop.component.ts ***!
  \******************************************************************/
/*! exports provided: DragDropComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DragDropComponent", function() { return DragDropComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DragDropComponent = class DragDropComponent {
    constructor() {
        this.files = [];
        this.imageUrl = "";
        this.imageSize = '';
        this.multiple = false;
        this.isMultiple = false;
        this.imageDetail = {
            name: '',
            size: '',
            url: ''
        };
        this.imagesResponse = [];
        this.images = [];
        this.onSelection = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onUpload = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.dropped = (files) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.files = files;
            files.forEach(droppedFile => {
                this.imagesResponse = [];
                this.images = [];
                // Is it a file?
                let imageUrl;
                if (droppedFile.fileEntry.isFile) {
                    var fileEntry = droppedFile.fileEntry;
                    fileEntry.file((file) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                        imageUrl = yield this.handleUpload(file);
                        this.imageSize = this.imageSizeCalculater(file.size, 2);
                        if (imageUrl != "" && imageUrl != undefined) {
                            let imageDetail = {
                                name: file.name,
                                size: this.imageSize,
                                url: imageUrl
                            };
                            this.imageDetail.name = file.name;
                            this.imageDetail.size = this.imageSize;
                            this.imagesResponse.push(file);
                            this.images.push(imageDetail);
                            this.imageUrl = "";
                        }
                        if (this.files.length == this.images.length)
                            this.onSelection.emit(this.imagesResponse);
                        console.log('images', this.images);
                    }));
                }
                else {
                    // It was a directory (empty directories are added, otherwise only files)
                    const fileEntry = droppedFile.fileEntry;
                    console.log(droppedFile.relativePath, fileEntry);
                }
            });
        });
        this.readUploadedFile = (inputFile) => {
            const temporaryFileReader = new FileReader();
            return new Promise((resolve, reject) => {
                temporaryFileReader.onerror = () => {
                    temporaryFileReader.abort();
                    console.log("Problem parsing input file.");
                };
                temporaryFileReader.onload = () => {
                    resolve(temporaryFileReader.result);
                };
                temporaryFileReader.readAsDataURL(inputFile);
            });
        };
        this.handleUpload = (file) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let fileContents = yield this.readUploadedFile(file);
            return fileContents;
        });
    }
    imageSizeCalculater(bytes, decimals) {
        if (bytes === 0 || bytes == undefined || bytes == null)
            return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
    remove(index) {
        this.images.splice(index, 1);
        this.onSelection.emit(this.images);
    }
    // upload() {
    //   this.onUpload.emit(this.imagesResponse);
    // }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], DragDropComponent.prototype, "isMultiple", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], DragDropComponent.prototype, "onSelection", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], DragDropComponent.prototype, "onUpload", void 0);
DragDropComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-drag-drop',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./drag-drop.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/drag-drop/drag-drop.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./drag-drop.component.css */ "./src/app/core/components/drag-drop/drag-drop.component.css")).default]
    })
], DragDropComponent);



/***/ }),

/***/ "./src/app/core/components/footer/footer.component.css":
/*!*************************************************************!*\
  !*** ./src/app/core/components/footer/footer.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("footer{\r\n    padding-bottom: 26px;\r\n    \r\n}\r\n.footer_inner{\r\n    padding: 20px 25px;\r\n    background-color: #FFFFFF;\r\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\r\n\r\n   \r\n}\r\n.footer_logo{\r\n    display: inline-block;\r\n    width: 100%;\r\n}\r\n.footer_logo a img{\r\n    max-width: 78px;\r\n}\r\n.footer_menu{\r\n    display: inline-block;\r\n    width: 100%;\r\n}\r\n.footer_menu ul {\r\n    margin-top: 7px;\r\n}\r\n.footer_menu ul li{\r\n    display: inline-block;\r\n    margin: 0 27px 0 0;  \r\n}\r\n.footer_menu ul a{\r\n    font-weight: normal;\r\n    font-size: 13px;\r\n    line-height: 18px;\r\n    letter-spacing: 0.001em;\r\n    color: #748494;\r\n}\r\n.footer_menu ul a:hover{\r\ncolor: #777CEA;\r\n}\r\n.copyright_text{\r\n    margin-top: 13px;\r\n}\r\n.copyright_text p{\r\nfont-weight: normal;\r\nfont-size: 12px;\r\nline-height: 16px;\r\nmargin: 0;\r\ncolor: #748494;\r\n}\r\nul,\r\nli,\r\na {\r\n    list-style: none;\r\n    padding: 0;\r\n    margin: 0;\r\n    text-decoration: none;\r\n}\r\na:hover {\r\n    text-decoration: none;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG9CQUFvQjs7QUFFeEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsMENBQTBDOzs7QUFHOUM7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsU0FBUztBQUNULGNBQWM7QUFDZDtBQUNBOzs7SUFHSSxnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLFNBQVM7SUFDVCxxQkFBcUI7QUFDekI7QUFDQTtJQUNJLHFCQUFxQjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJmb290ZXJ7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjZweDtcclxuICAgIFxyXG59XHJcbi5mb290ZXJfaW5uZXJ7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG5cclxuICAgXHJcbn1cclxuLmZvb3Rlcl9sb2dve1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmZvb3Rlcl9sb2dvIGEgaW1ne1xyXG4gICAgbWF4LXdpZHRoOiA3OHB4O1xyXG59XHJcbi5mb290ZXJfbWVudXtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5mb290ZXJfbWVudSB1bCB7XHJcbiAgICBtYXJnaW4tdG9wOiA3cHg7XHJcbn1cclxuLmZvb3Rlcl9tZW51IHVsIGxpe1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiAwIDI3cHggMCAwOyAgXHJcbn1cclxuLmZvb3Rlcl9tZW51IHVsIGF7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcclxuICAgIGNvbG9yOiAjNzQ4NDk0O1xyXG59XHJcbi5mb290ZXJfbWVudSB1bCBhOmhvdmVye1xyXG5jb2xvcjogIzc3N0NFQTtcclxufVxyXG4uY29weXJpZ2h0X3RleHR7XHJcbiAgICBtYXJnaW4tdG9wOiAxM3B4O1xyXG59XHJcbi5jb3B5cmlnaHRfdGV4dCBwe1xyXG5mb250LXdlaWdodDogbm9ybWFsO1xyXG5mb250LXNpemU6IDEycHg7XHJcbmxpbmUtaGVpZ2h0OiAxNnB4O1xyXG5tYXJnaW46IDA7XHJcbmNvbG9yOiAjNzQ4NDk0O1xyXG59XHJcbnVsLFxyXG5saSxcclxuYSB7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5hOmhvdmVyIHtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxuIl19 */");

/***/ }),

/***/ "./src/app/core/components/footer/footer.component.ts":
/*!************************************************************!*\
  !*** ./src/app/core/components/footer/footer.component.ts ***!
  \************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/footer/footer.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./footer.component.css */ "./src/app/core/components/footer/footer.component.css")).default]
    })
], FooterComponent);



/***/ }),

/***/ "./src/app/core/components/footer2/footer2.component.css":
/*!***************************************************************!*\
  !*** ./src/app/core/components/footer2/footer2.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\nfooter {\n    padding-bottom: 26px;\n}\n\n.footer_inner {\n    padding: 20px 25px;\n    background-color: #FFFFFF;\n}\n\n.footer_logo {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_logo a img {\n    max-width: 78px;\n}\n\n.footer_menu {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_menu ul {\n    margin-top: 7px;\n}\n\n.footer_menu ul li {\n    display: inline-block;\n    margin: 0 27px 0 0;\n}\n\n.footer_menu ul a {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.footer_menu ul a:hover {\n    color: #777CEA;\n}\n\n.copyright_text {\n    margin-top: 13px;\n}\n\n.copyright_text p {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    margin: 0;\n    color: #748494;\n}\n\nul,\nli,\na {\n    list-style: none;\n    padding: 0;\n    margin: 0;\n    text-decoration: none;\n}\n\na:hover {\n    text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2Zvb3RlcjIvZm9vdGVyMi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQTtJQUNJLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQix5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxjQUFjO0FBQ2xCOztBQUVBOzs7SUFHSSxnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLFNBQVM7SUFDVCxxQkFBcUI7QUFDekI7O0FBQ0E7SUFDSSxxQkFBcUI7QUFDekIiLCJmaWxlIjoic3JjL2FwcC9jb3JlL2NvbXBvbmVudHMvZm9vdGVyMi9mb290ZXIyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbmZvb3RlciB7XG4gICAgcGFkZGluZy1ib3R0b206IDI2cHg7XG59XG5cbi5mb290ZXJfaW5uZXIge1xuICAgIHBhZGRpbmc6IDIwcHggMjVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xufVxuXG4uZm9vdGVyX2xvZ28ge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmZvb3Rlcl9sb2dvIGEgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDc4cHg7XG59XG5cbi5mb290ZXJfbWVudSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9vdGVyX21lbnUgdWwge1xuICAgIG1hcmdpbi10b3A6IDdweDtcbn1cblxuLmZvb3Rlcl9tZW51IHVsIGxpIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luOiAwIDI3cHggMCAwO1xufVxuXG4uZm9vdGVyX21lbnUgdWwgYSB7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5mb290ZXJfbWVudSB1bCBhOmhvdmVyIHtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmNvcHlyaWdodF90ZXh0IHtcbiAgICBtYXJnaW4tdG9wOiAxM3B4O1xufVxuXG4uY29weXJpZ2h0X3RleHQgcCB7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbWFyZ2luOiAwO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG51bCxcbmxpLFxuYSB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5hOmhvdmVyIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59Il19 */");

/***/ }),

/***/ "./src/app/core/components/footer2/footer2.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/core/components/footer2/footer2.component.ts ***!
  \**************************************************************/
/*! exports provided: Footer2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Footer2Component", function() { return Footer2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let Footer2Component = class Footer2Component {
    constructor() { }
    ngOnInit() {
    }
};
Footer2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/footer2/footer2.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./footer2.component.css */ "./src/app/core/components/footer2/footer2.component.css")).default]
    })
], Footer2Component);



/***/ }),

/***/ "./src/app/core/components/header/header.component.css":
/*!*************************************************************!*\
  !*** ./src/app/core/components/header/header.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("header {\r\n    background-color: #fff;\r\n    padding: 19px 35px;\r\n    box-shadow: inset 0px -1px 0px #E5E9EC;\r\n    display: inline-block;\r\n    width: 100%;\r\n}\r\n\r\nheader .logo {\r\n    max-width: 136px;\r\n    float: left;\r\n    margin-top: 8px;\r\n}\r\n\r\nheader .header_right {\r\n    float: right;\r\n}\r\n\r\nheader .logo img {\r\n    max-width: 100%;\r\n}\r\n\r\nheader .navbar {\r\n    padding: 0;\r\n}\r\n\r\n.header_right .help img {\r\n    width: 20px;\r\n}\r\n\r\n.header_right a {\r\n    display: inline-block;\r\n    margin: 0 0 0 30px;\r\n}\r\n\r\n.header_right .head_profile {\r\n    width: 38px;\r\n    height: 38px;\r\n    border-radius: 50%;\r\n    overflow: hidden;\r\n    display: inline-block;\r\n    vertical-align: middle;\r\n}\r\n\r\n.header_right .head_profile img {\r\n    max-width: 100%;\r\n    width: 100%;\r\n    -o-object-fit: cover;\r\n       object-fit: cover;\r\n}\r\n\r\n.main_outer {\r\n    min-height: calc(100vh - 239px);\r\n}\r\n\r\n/* start progress bar css */\r\n\r\n.header_right .progress {\r\n    width: 33px;\r\n    height: 34px;\r\n    background: none;\r\n    margin: 0 auto;\r\n    box-shadow: none;\r\n    position: relative;\r\n  }\r\n\r\n.header_right .progress:after {\r\n    content: \"\";\r\n    width: 100%;\r\n    height: 100%;\r\n    border-radius: 50%;\r\n    border: 3px solid #efefef;\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n  }\r\n\r\n.header_right .progress > span {\r\n    width: 50%;\r\n    height: 100%;\r\n    overflow: hidden;\r\n    position: absolute;\r\n    top: 0;\r\n    z-index: 1;\r\n  }\r\n\r\n.header_right .progress .progress-left {\r\n    left: 0;\r\n  }\r\n\r\n.header_right .progress .progress-bar {\r\n    width: 100%;\r\n    height: 100%;\r\n    background: none;\r\n    border-width: 3px;\r\n    border-style: solid;\r\n    position: absolute;\r\n    top: 0;\r\n    border-color:#FFB206;\r\n  }\r\n\r\n.header_right .progress .progress-left .progress-bar {\r\n    left: 100%;\r\n    border-top-right-radius: 80px;\r\n    border-bottom-right-radius: 80px;\r\n    border-left: 0;\r\n    transform-origin: center left;\r\n  }\r\n\r\n.header_right .progress .progress-right {\r\n    right: 0;\r\n  }\r\n\r\n.header_right .progress .progress-right .progress-bar {\r\n    left: -100%;\r\n    border-top-left-radius: 80px;\r\n    border-bottom-left-radius: 80px;\r\n    border-right: 0;\r\n    transform-origin: center right;\r\n  }\r\n\r\n.header_right .progress .progress-value {\r\n    display: flex;\r\n    border-radius: 50%;\r\n    font-size: 10px;\r\n    text-align: center;\r\n    align-items: center;\r\n    justify-content: center;\r\n    height: 100%;\r\n    width: 100%;\r\n    font-weight: 900;\r\n    color: #FFB206;\r\n  }\r\n\r\n.header_right .progress .progress-value span {\r\n    font-size: 12px;\r\n    text-transform: uppercase;\r\n  }\r\n\r\n/* This for loop creates the necessary css animation names \r\n  Due to the split circle of progress-left and progress right, we must use the animations on each side. \r\n  */\r\n\r\n.progress[data-percentage=\"1\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-1 0.5s linear forwards;\r\n            animation: loading-1 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"1\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"2\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-2 0.5s linear forwards;\r\n            animation: loading-2 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"2\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"3\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-3 0.5s linear forwards;\r\n            animation: loading-3 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"3\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"4\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-4 0.5s linear forwards;\r\n            animation: loading-4 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"4\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"5\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-5 0.5s linear forwards;\r\n            animation: loading-5 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"5\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"6\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-6 0.5s linear forwards;\r\n            animation: loading-6 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"6\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"7\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-7 0.5s linear forwards;\r\n            animation: loading-7 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"7\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"8\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-8 0.5s linear forwards;\r\n            animation: loading-8 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"8\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"9\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-9 0.5s linear forwards;\r\n            animation: loading-9 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"9\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"10\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-10 0.5s linear forwards;\r\n            animation: loading-10 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"10\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"11\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-11 0.5s linear forwards;\r\n            animation: loading-11 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"11\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"12\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-12 0.5s linear forwards;\r\n            animation: loading-12 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"12\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"13\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-13 0.5s linear forwards;\r\n            animation: loading-13 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"13\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"14\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-14 0.5s linear forwards;\r\n            animation: loading-14 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"14\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"15\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-15 0.5s linear forwards;\r\n            animation: loading-15 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"15\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"16\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-16 0.5s linear forwards;\r\n            animation: loading-16 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"16\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"17\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-17 0.5s linear forwards;\r\n            animation: loading-17 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"17\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"18\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-18 0.5s linear forwards;\r\n            animation: loading-18 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"18\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"19\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-19 0.5s linear forwards;\r\n            animation: loading-19 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"19\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"20\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-20 0.5s linear forwards;\r\n            animation: loading-20 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"20\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"21\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-21 0.5s linear forwards;\r\n            animation: loading-21 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"21\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"22\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-22 0.5s linear forwards;\r\n            animation: loading-22 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"22\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"23\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-23 0.5s linear forwards;\r\n            animation: loading-23 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"23\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"24\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-24 0.5s linear forwards;\r\n            animation: loading-24 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"24\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"25\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-25 0.5s linear forwards;\r\n            animation: loading-25 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"25\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"26\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-26 0.5s linear forwards;\r\n            animation: loading-26 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"26\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"27\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-27 0.5s linear forwards;\r\n            animation: loading-27 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"27\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"28\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-28 0.5s linear forwards;\r\n            animation: loading-28 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"28\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"29\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-29 0.5s linear forwards;\r\n            animation: loading-29 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"29\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"30\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-30 0.5s linear forwards;\r\n            animation: loading-30 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"30\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"31\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-31 0.5s linear forwards;\r\n            animation: loading-31 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"31\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"32\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-32 0.5s linear forwards;\r\n            animation: loading-32 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"32\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"33\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-33 0.5s linear forwards;\r\n            animation: loading-33 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"33\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"34\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-34 0.5s linear forwards;\r\n            animation: loading-34 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"34\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"35\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-35 0.5s linear forwards;\r\n            animation: loading-35 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"35\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"36\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-36 0.5s linear forwards;\r\n            animation: loading-36 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"36\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"37\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-37 0.5s linear forwards;\r\n            animation: loading-37 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"37\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"38\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-38 0.5s linear forwards;\r\n            animation: loading-38 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"38\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"39\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-39 0.5s linear forwards;\r\n            animation: loading-39 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"39\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"40\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-40 0.5s linear forwards;\r\n            animation: loading-40 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"40\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"41\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-41 0.5s linear forwards;\r\n            animation: loading-41 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"41\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"42\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-42 0.5s linear forwards;\r\n            animation: loading-42 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"42\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"43\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-43 0.5s linear forwards;\r\n            animation: loading-43 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"43\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"44\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-44 0.5s linear forwards;\r\n            animation: loading-44 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"44\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"45\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-45 0.5s linear forwards;\r\n            animation: loading-45 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"45\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"46\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-46 0.5s linear forwards;\r\n            animation: loading-46 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"46\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"47\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-47 0.5s linear forwards;\r\n            animation: loading-47 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"47\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"48\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-48 0.5s linear forwards;\r\n            animation: loading-48 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"48\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"49\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-49 0.5s linear forwards;\r\n            animation: loading-49 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"49\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"50\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"50\"] .progress-left .progress-bar {\r\n    -webkit-animation: 0;\r\n            animation: 0;\r\n  }\r\n\r\n.progress[data-percentage=\"51\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"51\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-1 0.5s linear forwards 0.5s;\r\n            animation: loading-1 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"52\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"52\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-2 0.5s linear forwards 0.5s;\r\n            animation: loading-2 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"53\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"53\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-3 0.5s linear forwards 0.5s;\r\n            animation: loading-3 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"54\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"54\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-4 0.5s linear forwards 0.5s;\r\n            animation: loading-4 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"55\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"55\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-5 0.5s linear forwards 0.5s;\r\n            animation: loading-5 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"56\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"56\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-6 0.5s linear forwards 0.5s;\r\n            animation: loading-6 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"57\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"57\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-7 0.5s linear forwards 0.5s;\r\n            animation: loading-7 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"58\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"58\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-8 0.5s linear forwards 0.5s;\r\n            animation: loading-8 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"59\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"59\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-9 0.5s linear forwards 0.5s;\r\n            animation: loading-9 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"60\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"60\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-10 0.5s linear forwards 0.5s;\r\n            animation: loading-10 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"61\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"61\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-11 0.5s linear forwards 0.5s;\r\n            animation: loading-11 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"62\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"62\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-12 0.5s linear forwards 0.5s;\r\n            animation: loading-12 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"63\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"63\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-13 0.5s linear forwards 0.5s;\r\n            animation: loading-13 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"64\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"64\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-14 0.5s linear forwards 0.5s;\r\n            animation: loading-14 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"65\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"65\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-15 0.5s linear forwards 0.5s;\r\n            animation: loading-15 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"66\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"66\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-16 0.5s linear forwards 0.5s;\r\n            animation: loading-16 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"67\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"67\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-17 0.5s linear forwards 0.5s;\r\n            animation: loading-17 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"68\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"68\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-18 0.5s linear forwards 0.5s;\r\n            animation: loading-18 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"69\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"69\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-19 0.5s linear forwards 0.5s;\r\n            animation: loading-19 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"70\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"70\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-20 0.5s linear forwards 0.5s;\r\n            animation: loading-20 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"71\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"71\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-21 0.5s linear forwards 0.5s;\r\n            animation: loading-21 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"72\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"72\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-22 0.5s linear forwards 0.5s;\r\n            animation: loading-22 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"73\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"73\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-23 0.5s linear forwards 0.5s;\r\n            animation: loading-23 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"74\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"74\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-24 0.5s linear forwards 0.5s;\r\n            animation: loading-24 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"75\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"75\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-25 0.5s linear forwards 0.5s;\r\n            animation: loading-25 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"76\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"76\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-26 0.5s linear forwards 0.5s;\r\n            animation: loading-26 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"77\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"77\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-27 0.5s linear forwards 0.5s;\r\n            animation: loading-27 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"78\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"78\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-28 0.5s linear forwards 0.5s;\r\n            animation: loading-28 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"79\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"79\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-29 0.5s linear forwards 0.5s;\r\n            animation: loading-29 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"80\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"80\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-30 0.5s linear forwards 0.5s;\r\n            animation: loading-30 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"81\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"81\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-31 0.5s linear forwards 0.5s;\r\n            animation: loading-31 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"82\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"82\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-32 0.5s linear forwards 0.5s;\r\n            animation: loading-32 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"83\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"83\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-33 0.5s linear forwards 0.5s;\r\n            animation: loading-33 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"84\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"84\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-34 0.5s linear forwards 0.5s;\r\n            animation: loading-34 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"85\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"85\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-35 0.5s linear forwards 0.5s;\r\n            animation: loading-35 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"86\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"86\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-36 0.5s linear forwards 0.5s;\r\n            animation: loading-36 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"87\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"87\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-37 0.5s linear forwards 0.5s;\r\n            animation: loading-37 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"88\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"88\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-38 0.5s linear forwards 0.5s;\r\n            animation: loading-38 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"89\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"89\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-39 0.5s linear forwards 0.5s;\r\n            animation: loading-39 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"90\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"90\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-40 0.5s linear forwards 0.5s;\r\n            animation: loading-40 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"91\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"91\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-41 0.5s linear forwards 0.5s;\r\n            animation: loading-41 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"92\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"92\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-42 0.5s linear forwards 0.5s;\r\n            animation: loading-42 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"93\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"93\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-43 0.5s linear forwards 0.5s;\r\n            animation: loading-43 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"94\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"94\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-44 0.5s linear forwards 0.5s;\r\n            animation: loading-44 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"95\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"95\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-45 0.5s linear forwards 0.5s;\r\n            animation: loading-45 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"96\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"96\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-46 0.5s linear forwards 0.5s;\r\n            animation: loading-46 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"97\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"97\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-47 0.5s linear forwards 0.5s;\r\n            animation: loading-47 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"98\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"98\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-48 0.5s linear forwards 0.5s;\r\n            animation: loading-48 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"99\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"99\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-49 0.5s linear forwards 0.5s;\r\n            animation: loading-49 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n.progress[data-percentage=\"100\"] .progress-right .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards;\r\n            animation: loading-50 0.5s linear forwards;\r\n  }\r\n\r\n.progress[data-percentage=\"100\"] .progress-left .progress-bar {\r\n    -webkit-animation: loading-50 0.5s linear forwards 0.5s;\r\n            animation: loading-50 0.5s linear forwards 0.5s;\r\n  }\r\n\r\n@-webkit-keyframes loading-1 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(3.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-1 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(3.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-2 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(7.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-2 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(7.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-3 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(10.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-3 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(10.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-4 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(14.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-4 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(14.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-5 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(18deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-5 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(18deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-6 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(21.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-6 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(21.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-7 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(25.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-7 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(25.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-8 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(28.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-8 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(28.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-9 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(32.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-9 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(32.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-10 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(36deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-10 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(36deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-11 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(39.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-11 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(39.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-12 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(43.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-12 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(43.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-13 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(46.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-13 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(46.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-14 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(50.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-14 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(50.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-15 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(54deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-15 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(54deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-16 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(57.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-16 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(57.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-17 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(61.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-17 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(61.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-18 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(64.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-18 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(64.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-19 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(68.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-19 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(68.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-20 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(72deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-20 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(72deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-21 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(75.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-21 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(75.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-22 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(79.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-22 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(79.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-23 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(82.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-23 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(82.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-24 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(86.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-24 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(86.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-25 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(90deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-25 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(90deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-26 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(93.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-26 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(93.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-27 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(97.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-27 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(97.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-28 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(100.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-28 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(100.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-29 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(104.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-29 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(104.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-30 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(108deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-30 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(108deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-31 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(111.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-31 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(111.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-32 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(115.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-32 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(115.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-33 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(118.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-33 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(118.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-34 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(122.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-34 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(122.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-35 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(126deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-35 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(126deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-36 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(129.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-36 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(129.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-37 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(133.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-37 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(133.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-38 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(136.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-38 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(136.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-39 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(140.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-39 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(140.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-40 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(144deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-40 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(144deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-41 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(147.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-41 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(147.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-42 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(151.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-42 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(151.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-43 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(154.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-43 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(154.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-44 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(158.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-44 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(158.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-45 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(162deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-45 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(162deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-46 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(165.6deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-46 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(165.6deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-47 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(169.2deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-47 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(169.2deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-48 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(172.8deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-48 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(172.8deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-49 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(176.4deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-49 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(176.4deg);\r\n    }\r\n  }\r\n\r\n@-webkit-keyframes loading-50 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(180deg);\r\n    }\r\n  }\r\n\r\n@keyframes loading-50 {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(180deg);\r\n    }\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsc0NBQXNDO0lBQ3RDLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLFVBQVU7QUFDZDs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIscUJBQXFCO0lBQ3JCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixXQUFXO0lBQ1gsb0JBQWlCO09BQWpCLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLCtCQUErQjtBQUNuQzs7QUFFQSwyQkFBMkI7O0FBQzNCO0lBQ0ksV0FBVztJQUNYLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixrQkFBa0I7RUFDcEI7O0FBQ0E7SUFDRSxXQUFXO0lBQ1gsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sT0FBTztFQUNUOztBQUNBO0lBQ0UsVUFBVTtJQUNWLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixVQUFVO0VBQ1o7O0FBQ0E7SUFDRSxPQUFPO0VBQ1Q7O0FBQ0E7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sb0JBQW9CO0VBQ3RCOztBQUNBO0lBQ0UsVUFBVTtJQUNWLDZCQUE2QjtJQUM3QixnQ0FBZ0M7SUFDaEMsY0FBYztJQUVkLDZCQUE2QjtFQUMvQjs7QUFDQTtJQUNFLFFBQVE7RUFDVjs7QUFDQTtJQUNFLFdBQVc7SUFDWCw0QkFBNEI7SUFDNUIsK0JBQStCO0lBQy9CLGVBQWU7SUFFZiw4QkFBOEI7RUFDaEM7O0FBQ0E7SUFDRSxhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osV0FBVztJQUNYLGdCQUFnQjtJQUNoQixjQUFjO0VBQ2hCOztBQUNBO0lBQ0UsZUFBZTtJQUNmLHlCQUF5QjtFQUMzQjs7QUFFQTs7R0FFQzs7QUFDRDtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGlEQUF5QztZQUF6Qyx5Q0FBeUM7RUFDM0M7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxvQkFBWTtZQUFaLFlBQVk7RUFDZDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxzREFBOEM7WUFBOUMsOENBQThDO0VBQ2hEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHNEQUE4QztZQUE5Qyw4Q0FBOEM7RUFDaEQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0Usc0RBQThDO1lBQTlDLDhDQUE4QztFQUNoRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxzREFBOEM7WUFBOUMsOENBQThDO0VBQ2hEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHNEQUE4QztZQUE5Qyw4Q0FBOEM7RUFDaEQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0Usc0RBQThDO1lBQTlDLDhDQUE4QztFQUNoRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSxzREFBOEM7WUFBOUMsOENBQThDO0VBQ2hEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHNEQUE4QztZQUE5Qyw4Q0FBOEM7RUFDaEQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0Usc0RBQThDO1lBQTlDLDhDQUE4QztFQUNoRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRSxrREFBMEM7WUFBMUMsMENBQTBDO0VBQzVDOztBQUNBO0lBQ0UsdURBQStDO1lBQS9DLCtDQUErQztFQUNqRDs7QUFFQTtJQUNFLGtEQUEwQztZQUExQywwQ0FBMEM7RUFDNUM7O0FBQ0E7SUFDRSx1REFBK0M7WUFBL0MsK0NBQStDO0VBQ2pEOztBQUVBO0lBQ0Usa0RBQTBDO1lBQTFDLDBDQUEwQztFQUM1Qzs7QUFDQTtJQUNFLHVEQUErQztZQUEvQywrQ0FBK0M7RUFDakQ7O0FBRUE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsd0JBQXdCO0lBQzFCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMEJBQTBCO0lBQzVCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUsMkJBQTJCO0lBQzdCO0VBQ0Y7O0FBQ0E7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0Y7O0FBVEE7SUFDRTtNQUVFLHVCQUF1QjtJQUN6QjtJQUNBO01BRUUseUJBQXlCO0lBQzNCO0VBQ0YiLCJmaWxlIjoic3JjL2FwcC9jb3JlL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaGVhZGVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgICBwYWRkaW5nOiAxOXB4IDM1cHg7XHJcbiAgICBib3gtc2hhZG93OiBpbnNldCAwcHggLTFweCAwcHggI0U1RTlFQztcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5oZWFkZXIgLmxvZ28ge1xyXG4gICAgbWF4LXdpZHRoOiAxMzZweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG59XHJcblxyXG5oZWFkZXIgLmhlYWRlcl9yaWdodCB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbmhlYWRlciAubG9nbyBpbWcge1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5oZWFkZXIgLm5hdmJhciB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4uaGVhZGVyX3JpZ2h0IC5oZWxwIGltZyB7XHJcbiAgICB3aWR0aDogMjBweDtcclxufVxyXG5cclxuLmhlYWRlcl9yaWdodCBhIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIG1hcmdpbjogMCAwIDAgMzBweDtcclxufVxyXG5cclxuLmhlYWRlcl9yaWdodCAuaGVhZF9wcm9maWxlIHtcclxuICAgIHdpZHRoOiAzOHB4O1xyXG4gICAgaGVpZ2h0OiAzOHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbi5oZWFkZXJfcmlnaHQgLmhlYWRfcHJvZmlsZSBpbWcge1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxufVxyXG5cclxuLm1haW5fb3V0ZXIge1xyXG4gICAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDIzOXB4KTtcclxufVxyXG5cclxuLyogc3RhcnQgcHJvZ3Jlc3MgYmFyIGNzcyAqL1xyXG4uaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyB7XHJcbiAgICB3aWR0aDogMzNweDtcclxuICAgIGhlaWdodDogMzRweDtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5oZWFkZXJfcmlnaHQgLnByb2dyZXNzOmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGJvcmRlcjogM3B4IHNvbGlkICNlZmVmZWY7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gIH1cclxuICAuaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyA+IHNwYW4ge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gIH1cclxuICAuaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyAucHJvZ3Jlc3MtbGVmdCB7XHJcbiAgICBsZWZ0OiAwO1xyXG4gIH1cclxuICAuaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgIGJvcmRlci13aWR0aDogM3B4O1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvcmRlci1jb2xvcjojRkZCMjA2O1xyXG4gIH1cclxuICAuaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGxlZnQ6IDEwMCU7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogODBweDtcclxuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA4MHB4O1xyXG4gICAgYm9yZGVyLWxlZnQ6IDA7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IGNlbnRlciBsZWZ0O1xyXG4gICAgdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyIGxlZnQ7XHJcbiAgfVxyXG4gIC5oZWFkZXJfcmlnaHQgLnByb2dyZXNzIC5wcm9ncmVzcy1yaWdodCB7XHJcbiAgICByaWdodDogMDtcclxuICB9XHJcbiAgLmhlYWRlcl9yaWdodCAucHJvZ3Jlc3MgLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgbGVmdDogLTEwMCU7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA4MHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogODBweDtcclxuICAgIGJvcmRlci1yaWdodDogMDtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyIHJpZ2h0O1xyXG4gICAgdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyIHJpZ2h0O1xyXG4gIH1cclxuICAuaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyAucHJvZ3Jlc3MtdmFsdWUge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcclxuICAgIGNvbG9yOiAjRkZCMjA2O1xyXG4gIH1cclxuICAuaGVhZGVyX3JpZ2h0IC5wcm9ncmVzcyAucHJvZ3Jlc3MtdmFsdWUgc3BhbiB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIH1cclxuICBcclxuICAvKiBUaGlzIGZvciBsb29wIGNyZWF0ZXMgdGhlIG5lY2Vzc2FyeSBjc3MgYW5pbWF0aW9uIG5hbWVzIFxyXG4gIER1ZSB0byB0aGUgc3BsaXQgY2lyY2xlIG9mIHByb2dyZXNzLWxlZnQgYW5kIHByb2dyZXNzIHJpZ2h0LCB3ZSBtdXN0IHVzZSB0aGUgYW5pbWF0aW9ucyBvbiBlYWNoIHNpZGUuIFxyXG4gICovXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjFcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTEgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMiAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjJcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiM1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjRcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNSAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjVcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy02IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjdcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTcgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctOCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjhcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy05IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjEwXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xMCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjEwXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTFcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTExIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTFcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxMlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMTIgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxMlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjEzXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xMyAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjEzXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTRcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTE0IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTRcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxNVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMTUgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxNVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjE2XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xNiAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjE2XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTdcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTE3IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTdcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxOFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMTggMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIxOFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjE5XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xOSAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjE5XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjBcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTIwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjBcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIyMVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjEgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIyMVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjIyXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yMiAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjIyXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjNcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTIzIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjNcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIyNFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjQgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIyNFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjI1XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yNSAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjI1XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjZcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTI2IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjZcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIyN1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjcgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIyN1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjI4XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yOCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjI4XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjlcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTI5IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMjlcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzMFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzMFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjMxXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zMSAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjMxXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMzJcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTMyIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMzJcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzM1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzMgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzM1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjM0XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zNCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjM0XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMzVcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTM1IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMzVcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzNlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzYgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzNlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjM3XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zNyAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjM3XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMzhcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTM4IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMzhcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzOVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzkgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCIzOVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQwXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQwXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNDFcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQxIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNDFcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0MlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNDIgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0MlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQzXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00MyAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQzXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNDRcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQ0IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNDRcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0NVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNDUgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0NVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQ2XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00NiAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQ2XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNDdcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQ3IDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNDdcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0OFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNDggMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI0OFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogMDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQ5XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00OSAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjQ5XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiAwO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNTBcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNTBcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1MVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1MVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1MlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1MlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1M1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1M1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1NFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1NFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1NVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1NVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1NlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1NlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy02IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1N1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1N1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy03IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1OFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1OFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy04IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1OVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI1OVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy05IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2MFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2MFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xMCAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNjFcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNjFcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMTEgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjYyXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjYyXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTEyIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2M1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2M1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xMyAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNjRcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNjRcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMTQgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjY1XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjY1XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTE1IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2NlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2NlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xNiAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNjdcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNjdcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMTcgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjY4XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjY4XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTE4IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2OVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI2OVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0xOSAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzBcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzBcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjAgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjcxXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjcxXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTIxIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3MlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3MlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yMiAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzNcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzNcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjMgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjc0XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjc0XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTI0IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3NVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3NVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yNSAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzZcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzZcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjYgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjc3XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjc3XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTI3IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3OFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI3OFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0yOCAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzlcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiNzlcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMjkgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjgwXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjgwXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTMwIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI4MVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI4MVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zMSAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiODJcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiODJcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzIgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjgzXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjgzXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTMzIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI4NFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI4NFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zNCAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiODVcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiODVcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzUgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjg2XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjg2XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTM2IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI4N1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI4N1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy0zNyAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiODhcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiODhcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctMzggMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjg5XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjg5XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTM5IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5MFwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5MFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00MCAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOTFcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOTFcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNDEgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjkyXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjkyXCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQyIDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5M1wiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5M1wiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00MyAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOTRcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOTRcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNDQgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjk1XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjk1XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQ1IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5NlwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5NlwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00NiAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOTdcIl0gLnByb2dyZXNzLXJpZ2h0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTUwIDAuNXMgbGluZWFyIGZvcndhcmRzO1xyXG4gIH1cclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiOTdcIl0gLnByb2dyZXNzLWxlZnQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNDcgMC41cyBsaW5lYXIgZm9yd2FyZHMgMC41cztcclxuICB9XHJcbiAgXHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjk4XCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjk4XCJdIC5wcm9ncmVzcy1sZWZ0IC5wcm9ncmVzcy1iYXIge1xyXG4gICAgYW5pbWF0aW9uOiBsb2FkaW5nLTQ4IDAuNXMgbGluZWFyIGZvcndhcmRzIDAuNXM7XHJcbiAgfVxyXG4gIFxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5OVwiXSAucHJvZ3Jlc3MtcmlnaHQgLnByb2dyZXNzLWJhciB7XHJcbiAgICBhbmltYXRpb246IGxvYWRpbmctNTAgMC41cyBsaW5lYXIgZm9yd2FyZHM7XHJcbiAgfVxyXG4gIC5wcm9ncmVzc1tkYXRhLXBlcmNlbnRhZ2U9XCI5OVwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy00OSAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICAucHJvZ3Jlc3NbZGF0YS1wZXJjZW50YWdlPVwiMTAwXCJdIC5wcm9ncmVzcy1yaWdodCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcztcclxuICB9XHJcbiAgLnByb2dyZXNzW2RhdGEtcGVyY2VudGFnZT1cIjEwMFwiXSAucHJvZ3Jlc3MtbGVmdCAucHJvZ3Jlc3MtYmFyIHtcclxuICAgIGFuaW1hdGlvbjogbG9hZGluZy01MCAwLjVzIGxpbmVhciBmb3J3YXJkcyAwLjVzO1xyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMSB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMy42KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMy42ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTIge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDcuMik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDcuMmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0zIHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMC44KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTAuOGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy00IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxNC40KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTQuNGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy01IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxOCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE4ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTYge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDIxLjYpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyMS42ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTcge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI1LjIpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyNS4yZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTgge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI4LjgpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyOC44ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTkge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDMyLjQpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzMi40ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTEwIHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDM2ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTExIHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzOS42KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMzkuNmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0xMiB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDMuMik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDQzLjJkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMTMge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ2LjgpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0Ni44ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTE0IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg1MC40KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoNTAuNGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0xNSB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNTQpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg1NGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0xNiB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNTcuNik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDU3LjZkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMTcge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDYxLjIpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg2MS4yZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTE4IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg2NC44KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoNjQuOGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0xOSB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNjguNCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDY4LjRkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMjAge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDcyKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoNzJkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMjEge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDc1LjYpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg3NS42ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTIyIHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg3OS4yKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoNzkuMmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0yMyB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoODIuOCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDgyLjhkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMjQge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDg2LjQpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg4Ni40ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTI1IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTI2IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5My42KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTMuNmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0yNyB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoOTcuMik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDk3LjJkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMjgge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDEwMC44KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTAwLjhkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMjkge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDEwNC40KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTA0LjRkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctMzAge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDEwOCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDEwOGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0zMSB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTExLjYpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxMTEuNmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0zMiB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTE1LjIpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxMTUuMmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0zMyB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTE4LjgpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxMTguOGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0zNCB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTIyLjQpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxMjIuNGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy0zNSB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTI2KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTI2ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTM2IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMjkuNik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDEyOS42ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTM3IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMzMuMik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDEzMy4yZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTM4IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxMzYuOCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDEzNi44ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTM5IHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxNDAuNCk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE0MC40ZGVnKTtcclxuICAgIH1cclxuICB9XHJcbiAgQGtleWZyYW1lcyBsb2FkaW5nLTQwIHtcclxuICAgIDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICAxMDAlIHtcclxuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxNDQpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNDRkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctNDEge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE0Ny42KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTQ3LjZkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctNDIge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE1MS4yKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTUxLjJkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctNDMge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE1NC44KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTU0LjhkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctNDQge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE1OC40KTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTU4LjRkZWcpO1xyXG4gICAgfVxyXG4gIH1cclxuICBAa2V5ZnJhbWVzIGxvYWRpbmctNDUge1xyXG4gICAgMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDE2Mik7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE2MmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy00NiB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTY1LjYpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNjUuNmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy00NyB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTY5LjIpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNjkuMmRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy00OCB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTcyLjgpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNzIuOGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy00OSB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTc2LjQpO1xyXG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNzYuNGRlZyk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIEBrZXlmcmFtZXMgbG9hZGluZy01MCB7XHJcbiAgICAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMTgwKTtcclxuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcclxuICAgIH1cclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/core/components/header/header.component.ts":
/*!************************************************************!*\
  !*** ./src/app/core/components/header/header.component.ts ***!
  \************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models */ "./src/app/core/models/index.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _services_api_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/api.service.service */ "./src/app/core/services/api.service.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_user_data_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/user-data.service */ "./src/app/core/services/user-data.service.ts");








let HeaderComponent = class HeaderComponent {
    constructor(router, auth, activatedRoute, apiservice, userdataservice, snack) {
        this.router = router;
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.apiservice = apiservice;
        this.userdataservice = userdataservice;
        this.snack = snack;
        this.user = new _models__WEBPACK_IMPORTED_MODULE_3__["User"];
        this.program = "";
        this.setting = "";
        this.showMenu = false;
        this.menuClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this._user = {};
        this.userData = {};
        this.profileProgress = '';
        this.message = 'Please Login First!';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        this.user = this.auth.currentUser();
        this.auth.userChanges.subscribe(user => this.user = user);
        // var retrievedObject = localStorax```ge.getItem('userData');
        // this.userData = JSON.parse(retrievedObject);
        let config = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSnackBarConfig"]();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        this.getProfileProgress();
    }
    logo() {
        this.router.navigate(['/search']);
    }
    addProgram() {
        this.router.navigate(['/program/add']);
    }
    profile() {
        if (this.user.role === "parent") {
            this.router.navigate(['parent/parent-profile']);
        }
        else {
            if (this.user.role === "provider") {
                this.router.navigate(['/profile']);
            }
            else {
                let msg = 'Something Went Wrong!';
                this.snack.open(msg, 'OK', { duration: 5000 });
            }
            // }
        }
    }
    getProfileProgress() {
        this.apiservice.getProfileProgress(this.user.id, this.user.role).subscribe((res) => {
            this.profileProgress = res.data.profileProgress.toString();
            $("#progress").attr("data-percentage", this.profileProgress);
        });
    }
    getUserById() {
        this.apiservice.getUserById(this.user.id).subscribe(res => {
            console.log('got user Data by Id (header.component)', res);
            this.user = res;
        });
    }
    ngOnInit() {
        console.log('header', this.user);
        // this.getProfileProgress();
    }
    logout() {
        // this.auth.logout();
        this.userdataservice.logout();
        this.router.navigate(['/login']);
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_api_service_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] },
    { type: _services_user_data_service__WEBPACK_IMPORTED_MODULE_7__["UserDataService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSnackBar"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], HeaderComponent.prototype, "showMenu", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], HeaderComponent.prototype, "menuClicked", void 0);
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/header/header.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header.component.css */ "./src/app/core/components/header/header.component.css")).default]
    })
], HeaderComponent);



/***/ }),

/***/ "./src/app/core/components/header2/header2.component.css":
/*!***************************************************************!*\
  !*** ./src/app/core/components/header2/header2.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("header {\n    background-color: #fff;\n    padding: 19px 35px;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\nheader .logo {\n    max-width: 136px;\n}\n\nheader .logo img {\n\n    max-width: 100%;\n    \n}\n\n.content_outer {\n    padding-top: 99px;\n    padding-bottom: 36px;\n}\n\nheader .navbar {\n    padding: 0;\n}\n\nheader .navbar-light .navbar-nav .nav-link {\n    font-size: 13px;\n    font-weight: normal;\n    line-height: 16px;\n    color: #748494;\n    padding: 9px 0;\n    margin-right: 25px;\n}\n\nheader .navbar-nav .btn_style {\n    font-family: 'Open Sans';\n    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    font-weight: 600;\n    font-size: 13px;\n    color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL2hlYWRlcjIvaGVhZGVyMi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixzQ0FBc0M7QUFDMUM7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7O0lBRUksZUFBZTs7QUFFbkI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksVUFBVTtBQUNkOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGNBQWM7SUFDZCxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsNENBQTRDO0lBQzVDLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLFdBQVc7QUFDZiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvY29tcG9uZW50cy9oZWFkZXIyL2hlYWRlcjIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImhlYWRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBwYWRkaW5nOiAxOXB4IDM1cHg7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMHB4IC0xcHggMHB4ICNFNUU5RUM7XG59XG5cbmhlYWRlciAubG9nbyB7XG4gICAgbWF4LXdpZHRoOiAxMzZweDtcbn1cblxuaGVhZGVyIC5sb2dvIGltZyB7XG5cbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgXG59XG5cbi5jb250ZW50X291dGVyIHtcbiAgICBwYWRkaW5nLXRvcDogOTlweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMzZweDtcbn1cblxuaGVhZGVyIC5uYXZiYXIge1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbmhlYWRlciAubmF2YmFyLWxpZ2h0IC5uYXZiYXItbmF2IC5uYXYtbGluayB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgcGFkZGluZzogOXB4IDA7XG4gICAgbWFyZ2luLXJpZ2h0OiAyNXB4O1xufVxuXG5oZWFkZXIgLm5hdmJhci1uYXYgLmJ0bl9zdHlsZSB7XG4gICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnO1xuICAgIGJveC1zaGFkb3c6IDBweCA2cHggMjBweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogI2ZmZjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/core/components/header2/header2.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/core/components/header2/header2.component.ts ***!
  \**************************************************************/
/*! exports provided: Header2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header2Component", function() { return Header2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let Header2Component = class Header2Component {
    constructor() { }
    ngOnInit() {
    }
};
Header2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/header2/header2.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header2.component.css */ "./src/app/core/components/header2/header2.component.css")).default]
    })
], Header2Component);



/***/ }),

/***/ "./src/app/core/components/nav/nav.component.css":
/*!*******************************************************!*\
  !*** ./src/app/core/components/nav/nav.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n\n/* start navigation css */\n\n.navigation {\n  display: inline-block;\n  width: 100%;\n  background: #FFFFFF;\n  box-shadow: inset 0px -1px 0px #E5E9EC;\n  max-height: 57px;\n}\n\n.navigation li{\n  display: inline-block;  \n  margin-right: 47px;\n}\n\n.navigation li:last-child{\n  margin:0;\n}\n\n.navigation li a{\n  padding: 17px 10px 20px;\n  font-size: 13px;\n  display: inline-block;\n  color: #748494;\n  position: relative;\n  transition: 0.8s;\n  overflow: hidden;\n}\n\n.navigation li a::after{\n  content: '';\n  transition: 0.8s;\n  width: 100%;\n  height: 2px;\n  position: absolute;\n  bottom: 0;\n  left: -100%;\n  background: linear-gradient(to right, #777CEA, #777CEA);\n}\n\n.navigation li a.active,\n.navigation li a:hover\n{\n  background: linear-gradient(to right, #777CEA, #777CEA);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;;\n}\n\n.navigation li a.active::after,\n.navigation li a:hover::after\n{\nleft: 0;\n}\n\n@media (max-width:991px){\n  .navigation li{\n    margin-right: 23px;\n  }\n}\n\n@media (max-width:767px){\n  .navigation li{  \n    margin-right: 18px;\n  }\n  .navigation li a{\n    padding: 17px 10px 20px;\n    font-size: 12px;\n  }\n\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9jb21wb25lbnRzL25hdi9uYXYuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFHQSx5QkFBeUI7O0FBRXpCO0VBQ0UscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsc0NBQXNDO0VBQ3RDLGdCQUFnQjtBQUNsQjs7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQixrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxRQUFRO0FBQ1Y7O0FBQ0E7RUFDRSx1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUFDbEI7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxXQUFXO0VBQ1gsdURBQXVEO0FBQ3pEOztBQUNBOzs7RUFHRSx1REFBdUQ7RUFDdkQsNkJBQTZCO0VBQzdCLG9DQUFvQztBQUN0Qzs7QUFDQTs7O0FBR0EsT0FBTztBQUNQOztBQUNBO0VBQ0U7SUFDRSxrQkFBa0I7RUFDcEI7QUFDRjs7QUFFQTtFQUNFO0lBQ0Usa0JBQWtCO0VBQ3BCO0VBQ0E7SUFDRSx1QkFBdUI7SUFDdkIsZUFBZTtFQUNqQjs7QUFFRiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvY29tcG9uZW50cy9uYXYvbmF2LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuXG4vKiBzdGFydCBuYXZpZ2F0aW9uIGNzcyAqL1xuXG4ubmF2aWdhdGlvbiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gIGJveC1zaGFkb3c6IGluc2V0IDBweCAtMXB4IDBweCAjRTVFOUVDO1xuICBtYXgtaGVpZ2h0OiA1N3B4O1xufVxuLm5hdmlnYXRpb24gbGl7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgIFxuICBtYXJnaW4tcmlnaHQ6IDQ3cHg7XG59XG4ubmF2aWdhdGlvbiBsaTpsYXN0LWNoaWxke1xuICBtYXJnaW46MDtcbn1cbi5uYXZpZ2F0aW9uIGxpIGF7XG4gIHBhZGRpbmc6IDE3cHggMTBweCAyMHB4O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgY29sb3I6ICM3NDg0OTQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdHJhbnNpdGlvbjogMC44cztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5uYXZpZ2F0aW9uIGxpIGE6OmFmdGVye1xuICBjb250ZW50OiAnJztcbiAgdHJhbnNpdGlvbjogMC44cztcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogLTEwMCU7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzc3N0NFQSwgIzc3N0NFQSk7XG59XG4ubmF2aWdhdGlvbiBsaSBhLmFjdGl2ZSxcbi5uYXZpZ2F0aW9uIGxpIGE6aG92ZXJcbntcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjNzc3Q0VBLCAjNzc3Q0VBKTtcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDs7XG59XG4ubmF2aWdhdGlvbiBsaSBhLmFjdGl2ZTo6YWZ0ZXIsXG4ubmF2aWdhdGlvbiBsaSBhOmhvdmVyOjphZnRlclxue1xubGVmdDogMDtcbn1cbkBtZWRpYSAobWF4LXdpZHRoOjk5MXB4KXtcbiAgLm5hdmlnYXRpb24gbGl7XG4gICAgbWFyZ2luLXJpZ2h0OiAyM3B4O1xuICB9XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjc2N3B4KXtcbiAgLm5hdmlnYXRpb24gbGl7ICBcbiAgICBtYXJnaW4tcmlnaHQ6IDE4cHg7XG4gIH1cbiAgLm5hdmlnYXRpb24gbGkgYXtcbiAgICBwYWRkaW5nOiAxN3B4IDEwcHggMjBweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gIH1cblxufVxuIl19 */");

/***/ }),

/***/ "./src/app/core/components/nav/nav.component.ts":
/*!******************************************************!*\
  !*** ./src/app/core/components/nav/nav.component.ts ***!
  \******************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let NavComponent = class NavComponent {
    constructor(router) {
        this.router = router;
        this.dashboard = "";
        this.program = "";
        this.setting = "";
        this.profile = "";
        this.associates = "";
        this.hideNav = true;
        this.router.events.filter((event) => event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]).subscribe(event => {
            this.routeName = event.url;
        });
    }
    ngOnInit() {
        if (this.routeName === '/program/home') {
            this.dashboard = 'active';
        }
        if (this.routeName === '/program/list') {
            this.program = 'active';
            this.dashboard = '';
        }
        if (this.routeName === '/program/detail') {
            this.program = 'active';
            this.dashboard = '';
        }
        if (this.routeName === '/program/add') {
            this.program = 'active';
            this.dashboard = '';
        }
        if (this.routeName === 'profile') {
            this.profile = 'active';
            this.dashboard = '';
        }
        if (this.routeName === '/program/setting') {
            this.setting = 'active';
            this.dashboard = '';
        }
    }
};
NavComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nav',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/components/nav/nav.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./nav.component.css */ "./src/app/core/components/nav/nav.component.css")).default]
    })
], NavComponent);



/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_file_drop__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-file-drop */ "./node_modules/ngx-file-drop/fesm2015/ngx-file-drop.js");
/* harmony import */ var ng2_toasty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-toasty */ "./node_modules/ng2-toasty/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _app_core_components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../app/core/components/footer/footer.component */ "./src/app/core/components/footer/footer.component.ts");
/* harmony import */ var _app_core_components_header_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../app/core/components/header/header.component */ "./src/app/core/components/header/header.component.ts");
/* harmony import */ var _app_core_services_user_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../app/core/services/user.service */ "./src/app/core/services/user.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _guards_user_guard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./guards/user.guard */ "./src/app/core/guards/user.guard.ts");
/* harmony import */ var _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/nav/nav.component */ "./src/app/core/components/nav/nav.component.ts");
/* harmony import */ var _components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/breadcrumb/breadcrumb.component */ "./src/app/core/components/breadcrumb/breadcrumb.component.ts");
/* harmony import */ var _components_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/carousel/carousel.component */ "./src/app/core/components/carousel/carousel.component.ts");
/* harmony import */ var _components_drag_drop_drag_drop_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/drag-drop/drag-drop.component */ "./src/app/core/components/drag-drop/drag-drop.component.ts");
/* harmony import */ var _components_header2_header2_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/header2/header2.component */ "./src/app/core/components/header2/header2.component.ts");
/* harmony import */ var _components_footer2_footer2_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/footer2/footer2.component */ "./src/app/core/components/footer2/footer2.component.ts");



















const components = [
    _app_core_components_header_header_component__WEBPACK_IMPORTED_MODULE_9__["HeaderComponent"],
    _app_core_components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
    _components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_14__["BreadcrumbComponent"],
    _components_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_15__["CarouselComponent"],
    _components_drag_drop_drag_drop_component__WEBPACK_IMPORTED_MODULE_16__["DragDropComponent"],
    _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_13__["NavComponent"],
    _components_header2_header2_component__WEBPACK_IMPORTED_MODULE_17__["Header2Component"],
    _components_footer2_footer2_component__WEBPACK_IMPORTED_MODULE_18__["Footer2Component"]
];
const thirdPartyModules = [
    ng2_toasty__WEBPACK_IMPORTED_MODULE_6__["ToastyModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTabsModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBarModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCheckboxModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRadioModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatFormFieldModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTooltipModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatChipsModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressSpinnerModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatExpansionModule"],
    _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
    ngx_file_drop__WEBPACK_IMPORTED_MODULE_5__["NgxFileDropModule"],
];
const services = [
    _app_core_services_user_service__WEBPACK_IMPORTED_MODULE_10__["UserService"],
    _services_auth_service__WEBPACK_IMPORTED_MODULE_11__["AuthService"],
];
const guards = [
    _guards_user_guard__WEBPACK_IMPORTED_MODULE_12__["UserGuard"],
];
let CoreModule = class CoreModule {
};
CoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            // BrowserAnimationsModule,
            ...thirdPartyModules,
        ],
        declarations: [...components, _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_13__["NavComponent"], _components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_14__["BreadcrumbComponent"], _components_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_15__["CarouselComponent"], _components_drag_drop_drag_drop_component__WEBPACK_IMPORTED_MODULE_16__["DragDropComponent"], _components_footer2_footer2_component__WEBPACK_IMPORTED_MODULE_18__["Footer2Component"],],
        exports: [...thirdPartyModules, ...components],
        entryComponents: [],
        providers: [
            ...services, ...guards
        ]
    })
], CoreModule);



/***/ }),

/***/ "./src/app/core/guards/user.guard.ts":
/*!*******************************************!*\
  !*** ./src/app/core/guards/user.guard.ts ***!
  \*******************************************/
/*! exports provided: UserGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserGuard", function() { return UserGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/core/services/auth.service.ts");




let UserGuard = class UserGuard {
    constructor(router, auth) {
        this.router = router;
        this.auth = auth;
    }
    canActivate(next, state) {
        const currentUser = this.auth.currentUser();
        if (!currentUser) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
};
UserGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
UserGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], UserGuard);



/***/ }),

/***/ "./src/app/core/models/category.model.ts":
/*!***********************************************!*\
  !*** ./src/app/core/models/category.model.ts ***!
  \***********************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return Category; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Category {
    constructor(obj) {
        if (!obj) {
            return;
        }
        this._id = obj._id;
        this.token = obj.token;
        this.name = obj.name;
        this.description = obj.description;
    }
}


/***/ }),

/***/ "./src/app/core/models/child.model.ts":
/*!********************************************!*\
  !*** ./src/app/core/models/child.model.ts ***!
  \********************************************/
/*! exports provided: Child */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Child", function() { return Child; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Child {
    constructor(obj) {
        this.interestInfo = [];
        if (!obj) {
            return;
        }
        this.id = obj.id;
        this.name = obj.name;
        this.emailId = obj.emailId;
        this.email = obj.email;
        this.addressLine1 = obj.addressLine1;
        this.addressLine2 = obj.addressLine2;
        this.mobile = obj.mobile;
        this.phoneNumber = obj.phoneNumber;
        this._id = obj._id;
        this.parent = obj.parent;
        this.dob = obj.dob;
        this.age = obj.age;
        this.interestInfo = obj.interestInfo;
        this.relationToChild = obj.relationToChild;
        this.avtar = obj.avtar;
        this.contactOtherInfo = obj.contactOtherInfo;
        this.schoolInfo = obj.schoolInfo;
        this.dislikes = obj.dislikes;
        this.alergies = obj.alergies;
        this.parentNotes = obj.parentNotes;
        this.parentId = obj.parentId;
    }
}


/***/ }),

/***/ "./src/app/core/models/index.ts":
/*!**************************************!*\
  !*** ./src/app/core/models/index.ts ***!
  \**************************************/
/*! exports provided: User, Program, Child, Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.model */ "./src/app/core/models/user.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _user_model__WEBPACK_IMPORTED_MODULE_1__["User"]; });

/* harmony import */ var _program_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./program.model */ "./src/app/core/models/program.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Program", function() { return _program_model__WEBPACK_IMPORTED_MODULE_2__["Program"]; });

/* harmony import */ var _child_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./child.model */ "./src/app/core/models/child.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Child", function() { return _child_model__WEBPACK_IMPORTED_MODULE_3__["Child"]; });

/* harmony import */ var _category_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./category.model */ "./src/app/core/models/category.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return _category_model__WEBPACK_IMPORTED_MODULE_4__["Category"]; });






// export * from './payment-gateway.model';
// export * from './payment-type.model';
// export * from './payment-mode.model';
// export * from './payment-cardScheme.model';


/***/ }),

/***/ "./src/app/core/models/program.model.ts":
/*!**********************************************!*\
  !*** ./src/app/core/models/program.model.ts ***!
  \**********************************************/
/*! exports provided: Program */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Program", function() { return Program; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Program {
    constructor(obj) {
        this.time = {};
        this.date = {};
        this.ageGroup = {};
        this.bookingCancelledIn = {};
        this.capacity = {};
        this.tags = [];
        if (!obj) {
            return;
        }
        this.id = obj.id;
        this._id = obj._id;
        this.userId = obj.userId;
        this.programId = obj.programId;
        this.name = obj.name;
        this.programCoverPic = obj.programCoverPic;
        this.email = obj.name;
        this.description = obj.description;
        this.type = obj.type;
        this.price = obj.price;
        this.location = obj.location;
        this.code = obj.code;
        this.status = obj.status;
        this.time = obj.time;
        this.date = obj.date;
        this.ageGroup = obj.ageGroup;
        this.bookingCancelledIn = obj.bookingCancelledIn;
        this.duration = obj.duration;
        this.isFree = obj.isFree;
        this.pricePerParticipant = obj.pricePerParticipant;
        this.priceForSiblings = obj.priceForSiblings;
        this.specialInstructions = obj.specialInstructions;
        this.adultAssistanceIsRequried = obj.adultAssistanceIsRequried;
        this.capacity = obj.capacity;
        this.emails = obj.emails;
        this.userId = obj.userId;
        ;
        this.addresses = obj.addresses;
        this.tags = obj.tags;
        this.categoryId = obj.categoryId;
        this.timelinePics = obj.timelinePics;
        this.user = obj.user;
    }
}


/***/ }),

/***/ "./src/app/core/models/user.model.ts":
/*!*******************************************!*\
  !*** ./src/app/core/models/user.model.ts ***!
  \*******************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class User {
    constructor(obj) {
        if (!obj) {
            return;
        }
        this.id = obj.id;
        this._id = obj._id;
        this.banners = obj.banners;
        this.isActivated = obj.isActivated;
        this.isSuccess = obj.isSuccess;
        this.fullName = obj.fullName;
        this.firstName = obj.firstName;
        this.lastName = obj.lastName;
        this.about = obj.about;
        this.emailId = obj.emailId;
        this.email = obj.email;
        this.addressLine1 = obj.addressLine1;
        this.addressLine2 = obj.addressLine2;
        this.type = obj.type;
        this.mobile = obj.mobile;
        this.phoneNumber = obj.phoneNumber;
        this.password = obj.password;
        this.newPassword = obj.newPassword;
        this.confirmPassword = obj.confirmPassword;
        this.city = obj.city;
        this.token = obj.token;
        this.createdAt = obj.createdAt;
        this.otp = obj.otp;
        this.otpToken = obj.otpToken;
        this.avatarImages = obj.avatarImages;
        this.category = obj.category;
        this.description = obj.description;
        this.facebook = obj.facebook;
        this.website = obj.website;
        this.twitter = obj.twitter;
        this.instagram = obj.instagram;
        this.country = obj.country;
        this.zipCode = obj.zipCode;
        this.lat = obj.lat;
        this.long = obj.long;
        this.securityQuestion = obj.securityQuestion;
        this.answer = obj.answer;
        this.providerId = obj.providerId;
        this.programId = obj.programId;
        this.status = obj.status;
        this.name = obj.name;
    }
}


/***/ }),

/***/ "./src/app/core/services/api.service.service.ts":
/*!******************************************************!*\
  !*** ./src/app/core/services/api.service.service.ts ***!
  \******************************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! . */ "./src/app/core/services/index.ts");






let ApiService = class ApiService {
    constructor(http, store) {
        this.http = http;
        this.store = store;
        this.root = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrls.reports;
        this.token = '';
        this.header = {};
        console.log('constructor called');
        // this.getToken()
    }
    getHeader() {
        this.token = this.store.getItem('token');
        console.log('token', this.token);
        if (this.token) {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'x-access-token': this.token,
                })
            };
            return header;
        }
        else {
            let header = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                })
            };
            return header;
        }
    }
    signin(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    otpRequest(email) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/users/otp?email=${email}`).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    otpVerify(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/users/otpVerify`, model, { headers: null }).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    forgotPassword(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/users/forgotPassword`, model, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    resetPassword(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/users/resetPassword/${id}`, model, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    uploadUserImage(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/users/uploadProfilePic/${id}`, model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response);
        }, (error) => {
            console.log('error', error);
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getPicUrl(pic) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/uploads/getPicUrl`, pic, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response);
        }, (error) => {
            console.log('error', error);
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    uploadProviderBanner(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/providers/uploadBannerPic/${id}`, model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response);
        }, (error) => {
            console.log('error', error);
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    uploadChildImage(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/child/uploadChildPic`, model, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'enctype': 'multipart/form-data',
                'Accept': 'application/json',
                'x-access-token': this.token
            })
        }).subscribe((response) => {
            subject.next(response);
            console.log('response', response);
        }, (error) => {
            console.log('error', error);
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addUser(data) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/users/register`, data, { headers: null }).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getParentById(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/parents/getById/${id}`, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    updateParent(id, data) {
        console.log('data before updata api', data);
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/parents/update/${id}`, data, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    tellFriend(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/users/tellAFriend`, model).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    activeDeactiveUser(id, isActivated) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        let m;
        this.http.put(`${this.root}/users/activeOrDeactive?id=${id}&isActivated=${isActivated}`, m, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    giveFeedback(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/users/feedback`, model).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    claimRequest(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/claims/request`, model, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    ClaimRquestListByProvider(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/claims/requestListByProvider?id=${id}`, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getChildByParentId(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/child/byParentId/${id}`, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addChild(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/child/add`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addGuardian(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/guardians/add`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    updateChild(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/child/update/${id}`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    updateProviderById(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/providers/update/${id}`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getCategory() {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/categories/list`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    searchCategory(key) {
        console.log('TOKEN', this.token);
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        console.log('key', key);
        this.http.get(`${this.root}/categories/search?name=${key}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    programSearch(key) {
        console.log('TOKEN', this.token);
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/search?name=${key}`, this.getHeader()).subscribe((responseData) => {
            console.log('res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    programFilter(min, max, fromDate, toDate, fromTime, toTime, pageNo, pageSize) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/byFilter?ageFrom=${min}&ageTo=${max}&fromDate=${fromDate}&toDate=${toDate}&fromTime=${fromTime}&toTime=${toTime}&pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeader()).subscribe((responseData) => {
            console.log('res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    searchTag(key) {
        console.log('TOKEN', this.token);
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        console.log('key', key);
        this.http.get(`${this.root}/tags/search?name=${key}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getTag() {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/tags/list`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getTagByCategoryId(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/tags/byCategoryId?catrgoryIds=${id}`, this.getHeader()).subscribe((responseData) => {
            console.log('res from server', responseData);
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getFavouriteByParentId(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/favourites/getByParentId?parentId=${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getGuardianByParentId(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/guardians/byParentId/${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getUserById(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/users/getById/${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getProviderById(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/providers/getById/${id}`, this.getHeader()).subscribe((responseData) => {
            console.log('res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getProfileProgress(id, role) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/users/getProfileProgress?id=${id}&role=${role}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getCategoryBySearch(name) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/categories/search?name=${name}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getProgram(pageNo, pageSize) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/list?pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.items);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getProgramByProvider(userId, pageNo, pageSize) {
        // if (!this.token) {
        //     this.getToken
        // }
        console.log('TOKEN', this.token);
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/byProvider?userId=${userId}&pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            console.log('error', error);
            subject.next(error);
        });
        return subject.asObservable();
    }
    getProgramById(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/getById/${id}`, this.getHeader()).subscribe((responseData) => {
            console.log('get program bye id res from server', responseData);
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addFavProgram(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/favourites/add`, model, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    deleteFavProgram(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.delete(`${this.root}/favourites/delete/${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addProgram(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/programs/add`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    programActiveInActive(id, status) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        let m;
        this.http.put(`${this.root}/programs/activeOrDecactive?id=${id}&status=${status}`, m, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getView(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/getViewsCount?userId=${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    graphData(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/getGraphData?id=${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getProgramCount(id) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/count?userId=${id}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    updateProgram(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/programs/update/${id}`, model, this.getHeader()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    verifyAns(id, model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.put(`${this.root}/users/verifySecuirtyAns/${id}`, model, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    searchProgram(key) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.get(`${this.root}/programs/search?name=${key}`, this.getHeader()).subscribe((responseData) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addAction(data) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.http.post(`${this.root}/programs/addProgramAction`, data, this.getHeader()).subscribe((res) => {
            subject.next(res);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: ___WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"] }
];
ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ "./src/app/core/services/auth.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/auth.service.ts ***!
  \***********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _open_age_ng_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @open-age/ng-api */ "./node_modules/@open-age/ng-api/dist/index.js");
/* harmony import */ var ng2_toasty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-toasty */ "./node_modules/ng2-toasty/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! . */ "./src/app/core/services/index.ts");








let AuthService = class AuthService {
    constructor(http, toasty, store) {
        this.http = http;
        this.toasty = toasty;
        this.store = store;
        this.root = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrls.reports;
        this._currentUserSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.userChanges = this._currentUserSubject.asObservable();
        this.users = new _open_age_ng_api__WEBPACK_IMPORTED_MODULE_5__["GenericApi"](_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrls.reports, 'users', this.http);
    }
    setUser(user) {
        if (user) {
            this.store.setObject('userData', user);
            this.store.setItem('token', user.token);
        }
        else {
            this.store.clear();
        }
        this._user = user;
        this._currentUserSubject.next(user);
    }
    currentUser() {
        if (this._user) {
            return this._user;
        }
        this._user = this.store.getObject('userData');
        return this._user;
    }
    login(model) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData) => {
            if (responseData.status !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData.json();
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                }
                else {
                    throw new Error(responseData.status + '');
                }
            }
            this.setUser(dataModel.data);
            subject.next(dataModel.data);
        }, (responseData) => {
            const dataModel = responseData.json();
            this.toasty.error(dataModel.error);
            console.log('errrrrrrrr', dataModel.error);
            // subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    //   const subject = new Subject<User>();
    //   this.users.post(model, 'login').subscribe(user => {
    //     console.log(user)
    //     
    //     subject.next(user);
    //   }, err => {
    //     this.toasty.error(err.message);
    //     subject.error(err)
    //   });
    //   return subject.asObservable();
    // }
    logout() {
        localStorage.removeItem('token');
        this.setUser(null);
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"] },
    { type: ng2_toasty__WEBPACK_IMPORTED_MODULE_6__["ToastyService"] },
    { type: ___WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthService);



/***/ }),

/***/ "./src/app/core/services/index.ts":
/*!****************************************!*\
  !*** ./src/app/core/services/index.ts ***!
  \****************************************/
/*! exports provided: LocalStorageService, UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _local_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./local-storage.service */ "./src/app/core/services/local-storage.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return _local_storage_service__WEBPACK_IMPORTED_MODULE_1__["LocalStorageService"]; });

/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.service */ "./src/app/core/services/user.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return _user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]; });




// export * from './auth.service';


/***/ }),

/***/ "./src/app/core/services/local-storage.service.ts":
/*!********************************************************!*\
  !*** ./src/app/core/services/local-storage.service.ts ***!
  \********************************************************/
/*! exports provided: LocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return LocalStorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LocalStorageService = class LocalStorageService {
    constructor() {
    }
    getItem(key) {
        return localStorage.getItem(key);
    }
    setItem(key, value) {
        localStorage.setItem(key, value);
    }
    removeItem(key) {
        localStorage.removeItem(key);
    }
    clear() {
        return localStorage.clear();
    }
    getObject(key) {
        const item = localStorage.getItem(key);
        return item ? JSON.parse(item) : null;
    }
    setObject(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
        return value;
    }
};
LocalStorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], LocalStorageService);



/***/ }),

/***/ "./src/app/core/services/user-data.service.ts":
/*!****************************************************!*\
  !*** ./src/app/core/services/user-data.service.ts ***!
  \****************************************************/
/*! exports provided: UserDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDataService", function() { return UserDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UserDataService = class UserDataService {
    constructor() { }
    // private data = {};
    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('userData');
        localStorage.removeItem('program');
        // this.data = null;
    }
};
UserDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UserDataService);



/***/ }),

/***/ "./src/app/core/services/user.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/user.service.ts ***!
  \***********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UserService = class UserService {
    constructor() { }
};
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], UserService);



/***/ }),

/***/ "./src/app/pages/landing/landing.component.css":
/*!*****************************************************!*\
  !*** ./src/app/pages/landing/landing.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n.landing_banner .banner_overlay {\n    background: linear-gradient(180deg, rgba(255, 255, 255, 0.58) 0%, rgba(255, 255, 255, 0) 100%);\n    position: absolute;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n}\n\n.landing_banner .best_activities {\n    background-color: rgba(255, 255, 255, 0.8);\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 2px;\n    padding: 45px;\n    max-width: 442px;\n    position: relative;\n    z-index: 1;\n}\n\n.landing_banner .best_activities h1 {\n    font-family: 'Patrick Hand';\n    font-weight: normal;\n    font-size: 28px;\n    line-height: 30px;\n    color: #777CEA;\n}\n\n.banner_form {\n    margin-top: 60px;\n}\n\n.banner_form .form-group input {\n    border: 1px solid #E5E9EC;\n    border-radius: 6px;\n    min-height: 52px;\n}\n\n.banner_form .form-group .form-control.date_input::-webkit-calendar-picker-indicator {\n    display: none;\n    -webkit-appearance: none;\n}\n\n.banner_form .form-group input::-webkit-input-placeholder {\n    color: #D5DCE1;\n}\n\n.banner_form .form-group input::-moz-placeholder {\n    color: #D5DCE1;\n}\n\n.banner_form .form-group input::-ms-input-placeholder {\n    color: #D5DCE1;\n}\n\n.banner_form .form-group input::placeholder {\n    color: #D5DCE1;\n}\n\n.banner_form .form-group input::-webkit-input-placeholder {\n    color: #D5DCE1;\n}\n\n.banner_form .form-group input:-moz-placeholder {\n    color: #D5DCE1;\n}\n\n.banner_form .banner_button {\n    background: #FFB206;\n    border: 1px solid #FFB206;\n    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.1);\n    border-radius: 6px;\n    display: inline-block;\n    min-width: 52px;\n    line-height: 52px;\n    width: 100%;\n    font-weight: 600;\n    font-size: 15px;\n    color: #fff;\n    margin-top: 13px;\n}\n\n/* start activity_category css */\n\n.activity_category {\n    padding: 140px 0 54px 0;\n}\n\nh2.title {\n    font-family: 'Patrick Hand';\n    font-weight: normal;\n    font-size: 28px;\n    line-height: 27px;\n    color: #777CEA;\n}\n\n.activity_category_list {\n    padding-top: 18px;\n}\n\n.activity_category .category_list_box {\n    max-height: 276px;\n    border-radius: 4px;\n    margin-top: 30px;\n}\n\n.category_list_box {\n    overflow: hidden;\n    position: relative;\n}\n\n.activity_category .category_list_box img {\n    max-width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    border-radius: 4px;\n}\n\n.category_list_box .category_title {\n    position: absolute;\n    bottom: 23px;\n    left: 33px;\n}\n\n.category_list_box .category_title p {\n    font-family: 'Patrick Hand';\n    font-weight: normal;\n    font-size: 22px;\n    color: #FFFFFF;\n    min-width: 177px;\n    min-height: 44px;\n    line-height: 44px;\n    background: #F15C20;\n    border-radius: 22px;\n    text-align: center;\n    transition: 0.8s;\n}\n\n.category_list_box .category_title p:hover {\n    background: #c74009;\n}\n\n.category_list_box .category_title {\n    position: absolute;\n    bottom: 23px;\n    left: 33px;\n    z-index: 1;\n}\n\n/* start featured_activity css */\n\n.featured_activity {\n    border-top: 1px solid #E5E9EC;\n    padding: 37px 0 32px 0;\n}\n\n.featured_activity h2.title {\n    float: left;\n}\n\n.featured_box {\n    margin-top: 46px;\n}\n\n.custom_select.featured {\n    float: right;\n    width: auto;\n    min-height: 30px;\n    line-height: 30px;\n    min-width: 150px;\n}\n\n.custom_select.featured select {\n    background-size: 10px;\n    background-position: 93% 13px;\n}\n\n.featured_img {\n    border-radius: 4px 4px 0 0;\n    overflow: hidden;\n    max-height: 156px;\n}\n\n.featured_img img {\n    max-width: 100%;\n    width: 100%;\n    height: 100%;\n}\n\n.featured_text {\n    padding: 12px 0;\n    position: relative;\n}\n\n.featured_text h3 {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    color: #777CEA;\n}\n\n.featured_text p {\n    font-weight: 600;\n    font-size: 14px;\n    line-height: 18px;\n    color: #748494;\n    margin: 9px 0 0 0;\n}\n\n.featured_text .rating_box span {\n    font-weight: 600;\n    font-size: 12px;\n    line-height: 16px;\n    color: #748494;\n}\n\n.featured_text .rating_box img{\n    margin:0 6px 0 0;\n}\n\n.rating_box {\n    position: absolute;\n    right: 0;\n    top: 5px;\n}\n\n/* start trending_discussions css */\n\n.trending_discussions {\n    padding: 25px 0 54px 0;\n}\n\n.trending_discussions h2.title {\n    float: left;\n}\n\n.trending_discussions a.view {\n    float: right;\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #748494;\n}\n\n.trending_outer {\n    margin-top: 35px;\n    display: inline-block;\n    width: 100%;\n}\n\n.trending_outer .trending_box {\n    margin: 0 0 36px 0;\n}\n\n.trending_outer .trending_box:last-child {\n    margin: 0;\n}\n\n.trending_outer .trending_box h3 {\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    color: #F15C20;\n}\n\n.trending_outer .trending_box p {\n    font-size: 13px;\n    line-height: 16px;\n    color: #748494;\n    margin: 3px 0 0 0;\n}\n\n/* start upto_date section css */\n\n.upto_date {\n    padding: 0 0 46px 0;\n}\n\n.upto_date .container{\npadding-top: 53px;\nborder-top: 1px solid #E5E9EC;\n}\n\n.upto_inner {\n    max-width: 470px;\n}\n\n.upto_date p {\n    font-size: 13px;\n    line-height: 19px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 31px 0 0 0;\n}\n\n.upto_date .input-group {\n    background: #FFFFFF;\n    border: 1px solid #E5E9EC;\n    border-radius: 2px;\n    padding: 5px;\n    margin-top: 50px;\n}\n\n.upto_date .input-group:focus-within,\n.upto_date .input-group:focus {\n    border: 1px solid #80bdff;\n}\n\n.upto_date .input-group .form-control {\n    border: none;\n    box-shadow: none;\n    font-size: 13px;\n    min-height: 46px;\n}\n\n.upto_date .input-group .form-control::-webkit-input-placeholder {\n    color: rgba(0, 0, 0, 0.3);\n}\n\n.upto_date .input-group .form-control::-moz-placeholder {\n    color: rgba(0, 0, 0, 0.3);\n}\n\n.upto_date .input-group .form-control::-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.3);\n}\n\n.upto_date .input-group .form-control::placeholder {\n    color: rgba(0, 0, 0, 0.3);\n}\n\n.upto_date .input-group .form-control::-moz-placeholder {\n    color: rgba(0, 0, 0, 0.3);\n}\n\n.upto_date .input-group .input-group-append {\n    margin-left: 0;\n}\n\n.upto_date .input-group .Submit_btn {\n    background: #F15A29;\n    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);\n    border-radius: 2px;\n    border: none;\n    box-shadow: none;\n    min-width: 124px;\n    min-height: 46px;\n    line-height: 46px;\n    font-weight: 600;\n    font-size: 13px;\n    color: #FFFFFF;\n}\n\nfooter {\n    background-color: #777cea;\n    padding: 19px 0;\n}\n\n.footer_logo {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_logo a img {\n    max-width: 78px;\n}\n\n.footer_menu {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_menu ul {\n    margin-top: 7px;\n}\n\n.footer_menu ul li {\n    display: inline-block;\n    margin: 0 27px 0 0;\n}\n\n.footer_menu ul a {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #f1f1f1;\n}\n\n.footer_menu ul a:hover {\n    color: #fff;\n}\n\n.copyright_text {\n    margin-top: 13px;\n}\n\n.copyright_text p {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    margin: 0;\n    color: #ffffff;\n}\n\n.footer_left {\n    float: left;\n}\n\n.social_icon_footer {\n    float: right;\n    margin-top: 29px;\n}\n\n.social_icon_footer ul li {\n    display: inline-block;\n}\n\n.social_icon_footer li a {\n    display: inline-block;\n    margin-right: 14px;\n}\n\n.social_icon_footer li:last-child a {\n    margin: 0;\n}\n\n.social_icon_footer li a .icon {\n    font-size: 18px;\n    color: #fff;\n    transition: 0.8s;\n}\n\n.social_icon_footer li a:hover .icon.icon-facebook {\n    color: #395794;\n}\n\n.social_icon_footer li a:hover .icon.icon-instagram {\n    color: #d60044;\n}\n\n.social_icon_footer li a:hover .icon.icon-pinterest {\n    color: #e11e27;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGFuZGluZy9sYW5kaW5nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUNJLDhGQUE4RjtJQUM5RixrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxRQUFRO0lBQ1IsU0FBUztBQUNiOztBQUVBO0lBQ0ksMENBQTBDO0lBQzFDLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLFVBQVU7QUFDZDs7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixnQkFBZ0I7QUFDcEI7O0FBR0E7SUFDSSxhQUFhO0lBQ2Isd0JBQXdCO0FBQzVCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFGQTtJQUNJLGNBQWM7QUFDbEI7O0FBRkE7SUFDSSxjQUFjO0FBQ2xCOztBQUZBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QiwyQ0FBMkM7SUFDM0Msa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLFdBQVc7SUFDWCxnQkFBZ0I7QUFDcEI7O0FBRUEsZ0NBQWdDOztBQUNoQztJQUNJLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLFlBQVk7SUFDWixvQkFBaUI7T0FBakIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osVUFBVTtBQUNkOztBQUVBO0lBQ0ksMkJBQTJCO0lBQzNCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osVUFBVTtJQUNWLFVBQVU7QUFDZDs7QUFFQSxnQ0FBZ0M7O0FBQ2hDO0lBQ0ksNkJBQTZCO0lBQzdCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLFlBQVk7SUFDWixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsNkJBQTZCO0FBQ2pDOztBQUVBO0lBQ0ksMEJBQTBCO0lBQzFCLGdCQUFnQjtJQUNoQixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2Ysa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixRQUFRO0FBQ1o7O0FBRUEsbUNBQW1DOztBQUNuQztJQUNJLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixxQkFBcUI7SUFDckIsV0FBVztBQUNmOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksU0FBUztBQUNiOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGlCQUFpQjtBQUNyQjs7QUFFQSxnQ0FBZ0M7O0FBQ2hDO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLDZCQUE2QjtBQUM3Qjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osZ0JBQWdCO0FBQ3BCOztBQUVBOztJQUVJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFGQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFGQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFGQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsMkNBQTJDO0lBQzNDLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsY0FBYztBQUNsQjs7QUFFQTtJQUNJLHlCQUF5QjtJQUN6QixlQUFlO0FBQ25COztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksV0FBVztBQUNmOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxTQUFTO0FBQ2I7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsV0FBVztJQUNYLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztBQUNsQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xhbmRpbmcvbGFuZGluZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbi5sYW5kaW5nX2Jhbm5lciAuYmFubmVyX292ZXJsYXkge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41OCkgMCUsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMCkgMTAwJSk7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIGJvdHRvbTogMDtcbn1cblxuLmxhbmRpbmdfYmFubmVyIC5iZXN0X2FjdGl2aXRpZXMge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIHBhZGRpbmc6IDQ1cHg7XG4gICAgbWF4LXdpZHRoOiA0NDJweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbn1cblxuLmxhbmRpbmdfYmFubmVyIC5iZXN0X2FjdGl2aXRpZXMgaDEge1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmJhbm5lcl9mb3JtIHtcbiAgICBtYXJnaW4tdG9wOiA2MHB4O1xufVxuXG4uYmFubmVyX2Zvcm0gLmZvcm0tZ3JvdXAgaW5wdXQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xuICAgIG1pbi1oZWlnaHQ6IDUycHg7XG59XG5cblxuLmJhbm5lcl9mb3JtIC5mb3JtLWdyb3VwIC5mb3JtLWNvbnRyb2wuZGF0ZV9pbnB1dDo6LXdlYmtpdC1jYWxlbmRhci1waWNrZXItaW5kaWNhdG9yIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcbn1cblxuLmJhbm5lcl9mb3JtIC5mb3JtLWdyb3VwIGlucHV0OjpwbGFjZWhvbGRlciB7XG4gICAgY29sb3I6ICNENURDRTE7XG59XG5cbi5iYW5uZXJfZm9ybSAuZm9ybS1ncm91cCBpbnB1dDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgY29sb3I6ICNENURDRTE7XG59XG5cbi5iYW5uZXJfZm9ybSAuZm9ybS1ncm91cCBpbnB1dDotbW96LXBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogI0Q1RENFMTtcbn1cblxuLmJhbm5lcl9mb3JtIC5iYW5uZXJfYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZCMjA2O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGRkIyMDY7XG4gICAgYm94LXNoYWRvdzogMHB4IDZweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1pbi13aWR0aDogNTJweDtcbiAgICBsaW5lLWhlaWdodDogNTJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBtYXJnaW4tdG9wOiAxM3B4O1xufVxuXG4vKiBzdGFydCBhY3Rpdml0eV9jYXRlZ29yeSBjc3MgKi9cbi5hY3Rpdml0eV9jYXRlZ29yeSB7XG4gICAgcGFkZGluZzogMTQwcHggMCA1NHB4IDA7XG59XG5cbmgyLnRpdGxlIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDI4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDI3cHg7XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5hY3Rpdml0eV9jYXRlZ29yeV9saXN0IHtcbiAgICBwYWRkaW5nLXRvcDogMThweDtcbn1cblxuLmFjdGl2aXR5X2NhdGVnb3J5IC5jYXRlZ29yeV9saXN0X2JveCB7XG4gICAgbWF4LWhlaWdodDogMjc2cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIG1hcmdpbi10b3A6IDMwcHg7XG59XG5cbi5jYXRlZ29yeV9saXN0X2JveCB7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5hY3Rpdml0eV9jYXRlZ29yeSAuY2F0ZWdvcnlfbGlzdF9ib3ggaW1nIHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmNhdGVnb3J5X2xpc3RfYm94IC5jYXRlZ29yeV90aXRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMjNweDtcbiAgICBsZWZ0OiAzM3B4O1xufVxuXG4uY2F0ZWdvcnlfbGlzdF9ib3ggLmNhdGVnb3J5X3RpdGxlIHAge1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICBtaW4td2lkdGg6IDE3N3B4O1xuICAgIG1pbi1oZWlnaHQ6IDQ0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ0cHg7XG4gICAgYmFja2dyb3VuZDogI0YxNUMyMDtcbiAgICBib3JkZXItcmFkaXVzOiAyMnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4uY2F0ZWdvcnlfbGlzdF9ib3ggLmNhdGVnb3J5X3RpdGxlIHA6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICNjNzQwMDk7XG59XG5cbi5jYXRlZ29yeV9saXN0X2JveCAuY2F0ZWdvcnlfdGl0bGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDIzcHg7XG4gICAgbGVmdDogMzNweDtcbiAgICB6LWluZGV4OiAxO1xufVxuXG4vKiBzdGFydCBmZWF0dXJlZF9hY3Rpdml0eSBjc3MgKi9cbi5mZWF0dXJlZF9hY3Rpdml0eSB7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgcGFkZGluZzogMzdweCAwIDMycHggMDtcbn1cblxuLmZlYXR1cmVkX2FjdGl2aXR5IGgyLnRpdGxlIHtcbiAgICBmbG9hdDogbGVmdDtcbn1cblxuLmZlYXR1cmVkX2JveCB7XG4gICAgbWFyZ2luLXRvcDogNDZweDtcbn1cblxuLmN1c3RvbV9zZWxlY3QuZmVhdHVyZWQge1xuICAgIGZsb2F0OiByaWdodDtcbiAgICB3aWR0aDogYXV0bztcbiAgICBtaW4taGVpZ2h0OiAzMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTUwcHg7XG59XG5cbi5jdXN0b21fc2VsZWN0LmZlYXR1cmVkIHNlbGVjdCB7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMHB4O1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IDkzJSAxM3B4O1xufVxuXG4uZmVhdHVyZWRfaW1nIHtcbiAgICBib3JkZXItcmFkaXVzOiA0cHggNHB4IDAgMDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIG1heC1oZWlnaHQ6IDE1NnB4O1xufVxuXG4uZmVhdHVyZWRfaW1nIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbn1cblxuLmZlYXR1cmVkX3RleHQge1xuICAgIHBhZGRpbmc6IDEycHggMDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5mZWF0dXJlZF90ZXh0IGgzIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMTRweDtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLmZlYXR1cmVkX3RleHQgcCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiA5cHggMCAwIDA7XG59XG5cbi5mZWF0dXJlZF90ZXh0IC5yYXRpbmdfYm94IHNwYW4ge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuLmZlYXR1cmVkX3RleHQgLnJhdGluZ19ib3ggaW1ne1xuICAgIG1hcmdpbjowIDZweCAwIDA7XG59XG5cbi5yYXRpbmdfYm94IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiA1cHg7XG59XG5cbi8qIHN0YXJ0IHRyZW5kaW5nX2Rpc2N1c3Npb25zIGNzcyAqL1xuLnRyZW5kaW5nX2Rpc2N1c3Npb25zIHtcbiAgICBwYWRkaW5nOiAyNXB4IDAgNTRweCAwO1xufVxuXG4udHJlbmRpbmdfZGlzY3Vzc2lvbnMgaDIudGl0bGUge1xuICAgIGZsb2F0OiBsZWZ0O1xufVxuXG4udHJlbmRpbmdfZGlzY3Vzc2lvbnMgYS52aWV3IHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLnRyZW5kaW5nX291dGVyIHtcbiAgICBtYXJnaW4tdG9wOiAzNXB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnRyZW5kaW5nX291dGVyIC50cmVuZGluZ19ib3gge1xuICAgIG1hcmdpbjogMCAwIDM2cHggMDtcbn1cblxuLnRyZW5kaW5nX291dGVyIC50cmVuZGluZ19ib3g6bGFzdC1jaGlsZCB7XG4gICAgbWFyZ2luOiAwO1xufVxuXG4udHJlbmRpbmdfb3V0ZXIgLnRyZW5kaW5nX2JveCBoMyB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgY29sb3I6ICNGMTVDMjA7XG59XG5cbi50cmVuZGluZ19vdXRlciAudHJlbmRpbmdfYm94IHAge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBtYXJnaW46IDNweCAwIDAgMDtcbn1cblxuLyogc3RhcnQgdXB0b19kYXRlIHNlY3Rpb24gY3NzICovXG4udXB0b19kYXRlIHtcbiAgICBwYWRkaW5nOiAwIDAgNDZweCAwO1xufVxuLnVwdG9fZGF0ZSAuY29udGFpbmVye1xucGFkZGluZy10b3A6IDUzcHg7XG5ib3JkZXItdG9wOiAxcHggc29saWQgI0U1RTlFQztcbn1cblxuLnVwdG9faW5uZXIge1xuICAgIG1heC13aWR0aDogNDcwcHg7XG59XG5cbi51cHRvX2RhdGUgcCB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbjogMzFweCAwIDAgMDtcbn1cblxuLnVwdG9fZGF0ZSAuaW5wdXQtZ3JvdXAge1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0U1RTlFQztcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG59XG5cbi51cHRvX2RhdGUgLmlucHV0LWdyb3VwOmZvY3VzLXdpdGhpbixcbi51cHRvX2RhdGUgLmlucHV0LWdyb3VwOmZvY3VzIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjODBiZGZmO1xufVxuXG4udXB0b19kYXRlIC5pbnB1dC1ncm91cCAuZm9ybS1jb250cm9sIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbWluLWhlaWdodDogNDZweDtcbn1cblxuLnVwdG9fZGF0ZSAuaW5wdXQtZ3JvdXAgLmZvcm0tY29udHJvbDo6cGxhY2Vob2xkZXIge1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG59XG5cbi51cHRvX2RhdGUgLmlucHV0LWdyb3VwIC5mb3JtLWNvbnRyb2w6Oi1tb3otcGxhY2Vob2xkZXIge1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG59XG5cbi51cHRvX2RhdGUgLmlucHV0LWdyb3VwIC5pbnB1dC1ncm91cC1hcHBlbmQge1xuICAgIG1hcmdpbi1sZWZ0OiAwO1xufVxuXG4udXB0b19kYXRlIC5pbnB1dC1ncm91cCAuU3VibWl0X2J0biB7XG4gICAgYmFja2dyb3VuZDogI0YxNUEyOTtcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDFweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtaW4td2lkdGg6IDEyNHB4O1xuICAgIG1pbi1oZWlnaHQ6IDQ2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDQ2cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgY29sb3I6ICNGRkZGRkY7XG59XG5cbmZvb3RlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc3N2NlYTtcbiAgICBwYWRkaW5nOiAxOXB4IDA7XG59XG5cbi5mb290ZXJfbG9nbyB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9vdGVyX2xvZ28gYSBpbWcge1xuICAgIG1heC13aWR0aDogNzhweDtcbn1cblxuLmZvb3Rlcl9tZW51IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5mb290ZXJfbWVudSB1bCB7XG4gICAgbWFyZ2luLXRvcDogN3B4O1xufVxuXG4uZm9vdGVyX21lbnUgdWwgbGkge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW46IDAgMjdweCAwIDA7XG59XG5cbi5mb290ZXJfbWVudSB1bCBhIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogI2YxZjFmMTtcbn1cblxuLmZvb3Rlcl9tZW51IHVsIGE6aG92ZXIge1xuICAgIGNvbG9yOiAjZmZmO1xufVxuXG4uY29weXJpZ2h0X3RleHQge1xuICAgIG1hcmdpbi10b3A6IDEzcHg7XG59XG5cbi5jb3B5cmlnaHRfdGV4dCBwIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBtYXJnaW46IDA7XG4gICAgY29sb3I6ICNmZmZmZmY7XG59XG5cbi5mb290ZXJfbGVmdCB7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5zb2NpYWxfaWNvbl9mb290ZXIge1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW4tdG9wOiAyOXB4O1xufVxuXG4uc29jaWFsX2ljb25fZm9vdGVyIHVsIGxpIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5zb2NpYWxfaWNvbl9mb290ZXIgbGkgYSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogMTRweDtcbn1cblxuLnNvY2lhbF9pY29uX2Zvb3RlciBsaTpsYXN0LWNoaWxkIGEge1xuICAgIG1hcmdpbjogMDtcbn1cblxuLnNvY2lhbF9pY29uX2Zvb3RlciBsaSBhIC5pY29uIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdHJhbnNpdGlvbjogMC44cztcbn1cblxuLnNvY2lhbF9pY29uX2Zvb3RlciBsaSBhOmhvdmVyIC5pY29uLmljb24tZmFjZWJvb2sge1xuICAgIGNvbG9yOiAjMzk1Nzk0O1xufVxuXG4uc29jaWFsX2ljb25fZm9vdGVyIGxpIGE6aG92ZXIgLmljb24uaWNvbi1pbnN0YWdyYW0ge1xuICAgIGNvbG9yOiAjZDYwMDQ0O1xufVxuXG4uc29jaWFsX2ljb25fZm9vdGVyIGxpIGE6aG92ZXIgLmljb24uaWNvbi1waW50ZXJlc3Qge1xuICAgIGNvbG9yOiAjZTExZTI3O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/landing/landing.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/landing/landing.component.ts ***!
  \****************************************************/
/*! exports provided: LandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingComponent", function() { return LandingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let LandingComponent = class LandingComponent {
    constructor(router) {
        this.router = router;
        this.isMap = true;
        this.isLogin = false;
        this.userData = {};
        this.userData = JSON.parse(localStorage.getItem('userData'));
        if (this.userData) {
            this.isLogin = true;
        }
    }
    search() {
        this.router.navigate(['/search']);
    }
    ngOnInit() {
    }
};
LandingComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
LandingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-landing',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./landing.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/landing/landing.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./landing.component.css */ "./src/app/pages/landing/landing.component.css")).default]
    })
], LandingComponent);



/***/ }),

/***/ "./src/app/pages/parent/parent.component.css":
/*!***************************************************!*\
  !*** ./src/app/pages/parent/parent.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BhcmVudC9wYXJlbnQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/parent/parent.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/parent/parent.component.ts ***!
  \**************************************************/
/*! exports provided: ParentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParentComponent", function() { return ParentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ParentComponent = class ParentComponent {
    constructor() { }
    ngOnInit() {
    }
};
ParentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-parent',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./parent.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parent/parent.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./parent.component.css */ "./src/app/pages/parent/parent.component.css")).default]
    })
], ParentComponent);



/***/ }),

/***/ "./src/app/pages/program-detail/program-detail.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/pages/program-detail/program-detail.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("header {\n    background-color: #fff;\n    padding: 19px 35px;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\nheader .logo {\n    max-width: 136px;\n}\n\nheader .logo img {\n    max-width: 100%;\n}\n\n.content_outer {\n    padding-top: 99px;\n    padding-bottom: 36px;\n}\n\nheader .navbar {\n    padding: 0;\n}\n\nheader .navbar-light .navbar-nav .nav-link {\n    font-size: 13px;\n    font-weight: normal;\n    line-height: 16px;\n    color: #748494;\n    padding: 9px 0;\n    margin-right: 25px;\n}\n\nheader .navbar-nav .btn_style {\n    font-family: 'Open Sans';\n    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    font-weight: 600;\n    font-size: 13px;\n    color: #fff;\n}\n\n.landing_banner .banner_overlay{\n    background: linear-gradient(180deg, rgba(255, 255, 255, 0.58) 0%, rgba(255, 255, 255, 0) 100%);\n    position: absolute;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n}\n\n.program_outer{\n    padding:50px 95px 30px 95px;\n}\n\n.program_images{\n    width: 100% ;\n    display: flex;\n}\n\n.program_images .program_box{\nwidth: 100%;\nmax-width: 350px;\nmargin-right: 5px;\n}\n\n.program_images .program_box:last-child{\n    margin-right: 0;\n}\n\n.program_images .program_box img{\n    max-width: 100%;\n}\n\n.program_text{\n    padding: 35px 0 31px 0;\n}\n\n.program_text .CategoryText{\n    position: relative;\n}\n\n.program_text .CategoryText h5{\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;    \n    color: #777CEA;\n    text-transform: uppercase;\n}\n\n.program_text .CategoryText .learn_code{\nwidth: 60px;\nheight: 60px;\nborder-radius: 6px;\noverflow: hidden;\nposition: absolute;\ntop: 0;\nright: 0;\n}\n\n.program_text .CategoryText .learn_code img{\nmax-width: 100%;\nheight: 100%;\n}\n\n.program_text .CategoryText h3{\nfont-size: 36px;\nline-height: 37px;\ncolor: #748494;\nfont-family: 'Patrick Hand';\nmargin: 2px 0 0 0;\n}\n\n.session_details{\n    margin-top: 3px;\n}\n\n.session_details p{\nfont-size: 13px;\nline-height: 16px;\nletter-spacing: 0.001em;\ncolor: #748494;\ndisplay: inline-block;\nmargin:0 28px 0 0;\n}\n\n.session_details p img {\n    margin-right: 6px;\n    vertical-align: middle;\n}\n\n.insured_outer{\n    border-top: 1px solid #E5E9EC;\n    border-bottom: 1px solid #E5E9EC;\n    padding: 13px 0;\n    margin-top: 29px;\n}\n\n.insured_outer p{\ndisplay: inline-block;\nfont-size: 13px;\nline-height: 16px;\nletter-spacing: 0.001em;\ncolor: #748494;\nmargin:0 35px 0 0;\n}\n\n.insured_outer p img {\n    max-width: 27px;\n    margin-right: 12px;\n}\n\n.batch_outer .batch_left{\nbackground: #FFFFFF;\nbox-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\nborder-radius: 2px;\npadding: 30px 35px;\n}\n\n.batch_outer .batch_left h2{\n    font-family: 'Patrick Hand';\nfont-size: 26px;\nline-height: 18px;\nletter-spacing: 0.005em;\ncolor: #777CEA;\n}\n\n.batch_box{\n    border: 1px solid #E5E9EC;\n    border-radius: 4px;\n    padding: 16px 28px;\n    margin-top: 11px;\n}\n\n.batch_box .batch_title p{\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n}\n\n.listing_outer{\n    margin-top: 29px;\n}\n\n.batch_outer .batch_right{\n    background: #FFFFFF;\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 2px;\n    padding: 21px;\n}\n\n.batch_outer .batch_right img{\n    max-width: 100%;\n}\n\n.batch_outer .batch_right.border_none{\n    border:none;\n    margin-top: 21px;\n    text-align: center;\n    padding: 32px;\n}\n\n.batch_outer .batch_right h2{\n    font-family: 'Patrick Hand';\n    font-size: 26px;\n    line-height: 18px;\n    letter-spacing: 0.005em;\n    color: #777CEA;\n    margin-top: 16px;\n}\n\n.batch_outer .batch_right P{\nfont-size: 12px;\nline-height: 18px;\ntext-align: center;\ncolor: #748494;\nmargin: 26px 0 0 0;\n}\n\n.batch_outer .batch_right a.business_btn{\nmin-height: 52px;\nline-height: 52px;\nwidth: 100%;\nmax-width: 100%;\ntext-align: center;\nbackground-color: #f15c20;\ncolor: #fff;\nfont-size: 18px;\nletter-spacing: 0.005em;\npadding: 0 10px;\nborder-radius: 4px;\ndisplay: inline-block;\nmargin-top: 32px;\n}\n\n.batch_list_outer{\n    display: flex;\n}\n\n.batch_list_outer .batch_listing{\n    display: inline-block;\n    margin: 0 15px 0 0;\n}\n\n.batch_list_outer .batch_listing label{\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    color: #777CEA;\n    text-transform: uppercase;\n    width: 100%;\n}\n\n.batch_list_outer .batch_listing .batch_profile{\nwidth: 22px;\nheight: 22px;\nborder-radius: 4px;\ndisplay: inline-block;\nmargin-right: 14px;\noverflow: hidden;\nvertical-align: middle;\n}\n\n.batch_list_outer .batch_listing .batch_profile img{\n    max-width: 100%;\n    height: 100%;\n}\n\n.batch_list_outer .batch_listing p{\nfont-weight: normal;\nfont-size: 13px;\nline-height: 16px;\nletter-spacing: 0.001em;\ndisplay: inline-block;\ncolor: #748494;\n}\n\n.description_text{\n    background: #FFFFFF;\nbox-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\nborder-radius: 2px;\npadding: 30px 35px;\nmargin-top: 21px;\n}\n\n.description_text h2{\n    font-family: 'Patrick Hand';\n    font-size: 26px;\n    line-height: 18px;\n    letter-spacing: 0.005em;\n    color: #777CEA;\n}\n\n.description_text p{\n    font-size: 13px;\n    line-height: 19px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin-top: 22px;\n}\n\n.description_text label{\nfont-weight: 600;\nfont-size: 10px;\nline-height: 14px;\ntext-transform: uppercase;\ncolor: #777CEA;\nwidth: 100%;\ndisplay: inline-block\n}\n\n.description_text h2 .view_all{\nfont-weight: 600;\nfont-size: 10px;\nline-height: 14px;\nletter-spacing: 0.01em;\ntext-transform: uppercase;\ncolor: #748494;\nfloat: right;\n}\n\n.reviews_outer {\n    margin-top: 36px;\n}\n\n.reviews_outer .media {\n    margin: 0 0 15px 0;\n}\n\n.reviews_outer .media .media-left{\n    width: 50px;\n    height: 50px;\n    border-radius: 4px;\n    overflow: hidden;\n}\n\n.reviews_outer .media .media-left img{\n    max-width: 100%;\n    height: 100%;\n}\n\n.reviews_outer .media .media-body{\n    padding: 0 0 0 12px;\n}\n\n.reviews_outer .media .media-body label{\nfont-weight: 600;\nfont-size: 13px;\nline-height: 18px;\nletter-spacing: 0.001em;\ncolor: #777CEA;\n}\n\n.reviews_outer .media .media-body p{\nfont-size: 13px;\nline-height: 16px;\nletter-spacing: 0.001em;\ncolor: #748494;\nmargin: 0;\n}\n\n.featured_box {\n    margin-top: 46px;\n}\n\n.featured_img {\n    border-radius: 4px 4px 0 0;\n    overflow: hidden;\n    max-height: 180px;\n    position: relative;\n}\n\n.featured_img img {\n    max-width: 100%;\n    width: 100%;\n    height: 100%;\n}\n\n.menu_control{\nposition: absolute;\ntop: 0;\nright: 0;\ntext-align: center;\n}\n\n.featured_img .menu_control .bookmark_icon img{\n    max-width: 19px;\n}\n\n.featured_text {\n    padding: 12px 0;\n    position: relative;\n}\n\n.featured_text h3 {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    color: #777CEA;\n    margin-top: 13px;\n    text-transform: uppercase;\n}\n\n.featured_text p {\n    font-weight: normal;\n    font-size: 22px;\nline-height: 30px;\n    color: #748494;\n    margin: 9px 0 0 0;\n    font-family: 'Patrick Hand';\n}\n\n.featured_text .feature_box{\n    display: inline-block;\n    width: 100%;\n}\n\n.featured_text .feature_box p{\n    font-size: 12px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    display: inline-block;\n    margin: 0 28px 0 0;\n    font-family: 'Open Sans';\n}\n\n.feature_box p img {\n    margin-right: 6px;\n    vertical-align: middle;\n}\n\n.featured_text .rating_box span {\n    font-weight: 600;\n    font-size: 12px;\n    line-height: 16px;\n    color: #748494;\n}\n\n.featured_text .featured_btn{\n    min-width: 75px;\n    line-height: 20px;\n    min-height: 20px;\nborder: 1px solid #777CEA;\nborder-radius: 3px;\nfont-size: 10px;\ndisplay: inline-block;\ntext-align: center;\nletter-spacing: 0.01em;\ntext-transform: uppercase;\ncolor: #777CEA;\n}\n\n.featured_text .featured_btn:hover{\n    background-color: #777CEA;\n    color: #fff;\n}\n\n.rating_box {\n    position: absolute;\n    right: 0;\n    top: 5px;\n}\n\n/* start program detail2 page css */\n\n.batch_outer.detailed2{\npadding: 30px 0 0 0;\n}\n\n.batch_outer.detailed2 .program_text{\npadding: 0 0 31px 0;\n}\n\n.learn_code.left img{\n    max-width: 100px;\n}\n\n.description_text .des_media{\n    display: inline-block;\n    margin: 0 57px 0 0;\n}\n\n.description_text .des_media:last-child{\nmargin: 0;\n}\n\n.description_text .des_media .media{\n    margin-top: 26px;\n}\n\n.description_text .des_media .media-body{\n    padding: 0 0 0 20px;\n}\n\n.description_text .des_media .media-body p{\nmargin: 0;\n}\n\n.subscription_text{\n    margin-top: 56px;\n}\n\n.subscription_text .subscription_box{\n    display: inline-block;\n    vertical-align: top;\n    margin: 0 161px 0 0;\n}\n\n.subscription_text .subscription_box:last-child{\n    margin: 0;\n}\n\n.social_media{\n    margin: 28px 0 0 0;\n}\n\n.social_media a .icon{\n    color: #748494;\n    margin: 0 6px 0 0;\n    font-size: 19px;\n}\n\n.featured_text .rating_box {\n    position: absolute;\n    right: 0;\n    bottom: 13px;\n    top: auto;\n}\n\n.featured_text .feature_box p{\n    margin: 0 16px 0 0;\n}\n\n.featured_text p{\n    color: #777CEA;\n}\n\n.featured_img .coding_text{\n    position: absolute;\n    bottom: 9px;\n    left: 9px;\n}\n\n.featured_img .coding_text h3{\n    min-width: 75px;\n    min-height: 21px;\n    line-height: 21px;\n    background: #F15C20;\n    border-radius: 10.5px;\n    display: inline-block;\n    color: #fff;\n    font-size: 10px;\n    letter-spacing: 0.01em;\n    text-align: center;\n    text-transform: uppercase;\n}\n\n.featured_text .featured_btn{\n    border: 1px solid #F15C20;\n    color: #F15C20;\n}\n\n.featured_text .feature_box {\n    margin: 5px 0 12px 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZ3JhbS1kZXRhaWwvcHJvZ3JhbS1kZXRhaWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsc0NBQXNDO0FBQzFDOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUdBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsY0FBYztJQUNkLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLHdCQUF3QjtJQUN4Qiw0Q0FBNEM7SUFDNUMsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsV0FBVztBQUNmOztBQUVBO0lBQ0ksOEZBQThGO0lBQzlGLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sT0FBTztJQUNQLFFBQVE7SUFDUixTQUFTO0FBQ2I7O0FBRUE7SUFDSSwyQkFBMkI7QUFDL0I7O0FBQ0E7SUFDSSxZQUFZO0lBQ1osYUFBYTtBQUNqQjs7QUFDQTtBQUNBLFdBQVc7QUFDWCxnQkFBZ0I7QUFDaEIsaUJBQWlCO0FBQ2pCOztBQUNBO0lBQ0ksZUFBZTtBQUNuQjs7QUFDQTtJQUNJLGVBQWU7QUFDbkI7O0FBQ0E7SUFDSSxzQkFBc0I7QUFDMUI7O0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7O0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QseUJBQXlCO0FBQzdCOztBQUNBO0FBQ0EsV0FBVztBQUNYLFlBQVk7QUFDWixrQkFBa0I7QUFDbEIsZ0JBQWdCO0FBQ2hCLGtCQUFrQjtBQUNsQixNQUFNO0FBQ04sUUFBUTtBQUNSOztBQUNBO0FBQ0EsZUFBZTtBQUNmLFlBQVk7QUFDWjs7QUFDQTtBQUNBLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsY0FBYztBQUNkLDJCQUEyQjtBQUMzQixpQkFBaUI7QUFDakI7O0FBQ0E7SUFDSSxlQUFlO0FBQ25COztBQUNBO0FBQ0EsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQix1QkFBdUI7QUFDdkIsY0FBYztBQUNkLHFCQUFxQjtBQUNyQixpQkFBaUI7QUFDakI7O0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsc0JBQXNCO0FBQzFCOztBQUNBO0lBQ0ksNkJBQTZCO0lBQzdCLGdDQUFnQztJQUNoQyxlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCOztBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsdUJBQXVCO0FBQ3ZCLGNBQWM7QUFDZCxpQkFBaUI7QUFDakI7O0FBQ0E7SUFDSSxlQUFlO0lBQ2Ysa0JBQWtCO0FBQ3RCOztBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLDBDQUEwQztBQUMxQyxrQkFBa0I7QUFDbEIsa0JBQWtCO0FBQ2xCOztBQUNBO0lBQ0ksMkJBQTJCO0FBQy9CLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsdUJBQXVCO0FBQ3ZCLGNBQWM7QUFDZDs7QUFDQTtJQUNJLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGFBQWE7QUFDakI7O0FBQ0E7SUFDSSxlQUFlO0FBQ25COztBQUNBO0lBQ0ksV0FBVztJQUNYLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsYUFBYTtBQUNqQjs7QUFDQTtJQUNJLDJCQUEyQjtJQUMzQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCOztBQUNBO0FBQ0EsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixrQkFBa0I7QUFDbEIsY0FBYztBQUNkLGtCQUFrQjtBQUNsQjs7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQixpQkFBaUI7QUFDakIsV0FBVztBQUNYLGVBQWU7QUFDZixrQkFBa0I7QUFDbEIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxlQUFlO0FBQ2YsdUJBQXVCO0FBQ3ZCLGVBQWU7QUFDZixrQkFBa0I7QUFDbEIscUJBQXFCO0FBQ3JCLGdCQUFnQjtBQUNoQjs7QUFDQTtJQUNJLGFBQWE7QUFDakI7O0FBQ0E7SUFDSSxxQkFBcUI7SUFDckIsa0JBQWtCO0FBQ3RCOztBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsc0JBQXNCO0lBQ3RCLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsV0FBVztBQUNmOztBQUNBO0FBQ0EsV0FBVztBQUNYLFlBQVk7QUFDWixrQkFBa0I7QUFDbEIscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQixnQkFBZ0I7QUFDaEIsc0JBQXNCO0FBQ3RCOztBQUNBO0lBQ0ksZUFBZTtJQUNmLFlBQVk7QUFDaEI7O0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQix1QkFBdUI7QUFDdkIscUJBQXFCO0FBQ3JCLGNBQWM7QUFDZDs7QUFDQTtJQUNJLG1CQUFtQjtBQUN2QiwwQ0FBMEM7QUFDMUMsa0JBQWtCO0FBQ2xCLGtCQUFrQjtBQUNsQixnQkFBZ0I7QUFDaEI7O0FBQ0E7SUFDSSwyQkFBMkI7SUFDM0IsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztBQUNsQjs7QUFDQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxnQkFBZ0I7QUFDcEI7O0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEIsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQix5QkFBeUI7QUFDekIsY0FBYztBQUNkLFdBQVc7QUFDWDtBQUNBOztBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsc0JBQXNCO0FBQ3RCLHlCQUF5QjtBQUN6QixjQUFjO0FBQ2QsWUFBWTtBQUNaOztBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUNBO0lBQ0ksV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksZUFBZTtJQUNmLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxtQkFBbUI7QUFDdkI7O0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEIsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQix1QkFBdUI7QUFDdkIsY0FBYztBQUNkOztBQUNBO0FBQ0EsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQix1QkFBdUI7QUFDdkIsY0FBYztBQUNkLFNBQVM7QUFDVDs7QUFHQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLDBCQUEwQjtJQUMxQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtBQUNoQjs7QUFFQTtBQUNBLGtCQUFrQjtBQUNsQixNQUFNO0FBQ04sUUFBUTtBQUNSLGtCQUFrQjtBQUNsQjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxlQUFlO0lBQ2Ysa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQix5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtBQUNuQixpQkFBaUI7SUFDYixjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLDJCQUEyQjtBQUMvQjs7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7O0FBQ0E7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQix3QkFBd0I7QUFDNUI7O0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztBQUNsQjs7QUFDQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0FBQ3BCLHlCQUF5QjtBQUN6QixrQkFBa0I7QUFDbEIsZUFBZTtBQUNmLHFCQUFxQjtBQUNyQixrQkFBa0I7QUFDbEIsc0JBQXNCO0FBQ3RCLHlCQUF5QjtBQUN6QixjQUFjO0FBQ2Q7O0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsV0FBVztBQUNmOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixRQUFRO0FBQ1o7O0FBR0EsbUNBQW1DOztBQUNuQztBQUNBLG1CQUFtQjtBQUNuQjs7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjs7QUFDQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7QUFDdEI7O0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBQ0E7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBQ0E7SUFDSSxtQkFBbUI7QUFDdkI7O0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBQ0E7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBQ0E7SUFDSSxxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLG1CQUFtQjtBQUN2Qjs7QUFDQTtJQUNJLFNBQVM7QUFDYjs7QUFDQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFDQTtJQUNJLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsZUFBZTtBQUNuQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsWUFBWTtJQUNaLFNBQVM7QUFDYjs7QUFDQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFDQTtJQUNJLGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFNBQVM7QUFDYjs7QUFDQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQix5QkFBeUI7QUFDN0I7O0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFDQTtJQUNJLG9CQUFvQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb2dyYW0tZGV0YWlsL3Byb2dyYW0tZGV0YWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoZWFkZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgcGFkZGluZzogMTlweCAzNXB4O1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAtMXB4IDBweCAjRTVFOUVDO1xufVxuXG5oZWFkZXIgLmxvZ28ge1xuICAgIG1heC13aWR0aDogMTM2cHg7XG59XG5cblxuaGVhZGVyIC5sb2dvIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xufVxuXG4uY29udGVudF9vdXRlciB7XG4gICAgcGFkZGluZy10b3A6IDk5cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDM2cHg7XG59XG5cbmhlYWRlciAubmF2YmFyIHtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG5oZWFkZXIgLm5hdmJhci1saWdodCAubmF2YmFyLW5hdiAubmF2LWxpbmsge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIHBhZGRpbmc6IDlweCAwO1xuICAgIG1hcmdpbi1yaWdodDogMjVweDtcbn1cblxuaGVhZGVyIC5uYXZiYXItbmF2IC5idG5fc3R5bGUge1xuICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJztcbiAgICBib3gtc2hhZG93OiAwcHggNnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5sYW5kaW5nX2Jhbm5lciAuYmFubmVyX292ZXJsYXl7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjU4KSAwJSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwKSAxMDAlKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xufVxuXG4ucHJvZ3JhbV9vdXRlcntcbiAgICBwYWRkaW5nOjUwcHggOTVweCAzMHB4IDk1cHg7XG59XG4ucHJvZ3JhbV9pbWFnZXN7XG4gICAgd2lkdGg6IDEwMCUgO1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG4ucHJvZ3JhbV9pbWFnZXMgLnByb2dyYW1fYm94e1xud2lkdGg6IDEwMCU7XG5tYXgtd2lkdGg6IDM1MHB4O1xubWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4ucHJvZ3JhbV9pbWFnZXMgLnByb2dyYW1fYm94Omxhc3QtY2hpbGR7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xufVxuLnByb2dyYW1faW1hZ2VzIC5wcm9ncmFtX2JveCBpbWd7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xufVxuLnByb2dyYW1fdGV4dHtcbiAgICBwYWRkaW5nOiAzNXB4IDAgMzFweCAwO1xufVxuLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0e1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCBoNXtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMTRweDsgICAgXG4gICAgY29sb3I6ICM3NzdDRUE7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCAubGVhcm5fY29kZXtcbndpZHRoOiA2MHB4O1xuaGVpZ2h0OiA2MHB4O1xuYm9yZGVyLXJhZGl1czogNnB4O1xub3ZlcmZsb3c6IGhpZGRlbjtcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbnRvcDogMDtcbnJpZ2h0OiAwO1xufVxuLnByb2dyYW1fdGV4dCAuQ2F0ZWdvcnlUZXh0IC5sZWFybl9jb2RlIGltZ3tcbm1heC13aWR0aDogMTAwJTtcbmhlaWdodDogMTAwJTtcbn1cbi5wcm9ncmFtX3RleHQgLkNhdGVnb3J5VGV4dCBoM3tcbmZvbnQtc2l6ZTogMzZweDtcbmxpbmUtaGVpZ2h0OiAzN3B4O1xuY29sb3I6ICM3NDg0OTQ7XG5mb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG5tYXJnaW46IDJweCAwIDAgMDtcbn1cbi5zZXNzaW9uX2RldGFpbHN7XG4gICAgbWFyZ2luLXRvcDogM3B4O1xufVxuLnNlc3Npb25fZGV0YWlscyBwe1xuZm9udC1zaXplOiAxM3B4O1xubGluZS1oZWlnaHQ6IDE2cHg7XG5sZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbmNvbG9yOiAjNzQ4NDk0O1xuZGlzcGxheTogaW5saW5lLWJsb2NrO1xubWFyZ2luOjAgMjhweCAwIDA7XG59XG4uc2Vzc2lvbl9kZXRhaWxzIHAgaW1nIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDZweDtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuLmluc3VyZWRfb3V0ZXJ7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgcGFkZGluZzogMTNweCAwO1xuICAgIG1hcmdpbi10b3A6IDI5cHg7XG59XG4uaW5zdXJlZF9vdXRlciBwe1xuZGlzcGxheTogaW5saW5lLWJsb2NrO1xuZm9udC1zaXplOiAxM3B4O1xubGluZS1oZWlnaHQ6IDE2cHg7XG5sZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbmNvbG9yOiAjNzQ4NDk0O1xubWFyZ2luOjAgMzVweCAwIDA7XG59XG4uaW5zdXJlZF9vdXRlciBwIGltZyB7XG4gICAgbWF4LXdpZHRoOiAyN3B4O1xuICAgIG1hcmdpbi1yaWdodDogMTJweDtcbn1cbi5iYXRjaF9vdXRlciAuYmF0Y2hfbGVmdHtcbmJhY2tncm91bmQ6ICNGRkZGRkY7XG5ib3gtc2hhZG93OiAwcHggMXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG5ib3JkZXItcmFkaXVzOiAycHg7XG5wYWRkaW5nOiAzMHB4IDM1cHg7XG59XG4uYmF0Y2hfb3V0ZXIgLmJhdGNoX2xlZnQgaDJ7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xuZm9udC1zaXplOiAyNnB4O1xubGluZS1oZWlnaHQ6IDE4cHg7XG5sZXR0ZXItc3BhY2luZzogMC4wMDVlbTtcbmNvbG9yOiAjNzc3Q0VBO1xufVxuLmJhdGNoX2JveHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBwYWRkaW5nOiAxNnB4IDI4cHg7XG4gICAgbWFyZ2luLXRvcDogMTFweDtcbn1cbi5iYXRjaF9ib3ggLmJhdGNoX3RpdGxlIHB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG4ubGlzdGluZ19vdXRlcntcbiAgICBtYXJnaW4tdG9wOiAyOXB4O1xufVxuLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgcGFkZGluZzogMjFweDtcbn1cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQgaW1ne1xuICAgIG1heC13aWR0aDogMTAwJTtcbn1cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQuYm9yZGVyX25vbmV7XG4gICAgYm9yZGVyOm5vbmU7XG4gICAgbWFyZ2luLXRvcDogMjFweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMzJweDtcbn1cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQgaDJ7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xuICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDVlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xufVxuLmJhdGNoX291dGVyIC5iYXRjaF9yaWdodCBQe1xuZm9udC1zaXplOiAxMnB4O1xubGluZS1oZWlnaHQ6IDE4cHg7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5jb2xvcjogIzc0ODQ5NDtcbm1hcmdpbjogMjZweCAwIDAgMDtcbn1cbi5iYXRjaF9vdXRlciAuYmF0Y2hfcmlnaHQgYS5idXNpbmVzc19idG57XG5taW4taGVpZ2h0OiA1MnB4O1xubGluZS1oZWlnaHQ6IDUycHg7XG53aWR0aDogMTAwJTtcbm1heC13aWR0aDogMTAwJTtcbnRleHQtYWxpZ246IGNlbnRlcjtcbmJhY2tncm91bmQtY29sb3I6ICNmMTVjMjA7XG5jb2xvcjogI2ZmZjtcbmZvbnQtc2l6ZTogMThweDtcbmxldHRlci1zcGFjaW5nOiAwLjAwNWVtO1xucGFkZGluZzogMCAxMHB4O1xuYm9yZGVyLXJhZGl1czogNHB4O1xuZGlzcGxheTogaW5saW5lLWJsb2NrO1xubWFyZ2luLXRvcDogMzJweDtcbn1cbi5iYXRjaF9saXN0X291dGVye1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG4uYmF0Y2hfbGlzdF9vdXRlciAuYmF0Y2hfbGlzdGluZ3tcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luOiAwIDE1cHggMCAwO1xufVxuLmJhdGNoX2xpc3Rfb3V0ZXIgLmJhdGNoX2xpc3RpbmcgbGFiZWx7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDFlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmJhdGNoX2xpc3Rfb3V0ZXIgLmJhdGNoX2xpc3RpbmcgLmJhdGNoX3Byb2ZpbGV7XG53aWR0aDogMjJweDtcbmhlaWdodDogMjJweDtcbmJvcmRlci1yYWRpdXM6IDRweDtcbmRpc3BsYXk6IGlubGluZS1ibG9jaztcbm1hcmdpbi1yaWdodDogMTRweDtcbm92ZXJmbG93OiBoaWRkZW47XG52ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuLmJhdGNoX2xpc3Rfb3V0ZXIgLmJhdGNoX2xpc3RpbmcgLmJhdGNoX3Byb2ZpbGUgaW1ne1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4uYmF0Y2hfbGlzdF9vdXRlciAuYmF0Y2hfbGlzdGluZyBwe1xuZm9udC13ZWlnaHQ6IG5vcm1hbDtcbmZvbnQtc2l6ZTogMTNweDtcbmxpbmUtaGVpZ2h0OiAxNnB4O1xubGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG5kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG5jb2xvcjogIzc0ODQ5NDtcbn1cbi5kZXNjcmlwdGlvbl90ZXh0e1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG5ib3gtc2hhZG93OiAwcHggMXB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG5ib3JkZXItcmFkaXVzOiAycHg7XG5wYWRkaW5nOiAzMHB4IDM1cHg7XG5tYXJnaW4tdG9wOiAyMXB4O1xufVxuLmRlc2NyaXB0aW9uX3RleHQgaDJ7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xuICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDVlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cbi5kZXNjcmlwdGlvbl90ZXh0IHB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbi10b3A6IDIycHg7XG59XG4uZGVzY3JpcHRpb25fdGV4dCBsYWJlbHtcbmZvbnQtd2VpZ2h0OiA2MDA7XG5mb250LXNpemU6IDEwcHg7XG5saW5lLWhlaWdodDogMTRweDtcbnRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG5jb2xvcjogIzc3N0NFQTtcbndpZHRoOiAxMDAlO1xuZGlzcGxheTogaW5saW5lLWJsb2NrXG59XG4uZGVzY3JpcHRpb25fdGV4dCBoMiAudmlld19hbGx7XG5mb250LXdlaWdodDogNjAwO1xuZm9udC1zaXplOiAxMHB4O1xubGluZS1oZWlnaHQ6IDE0cHg7XG5sZXR0ZXItc3BhY2luZzogMC4wMWVtO1xudGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbmNvbG9yOiAjNzQ4NDk0O1xuZmxvYXQ6IHJpZ2h0O1xufVxuLnJldmlld3Nfb3V0ZXIge1xuICAgIG1hcmdpbi10b3A6IDM2cHg7XG59XG4ucmV2aWV3c19vdXRlciAubWVkaWEge1xuICAgIG1hcmdpbjogMCAwIDE1cHggMDtcbn1cbi5yZXZpZXdzX291dGVyIC5tZWRpYSAubWVkaWEtbGVmdHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG4ucmV2aWV3c19vdXRlciAubWVkaWEgLm1lZGlhLWxlZnQgaW1ne1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG4ucmV2aWV3c19vdXRlciAubWVkaWEgLm1lZGlhLWJvZHl7XG4gICAgcGFkZGluZzogMCAwIDAgMTJweDtcbn1cbi5yZXZpZXdzX291dGVyIC5tZWRpYSAubWVkaWEtYm9keSBsYWJlbHtcbmZvbnQtd2VpZ2h0OiA2MDA7XG5mb250LXNpemU6IDEzcHg7XG5saW5lLWhlaWdodDogMThweDtcbmxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuY29sb3I6ICM3NzdDRUE7XG59XG4ucmV2aWV3c19vdXRlciAubWVkaWEgLm1lZGlhLWJvZHkgcHtcbmZvbnQtc2l6ZTogMTNweDtcbmxpbmUtaGVpZ2h0OiAxNnB4O1xubGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG5jb2xvcjogIzc0ODQ5NDtcbm1hcmdpbjogMDtcbn1cblxuXG4uZmVhdHVyZWRfYm94IHtcbiAgICBtYXJnaW4tdG9wOiA0NnB4O1xufVxuXG4uZmVhdHVyZWRfaW1nIHtcbiAgICBib3JkZXItcmFkaXVzOiA0cHggNHB4IDAgMDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIG1heC1oZWlnaHQ6IDE4MHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmZlYXR1cmVkX2ltZyBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5tZW51X2NvbnRyb2x7XG5wb3NpdGlvbjogYWJzb2x1dGU7XG50b3A6IDA7XG5yaWdodDogMDtcbnRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZlYXR1cmVkX2ltZyAubWVudV9jb250cm9sIC5ib29rbWFya19pY29uIGltZ3tcbiAgICBtYXgtd2lkdGg6IDE5cHg7XG59XG5cbi5mZWF0dXJlZF90ZXh0IHtcbiAgICBwYWRkaW5nOiAxMnB4IDA7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZmVhdHVyZWRfdGV4dCBoMyB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgY29sb3I6ICM3NzdDRUE7XG4gICAgbWFyZ2luLXRvcDogMTNweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uZmVhdHVyZWRfdGV4dCBwIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbmxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbjogOXB4IDAgMCAwO1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbn1cbi5mZWF0dXJlZF90ZXh0IC5mZWF0dXJlX2JveHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uZmVhdHVyZWRfdGV4dCAuZmVhdHVyZV9ib3ggcHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCAyOHB4IDAgMDtcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2Fucyc7XG59XG4uZmVhdHVyZV9ib3ggcCBpbWcge1xuICAgIG1hcmdpbi1yaWdodDogNnB4O1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG5cbi5mZWF0dXJlZF90ZXh0IC5yYXRpbmdfYm94IHNwYW4ge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuLmZlYXR1cmVkX3RleHQgLmZlYXR1cmVkX2J0bntcbiAgICBtaW4td2lkdGg6IDc1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgbWluLWhlaWdodDogMjBweDtcbmJvcmRlcjogMXB4IHNvbGlkICM3NzdDRUE7XG5ib3JkZXItcmFkaXVzOiAzcHg7XG5mb250LXNpemU6IDEwcHg7XG5kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5sZXR0ZXItc3BhY2luZzogMC4wMWVtO1xudGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbmNvbG9yOiAjNzc3Q0VBO1xufVxuLmZlYXR1cmVkX3RleHQgLmZlYXR1cmVkX2J0bjpob3ZlcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzc3Q0VBO1xuICAgIGNvbG9yOiAjZmZmO1xufVxuXG4ucmF0aW5nX2JveCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwO1xuICAgIHRvcDogNXB4O1xufVxuXG5cbi8qIHN0YXJ0IHByb2dyYW0gZGV0YWlsMiBwYWdlIGNzcyAqL1xuLmJhdGNoX291dGVyLmRldGFpbGVkMntcbnBhZGRpbmc6IDMwcHggMCAwIDA7XG59XG4uYmF0Y2hfb3V0ZXIuZGV0YWlsZWQyIC5wcm9ncmFtX3RleHR7XG5wYWRkaW5nOiAwIDAgMzFweCAwO1xufVxuLmxlYXJuX2NvZGUubGVmdCBpbWd7XG4gICAgbWF4LXdpZHRoOiAxMDBweDtcbn1cbi5kZXNjcmlwdGlvbl90ZXh0IC5kZXNfbWVkaWF7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCA1N3B4IDAgMDtcbn1cbi5kZXNjcmlwdGlvbl90ZXh0IC5kZXNfbWVkaWE6bGFzdC1jaGlsZHtcbm1hcmdpbjogMDtcbn1cbi5kZXNjcmlwdGlvbl90ZXh0IC5kZXNfbWVkaWEgLm1lZGlhe1xuICAgIG1hcmdpbi10b3A6IDI2cHg7XG59XG4uZGVzY3JpcHRpb25fdGV4dCAuZGVzX21lZGlhIC5tZWRpYS1ib2R5e1xuICAgIHBhZGRpbmc6IDAgMCAwIDIwcHg7XG59XG4uZGVzY3JpcHRpb25fdGV4dCAuZGVzX21lZGlhIC5tZWRpYS1ib2R5IHB7XG5tYXJnaW46IDA7XG59XG4uc3Vic2NyaXB0aW9uX3RleHR7XG4gICAgbWFyZ2luLXRvcDogNTZweDtcbn1cbi5zdWJzY3JpcHRpb25fdGV4dCAuc3Vic2NyaXB0aW9uX2JveHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICBtYXJnaW46IDAgMTYxcHggMCAwO1xufVxuLnN1YnNjcmlwdGlvbl90ZXh0IC5zdWJzY3JpcHRpb25fYm94Omxhc3QtY2hpbGR7XG4gICAgbWFyZ2luOiAwO1xufVxuLnNvY2lhbF9tZWRpYXtcbiAgICBtYXJnaW46IDI4cHggMCAwIDA7XG59XG4uc29jaWFsX21lZGlhIGEgLmljb257XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiAwIDZweCAwIDA7XG4gICAgZm9udC1zaXplOiAxOXB4O1xufVxuLmZlYXR1cmVkX3RleHQgLnJhdGluZ19ib3gge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDEzcHg7XG4gICAgdG9wOiBhdXRvO1xufVxuLmZlYXR1cmVkX3RleHQgLmZlYXR1cmVfYm94IHB7XG4gICAgbWFyZ2luOiAwIDE2cHggMCAwO1xufVxuLmZlYXR1cmVkX3RleHQgcHtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cbi5mZWF0dXJlZF9pbWcgLmNvZGluZ190ZXh0e1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDlweDtcbiAgICBsZWZ0OiA5cHg7XG59XG4uZmVhdHVyZWRfaW1nIC5jb2RpbmdfdGV4dCBoM3tcbiAgICBtaW4td2lkdGg6IDc1cHg7XG4gICAgbWluLWhlaWdodDogMjFweDtcbiAgICBsaW5lLWhlaWdodDogMjFweDtcbiAgICBiYWNrZ3JvdW5kOiAjRjE1QzIwO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwLjVweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAxZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uZmVhdHVyZWRfdGV4dCAuZmVhdHVyZWRfYnRue1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNGMTVDMjA7XG4gICAgY29sb3I6ICNGMTVDMjA7XG59XG4uZmVhdHVyZWRfdGV4dCAuZmVhdHVyZV9ib3gge1xuICAgIG1hcmdpbjogNXB4IDAgMTJweCAwO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/program-detail/program-detail.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/program-detail/program-detail.component.ts ***!
  \******************************************************************/
/*! exports provided: ProgramDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramDetailComponent", function() { return ProgramDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_core_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/models */ "./src/app/core/models/index.ts");
/* harmony import */ var src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services/api.service.service */ "./src/app/core/services/api.service.service.ts");
/* harmony import */ var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-ui-loader */ "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");
/* harmony import */ var src_app_services_map_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/map.service */ "./src/app/services/map.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var ng2_toasty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-toasty */ "./node_modules/ng2-toasty/index.js");









let ProgramDetailComponent = class ProgramDetailComponent {
    constructor(router, ngxLoader, map, snack, apiservice, toastyService) {
        this.router = router;
        this.ngxLoader = ngxLoader;
        this.map = map;
        this.snack = snack;
        this.apiservice = apiservice;
        this.toastyService = toastyService;
        this.lat = 51.673858;
        this.lng = 7.815982;
        this.userData = {};
        this.program = new src_app_core_models__WEBPACK_IMPORTED_MODULE_3__["Program"];
        this.User = new src_app_core_models__WEBPACK_IMPORTED_MODULE_3__["User"];
        this.minValue = 0;
        this.maxValue = 100;
        this.options = {
            floor: 0,
            ceil: 100,
            translate: (value) => {
                return value + ' YRS';
            }
        };
        this.isLogin = false;
        this.providerRole = false;
        this.message = 'claim request submited!';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.program = JSON.parse(localStorage.getItem('program'));
        if (this.userData) {
            this.isLogin = true;
            if (this.userData.role === "provider") {
                this.providerRole = true;
            }
        }
    }
    getProgramById() {
        this.apiservice.getProgramById(this.program._id).subscribe(res => {
            console.log('response get program', res);
            localStorage.removeItem('program');
            localStorage.setItem('program', JSON.stringify(res));
        });
        this.ngxLoader.stop();
    }
    claimBusiness() {
        if (this.userData && this.userData.role === 'provider') {
            this.User.providerId = this.userData.id;
            this.User.userId = this.userData.id;
            this.User.programId = this.program._id;
            this.ngxLoader.start();
            this.apiservice.claimRequest(this.User).subscribe(res => {
                console.log('claim request response ', res);
                this.toastyService.info({ title: 'Info', msg: this.message });
                // this.snack.open(this.message, 'OK', { duration: 4000 });
                this.ngxLoader.stop();
            });
            this.ngxLoader.stop();
        }
        else {
            if (this.userData && this.userData.role === 'parent') {
                this.ngxLoader.start();
                let msg = 'please  register or login as provider to claim this business!';
                this.snack.open(msg, 'OK', { duration: 4000 });
                this.ngxLoader.stop();
            }
            else {
                this.ngxLoader.start();
                let msg = 'please login to claim this business and try again!';
                this.snack.open(msg, 'OK', { duration: 4000 });
                this.router.navigate(['/login']);
                this.ngxLoader.stop();
            }
        }
    }
    signup() {
        this.router.navigate(['/sign-up']);
    }
    login() {
        this.router.navigate(['/login']);
    }
    ngOnInit() {
        this.getProgramById();
    }
};
ProgramDetailComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_5__["NgxUiLoaderService"] },
    { type: src_app_services_map_service__WEBPACK_IMPORTED_MODULE_6__["MapService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBar"] },
    { type: src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] },
    { type: ng2_toasty__WEBPACK_IMPORTED_MODULE_8__["ToastyService"] }
];
ProgramDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-program-detail',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./program-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/program-detail/program-detail.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./program-detail.component.css */ "./src/app/pages/program-detail/program-detail.component.css")).default]
    })
], ProgramDetailComponent);



/***/ }),

/***/ "./src/app/pages/provider/program/program.component.css":
/*!**************************************************************!*\
  !*** ./src/app/pages/provider/program/program.component.css ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb3ZpZGVyL3Byb2dyYW0vcHJvZ3JhbS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/provider/program/program.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/provider/program/program.component.ts ***!
  \*************************************************************/
/*! exports provided: ProgramComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramComponent", function() { return ProgramComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProgramComponent = class ProgramComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProgramComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-program',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./program.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/provider/program/program.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./program.component.css */ "./src/app/pages/provider/program/program.component.css")).default]
    })
], ProgramComponent);



/***/ }),

/***/ "./src/app/pages/term-condition/term-condition.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/pages/term-condition/term-condition.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\nbody {\n    margin: 0;\n    padding: 0;\n    height: 100%;\n    width: 100%;\n    background-color: #f9fafa;\n    font-family: 'Open Sans';\n    font-weight: 400;\n}\n\nbody ul,\nli,\na {\n    list-style: none;\n    padding: 0;\n    margin: 0;\n    text-decoration: none;\n}\n\nbody h1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n    margin: 0;\n}\n\nbody a:hover {\n    text-decoration: none;\n}\n\n.row-0 {\n    margin-right: 0;\n    margin-left: 0;\n}\n\n.row-0 [class*=\"col-\"] {\n    padding-left: 0;\n    padding-right: 0;\n}\n\n.container {\n    max-width: 1160px;\n}\n\n.overlay {\n    background-color: rgba(0, 0, 0, 0.4);\n    position: absolute;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n  }\n\ninput.form-control,\nbutton {\n    box-shadow: none;\n    outline: none;\n}\n\ninput.form-control:focus {\n    box-shadow: none;\n    outline: none;\n}\n\nbutton:focus {\n    box-shadow: none;\n    outline: none;\n}\n\n.btn_style {\n    width: 120px;\n    min-height: 36px;\n    line-height: 36px;\n    background-color: #777CEA;\n    border: 1px solid #777CEA;\n    box-sizing: border-box;\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    display: inline-block;\n    text-align: center;\n    font-size: 22px;\n    letter-spacing: 0.02em;\n    text-transform: uppercase;\n    font-family: 'Nanum Pen';\n    color: #fff;\n    transition: 0.8s;\n}\n\n.btn_style:hover {\n    color: #fff;\n    background-color: #296eb9;\n}\n\n.custom_select {\n    height: auto;\n    display: inline-block;\n    border-radius: 3px;\n    min-width: 200px;\n    min-height: 52px;\n    line-height: 52px;\n    border: 1px solid #E5E9EC;\n    margin: 0;\n    padding: 0;\n    width: 100%;\n}\n\n.content_outer {\n    padding-top: 99px;\n    padding-bottom: 36px;\n}\n\n.breadcrump_top a {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    color: #777CEA;\n}\n\n.breadcrump_top a span {\n    color: #748494;\n    font-size: 14px;\n    margin-right: 3px;\n}\n\n.page_heading {\n    margin-top: 11px;\n}\n\n.page_heading h2 {\n    font-family: 'Nanum Pen';\n    font-style: normal;\n    font-weight: normal;\n    font-size: 28px;\n    line-height: 18px;\n    color: #748494;\n    background: linear-gradient(to right, #cb3651, #f15a29);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n}\n\n.side_bar .progress-title {\n    font-size: 16px;\n    font-weight: 700;\n    color: #333;\n    margin: 0 0 20px;\n}\n\n.side_bar .progress {\n    height: 8px;\n    background: #e5e9ec;\n    border-radius: 0;\n    box-shadow: none;\n    margin-top: 46px;\n    overflow: visible;\n}\n\n.side_bar .progress .progress-bar {\n    position: relative;\n    -webkit-animation: animate-positive 2s;\n    animation: animate-positive 2s;\n    overflow: visible;\n}\n\n.side_bar .progress .progress-value {\n    display: block;\n    font-size: 15px;\n    font-weight: 600;\n    color: #F15C20;\n    position: absolute;\n    top: -11px;\n    left: 0;\n}\n\n.side_bar {\n    width: 100%;\n    max-width: 245px;\n    display: inline-block;\n    padding-right: 65px;\n}\n\n.side_bar .complete_score {\n    margin-top: 7px;\n}\n\n.side_bar .complete_score p {\n    font-style: normal;\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #748494;\n}\n\n.side_bar .complete_score p span {\n    color: #F15C20;\n    float: right;\n    font-size: 14px;\n}\n\n/* side menu css start */\n\n.side_menu {\n    margin-top: 33px;\n}\n\n.side_menu ul li a {\n    font-family: 'Nanum Pen';\n    font-weight: normal;\n    font-size: 20px;\n    line-height: 25px;\n    color: #748494;\n    padding: 10px 16px;\n    display: inline-block;\n    width: 100%;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\n.side_menu ul li a {\n    position: relative;\n}\n\n.side_menu ul li a::after {\n    content: '';\n    position: absolute;\n    left: 0;\n    top: 50%;\n    width: 4px;\n    height: 20px;\n    background-color: transparent;\n    transform: translateY(-50%);\n    border-radius: 0 4px 4px 0;\n    transition: 0.8s;\n}\n\n.side_menu ul li a.active {\n    background-color: #fff;\n}\n\n.side_menu ul li a:hover {\n    background-color: #fff;\n}\n\n.side_menu ul li a:hover::after {\n    background-color: #777CEA;\n}\n\n.side_menu ul li a.active::after {\n    background-color: #777CEA;\n}\n\n/* start content_right css */\n\n.MainContent {\n    display: flex;\n}\n\n.content_right {\n    background: #FFFFFF;\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\n    border-radius: 4px;\n    padding: 58px 34px 38px 34px;\n    width: 100%;\n    float: right;\n    max-width: 955px;\n}\n\n.content_right h2 {\n    font-family: 'Nanum Pen';\n    font-style: normal;\n    font-weight: normal;\n    font-size: 26px;\n    line-height: 32px;\n    color: #748494;\n    margin: 0;\n    background: linear-gradient(to right, #cb3651, #f15a29);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n}\n\n.details_form {\n    display: inline-block;\n    max-width: 440px;\n    width: 100%;\n    margin-top: 15px;\n}\n\n.details_form .form-group {\n    margin-bottom: 4px;\n    position: relative;\n}\n\n.details_form .form-group.delete_account a {\n    color: #777CEA;\n}\n\n.details_form .form-group.delete_account {\n    margin-top: 40px;\n}\n\n.form-group label {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #777CEA;\n    padding: 0 0 0 15px;\n}\n\n.form-group .form-control,\n.form-group input {\n    width: 100%;\n    border-radius: 4px;\n    border: 1px solid #E5E9EC;\n    min-height: 52px;\n    height: auto;\n    font-size: 13px;\n    color: #748494;\n    transition: 0.8s;\n    padding: 14px;\n}\n\n.form-group textarea.form-control {\n    min-height: 180px;\n    resize: none;\n}\n\n.details_form .form-group .input_outer {\n    position: relative;\n}\n\n.details_form .form-group .input_outer::after {\n    position: absolute;\n    content: \"\\e900\";\n    font-family: 'icomoon' !important;\n    font-size: 18px;\n    top: 50%;\n    transform: translateY(-50%);\n    right: 9px;\n    color: #F15C20;\n    opacity: 0;\n}\n\n.details_form .form-group .input_outer:focus::after,\n.details_form .form-group .input_outer:focus-within::after {\n    opacity: 1;\n}\n\n.form-group .form-control:focus,\n.form-group input:focus {\n    outline: none;\n    box-shadow: none;\n    border: 1px solid #666;\n}\n\n.profile_img {\n    max-width: 180px;\n    max-height: 150px;\n    width: 100%;\n    height: 100%;\n    float: right;\n}\n\n.profile_img img {\n    max-width: 100%;\n}\n\n.profile_img.edit {\n    background: #E5E9EC;\n    border-radius: 4px;\n    text-align: center;\n    position: relative;\n}\n\n.profile_img.edit a {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 18px;\n    color: #748494;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n}\n\n.profile_img.edit .icon-edit_profile {\n    display: inline-block;\n    width: 100%;\n    font-size: 20px;\n    margin-bottom: 10px;\n}\n\n.back_arrow {\n    font-size: 13px;\n    line-height: 16px;\n    text-align: justify;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.bottom_arrow {\n    padding: 11px 0;\n    background: rgba(255, 255, 255, 0.1);\n    box-shadow: inset 0px 1px 0px rgba(0, 0, 0, 0.1);\n    width: 100%;\n    margin-top: 148px;\n}\n\n.back_arrow span {\n    vertical-align: middle;\n    margin-right: 14px;\n}\n\n.btn_style.save_btn {\n    float: right;\n}\n\nfooter {\n    padding-bottom: 26px;\n}\n\n.footer_inner {\n    border-top: 1px solid #E5E9EC;\n    padding: 20px 25px;\n    background-color: #FFFFFF;\n}\n\n.footer_logo {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_logo a img {\n    max-width: 78px;\n}\n\n.footer_menu {\n    display: inline-block;\n    width: 100%;\n}\n\n.footer_menu ul {\n    margin-top: 7px;\n}\n\n.footer_menu ul li {\n    display: inline-block;\n    margin: 0 27px 0 0;\n}\n\n.footer_menu ul a {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.footer_menu ul a:hover {\n    color: #777CEA;\n}\n\n.copyright_text {\n    margin-top: 13px;\n}\n\n.copyright_text p {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 16px;\n    margin: 0;\n    color: #748494;\n}\n\n/* guardins page css */\n\n.profile_list .list_box {\n    display: inline-block;\n    width: 100%;\n    max-width: 120px;\n    margin-right: 32px;\n}\n\n.personal_details .margin_top {\n    margin-top: 38px;\n}\n\n.profile_list .list_box .profile_img {\n    max-width: 120px;\n    max-height: 100px;\n    height: 100%;\n    width: 100%;\n}\n\n.profile_list .list_box .profile_img img {\n    max-width: 100%;\n    width: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n}\n\n.profile_list .list_box .profile_text {\n    margin-top: 5px;\n    display: inline-block;\n    width: 100%;\n}\n\n.profile_list .list_box .profile_text h3 {\n    font-weight: 600;\n    font-size: 14px;\n    line-height: 18px;\n    color: #748494;\n}\n\n.profile_list .list_box .profile_text p {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    text-transform: uppercase;\n    color: #777CEA;\n}\n\n.activity .media-left {\n    max-width: 230px;\n    max-height: 200px;\n    position: relative;\n}\n\n.activity .media-left .share_icon {\n    position: absolute;\n    top: 12px;\n    right: 14px;\n}\n\n.activity .media-left img {\n    max-width: 100%;\n    height: 100%;\n    width: 100%;\n}\n\n.activity .media-body {\n    padding-left: 26px;\n}\n\n.list p {\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 16px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n\n.list p img {\n    margin: 0 8px 0 0;\n    vertical-align: middle;\n}\n\n.activity .media-body h3 {\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    letter-spacing: 0.01em;\n    text-transform: uppercase;\n    color: #777CEA;\n}\n\n.activity .media-body h2 {\n    font-size: 22px;\n    line-height: 27px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n    margin: 0px 0 11px 0;\n    font-family: 'Nanum Pen';\n    background: linear-gradient(to right, #cb3651, #f15a29);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n}\n\n.recomend_images a {\n    margin-right: 10px;\n}\n\n.recomend_images a:last-child {\n    margin: 0;\n}\n\n.recomend_images img {\n    max-width: 21px;\n}\n\n.recomend_btn {\n    margin-top: 10px;\n}\n\n.recomend_btn a {\n    min-width: 104px;\n    min-height: 20px;\n    font-size: 12px;\n    color: #F15C20;\n    line-height: 20px;\n    border: 1px solid #F15C20;\n    border-radius: 3px;\n    padding: 0 8px;\n    transition: 0.8s;\n}\n\n.recomend_btn a:hover {\n    background-color: #F15C20;\n    color: #fff;\n}\n\n.details_outer {\n    margin-top: 40px;\n}\n\n.details_outer .row {\n    margin-bottom: 10px;\n    background: #FFFFFF;\n}\n\n.details_outer .list_outer {\n    border: none;\n    border-radius: 0;\n    margin-bottom: 0;\n}\n\n.activity {\n    border: 1px solid #E5E9EC;\n    background-color: #fff;\n    border-radius: 4px;\n}\n\n.rating_box {\n    position: absolute;\n    top: 9px;\n    right: 20px;\n}\n\n.rating_box a {\n    font-size: 12px;\n    line-height: 16px;\n    text-align: justify;\n    color: #748494;\n}\n\n.row.bottom_margin {\n    margin-top: 26px;\n}\n\n.form-group .add_child {\n    font-style: normal;\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 13px 0 0 17px;\n    display: inline-block;\n}\n\n.form-group .add_child .icon-plus {\n    color: #777CEA;\n    font-size: 20px;\n    margin-right: 8px;\n    vertical-align: middle;\n}\n\n.media.account_name {\n    background: #FFFFFF;\n    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);\n    border-radius: 4px;\n    padding: 21px 15px;\n    margin-bottom: 6px;\n}\n\n.media.account_name .media-left {\n    max-width: 46px;\n    max-height: 46px;\n    border-radius: 5px;\n}\n\n.media.account_name .media-left img {\n    max-width: 100%;\n    height: 100%;\n    width: 100%;\n}\n\n.media.account_name .media-body {\n    padding-left: 24px;\n}\n\n.media.account_name .media-body h4 {\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #777CEA;\n}\n\n.media.account_name .media-body p {\n    font-weight: normal;\n    font-size: 12px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 3px 0 0 0;\n}\n\n.form-group.add_interests .input_outer {\n    border: 1px solid #E5E9EC;\n    padding: 14px;\n}\n\n.form-group.add_interests .input_outer a {\n    background: #777CEA;\n    border-radius: 40px;\n    min-width: 119px;\n    line-height: 30px;\n    min-height: 30px;\n    font-size: 12px;\n    display: inline-block;\n    text-align: center;\n    color: #FFFFFF;\n    margin-right: 2px;\n    transition: 0.8s;\n}\n\n.form-group.add_interests .input_outer a:hover {\n    background-color: #296eb9;\n}\n\n.form-group.add_interests .input_outer::after {\n    content: none;\n}\n\n.form-group.add_interests .input_outer .icon-close {\n    vertical-align: middle;\n    margin-right: 13px;\n    font-size: 15px;\n    float: right;\n    margin-top: 7px;\n}\n\n/* start search page css */\n\n.search_portion {\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n    background: #FFFFFF;\n    display: inline-block;\n    padding: 10px 45px;\n    width: 100%;\n}\n\n.left_search {\n    float: left;\n}\n\n.left_search input::-webkit-input-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-moz-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-ms-input-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-moz-placeholder {\n    color: #E5E9EC;\n}\n\n.left_search input::-webkit-placeholder {\n    color: #E5E9EC;\n}\n\n.right_search {\n    float: right;\n    padding-top: 6px;\n}\n\n.right_search a {\n    display: inline-block;\n    color: #C5CED6;\n    font-size: 20px;\n    margin: 0 0 0 11px;\n}\n\n.right_search a.active {\n    color: #F15C20;\n}\n\n.switch.notification {\n    position: relative;\n    display: inline-block;\n    width: 28px;\n    height: 17px;\n    margin: 0 0 0 12px;\n}\n\n.switch.notification input {\n    opacity: 0;\n    width: 0;\n    height: 0;\n}\n\n.switch.notification .slider {\n    position: absolute;\n    cursor: pointer;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #ccc;\n    transition: .4s;\n}\n\n.switch.notification .slider:before {\n    position: absolute;\n    content: \"\";\n    height: 12px;\n    width: 12px;\n    left: 4px;\n    bottom: 3px;\n    background-color: white;\n    transition: .4s;\n}\n\n.switch.notification input:checked+.slider {\n    background-color: #F15C20;\n}\n\n.switch.notification input:focus+.slider {\n    box-shadow: 0 0 1px #F15C20;\n}\n\n.switch.notification input:checked+.slider:before {\n    transform: translateX(9px);\n}\n\n/* Rounded sliders */\n\n.switch.notification .slider.round {\n    border-radius: 34px;\n}\n\n.switch.notification .slider.round:before {\n    border-radius: 50%;\n}\n\n.category_list {\n    display: inline-block;\n    width: 100%;\n    padding: 10px 46px 17px;\n    box-shadow: inset 0px -1px 0px #E5E9EC;\n}\n\n.category_list li {\n    display: inline-block;\n\n}\n\n.category_list li a {\n    font-size: 13px;\n    color: #748494;\n    border: 1px solid #E5E9EC;\n    box-sizing: border-box;\n    border-radius: 2px;\n    padding: 0 15px;\n    min-height: 36px;\n    line-height: 36px;\n    display: inline-block;\n    margin-right: 7px;\n    transition: 0.8s;\n}\n\n.category_list li a:hover,\n.category_list li a.active {\n    background-color: #ffb206;\n    color: #fff;\n    border-color: #ffb206;\n}\n\n.search_activity {\n    padding: 0 0 0 46px;\n}\n\n.search_activity .list_left {\n    padding-top: 20px;\n    padding-right: 20px;\n}\n\n.search_activity .activity {\n    margin-bottom: 8px;\n    position: relative;\n}\n\n.search_activity .activity:last-child {\n    margin: 0;\n}\n\n.search_activity .map_section img {\n    max-width: 100%;\n}\n\n.select_avtar p {\n    font-weight: 600;\n    font-size: 10px;\n    text-transform: uppercase;\n    color: #C5CED6;\n    margin: 14px 0 0 0;\n}\n\n.select_avtar {\n    text-align: center;\n    width: 100%;\n    float: right;\n    max-width: 180px;\n}\n\n.select_avtar a {\n    font-style: normal;\n    font-weight: 600;\n    font-size: 10px;\n    line-height: 14px;\n    color: #777CEA;\n}\n\n.select_avtar a img {\n    margin: 0 0 0 5px;\n}\n\nheader .navbar {\n    padding: 0;\n}\n\nheader .navbar-light .navbar-nav .nav-link {\n    font-size: 13px;\n    font-weight: normal;\n    line-height: 16px;\n    color: #748494;\n    padding: 9px 0;\n    margin-right: 25px;\n}\n\nheader .navbar-nav .btn_style {\n    font-family: 'Open Sans';\n    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    font-weight: 600;\n    font-size: 13px;\n    color: #fff;\n}\n\n.landing_banner .btn_style {\n    font-family: 'Open Sans';\n    min-width: 274px;\n    min-height: 52px;\n    line-height: 52px;\n    background: #F15C20;\n    border: 1px solid #F15C20;\n    box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.1);\n    border-radius: 6px;\n    text-transform: uppercase;\n    font-weight: 600;\n    font-size: 15px;\n}\n\n/* start activity_category css */\n\n.activity_category{\n    padding: 140px 0 54px 0;\n}\n\n.activity_category h2{\n    font-family: Nanum Pen;\nfont-weight: normal;\nfont-size: 28px;\nline-height: 18px;\ncolor: #3D3789;\n}\n\n.activity_category_list{\n    padding-top: 48px;\n}\n\n.activity_category .category_list_box{\n    max-height: 276px;\n    border-radius: 4px;\n}\n\n.category_list_box{\n    overflow: hidden;\n    position: relative;\n}\n\n.activity_category .category_list_box img{\n    max-width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    border-radius: 4px;\n}\n\n.category_list_box .category_title{\nposition: absolute;\nbottom: 23px;\nleft: 33px;\n}\n\n.category_list_box .category_title p{\n    font-family: Nanum Pen;\nfont-weight: normal;\nfont-size: 28px;\nline-height: 18px;\ncolor: #FFFFFF;\n}\n\n.category_list_box .category_title {\n    position: absolute;\n    bottom: 23px;\n    left: 33px;\n    z-index: 1;\n}\n\n/* start notifications page css */\n\n.notifications{\n    margin-top: 28px;\n}\n\n.notifications h3{\n    font-weight: 600;\n    font-size: 15px;\n    line-height: 18px; \n    color: #777CEA;\n}\n\n.notifications .switch.notification {\n    margin: 14px 0 0 0;\n}\n\n.notifications .switch.notification input:checked+.slider {\n    background-color: #777CEA;\n}\n\n.ch-radio {\n    display: inline-block;\n    overflow: hidden;\n    cursor: pointer;\n    position: relative;\n    transition: all 0.4s;\n  }\n\n.ch-radio * {\n    transition: all 0.4s;\n  }\n\n.ch-radio input[type=\"checkbox\"],\n  .ch-radio input[type=\"radio\"] {\n    position: absolute;\n    top: 0;\n    left: 0;\n    opacity: 0;\n  }\n\n.ch-radio input[type=\"checkbox\"]:checked + span:before,\n  .ch-radio input[type=\"checkbox\"]:checked ~ span:before,\n  .ch-radio input[type=\"radio\"]:checked + span:before,\n  .ch-radio input[type=\"radio\"]:checked ~ span:before {\n    border-color: #4b97d7;\n    background-color: #4b97d7;\n  }\n\n.ch-radio input[type=\"checkbox\"]:checked + span .ch-icon,\n  .ch-radio input[type=\"checkbox\"]:checked ~ span .ch-icon,\n  .ch-radio input[type=\"radio\"]:checked + span .ch-icon,\n  .ch-radio input[type=\"radio\"]:checked ~ span .ch-icon {\n    opacity: 1;\n  }\n\n.ch-radio span {\n    display: block;\n    overflow: hidden;\n    line-height: 16px;\n    position: relative;\n    padding-left: 30px;\n    font-size: 13px;\n    font-weight: normal;\n    color: #748494;\n  }\n\n.ch-radio span:before {\n    content: \"\";\n    background-color: transparent;\n    position: absolute;\n    top: 1px;\n    left: 1px;\n    width: 15px;\n    height: 15px;\n    transition: inherit;\n    border: 1px solid #C5CED6;\n    border-radius: 1px;\n  }\n\n.ch-radio span .ch-icon {\n    color: #FFFFFF;\n    position: absolute;\n    top:0;\n    left: 5px;\n    font-size: 9px;\n    opacity: 0;\n    transform: translateY(0);\n    line-height: 17px;\n  }\n\n.check_list{\n    margin-top: 23px;\n  }\n\n.check_list .ch-radio{\n      display: inline-block;\n      margin-right: 44px;\n  }\n\n.textarea_length {\n    position: absolute;\n    bottom: -4px;\n    right: 12px;\n}\n\n.textarea_length p{\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 18px;\n    color: rgba(0, 0, 0, 0.3);\n  }\n\n/* start terms page css */\n\n.terms_body{\n      background-color: #FFFFFF;\n  }\n\n.terms_outer{\n      padding: 30px 0;\n  }\n\n.terms_outer .terms_top h5{\n    font-weight: 600;\n    font-size: 13px;\n    line-height: 18px;\n    letter-spacing: 0.001em;\n    background: linear-gradient(to right, #cb3651, #f15a29);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n    margin: 0;\n  }\n\n.terms_outer .terms_top h2{\n    font-weight: 600;\n    font-size: 26px;\n    line-height: 32px;\n    letter-spacing: 0.01em;\n    color: #748494;\n    margin: 17px 0 0 0;\n  }\n\n.terms_outer .terms_top h3{\n    font-weight: 600;\n    font-size: 15px;\n    line-height: 22px; \n    text-align: justify;\n    letter-spacing: 0.02em;\n    background:linear-gradient(90deg, #B21F6B -54.49%, #F15A29 86.09%);\n    -webkit-background-clip: text;\n    -webkit-text-fill-color: transparent;\n    margin: 16px 0 0 0;\n  }\n\n.terms_outer .terms_box {\n    margin: 49px 0 0 0;\n}\n\n.terms_outer p{\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 16px;\n    text-align: justify;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin: 21px 0 0 0;\n  }\n\n.terms_content_outer .terms_box h2{\n    font-weight: 600;\n    font-size: 26px;\n    line-height: 32px;\n    letter-spacing: 0.01em;\n    color: #748494;\n  }\n\n.terms_content_outer .terms_box ul li{\n    font-weight: normal;\n    font-size: 13px;\n    line-height: 16px;\n    text-align: justify;\n    letter-spacing: 0.001em;\n    color: #748494;\n    position: relative;\n    padding-left: 28px;\n    margin: 3px 0 0 0;\n  }\n\n.terms_content_outer .terms_box ul{\n    margin-top: 16px;\n  }\n\n.terms_content_outer .terms_box ul li::before{\n    content: '';\n    width: 5.33px;\n    height: 5.33px;\n    position: absolute;\n    top: 7px;\n    left: 9px;\n    background: #F23459;\n    border-radius: 50%;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGVybS1jb25kaXRpb24vdGVybS1jb25kaXRpb24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBO0lBQ0ksU0FBUztJQUNULFVBQVU7SUFDVixZQUFZO0lBQ1osV0FBVztJQUNYLHlCQUF5QjtJQUN6Qix3QkFBd0I7SUFDeEIsZ0JBQWdCO0FBQ3BCOztBQUVBOzs7SUFHSSxnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLFNBQVM7SUFDVCxxQkFBcUI7QUFDekI7O0FBRUE7Ozs7OztJQU1JLFNBQVM7QUFDYjs7QUFFQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLG9DQUFvQztJQUNwQyxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLFFBQVE7SUFDUixTQUFTO0lBQ1QsT0FBTztJQUNQLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7O0FBRUY7O0lBRUksZ0JBQWdCO0lBQ2hCLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsMkNBQTJDO0lBQzNDLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLHdCQUF3QjtJQUN4QixXQUFXO0lBRVgsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksV0FBVztJQUNYLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixTQUFTO0lBQ1QsVUFBVTtJQUNWLFdBQVc7QUFDZjs7QUFJQTtJQUNJLGlCQUFpQjtJQUNqQixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCx1REFBdUQ7SUFDdkQsNkJBQTZCO0lBQzdCLG9DQUFvQztBQUN4Qzs7QUFFQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHNDQUFzQztJQUN0Qyw4QkFBOEI7SUFDOUIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsT0FBTztBQUNYOztBQUVBO0lBQ0ksV0FBVztJQUNYLGdCQUFnQjtJQUNoQixxQkFBcUI7SUFDckIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGNBQWM7SUFDZCxZQUFZO0lBQ1osZUFBZTtBQUNuQjs7QUFFQSx3QkFBd0I7O0FBQ3hCO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLFdBQVc7SUFDWCxzQ0FBc0M7QUFDMUM7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7SUFDWiw2QkFBNkI7SUFDN0IsMkJBQTJCO0lBQzNCLDBCQUEwQjtJQUMxQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSx5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSx5QkFBeUI7QUFDN0I7O0FBRUEsNEJBQTRCOztBQUM1QjtJQUNJLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsMENBQTBDO0lBQzFDLGtCQUFrQjtJQUNsQiw0QkFBNEI7SUFDNUIsV0FBVztJQUNYLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxTQUFTO0lBQ1QsdURBQXVEO0lBQ3ZELDZCQUE2QjtJQUM3QixvQ0FBb0M7QUFDeEM7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixjQUFjO0lBQ2QsbUJBQW1CO0FBQ3ZCOztBQUVBOztJQUVJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osZUFBZTtJQUNmLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixpQ0FBaUM7SUFDakMsZUFBZTtJQUNmLFFBQVE7SUFFUiwyQkFBMkI7SUFDM0IsVUFBVTtJQUNWLGNBQWM7SUFDZCxVQUFVO0FBQ2Q7O0FBRUE7O0lBRUksVUFBVTtBQUNkOztBQUVBOztJQUVJLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsWUFBWTtJQUNaLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsU0FBUztJQUNULGdDQUFnQztBQUNwQzs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0lBQ1gsZUFBZTtJQUNmLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLG9DQUFvQztJQUNwQyxnREFBZ0Q7SUFDaEQsV0FBVztJQUNYLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLHNCQUFzQjtJQUN0QixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksNkJBQTZCO0lBQzdCLGtCQUFrQjtJQUNsQix5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztBQUNmOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFNBQVM7SUFDVCxjQUFjO0FBQ2xCOztBQUVBLHNCQUFzQjs7QUFDdEI7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsV0FBVztJQUNYLG9CQUFpQjtPQUFqQixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsWUFBWTtJQUNaLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxvQkFBb0I7SUFDcEIsd0JBQXdCO0lBQ3hCLHVEQUF1RDtJQUN2RCw2QkFBNkI7SUFDN0Isb0NBQW9DO0FBQ3hDOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksU0FBUztBQUNiOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLHlCQUF5QjtJQUN6QixzQkFBc0I7SUFDdEIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QscUJBQXFCO0lBQ3JCLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQiwwQ0FBMEM7SUFDMUMsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixZQUFZO0lBQ1osV0FBVztBQUNmOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLHlCQUF5QjtJQUN6QixhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsaUJBQWlCO0lBRWpCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixZQUFZO0lBQ1osZUFBZTtBQUNuQjs7QUFFQSwwQkFBMEI7O0FBQzFCO0lBQ0ksc0NBQXNDO0lBQ3RDLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFJQTtJQUNJLGNBQWM7QUFDbEI7O0FBRkE7SUFDSSxjQUFjO0FBQ2xCOztBQUZBO0lBQ0ksY0FBYztBQUNsQjs7QUFGQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsY0FBYztJQUNkLGVBQWU7SUFDZixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFVBQVU7SUFDVixRQUFRO0lBQ1IsU0FBUztBQUNiOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixNQUFNO0lBQ04sT0FBTztJQUNQLFFBQVE7SUFDUixTQUFTO0lBQ1Qsc0JBQXNCO0lBRXRCLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWixXQUFXO0lBQ1gsU0FBUztJQUNULFdBQVc7SUFDWCx1QkFBdUI7SUFFdkIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLHlCQUF5QjtBQUM3Qjs7QUFFQTtJQUNJLDJCQUEyQjtBQUMvQjs7QUFFQTtJQUdJLDBCQUEwQjtBQUM5Qjs7QUFFQSxvQkFBb0I7O0FBQ3BCO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLFdBQVc7SUFDWCx1QkFBdUI7SUFDdkIsc0NBQXNDO0FBQzFDOztBQUVBO0lBQ0kscUJBQXFCOztBQUV6Qjs7QUFFQTtJQUNJLGVBQWU7SUFDZixjQUFjO0lBQ2QseUJBQXlCO0lBQ3pCLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLGlCQUFpQjtJQUNqQixnQkFBZ0I7QUFDcEI7O0FBRUE7O0lBRUkseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFNBQVM7QUFDYjs7QUFFQTtJQUNJLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixjQUFjO0lBQ2Qsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsY0FBYztJQUNkLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLHdCQUF3QjtJQUN4Qiw0Q0FBNEM7SUFDNUMsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsV0FBVztBQUNmOztBQUlBO0lBQ0ksd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsMkNBQTJDO0lBQzNDLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsZ0JBQWdCO0lBQ2hCLGVBQWU7QUFDbkI7O0FBQ0EsZ0NBQWdDOztBQUNoQztJQUNJLHVCQUF1QjtBQUMzQjs7QUFDQTtJQUNJLHNCQUFzQjtBQUMxQixtQkFBbUI7QUFDbkIsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2Q7O0FBQ0E7SUFDSSxpQkFBaUI7QUFDckI7O0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCOztBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0Qjs7QUFDQTtJQUNJLGVBQWU7SUFDZixZQUFZO0lBQ1osb0JBQWlCO09BQWpCLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEI7O0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsWUFBWTtBQUNaLFVBQVU7QUFDVjs7QUFDQTtJQUNJLHNCQUFzQjtBQUMxQixtQkFBbUI7QUFDbkIsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixjQUFjO0FBQ2Q7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFVBQVU7SUFDVixVQUFVO0FBQ2Q7O0FBRUEsaUNBQWlDOztBQUNqQztJQUNJLGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7O0FBQ0E7SUFDSSx5QkFBeUI7QUFDN0I7O0FBQ0E7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFFbEIsb0JBQW9CO0VBQ3RCOztBQUVBO0lBRUUsb0JBQW9CO0VBQ3RCOztBQUVBOztJQUVFLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sT0FBTztJQUNQLFVBQVU7RUFDWjs7QUFFQTs7OztJQUlFLHFCQUFxQjtJQUNyQix5QkFBeUI7RUFDM0I7O0FBRUE7Ozs7SUFJRSxVQUFVO0VBQ1o7O0FBRUE7SUFDRSxjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsY0FBYztFQUNoQjs7QUFFQTtJQUNFLFdBQVc7SUFDWCw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1QsV0FBVztJQUNYLFlBQVk7SUFFWixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLGtCQUFrQjtFQUNwQjs7QUFFQTtJQUNFLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsS0FBSztJQUNMLFNBQVM7SUFDVCxjQUFjO0lBQ2QsVUFBVTtJQUVWLHdCQUF3QjtJQUN4QixpQkFBaUI7RUFDbkI7O0FBQ0E7SUFDRSxnQkFBZ0I7RUFDbEI7O0FBRUE7TUFDSSxxQkFBcUI7TUFDckIsa0JBQWtCO0VBQ3RCOztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixXQUFXO0FBQ2Y7O0FBQ0U7SUFDRSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix5QkFBeUI7RUFDM0I7O0FBRUEseUJBQXlCOztBQUN6QjtNQUNJLHlCQUF5QjtFQUM3Qjs7QUFDQTtNQUNJLGVBQWU7RUFDbkI7O0FBQ0E7SUFDRSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQix1QkFBdUI7SUFDdkIsdURBQXVEO0lBQ3ZELDZCQUE2QjtJQUM3QixvQ0FBb0M7SUFDcEMsU0FBUztFQUNYOztBQUNBO0lBQ0UsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsc0JBQXNCO0lBQ3RCLGNBQWM7SUFDZCxrQkFBa0I7RUFDcEI7O0FBQ0E7SUFDRSxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLGtFQUFrRTtJQUNsRSw2QkFBNkI7SUFDN0Isb0NBQW9DO0lBQ3BDLGtCQUFrQjtFQUNwQjs7QUFDQTtJQUNFLGtCQUFrQjtBQUN0Qjs7QUFDRTtJQUNFLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGtCQUFrQjtFQUNwQjs7QUFDQTtJQUNFLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0QixjQUFjO0VBQ2hCOztBQUNBO0lBQ0UsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixpQkFBaUI7RUFDbkI7O0FBQ0E7SUFDRSxnQkFBZ0I7RUFDbEI7O0FBQ0E7SUFDRSxXQUFXO0lBQ1gsYUFBYTtJQUNiLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxtQkFBbUI7SUFDbkIsa0JBQWtCO0VBQ3BCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGVybS1jb25kaXRpb24vdGVybS1jb25kaXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG5ib2R5IHtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y5ZmFmYTtcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2Fucyc7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuYm9keSB1bCxcbmxpLFxuYSB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmJvZHkgaDEsXG5oMixcbmgzLFxuaDQsXG5oNSxcbmg2IHtcbiAgICBtYXJnaW46IDA7XG59XG5cbmJvZHkgYTpob3ZlciB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4ucm93LTAge1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBtYXJnaW4tbGVmdDogMDtcbn1cblxuLnJvdy0wIFtjbGFzcyo9XCJjb2wtXCJdIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgcGFkZGluZy1yaWdodDogMDtcbn1cblxuLmNvbnRhaW5lciB7XG4gICAgbWF4LXdpZHRoOiAxMTYwcHg7XG59XG5cbi5vdmVybGF5IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNCk7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gIH1cblxuaW5wdXQuZm9ybS1jb250cm9sLFxuYnV0dG9uIHtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIG91dGxpbmU6IG5vbmU7XG59XG5cbmlucHV0LmZvcm0tY29udHJvbDpmb2N1cyB7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBvdXRsaW5lOiBub25lO1xufVxuXG5idXR0b246Zm9jdXMge1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgb3V0bGluZTogbm9uZTtcbn1cblxuLmJ0bl9zdHlsZSB7XG4gICAgd2lkdGg6IDEyMHB4O1xuICAgIG1pbi1oZWlnaHQ6IDM2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDM2cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc3N0NFQTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNzc3Q0VBO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm94LXNoYWRvdzogMHB4IDJweCAycHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDJlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtZmFtaWx5OiAnTmFudW0gUGVuJztcbiAgICBjb2xvcjogI2ZmZjtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IDAuOHM7XG4gICAgdHJhbnNpdGlvbjogMC44cztcbn1cblxuLmJ0bl9zdHlsZTpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI5NmViOTtcbn1cblxuLmN1c3RvbV9zZWxlY3Qge1xuICAgIGhlaWdodDogYXV0bztcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIG1pbi13aWR0aDogMjAwcHg7XG4gICAgbWluLWhlaWdodDogNTJweDtcbiAgICBsaW5lLWhlaWdodDogNTJweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5cblxuLmNvbnRlbnRfb3V0ZXIge1xuICAgIHBhZGRpbmctdG9wOiA5OXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAzNnB4O1xufVxuXG4uYnJlYWRjcnVtcF90b3AgYSB7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5icmVhZGNydW1wX3RvcCBhIHNwYW4ge1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDNweDtcbn1cblxuLnBhZ2VfaGVhZGluZyB7XG4gICAgbWFyZ2luLXRvcDogMTFweDtcbn1cblxuLnBhZ2VfaGVhZGluZyBoMiB7XG4gICAgZm9udC1mYW1pbHk6ICdOYW51bSBQZW4nO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNjYjM2NTEsICNmMTVhMjkpO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cblxuLnNpZGVfYmFyIC5wcm9ncmVzcy10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgbWFyZ2luOiAwIDAgMjBweDtcbn1cblxuLnNpZGVfYmFyIC5wcm9ncmVzcyB7XG4gICAgaGVpZ2h0OiA4cHg7XG4gICAgYmFja2dyb3VuZDogI2U1ZTllYztcbiAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogNDZweDtcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbn1cblxuLnNpZGVfYmFyIC5wcm9ncmVzcyAucHJvZ3Jlc3MtYmFyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLXdlYmtpdC1hbmltYXRpb246IGFuaW1hdGUtcG9zaXRpdmUgMnM7XG4gICAgYW5pbWF0aW9uOiBhbmltYXRlLXBvc2l0aXZlIDJzO1xuICAgIG92ZXJmbG93OiB2aXNpYmxlO1xufVxuXG4uc2lkZV9iYXIgLnByb2dyZXNzIC5wcm9ncmVzcy12YWx1ZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICNGMTVDMjA7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogLTExcHg7XG4gICAgbGVmdDogMDtcbn1cblxuLnNpZGVfYmFyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXgtd2lkdGg6IDI0NXB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nLXJpZ2h0OiA2NXB4O1xufVxuXG4uc2lkZV9iYXIgLmNvbXBsZXRlX3Njb3JlIHtcbiAgICBtYXJnaW4tdG9wOiA3cHg7XG59XG5cbi5zaWRlX2JhciAuY29tcGxldGVfc2NvcmUgcCB7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5zaWRlX2JhciAuY29tcGxldGVfc2NvcmUgcCBzcGFuIHtcbiAgICBjb2xvcjogI0YxNUMyMDtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4vKiBzaWRlIG1lbnUgY3NzIHN0YXJ0ICovXG4uc2lkZV9tZW51IHtcbiAgICBtYXJnaW4tdG9wOiAzM3B4O1xufVxuXG4uc2lkZV9tZW51IHVsIGxpIGEge1xuICAgIGZvbnQtZmFtaWx5OiAnTmFudW0gUGVuJztcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMjVweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAtMXB4IDBweCAjRTVFOUVDO1xufVxuXG4uc2lkZV9tZW51IHVsIGxpIGEge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnNpZGVfbWVudSB1bCBsaSBhOjphZnRlciB7XG4gICAgY29udGVudDogJyc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgdG9wOiA1MCU7XG4gICAgd2lkdGg6IDRweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xuICAgIHRyYW5zaXRpb246IDAuOHM7XG59XG5cbi5zaWRlX21lbnUgdWwgbGkgYS5hY3RpdmUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG5cbi5zaWRlX21lbnUgdWwgbGkgYTpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLnNpZGVfbWVudSB1bCBsaSBhOmhvdmVyOjphZnRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzc3N0NFQTtcbn1cblxuLnNpZGVfbWVudSB1bCBsaSBhLmFjdGl2ZTo6YWZ0ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM3NzdDRUE7XG59XG5cbi8qIHN0YXJ0IGNvbnRlbnRfcmlnaHQgY3NzICovXG4uTWFpbkNvbnRlbnQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5jb250ZW50X3JpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgcGFkZGluZzogNThweCAzNHB4IDM4cHggMzRweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWF4LXdpZHRoOiA5NTVweDtcbn1cblxuLmNvbnRlbnRfcmlnaHQgaDIge1xuICAgIGZvbnQtZmFtaWx5OiAnTmFudW0gUGVuJztcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDI2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiAwO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2NiMzY1MSwgI2YxNWEyOSk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuXG4uZGV0YWlsc19mb3JtIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWF4LXdpZHRoOiA0NDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwIHtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwLmRlbGV0ZV9hY2NvdW50IGEge1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwLmRlbGV0ZV9hY2NvdW50IHtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG4uZm9ybS1ncm91cCBsYWJlbCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbiAgICBwYWRkaW5nOiAwIDAgMCAxNXB4O1xufVxuXG4uZm9ybS1ncm91cCAuZm9ybS1jb250cm9sLFxuLmZvcm0tZ3JvdXAgaW5wdXQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIG1pbi1oZWlnaHQ6IDUycHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xuICAgIHBhZGRpbmc6IDE0cHg7XG59XG5cbi5mb3JtLWdyb3VwIHRleHRhcmVhLmZvcm0tY29udHJvbCB7XG4gICAgbWluLWhlaWdodDogMTgwcHg7XG4gICAgcmVzaXplOiBub25lO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZGV0YWlsc19mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlcjo6YWZ0ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBjb250ZW50OiBcIlxcZTkwMFwiO1xuICAgIGZvbnQtZmFtaWx5OiAnaWNvbW9vbicgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgdG9wOiA1MCU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIHJpZ2h0OiA5cHg7XG4gICAgY29sb3I6ICNGMTVDMjA7XG4gICAgb3BhY2l0eTogMDtcbn1cblxuLmRldGFpbHNfZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXI6Zm9jdXM6OmFmdGVyLFxuLmRldGFpbHNfZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXI6Zm9jdXMtd2l0aGluOjphZnRlciB7XG4gICAgb3BhY2l0eTogMTtcbn1cblxuLmZvcm0tZ3JvdXAgLmZvcm0tY29udHJvbDpmb2N1cyxcbi5mb3JtLWdyb3VwIGlucHV0OmZvY3VzIHtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzY2Njtcbn1cblxuLnByb2ZpbGVfaW1nIHtcbiAgICBtYXgtd2lkdGg6IDE4MHB4O1xuICAgIG1heC1oZWlnaHQ6IDE1MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbi5wcm9maWxlX2ltZyBpbWcge1xuICAgIG1heC13aWR0aDogMTAwJTtcbn1cblxuLnByb2ZpbGVfaW1nLmVkaXQge1xuICAgIGJhY2tncm91bmQ6ICNFNUU5RUM7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5wcm9maWxlX2ltZy5lZGl0IGEge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDUwJTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbi5wcm9maWxlX2ltZy5lZGl0IC5pY29uLWVkaXRfcHJvZmlsZSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uYmFja19hcnJvdyB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG5cbi5ib3R0b21fYXJyb3cge1xuICAgIHBhZGRpbmc6IDExcHggMDtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMSk7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMHB4IDFweCAwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDE0OHB4O1xufVxuXG4uYmFja19hcnJvdyBzcGFuIHtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgIG1hcmdpbi1yaWdodDogMTRweDtcbn1cblxuLmJ0bl9zdHlsZS5zYXZlX2J0biB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xufVxuXG5mb290ZXIge1xuICAgIHBhZGRpbmctYm90dG9tOiAyNnB4O1xufVxuXG4uZm9vdGVyX2lubmVyIHtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI0U1RTlFQztcbiAgICBwYWRkaW5nOiAyMHB4IDI1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRkZGRjtcbn1cblxuLmZvb3Rlcl9sb2dvIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5mb290ZXJfbG9nbyBhIGltZyB7XG4gICAgbWF4LXdpZHRoOiA3OHB4O1xufVxuXG4uZm9vdGVyX21lbnUge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmZvb3Rlcl9tZW51IHVsIHtcbiAgICBtYXJnaW4tdG9wOiA3cHg7XG59XG5cbi5mb290ZXJfbWVudSB1bCBsaSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbjogMCAyN3B4IDAgMDtcbn1cblxuLmZvb3Rlcl9tZW51IHVsIGEge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4uZm9vdGVyX21lbnUgdWwgYTpob3ZlciB7XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5jb3B5cmlnaHRfdGV4dCB7XG4gICAgbWFyZ2luLXRvcDogMTNweDtcbn1cblxuLmNvcHlyaWdodF90ZXh0IHAge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbjogMDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLyogZ3VhcmRpbnMgcGFnZSBjc3MgKi9cbi5wcm9maWxlX2xpc3QgLmxpc3RfYm94IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWF4LXdpZHRoOiAxMjBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDMycHg7XG59XG5cbi5wZXJzb25hbF9kZXRhaWxzIC5tYXJnaW5fdG9wIHtcbiAgICBtYXJnaW4tdG9wOiAzOHB4O1xufVxuXG4ucHJvZmlsZV9saXN0IC5saXN0X2JveCAucHJvZmlsZV9pbWcge1xuICAgIG1heC13aWR0aDogMTIwcHg7XG4gICAgbWF4LWhlaWdodDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ucHJvZmlsZV9saXN0IC5saXN0X2JveCAucHJvZmlsZV9pbWcgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5wcm9maWxlX2xpc3QgLmxpc3RfYm94IC5wcm9maWxlX3RleHQge1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5wcm9maWxlX2xpc3QgLmxpc3RfYm94IC5wcm9maWxlX3RleHQgaDMge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4ucHJvZmlsZV9saXN0IC5saXN0X2JveCAucHJvZmlsZV90ZXh0IHAge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5hY3Rpdml0eSAubWVkaWEtbGVmdCB7XG4gICAgbWF4LXdpZHRoOiAyMzBweDtcbiAgICBtYXgtaGVpZ2h0OiAyMDBweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5hY3Rpdml0eSAubWVkaWEtbGVmdCAuc2hhcmVfaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMTJweDtcbiAgICByaWdodDogMTRweDtcbn1cblxuLmFjdGl2aXR5IC5tZWRpYS1sZWZ0IGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmFjdGl2aXR5IC5tZWRpYS1ib2R5IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDI2cHg7XG59XG5cbi5saXN0IHAge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4ubGlzdCBwIGltZyB7XG4gICAgbWFyZ2luOiAwIDhweCAwIDA7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxuLmFjdGl2aXR5IC5tZWRpYS1ib2R5IGgzIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBsaW5lLWhlaWdodDogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMWVtO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG5cbi5hY3Rpdml0eSAubWVkaWEtYm9keSBoMiB7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyN3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xuICAgIG1hcmdpbjogMHB4IDAgMTFweCAwO1xuICAgIGZvbnQtZmFtaWx5OiAnTmFudW0gUGVuJztcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNjYjM2NTEsICNmMTVhMjkpO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cblxuLnJlY29tZW5kX2ltYWdlcyBhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5yZWNvbWVuZF9pbWFnZXMgYTpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5yZWNvbWVuZF9pbWFnZXMgaW1nIHtcbiAgICBtYXgtd2lkdGg6IDIxcHg7XG59XG5cbi5yZWNvbWVuZF9idG4ge1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5yZWNvbWVuZF9idG4gYSB7XG4gICAgbWluLXdpZHRoOiAxMDRweDtcbiAgICBtaW4taGVpZ2h0OiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBjb2xvcjogI0YxNUMyMDtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRjE1QzIwO1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBwYWRkaW5nOiAwIDhweDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4ucmVjb21lbmRfYnRuIGE6aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMTVDMjA7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5kZXRhaWxzX291dGVyIHtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG4uZGV0YWlsc19vdXRlciAucm93IHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG59XG5cbi5kZXRhaWxzX291dGVyIC5saXN0X291dGVyIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uYWN0aXZpdHkge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi5yYXRpbmdfYm94IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA5cHg7XG4gICAgcmlnaHQ6IDIwcHg7XG59XG5cbi5yYXRpbmdfYm94IGEge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xufVxuXG4ucm93LmJvdHRvbV9tYXJnaW4ge1xuICAgIG1hcmdpbi10b3A6IDI2cHg7XG59XG5cbi5mb3JtLWdyb3VwIC5hZGRfY2hpbGQge1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBtYXJnaW46IDEzcHggMCAwIDE3cHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4uZm9ybS1ncm91cCAuYWRkX2NoaWxkIC5pY29uLXBsdXMge1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDhweDtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuXG4ubWVkaWEuYWNjb3VudF9uYW1lIHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgcGFkZGluZzogMjFweCAxNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDZweDtcbn1cblxuLm1lZGlhLmFjY291bnRfbmFtZSAubWVkaWEtbGVmdCB7XG4gICAgbWF4LXdpZHRoOiA0NnB4O1xuICAgIG1heC1oZWlnaHQ6IDQ2cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4ubWVkaWEuYWNjb3VudF9uYW1lIC5tZWRpYS1sZWZ0IGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1lZGlhLmFjY291bnRfbmFtZSAubWVkaWEtYm9keSB7XG4gICAgcGFkZGluZy1sZWZ0OiAyNHB4O1xufVxuXG4ubWVkaWEuYWNjb3VudF9uYW1lIC5tZWRpYS1ib2R5IGg0IHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc3N0NFQTtcbn1cblxuLm1lZGlhLmFjY291bnRfbmFtZSAubWVkaWEtYm9keSBwIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBtYXJnaW46IDNweCAwIDAgMDtcbn1cblxuLmZvcm0tZ3JvdXAuYWRkX2ludGVyZXN0cyAuaW5wdXRfb3V0ZXIge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFNUU5RUM7XG4gICAgcGFkZGluZzogMTRweDtcbn1cblxuLmZvcm0tZ3JvdXAuYWRkX2ludGVyZXN0cyAuaW5wdXRfb3V0ZXIgYSB7XG4gICAgYmFja2dyb3VuZDogIzc3N0NFQTtcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICAgIG1pbi13aWR0aDogMTE5cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgbWluLWhlaWdodDogMzBweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IDAuOHM7XG4gICAgdHJhbnNpdGlvbjogMC44cztcbn1cblxuLmZvcm0tZ3JvdXAuYWRkX2ludGVyZXN0cyAuaW5wdXRfb3V0ZXIgYTpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI5NmViOTtcbn1cblxuLmZvcm0tZ3JvdXAuYWRkX2ludGVyZXN0cyAuaW5wdXRfb3V0ZXI6OmFmdGVyIHtcbiAgICBjb250ZW50OiBub25lO1xufVxuXG4uZm9ybS1ncm91cC5hZGRfaW50ZXJlc3RzIC5pbnB1dF9vdXRlciAuaWNvbi1jbG9zZSB7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICBtYXJnaW4tcmlnaHQ6IDEzcHg7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW4tdG9wOiA3cHg7XG59XG5cbi8qIHN0YXJ0IHNlYXJjaCBwYWdlIGNzcyAqL1xuLnNlYXJjaF9wb3J0aW9uIHtcbiAgICBib3gtc2hhZG93OiBpbnNldCAwcHggLTFweCAwcHggI0U1RTlFQztcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nOiAxMHB4IDQ1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5sZWZ0X3NlYXJjaCB7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cblxuXG4ubGVmdF9zZWFyY2ggaW5wdXQ6OnBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogI0U1RTlFQztcbn1cblxuLmxlZnRfc2VhcmNoIGlucHV0OjotbW96LXBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogI0U1RTlFQztcbn1cblxuLmxlZnRfc2VhcmNoIGlucHV0Ojotd2Via2l0LXBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogI0U1RTlFQztcbn1cblxuLnJpZ2h0X3NlYXJjaCB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIHBhZGRpbmctdG9wOiA2cHg7XG59XG5cbi5yaWdodF9zZWFyY2ggYSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGNvbG9yOiAjQzVDRUQ2O1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBtYXJnaW46IDAgMCAwIDExcHg7XG59XG5cbi5yaWdodF9zZWFyY2ggYS5hY3RpdmUge1xuICAgIGNvbG9yOiAjRjE1QzIwO1xufVxuXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMjhweDtcbiAgICBoZWlnaHQ6IDE3cHg7XG4gICAgbWFyZ2luOiAwIDAgMCAxMnB4O1xufVxuXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiBpbnB1dCB7XG4gICAgb3BhY2l0eTogMDtcbiAgICB3aWR0aDogMDtcbiAgICBoZWlnaHQ6IDA7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIC5zbGlkZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAuNHM7XG4gICAgdHJhbnNpdGlvbjogLjRzO1xufVxuXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiAuc2xpZGVyOmJlZm9yZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgaGVpZ2h0OiAxMnB4O1xuICAgIHdpZHRoOiAxMnB4O1xuICAgIGxlZnQ6IDRweDtcbiAgICBib3R0b206IDNweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC40cztcbiAgICB0cmFuc2l0aW9uOiAuNHM7XG59XG5cbi5zd2l0Y2gubm90aWZpY2F0aW9uIGlucHV0OmNoZWNrZWQrLnNsaWRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0YxNUMyMDtcbn1cblxuLnN3aXRjaC5ub3RpZmljYXRpb24gaW5wdXQ6Zm9jdXMrLnNsaWRlciB7XG4gICAgYm94LXNoYWRvdzogMCAwIDFweCAjRjE1QzIwO1xufVxuXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiBpbnB1dDpjaGVja2VkKy5zbGlkZXI6YmVmb3JlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCg5cHgpO1xuICAgIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoOXB4KTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoOXB4KTtcbn1cblxuLyogUm91bmRlZCBzbGlkZXJzICovXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiAuc2xpZGVyLnJvdW5kIHtcbiAgICBib3JkZXItcmFkaXVzOiAzNHB4O1xufVxuXG4uc3dpdGNoLm5vdGlmaWNhdGlvbiAuc2xpZGVyLnJvdW5kOmJlZm9yZSB7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG4uY2F0ZWdvcnlfbGlzdCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDEwcHggNDZweCAxN3B4O1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAtMXB4IDBweCAjRTVFOUVDO1xufVxuXG4uY2F0ZWdvcnlfbGlzdCBsaSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuXG59XG5cbi5jYXRlZ29yeV9saXN0IGxpIGEge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjRTVFOUVDO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIHBhZGRpbmc6IDAgMTVweDtcbiAgICBtaW4taGVpZ2h0OiAzNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzNnB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tcmlnaHQ6IDdweDtcbiAgICB0cmFuc2l0aW9uOiAwLjhzO1xufVxuXG4uY2F0ZWdvcnlfbGlzdCBsaSBhOmhvdmVyLFxuLmNhdGVnb3J5X2xpc3QgbGkgYS5hY3RpdmUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmIyMDY7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLWNvbG9yOiAjZmZiMjA2O1xufVxuXG4uc2VhcmNoX2FjdGl2aXR5IHtcbiAgICBwYWRkaW5nOiAwIDAgMCA0NnB4O1xufVxuXG4uc2VhcmNoX2FjdGl2aXR5IC5saXN0X2xlZnQge1xuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5zZWFyY2hfYWN0aXZpdHkgLmFjdGl2aXR5IHtcbiAgICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uc2VhcmNoX2FjdGl2aXR5IC5hY3Rpdml0eTpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5zZWFyY2hfYWN0aXZpdHkgLm1hcF9zZWN0aW9uIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xufVxuXG4uc2VsZWN0X2F2dGFyIHAge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICNDNUNFRDY7XG4gICAgbWFyZ2luOiAxNHB4IDAgMCAwO1xufVxuXG4uc2VsZWN0X2F2dGFyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1heC13aWR0aDogMTgwcHg7XG59XG5cbi5zZWxlY3RfYXZ0YXIgYSB7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xufVxuXG4uc2VsZWN0X2F2dGFyIGEgaW1nIHtcbiAgICBtYXJnaW46IDAgMCAwIDVweDtcbn1cblxuaGVhZGVyIC5uYXZiYXIge1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbmhlYWRlciAubmF2YmFyLWxpZ2h0IC5uYXZiYXItbmF2IC5uYXYtbGluayB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgcGFkZGluZzogOXB4IDA7XG4gICAgbWFyZ2luLXJpZ2h0OiAyNXB4O1xufVxuXG5oZWFkZXIgLm5hdmJhci1uYXYgLmJ0bl9zdHlsZSB7XG4gICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnO1xuICAgIGJveC1zaGFkb3c6IDBweCA2cHggMjBweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuXG5cbi5sYW5kaW5nX2Jhbm5lciAuYnRuX3N0eWxlIHtcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2Fucyc7XG4gICAgbWluLXdpZHRoOiAyNzRweDtcbiAgICBtaW4taGVpZ2h0OiA1MnB4O1xuICAgIGxpbmUtaGVpZ2h0OiA1MnB4O1xuICAgIGJhY2tncm91bmQ6ICNGMTVDMjA7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0YxNUMyMDtcbiAgICBib3gtc2hhZG93OiAwcHggNnB4IDIwcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNXB4O1xufVxuLyogc3RhcnQgYWN0aXZpdHlfY2F0ZWdvcnkgY3NzICovXG4uYWN0aXZpdHlfY2F0ZWdvcnl7XG4gICAgcGFkZGluZzogMTQwcHggMCA1NHB4IDA7XG59XG4uYWN0aXZpdHlfY2F0ZWdvcnkgaDJ7XG4gICAgZm9udC1mYW1pbHk6IE5hbnVtIFBlbjtcbmZvbnQtd2VpZ2h0OiBub3JtYWw7XG5mb250LXNpemU6IDI4cHg7XG5saW5lLWhlaWdodDogMThweDtcbmNvbG9yOiAjM0QzNzg5O1xufVxuLmFjdGl2aXR5X2NhdGVnb3J5X2xpc3R7XG4gICAgcGFkZGluZy10b3A6IDQ4cHg7XG59XG4uYWN0aXZpdHlfY2F0ZWdvcnkgLmNhdGVnb3J5X2xpc3RfYm94e1xuICAgIG1heC1oZWlnaHQ6IDI3NnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cbi5jYXRlZ29yeV9saXN0X2JveHtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5hY3Rpdml0eV9jYXRlZ29yeSAuY2F0ZWdvcnlfbGlzdF9ib3ggaW1ne1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuLmNhdGVnb3J5X2xpc3RfYm94IC5jYXRlZ29yeV90aXRsZXtcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbmJvdHRvbTogMjNweDtcbmxlZnQ6IDMzcHg7XG59XG4uY2F0ZWdvcnlfbGlzdF9ib3ggLmNhdGVnb3J5X3RpdGxlIHB7XG4gICAgZm9udC1mYW1pbHk6IE5hbnVtIFBlbjtcbmZvbnQtd2VpZ2h0OiBub3JtYWw7XG5mb250LXNpemU6IDI4cHg7XG5saW5lLWhlaWdodDogMThweDtcbmNvbG9yOiAjRkZGRkZGO1xufVxuLmNhdGVnb3J5X2xpc3RfYm94IC5jYXRlZ29yeV90aXRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMjNweDtcbiAgICBsZWZ0OiAzM3B4O1xuICAgIHotaW5kZXg6IDE7XG59XG5cbi8qIHN0YXJ0IG5vdGlmaWNhdGlvbnMgcGFnZSBjc3MgKi9cbi5ub3RpZmljYXRpb25ze1xuICAgIG1hcmdpbi10b3A6IDI4cHg7XG59XG4ubm90aWZpY2F0aW9ucyBoM3tcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBsaW5lLWhlaWdodDogMThweDsgXG4gICAgY29sb3I6ICM3NzdDRUE7XG59XG4ubm90aWZpY2F0aW9ucyAuc3dpdGNoLm5vdGlmaWNhdGlvbiB7XG4gICAgbWFyZ2luOiAxNHB4IDAgMCAwO1xufVxuLm5vdGlmaWNhdGlvbnMgLnN3aXRjaC5ub3RpZmljYXRpb24gaW5wdXQ6Y2hlY2tlZCsuc2xpZGVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzc3Q0VBO1xufVxuLmNoLXJhZGlvIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNHM7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNHM7XG4gIH1cbiAgXG4gIC5jaC1yYWRpbyAqIHtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjRzO1xuICB9XG4gIFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICBcbiAgLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkICsgc3BhbjpiZWZvcmUsXG4gIC5jaC1yYWRpbyBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB+IHNwYW46YmVmb3JlLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgKyBzcGFuOmJlZm9yZSxcbiAgLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkIH4gc3BhbjpiZWZvcmUge1xuICAgIGJvcmRlci1jb2xvcjogIzRiOTdkNztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGI5N2Q3O1xuICB9XG4gIFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgKyBzcGFuIC5jaC1pY29uLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgfiBzcGFuIC5jaC1pY29uLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgKyBzcGFuIC5jaC1pY29uLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgfiBzcGFuIC5jaC1pY29uIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG4gIFxuICAuY2gtcmFkaW8gc3BhbiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZy1sZWZ0OiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICB9XG4gIFxuICAuY2gtcmFkaW8gc3BhbjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMXB4O1xuICAgIGxlZnQ6IDFweDtcbiAgICB3aWR0aDogMTVweDtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBpbmhlcml0O1xuICAgIHRyYW5zaXRpb246IGluaGVyaXQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M1Q0VENjtcbiAgICBib3JkZXItcmFkaXVzOiAxcHg7XG4gIH1cbiAgXG4gIC5jaC1yYWRpbyBzcGFuIC5jaC1pY29uIHtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOjA7XG4gICAgbGVmdDogNXB4O1xuICAgIGZvbnQtc2l6ZTogOXB4O1xuICAgIG9wYWNpdHk6IDA7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDApO1xuICAgIGxpbmUtaGVpZ2h0OiAxN3B4O1xuICB9XG4gIC5jaGVja19saXN0e1xuICAgIG1hcmdpbi10b3A6IDIzcHg7XG4gIH1cblxuICAuY2hlY2tfbGlzdCAuY2gtcmFkaW97XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDQ0cHg7XG4gIH1cbiAgLnRleHRhcmVhX2xlbmd0aCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogLTRweDtcbiAgICByaWdodDogMTJweDtcbn1cbiAgLnRleHRhcmVhX2xlbmd0aCBwe1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIH1cblxuICAvKiBzdGFydCB0ZXJtcyBwYWdlIGNzcyAqL1xuICAudGVybXNfYm9keXtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XG4gIH1cbiAgLnRlcm1zX291dGVye1xuICAgICAgcGFkZGluZzogMzBweCAwO1xuICB9XG4gIC50ZXJtc19vdXRlciAudGVybXNfdG9wIGg1e1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2NiMzY1MSwgI2YxNWEyOSk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIG1hcmdpbjogMDtcbiAgfVxuICAudGVybXNfb3V0ZXIgLnRlcm1zX3RvcCBoMntcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICBsaW5lLWhlaWdodDogMzJweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMWVtO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICAgIG1hcmdpbjogMTdweCAwIDAgMDtcbiAgfVxuICAudGVybXNfb3V0ZXIgLnRlcm1zX3RvcCBoM3tcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBsaW5lLWhlaWdodDogMjJweDsgXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMmVtO1xuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KDkwZGVnLCAjQjIxRjZCIC01NC40OSUsICNGMTVBMjkgODYuMDklKTtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtY2xpcDogdGV4dDtcbiAgICAtd2Via2l0LXRleHQtZmlsbC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luOiAxNnB4IDAgMCAwO1xuICB9XG4gIC50ZXJtc19vdXRlciAudGVybXNfYm94IHtcbiAgICBtYXJnaW46IDQ5cHggMCAwIDA7XG59XG4gIC50ZXJtc19vdXRlciBwe1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luOiAyMXB4IDAgMCAwO1xuICB9XG4gIC50ZXJtc19jb250ZW50X291dGVyIC50ZXJtc19ib3ggaDJ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDI2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbiAgfVxuICAudGVybXNfY29udGVudF9vdXRlciAudGVybXNfYm94IHVsIGxpe1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmctbGVmdDogMjhweDtcbiAgICBtYXJnaW46IDNweCAwIDAgMDtcbiAgfVxuICAudGVybXNfY29udGVudF9vdXRlciAudGVybXNfYm94IHVse1xuICAgIG1hcmdpbi10b3A6IDE2cHg7XG4gIH1cbiAgLnRlcm1zX2NvbnRlbnRfb3V0ZXIgLnRlcm1zX2JveCB1bCBsaTo6YmVmb3Jle1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIHdpZHRoOiA1LjMzcHg7XG4gICAgaGVpZ2h0OiA1LjMzcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogN3B4O1xuICAgIGxlZnQ6IDlweDtcbiAgICBiYWNrZ3JvdW5kOiAjRjIzNDU5O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/term-condition/term-condition.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/term-condition/term-condition.component.ts ***!
  \******************************************************************/
/*! exports provided: TermConditionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermConditionComponent", function() { return TermConditionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TermConditionComponent = class TermConditionComponent {
    constructor() {
        this.userData = {};
        this.isLogin = false;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        if (this.userData) {
            this.isLogin = true;
        }
    }
    ngOnInit() {
    }
};
TermConditionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-term-condition',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./term-condition.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/term-condition/term-condition.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./term-condition.component.css */ "./src/app/pages/term-condition/term-condition.component.css")).default]
    })
], TermConditionComponent);



/***/ }),

/***/ "./src/app/services/map.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/map.service.ts ***!
  \*****************************************/
/*! exports provided: MapService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapService", function() { return MapService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let MapService = class MapService {
    constructor(http) {
        this.http = http;
    }
    getLocation() {
        return this.http.get('');
    }
};
MapService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
MapService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], MapService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const environment = {
    production: false,
    silent: false,
    apiUrls: {
        reports: 'http://93.188.167.68:6700/api',
        master: 'http://93.188.167.68:6100/api',
    },
    name: 'dev'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/user/Desktop/code/letsplay_site_angular/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
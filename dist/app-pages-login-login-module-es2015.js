(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\n\n    <div class=\"otp_outer\">\n        <div class=\"container-fluid\">\n            <div class=\"row align-items-center\">\n                <div class=\"col-md-6 sign_outer\">\n                    <div class=\"otp_form\">\n                        <div class=\"otp_logo\">\n                            <a href=\"#\">\n                                <img src=\"assets/logo-1.png\" alt=\"Logo Image\">\n                            </a>\n                        </div>\n                        <div class=\"sing_text\">\n                            <img src=\"assets/hand_icon.svg\" alt=\"Hand icon\">\n                            <h1>Welcome Back</h1>\n                            <h3>We are delighted to see you back on LetsPlay</h3>\n                        </div>\n                        <div class=\"form\" [formGroup]=\"signinForm\">\n                            <div class=\"form-group\">\n                                <label>Email</label>\n                                <div class=\"input_outer\">\n                                    <input type=\"email\" class=\"form-control\"\n                                        [formControl]=\"signinForm.controls['username']\" [(ngModel)]=\"credentials.email\">\n                                    <small\n                                        *ngIf=\" signinForm.controls['username'].hasError('email') && signinForm.controls['username'].touched\"\n                                        class=\"form-error-msg\">INVALID EMAIL ADDRESS</small>\n                                    <small\n                                        *ngIf=\" signinForm.controls['username'].hasError('required') && signinForm.controls['username'].touched\"\n                                        class=\"form-error-msg\">USERNAME IS REQUIRED</small>\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label>Password</label>\n                                <div class=\"input_outer\">\n                                    <input type=\"password\" class=\"form-control\"\n                                        [formControl]=\"signinForm.controls['password']\"\n                                        [(ngModel)]=\"credentials.password\" [type]=\"hide ? 'password' : 'text'\">\n                                    <small\n                                        *ngIf=\" signinForm.controls['password'].hasError('required') && signinForm.controls['password'].touched\"\n                                        class=\"form-error-msg\">PASSWORD IS REQUIRED</small>\n                                    <mat-icon class=\"field-icon\" *ngIf=\"!hide\" (click)=\"onPassword()\"><svg width=\"17\"\n                                            height=\"13\" viewBox=\"0 0 17 13\" fill=\"none\"\n                                            xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path\n                                                d=\"M0.875 6.375C0.875 6.375 3.875 1.125 8.375 1.125C12.875 1.125 15.875 6.375 15.875 6.375C15.875 6.375 12.875 11.625 8.375 11.625C3.875 11.625 0.875 6.375 0.875 6.375Z\"\n                                                stroke=\"url(#paint0_linear)\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                            <path\n                                                d=\"M8.375 8.625C9.61764 8.625 10.625 7.61764 10.625 6.375C10.625 5.13236 9.61764 4.125 8.375 4.125C7.13236 4.125 6.125 5.13236 6.125 6.375C6.125 7.61764 7.13236 8.625 8.375 8.625Z\"\n                                                stroke=\"url(#paint1_linear)\" stroke-miterlimit=\"10\"\n                                                stroke-linecap=\"square\" />\n                                            <defs>\n                                                <linearGradient id=\"paint0_linear\" x1=\"-11.5163\" y1=\"11.625\" x2=\"13.788\"\n                                                    y2=\"11.625\" gradientUnits=\"userSpaceOnUse\">\n                                                    <stop offset=\"0.166667\" stop-color=\"#B21F6B\" />\n                                                    <stop offset=\"1\" stop-color=\"#F15A29\" />\n                                                </linearGradient>\n                                                <linearGradient id=\"paint1_linear\" x1=\"2.40761\" y1=\"8.625\" x2=\"9.99891\"\n                                                    y2=\"8.625\" gradientUnits=\"userSpaceOnUse\">\n                                                    <stop offset=\"0.166667\" stop-color=\"#B21F6B\" />\n                                                    <stop offset=\"1\" stop-color=\"#F15A29\" />\n                                                </linearGradient>\n                                            </defs>\n                                        </svg>\n                                    </mat-icon>\n\n                                    <mat-icon class=\"field-icon\" *ngIf=\"hide\" (click)=\"onPassword()\"><svg width=\"17\"\n                                            height=\"13\" viewBox=\"0 0 17 13\" fill=\"none\"\n                                            xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path\n                                                d=\"M0.875 6.375C0.875 6.375 3.875 1.125 8.375 1.125C12.875 1.125 15.875 6.375 15.875 6.375C15.875 6.375 12.875 11.625 8.375 11.625C3.875 11.625 0.875 6.375 0.875 6.375Z\"\n                                                stroke=\"#C5CED6\" stroke-miterlimit=\"10\" stroke-linecap=\"square\" />\n                                            <path\n                                                d=\"M8.375 8.625C9.61764 8.625 10.625 7.61764 10.625 6.375C10.625 5.13236 9.61764 4.125 8.375 4.125C7.13236 4.125 6.125 5.13236 6.125 6.375C6.125 7.61764 7.13236 8.625 8.375 8.625Z\"\n                                                stroke=\"#C5CED6\" stroke-miterlimit=\"10\" stroke-linecap=\"square\" />\n                                        </svg></mat-icon>\n\n                                </div>\n                            </div>\n                            <div class=\"sign_in_btn\">\n                                <button class=\"SignBtn\" (click)=\"signin()\" [disabled]=\"signinForm.invalid\">Sign\n                                    IN</button>\n                                <button (click)=\"cancel()\" class=\"SignBtn cancel\">Cancel</button>\n                            </div>\n                        </div>\n                        <div class=\"row forgot_password\">\n                            <div class=\"col-md-6 forgot_text\">\n                                <p>Not a member yet? <a class=\"cursor\" routerLink=\"/sign-up\">SIGN Up</a></p>\n                            </div>\n                            <div class=\"col-md-6 forgot_text right\">\n                                <p><a class=\"cursor\" (click)=\"onForgotPassword()\">forgot password</a></p>\n                            </div>\n                        </div>\n                        <div class=\"copyright_text\">\n                            <p>© 2020 Letsplay Holdings Inc.</p>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-md-6\">\n                    <div class=\"otp_img\">\n                        <img src=\"assets/signin.jpg\" alt=\"Sign In Image\" class=\"img-fluid\">\n                        <div class=\"otp_img_overlay\"></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n\n    <!-- Modal -->\n    <button id=\"openModalButton\" [hidden]=\"true\" data-toggle=\"modal\" data-target=\"#confirmModal\">modal</button>\n    <div class=\"modal fade\" id=\"confirmModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n        aria-hidden=\"true\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <h5 class=\"modal-title\" id=\"exampleModalLabel\">ACTIVATE YOUR ACCOUNT</h5>\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                        <span aria-hidden=\"true\">&times;</span>\n                    </button>\n                </div>\n                <div class=\"modal-body\">\n                    Do you want to Activate you account ?\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">CANCEL</button>\n                    <button type=\"button\" (click)=\"activeDeactiveUser()\" data-dismiss=\"modal\"\n                        class=\"btn btn-primary\">CONFIRM</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</body>\n<ngx-ui-loader></ngx-ui-loader>");

/***/ }),

/***/ "./src/app/pages/login/login.component.css":
/*!*************************************************!*\
  !*** ./src/app/pages/login/login.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".otp_form {\n    max-width: 440px;\n    margin: 0 auto;\n}\n.otp_form .otp_logo{\nposition: absolute;\ntop: 40px;\n}\n.otp_form .form {\n    margin-top: 124px;\n}\n.otp_form .form .form-group {\n    margin: 0 0 10px 0;\n}\n.otp_form .form .form-group.email_text h2 {\n    font-family: 'Patrick Hand';\n    font-size: 20px;\n    line-height: 27px;\n    letter-spacing: 0.001em;\n    color: #748494;\n}\n.otp_form .form .form-group .input_outer {\n    position: relative;\n}\n.otp_form .form .form-group .input_outer:focus::after,\n.otp_form .form .form-group .input_outer:focus-within::after {\n    opacity: 1;\n}\n.otp_form .form .form-group .input_outer.password::after{\n    content:none;\n}\n.otp_form .form .form-group .input_outer.password .password_view{\nposition: absolute;\nright: 9px;\ntop: 50%;\ntransform: translateY(-50%);\n}\n.ch-radio input[type=\"checkbox\"]:checked+span:before,\n.ch-radio input[type=\"checkbox\"]:checked~span:before,\n.ch-radio input[type=\"radio\"]:checked+span:before,\n.ch-radio input[type=\"radio\"]:checked~span:before {\n    border-color: #27AE60;\n    background-color: #27AE60;\n}\n.sign_in_btn {\n    text-align: center;\n    margin: 61px 0 0 0;\n}\n.SignBtn {\n    background: #F15C20;\n    border: 1px solid #F15C20;\n    box-sizing: border-box;\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\n    border-radius: 4px;\n    min-width: 205px;\n    min-height: 56px;\n    line-height: 56px;\n    text-align: center;\n    color: #ffffff;\n    font-weight: 600;\n    font-size: 13px;\n    margin: 0 auto;\n    text-transform: uppercase;\n}\n.SignBtn:hover {\n    background: #c74009;\n}\n.SignBtn.cancel {\n    background: transparent;\n    border: 1px solid #F15C20;\n    color: #F15C20;\n    margin: 0 auto 0 16px;\n}\n.SignBtn.cancel:hover {\n    background: #F15C20;\n    color: #ffffff;\n}\n.otp_img {\n    position: relative;\n    overflow: hidden;\n}\n.otp_img_overlay {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    left: 0;\n    top: 0px;\n    background: linear-gradient(214.99deg, rgba(255, 255, 255, 0.4) 0%, rgba(255, 255, 255, 0) 100%);\n}\n.otp_outer {\n    position: relative;\n}\n.otp_outer .email_register {\n    position: inherit;\n}\n.go_back_page {\n    position: absolute;\n    bottom: 27px;\n}\n.go_back_page p {\n    font-size: 11px;\n    line-height: 15px;\n    letter-spacing: 0.015em;\n    text-transform: uppercase;\n    color: #748494;\n}\n.go_back_page p a {\n    color: #F15C20;\n}\n.sing_text {\n    margin-top: 84px;\n}\n.sing_text h1 {\n    font-family: 'Patrick Hand';\n    font-size: 36px;\n    line-height: 37px;\n    color: #777CEA;\n    margin-top: 31px;\n}\n.sing_text h3 {\n    font-family: 'Patrick Hand';\n    font-size: 20px;\n    line-height: 27px;\n    letter-spacing: 0.001em;\n    color: #748494;\n    margin-top: 10px;\n}\n.otp_outer .sign_outer{\nposition: inherit;\n}\n.forgot_text p{\nfont-size: 11px;\nline-height: 15px;\nletter-spacing: 0.015em;\ntext-transform: uppercase;\ncolor: #748494;\n}\n.forgot_text p a{\n    color: #F15C20;\n}\n.forgot_text.right{\n    text-align: right;\n}\n.forgot_password{\n    margin-top: 44px;\n    padding: 0 5px;\n}\n.copyright_text{\n    position: absolute;\n    bottom: 24px;\n}\n.copyright_text p{\nfont-size: 13px;\nline-height: 16px;\nletter-spacing: 0.001em;\ncolor: #748494;\n}\n.copyright_text{\n    position: absolute;\n    bottom: 24px;\n}\n.copyright_text p{\nfont-size: 13px;\nline-height: 16px;\nletter-spacing: 0.001em;\ncolor: #748494;\n}\nmat-icon.field-icon {\n    float: right;\n    margin-right: 10px;\n    margin-top: -37px;\n    position: relative;\n    z-index: 2;\n    \n}\n\n\n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGdCQUFnQjtJQUNoQixjQUFjO0FBQ2xCO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsU0FBUztBQUNUO0FBRUE7SUFDSSxpQkFBaUI7QUFDckI7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUVBO0lBQ0ksMkJBQTJCO0lBQzNCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLGNBQWM7QUFDbEI7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUdBOztJQUVJLFVBQVU7QUFDZDtBQUVBO0lBQ0ksWUFBWTtBQUNoQjtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCLFVBQVU7QUFDVixRQUFRO0FBRVIsMkJBQTJCO0FBQzNCO0FBRUE7Ozs7SUFJSSxxQkFBcUI7SUFDckIseUJBQXlCO0FBQzdCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0FBQ3RCO0FBRUE7SUFDSSxtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLHNCQUFzQjtJQUN0QiwyQ0FBMkM7SUFDM0Msa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixjQUFjO0lBQ2QseUJBQXlCO0FBQzdCO0FBRUE7SUFDSSxtQkFBbUI7QUFDdkI7QUFFQTtJQUNJLHVCQUF1QjtJQUN2Qix5QkFBeUI7SUFDekIsY0FBYztJQUNkLHFCQUFxQjtBQUN6QjtBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGNBQWM7QUFDbEI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLE9BQU87SUFDUCxRQUFRO0lBQ1IsZ0dBQWdHO0FBQ3BHO0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7QUFFQTtJQUNJLGlCQUFpQjtBQUNyQjtBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFlBQVk7QUFDaEI7QUFFQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLHlCQUF5QjtJQUN6QixjQUFjO0FBQ2xCO0FBRUE7SUFDSSxjQUFjO0FBQ2xCO0FBRUE7SUFDSSxnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJLDJCQUEyQjtJQUMzQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGVBQWU7QUFDZixpQkFBaUI7QUFDakIsdUJBQXVCO0FBQ3ZCLHlCQUF5QjtBQUN6QixjQUFjO0FBQ2Q7QUFDQTtJQUNJLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGNBQWM7QUFDbEI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0FBQ2hCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLHVCQUF1QjtBQUN2QixjQUFjO0FBQ2Q7QUFHQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0FBQ2hCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLHVCQUF1QjtBQUN2QixjQUFjO0FBQ2Q7QUFFRTtJQUNFLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixVQUFVOztBQUVkIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5vdHBfZm9ybSB7XG4gICAgbWF4LXdpZHRoOiA0NDBweDtcbiAgICBtYXJnaW46IDAgYXV0bztcbn1cbi5vdHBfZm9ybSAub3RwX2xvZ297XG5wb3NpdGlvbjogYWJzb2x1dGU7XG50b3A6IDQwcHg7XG59XG5cbi5vdHBfZm9ybSAuZm9ybSB7XG4gICAgbWFyZ2luLXRvcDogMTI0cHg7XG59XG5cbi5vdHBfZm9ybSAuZm9ybSAuZm9ybS1ncm91cCB7XG4gICAgbWFyZ2luOiAwIDAgMTBweCAwO1xufVxuXG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAuZW1haWxfdGV4dCBoMiB7XG4gICAgZm9udC1mYW1pbHk6ICdQYXRyaWNrIEhhbmQnO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMjdweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMDFlbTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLm90cF9mb3JtIC5mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG5cbi5vdHBfZm9ybSAuZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXI6Zm9jdXM6OmFmdGVyLFxuLm90cF9mb3JtIC5mb3JtIC5mb3JtLWdyb3VwIC5pbnB1dF9vdXRlcjpmb2N1cy13aXRoaW46OmFmdGVyIHtcbiAgICBvcGFjaXR5OiAxO1xufVxuXG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyLnBhc3N3b3JkOjphZnRlcntcbiAgICBjb250ZW50Om5vbmU7XG59XG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyLnBhc3N3b3JkIC5wYXNzd29yZF92aWV3e1xucG9zaXRpb246IGFic29sdXRlO1xucmlnaHQ6IDlweDtcbnRvcDogNTAlO1xuLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG50cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCtzcGFuOmJlZm9yZSxcbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZH5zcGFuOmJlZm9yZSxcbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwicmFkaW9cIl06Y2hlY2tlZCtzcGFuOmJlZm9yZSxcbi5jaC1yYWRpbyBpbnB1dFt0eXBlPVwicmFkaW9cIl06Y2hlY2tlZH5zcGFuOmJlZm9yZSB7XG4gICAgYm9yZGVyLWNvbG9yOiAjMjdBRTYwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMyN0FFNjA7XG59XG5cbi5zaWduX2luX2J0biB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogNjFweCAwIDAgMDtcbn1cblxuLlNpZ25CdG4ge1xuICAgIGJhY2tncm91bmQ6ICNGMTVDMjA7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0YxNUMyMDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJveC1zaGFkb3c6IDBweCAycHggMnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIG1pbi13aWR0aDogMjA1cHg7XG4gICAgbWluLWhlaWdodDogNTZweDtcbiAgICBsaW5lLWhlaWdodDogNTZweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLlNpZ25CdG46aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICNjNzQwMDk7XG59XG5cbi5TaWduQnRuLmNhbmNlbCB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0YxNUMyMDtcbiAgICBjb2xvcjogI0YxNUMyMDtcbiAgICBtYXJnaW46IDAgYXV0byAwIDE2cHg7XG59XG5cbi5TaWduQnRuLmNhbmNlbDpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI0YxNUMyMDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLm90cF9pbWcge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ub3RwX2ltZ19vdmVybGF5IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6IDA7XG4gICAgdG9wOiAwcHg7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDIxNC45OWRlZywgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjQpIDAlLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDApIDEwMCUpO1xufVxuXG4ub3RwX291dGVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5vdHBfb3V0ZXIgLmVtYWlsX3JlZ2lzdGVyIHtcbiAgICBwb3NpdGlvbjogaW5oZXJpdDtcbn1cblxuLmdvX2JhY2tfcGFnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMjdweDtcbn1cblxuLmdvX2JhY2tfcGFnZSBwIHtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE1cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDE1ZW07XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzc0ODQ5NDtcbn1cblxuLmdvX2JhY2tfcGFnZSBwIGEge1xuICAgIGNvbG9yOiAjRjE1QzIwO1xufVxuXG4uc2luZ190ZXh0IHtcbiAgICBtYXJnaW4tdG9wOiA4NHB4O1xufVxuXG4uc2luZ190ZXh0IGgxIHtcbiAgICBmb250LWZhbWlseTogJ1BhdHJpY2sgSGFuZCc7XG4gICAgZm9udC1zaXplOiAzNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzN3B4O1xuICAgIGNvbG9yOiAjNzc3Q0VBO1xuICAgIG1hcmdpbi10b3A6IDMxcHg7XG59XG5cbi5zaW5nX3RleHQgaDMge1xuICAgIGZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDI3cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG4gICAgY29sb3I6ICM3NDg0OTQ7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5vdHBfb3V0ZXIgLnNpZ25fb3V0ZXJ7XG5wb3NpdGlvbjogaW5oZXJpdDtcbn1cbi5mb3Jnb3RfdGV4dCBwe1xuZm9udC1zaXplOiAxMXB4O1xubGluZS1oZWlnaHQ6IDE1cHg7XG5sZXR0ZXItc3BhY2luZzogMC4wMTVlbTtcbnRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG5jb2xvcjogIzc0ODQ5NDtcbn1cbi5mb3Jnb3RfdGV4dCBwIGF7XG4gICAgY29sb3I6ICNGMTVDMjA7XG59XG4uZm9yZ290X3RleHQucmlnaHR7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG4uZm9yZ290X3Bhc3N3b3Jke1xuICAgIG1hcmdpbi10b3A6IDQ0cHg7XG4gICAgcGFkZGluZzogMCA1cHg7XG59XG5cbi5jb3B5cmlnaHRfdGV4dHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAyNHB4O1xufVxuLmNvcHlyaWdodF90ZXh0IHB7XG5mb250LXNpemU6IDEzcHg7XG5saW5lLWhlaWdodDogMTZweDtcbmxldHRlci1zcGFjaW5nOiAwLjAwMWVtO1xuY29sb3I6ICM3NDg0OTQ7XG59XG5cblxuLmNvcHlyaWdodF90ZXh0e1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDI0cHg7XG59XG4uY29weXJpZ2h0X3RleHQgcHtcbmZvbnQtc2l6ZTogMTNweDtcbmxpbmUtaGVpZ2h0OiAxNnB4O1xubGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG5jb2xvcjogIzc0ODQ5NDtcbn1cblxuICBtYXQtaWNvbi5maWVsZC1pY29uIHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IC0zN3B4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB6LWluZGV4OiAyO1xuICAgIFxufVxuXG5cbiAgIl19 */");

/***/ }),

/***/ "./src/app/pages/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/services/auth.service */ "./src/app/core/services/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services/api.service.service */ "./src/app/core/services/api.service.service.ts");
/* harmony import */ var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-ui-loader */ "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");
/* harmony import */ var ng2_toasty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-toasty */ "./node_modules/ng2-toasty/index.js");








;
// import { User } from '../../core/models/index'
let LoginComponent = class LoginComponent {
    constructor(router, auth, fb, apiservice, toastyService, ngxLoader) {
        this.router = router;
        this.auth = auth;
        this.fb = fb;
        this.apiservice = apiservice;
        this.toastyService = toastyService;
        this.ngxLoader = ngxLoader;
        this.credentials = {
            email: '',
            password: ''
        };
        this.isLoading = false;
        this.hide = true;
        this.message = 'Logged In Successfully!';
        this.action = true;
        this.isModal = false;
    }
    onPassword() {
        this.hide = !this.hide;
    }
    cancel() {
        this.router.navigate(['/landing']);
    }
    signin() {
        localStorage.removeItem('userId');
        this.ngxLoader.start();
        this.auth.login(this.credentials).subscribe((res) => {
            this.toastyService.success({ title: 'Success', msg: this.message });
            if (res.role === 'provider' && res.isActivated === true) {
                this.ngxLoader.stop();
                this.router.navigate(['program/home']);
            }
            else {
                if (res.isOnBoardingDone) {
                    this.router.navigate(['/search']);
                }
                else {
                    this.router.navigate(['/parent/login-parent']);
                }
            }
        });
        this.ngxLoader.stop();
    }
    activeDeactiveUser() {
        var booleanValue = true;
        this.ngxLoader.start();
        this.apiservice.activeDeactiveUser(this.response.data.id, booleanValue).subscribe(res => {
            this.activeDeactiveResponse = res;
            this.ngxLoader.stop();
            console.log('res from model', this.activeDeactiveResponse);
            if (this.activeDeactiveResponse.isSuccess === true) {
                console.log('active user data', this.activeDeactiveResponse);
                return this.signin();
            }
            else {
                this.toastyService.error({ title: 'Error', msg: this.activeDeactiveResponse.error });
            }
        });
        this.ngxLoader.stop();
    }
    onForgotPassword() {
        this.router.navigate(['/forgot-password']);
    }
    ngOnInit() {
        this.signinForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].min(1)]),
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] },
    { type: ng2_toasty__WEBPACK_IMPORTED_MODULE_7__["ToastyService"] },
    { type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_6__["NgxUiLoaderService"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.css */ "./src/app/pages/login/login.component.css")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login.component */ "./src/app/pages/login/login.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-ui-loader */ "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");








const routes = [
    {
        path: '', component: _login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"], children: []
    }
];
let LoginModule = class LoginModule {
};
LoginModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
            ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
        ]
    })
], LoginModule);



/***/ })

}]);
//# sourceMappingURL=app-pages-login-login-module-es2015.js.map
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-pages-forgot-password-forgot-password-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesForgotPasswordForgotPasswordComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<body>\n\n    <div class=\"otp_outer\">\n        <div class=\"container-fluid\">\n            <div class=\"row align-items-center\">\n                <div class=\"col-md-6 email_register\">\n                    <div class=\"otp_form\">\n                        <div class=\"otp_logo\">\n                            <a href=\"#\">\n                                <img src=\"assets/logo-1.png\" alt=\"Logo Image\">\n                            </a>\n                        </div>\n                        <div *ngIf=\"isForgot\" class=\"form\" [formGroup]=\"otpSendForm\">\n                            <div class=\"form-group email_text\">\n                                <h2>Please enter your registered email</h2>\n                            </div>\n                            <div class=\"form-group\">\n                                <label>email</label>\n                                <div class=\"input_outer\">\n                                    <input type=\"text\" class=\"form-control\" placeholder=\"Type\"\n                                        [formControl]=\"otpSendForm.controls['email']\" [(ngModel)]=\"credentials.email\">\n                                    <small\n                                        *ngIf=\" otpSendForm.controls['email'].hasError('email') && otpSendForm.controls['email'].touched\"\n                                        class=\"form-error-msg\">INVALID EMAIL ADDRESS</small>\n                                    <small\n                                        *ngIf=\" otpSendForm.controls['email'].hasError('required') && otpSendForm.controls['email'].touched\"\n                                        class=\"form-error-msg\">EMAIL IS REQUIRED</small>\n                                </div>\n                            </div>\n                            <div class=\"reset_btn\">\n                                <button class=\"ResetBtn\" [disabled]=\"otpSendForm.invalid\"\n                                    (click)=\"otpRequest()\">Send</button>\n                            </div>\n                            <div class=\"go_back_page\">\n                                <p> Go back to <a [routerLink]=\"['/login']\">SIGN IN</a> </p>\n                            </div>\n                        </div>\n\n\n                        <div *ngIf=\"isOTP\" class=\"form\" [formGroup]=\"otpVerifyForm\">\n                            <div class=\"form-group email_text\">\n                                <h2>Please enter the one time password send to your email</h2>\n                            </div>\n                            <div class=\"form-group\">\n                                <label>One Time Password</label>\n                                <div class=\"input_outer\">\n                                    <input type=\"password\" class=\"form-control\" placeholder=\"Password\"\n                                        [formControl]=\"otpVerifyForm.controls['otp']\" [(ngModel)]=\"credentials.otp\">\n                                    <mat-icon *ngIf=\"otpVerifyForm.valid\" class=\"field-icon\"><svg width=\"12\" height=\"10\"\n                                            viewBox=\"0 0 12 10\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                                            <path\n                                                d=\"M4.59502 9.13831C4.48047 9.25353 4.32419 9.31782 4.16185 9.31782C3.99951 9.31782 3.84322 9.25353 3.72868 9.13831L0.269259 5.67834C-0.089753 5.31932 -0.089753 4.73716 0.269259 4.37882L0.702429 3.94554C1.06155 3.58653 1.64304 3.58653 2.00205 3.94554L4.16185 6.10545L9.99792 0.269259C10.357 -0.089753 10.9391 -0.089753 11.2975 0.269259L11.7307 0.702542C12.0897 1.06155 12.0897 1.6436 11.7307 2.00205L4.59502 9.13831Z\"\n                                                fill=\"#27AE60\" />\n                                        </svg></mat-icon>\n                                    <small\n                                        *ngIf=\" otpVerifyForm.controls['otp'].hasError('minLength') && otpVerifyForm.controls['otp'].touched\"\n                                        class=\"form-error-msg\">FOUR DIGIT NO. REQUIRED</small>\n                                    <small\n                                        *ngIf=\" otpVerifyForm.controls['otp'].hasError('maxLength') && otpVerifyForm.controls['otp'].touched\"\n                                        class=\"form-error-msg\">FOUR DIGIT REQUIRED</small>\n                                    <small\n                                        *ngIf=\" otpVerifyForm.controls['otp'].hasError('required') && otpVerifyForm.controls['otp'].touched\"\n                                        class=\"form-error-msg\">OTP IS REQUIRED</small>\n\n                                </div>\n                            </div>\n                            <div class=\"reset_btn\">\n                                <button class=\"ResetBtn\" [disabled]=\"otpVerifyForm.invalid\"\n                                    (click)=\"otpVerify()\">Verify</button>\n                            </div>\n                        </div>\n\n                        <div *ngIf=\"isNewPassword\" class=\"form\" [formGroup]=\"passwordResetForm\">\n                            <div class=\"form-group\">\n                                <label>New Password</label>\n                                <div class=\"input_outer\">\n                                    <input type=\"password\" class=\"form-control\"\n                                        [formControl]=\"passwordResetForm.controls['newPassword']\"\n                                        [(ngModel)]=\"credentials.newPassword\">\n                                    <small\n                                        *ngIf=\" passwordResetForm.controls['newPassword'].hasError('required') && passwordResetForm.controls['newPassword'].touched\"\n                                        class=\"form-error-msg\">CAN'T LEAVE IT BLANK</small>\n                                </div>\n                            </div>\n                            <div class=\"form-group\">\n                                <label>Confirm new Password</label>\n                                <div class=\"input_outer\">\n                                    <input type=\"password\" class=\"form-control\"\n                                        [formControl]=\"passwordResetForm.controls['confirmPassword']\"\n                                        [(ngModel)]=\"credentials.confirmPassword\">\n                                    <small *ngIf=\" passwordResetForm.controls['confirmPassword'].hasError('equalTo')\"\n                                        class=\"form-error-msg\">PASSWORD DO NOT MATCH</small>\n                                </div>\n                            </div>\n                            <label class=\"ch-radio\">\n                                <input name=\"hiring\" type=\"checkbox\">\n                                <span>\n                                    <i class=\"ch-icon icon-tick\"></i>All devices will be required to sign in with new\n                                    password.\n                                </span>\n                            </label>\n                            <div class=\"reset_btn\">\n                                <button class=\"ResetBtn\" [disabled]=\"passwordResetForm.invalid\"\n                                    (click)=\"resetPassword()\">Reset</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-md-6\">\n                    <div class=\"otp_img\">\n                        <img *ngIf=\"isForgot\" src=\"assets/registered_email.jpg\" alt=\"Otp Image\" class=\"img-fluid\">\n                        <img *ngIf=\"isOTP\" src=\"assets/otp2.jpg\" alt=\"Otp Image\" class=\"img-fluid\">\n                        <img *ngIf=\"isNewPassword\" src=\"assets/otp_background.png\" alt=\"Otp Image\" class=\"img-fluid\">\n                        <div class=\"otp_img_overlay\"></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</body>\n<ngx-ui-loader></ngx-ui-loader>";
    /***/
  },

  /***/
  "./src/app/pages/forgot-password/forgot-password.component.css":
  /*!*********************************************************************!*\
    !*** ./src/app/pages/forgot-password/forgot-password.component.css ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesForgotPasswordForgotPasswordComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".otp_form{\n    max-width: 440px;\nmargin: 0 auto;\n}\n.otp_form .form {\n    margin-top: 160px;\n}\n.otp_form .otp_logo{\n    position: absolute;\n    top: 40px;\n    }\n.otp_form .form .form-group {\n    margin: 0 0 49px 0;\n}\n.otp_form .form .form-group.email_text h2{\nfont-family: 'Patrick Hand';\nfont-size: 20px;\nline-height: 27px;\nletter-spacing: 0.001em;\ncolor: #748494;\n}\n.otp_form .form .form-group .input_outer{\n    position: relative;\n}\n.otp_form .form .form-group .input_outer:focus::after, \n.otp_form .form .form-group .input_outer:focus-within::after {\n    opacity: 1;\n}\n.ch-radio input[type=\"checkbox\"]:checked + span:before, .ch-radio input[type=\"checkbox\"]:checked ~ span:before, .ch-radio input[type=\"radio\"]:checked + span:before, .ch-radio input[type=\"radio\"]:checked ~ span:before {\n    border-color: #27AE60;\n    background-color: #27AE60;\n}\n.reset_btn{\n    text-align: center;\n    margin: 91px 0 0 0;\n}\n.ResetBtn{\nbackground: #F15C20;\nborder: 1px solid #F15C20;\nbox-sizing: border-box;\nbox-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);\nborder-radius: 4px;\nmin-width: 210px;\nmin-height: 56px;\nline-height: 56px;\ntext-align: center;\ncolor: #ffffff;\nfont-weight: 600;\nfont-size: 13px;\nmargin: 0 auto;\n}\n.ResetBtn:hover{\n    background: #c74009;\n}\n.otp_img{\n    position: relative;\n    overflow: hidden;\n}\n.otp_img_overlay{\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    left:0;\n    top: 0px;\n    background: linear-gradient(214.99deg, rgba(255, 255, 255, 0.4) 0%, rgba(255, 255, 255, 0) 100%);\n}\n.otp_outer{\n    position: relative;\n}\n.otp_outer .email_register{\nposition: inherit;\n}\n.go_back_page{\n    position: absolute;\n    bottom: 27px;\n}\n.go_back_page p{\n    font-size: 11px;\n    line-height: 15px;\n    letter-spacing: 0.015em;\n    text-transform: uppercase;\n    color: #748494;\n}\n.go_back_page p a{\n    color: #F15C20;\n}\nbutton[disabled] {\n    background-color: #ebccc0;\n    border: 1px solid  #ebccc0;\n}\nbutton[disabled]:hover {\n    background-color: #ebccc0;\n    border: 1px solid  #ebccc0;\n\n}\nmat-icon.field-icon {\n    float: right;\n    margin-right: 10px;\n    margin-top: -37px;\n    position: relative;\n    z-index: 2;\n    \n}\n/* check box css */\n.ch-radio {\n    display: inline-block;\n    overflow: hidden;\n    cursor: pointer;\n    position: relative;\n    transition: all 0.4s;\n  }\n.ch-radio * {\n    transition: all 0.4s;\n  }\n.ch-radio input[type=\"checkbox\"],\n  .ch-radio input[type=\"radio\"] {\n    position: absolute;\n    top: 0;\n    left: 0;\n    opacity: 0;\n  }\n.ch-radio input[type=\"checkbox\"]:checked + span:before,\n  .ch-radio input[type=\"checkbox\"]:checked ~ span:before,\n  .ch-radio input[type=\"radio\"]:checked + span:before,\n  .ch-radio input[type=\"radio\"]:checked ~ span:before {\n    border-color: #50a050;\n    background-color: #50a050;\n  }\n.ch-radio input[type=\"checkbox\"]:checked + span .ch-icon,\n  .ch-radio input[type=\"checkbox\"]:checked ~ span .ch-icon,\n  .ch-radio input[type=\"radio\"]:checked + span .ch-icon,\n  .ch-radio input[type=\"radio\"]:checked ~ span .ch-icon {\n    opacity: 1;\n  }\n.ch-radio span {\n    display: block;\n    overflow: hidden;\n    line-height: 16px;\n    position: relative;\n    padding-left: 30px;\n    font-size: 13px;\n    font-weight: normal;\n    color: #748494;\n  }\n.ch-radio span:before {\n    content: \"\";\n    background-color: transparent;\n    position: absolute;\n    top: 1px;\n    left: 1px;\n    width: 15px;\n    height: 15px;\n    transition: inherit;\n    border: 1px solid #C5CED6;\n    border-radius: 1px;\n  }\n.ch-radio span .ch-icon {\n    color: #FFFFFF;\n    position: absolute;\n    top:0;\n    left: 5px;\n    font-size: 9px;\n    opacity: 0;\n    transform: translateY(0);\n    line-height: 17px;\n  }\n.check_list{\n    margin-top: 23px;\n  }\n.check_list .ch-radio{\n      display: inline-block;\n      margin-right: 44px;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZ0JBQWdCO0FBQ3BCLGNBQWM7QUFDZDtBQUNBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsU0FBUztJQUNUO0FBQ0o7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQixlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCLHVCQUF1QjtBQUN2QixjQUFjO0FBQ2Q7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUVBOztJQUVJLFVBQVU7QUFDZDtBQUNBO0lBQ0kscUJBQXFCO0lBQ3JCLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtBQUN0QjtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLHlCQUF5QjtBQUN6QixzQkFBc0I7QUFDdEIsMkNBQTJDO0FBQzNDLGtCQUFrQjtBQUNsQixnQkFBZ0I7QUFDaEIsZ0JBQWdCO0FBQ2hCLGlCQUFpQjtBQUNqQixrQkFBa0I7QUFDbEIsY0FBYztBQUNkLGdCQUFnQjtBQUNoQixlQUFlO0FBQ2YsY0FBYztBQUNkO0FBQ0E7SUFDSSxtQkFBbUI7QUFDdkI7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLE1BQU07SUFDTixRQUFRO0lBQ1IsZ0dBQWdHO0FBQ3BHO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsdUJBQXVCO0lBQ3ZCLHlCQUF5QjtJQUN6QixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxjQUFjO0FBQ2xCO0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsMEJBQTBCOztBQUU5QjtBQUVBO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFVBQVU7O0FBRWQ7QUFDQSxrQkFBa0I7QUFFbEI7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFFbEIsb0JBQW9CO0VBQ3RCO0FBRUE7SUFFRSxvQkFBb0I7RUFDdEI7QUFFQTs7SUFFRSxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxVQUFVO0VBQ1o7QUFFQTs7OztJQUlFLHFCQUFxQjtJQUNyQix5QkFBeUI7RUFDM0I7QUFFQTs7OztJQUlFLFVBQVU7RUFDWjtBQUVBO0lBQ0UsY0FBYztJQUNkLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGNBQWM7RUFDaEI7QUFFQTtJQUNFLFdBQVc7SUFDWCw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1QsV0FBVztJQUNYLFlBQVk7SUFFWixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLGtCQUFrQjtFQUNwQjtBQUVBO0lBQ0UsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixLQUFLO0lBQ0wsU0FBUztJQUNULGNBQWM7SUFDZCxVQUFVO0lBRVYsd0JBQXdCO0lBQ3hCLGlCQUFpQjtFQUNuQjtBQUNBO0lBQ0UsZ0JBQWdCO0VBQ2xCO0FBRUE7TUFDSSxxQkFBcUI7TUFDckIsa0JBQWtCO0VBQ3RCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm90cF9mb3Jte1xuICAgIG1heC13aWR0aDogNDQwcHg7XG5tYXJnaW46IDAgYXV0bztcbn1cbi5vdHBfZm9ybSAuZm9ybSB7XG4gICAgbWFyZ2luLXRvcDogMTYwcHg7XG59XG4ub3RwX2Zvcm0gLm90cF9sb2dve1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDQwcHg7XG4gICAgfVxuLm90cF9mb3JtIC5mb3JtIC5mb3JtLWdyb3VwIHtcbiAgICBtYXJnaW46IDAgMCA0OXB4IDA7XG59XG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAuZW1haWxfdGV4dCBoMntcbmZvbnQtZmFtaWx5OiAnUGF0cmljayBIYW5kJztcbmZvbnQtc2l6ZTogMjBweDtcbmxpbmUtaGVpZ2h0OiAyN3B4O1xubGV0dGVyLXNwYWNpbmc6IDAuMDAxZW07XG5jb2xvcjogIzc0ODQ5NDtcbn1cbi5vdHBfZm9ybSAuZm9ybSAuZm9ybS1ncm91cCAuaW5wdXRfb3V0ZXJ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyOmZvY3VzOjphZnRlciwgXG4ub3RwX2Zvcm0gLmZvcm0gLmZvcm0tZ3JvdXAgLmlucHV0X291dGVyOmZvY3VzLXdpdGhpbjo6YWZ0ZXIge1xuICAgIG9wYWNpdHk6IDE7XG59XG4uY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgKyBzcGFuOmJlZm9yZSwgLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkIH4gc3BhbjpiZWZvcmUsIC5jaC1yYWRpbyBpbnB1dFt0eXBlPVwicmFkaW9cIl06Y2hlY2tlZCArIHNwYW46YmVmb3JlLCAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgfiBzcGFuOmJlZm9yZSB7XG4gICAgYm9yZGVyLWNvbG9yOiAjMjdBRTYwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMyN0FFNjA7XG59XG4ucmVzZXRfYnRue1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IDkxcHggMCAwIDA7XG59XG4uUmVzZXRCdG57XG5iYWNrZ3JvdW5kOiAjRjE1QzIwO1xuYm9yZGVyOiAxcHggc29saWQgI0YxNUMyMDtcbmJveC1zaXppbmc6IGJvcmRlci1ib3g7XG5ib3gtc2hhZG93OiAwcHggMnB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuYm9yZGVyLXJhZGl1czogNHB4O1xubWluLXdpZHRoOiAyMTBweDtcbm1pbi1oZWlnaHQ6IDU2cHg7XG5saW5lLWhlaWdodDogNTZweDtcbnRleHQtYWxpZ246IGNlbnRlcjtcbmNvbG9yOiAjZmZmZmZmO1xuZm9udC13ZWlnaHQ6IDYwMDtcbmZvbnQtc2l6ZTogMTNweDtcbm1hcmdpbjogMCBhdXRvO1xufVxuLlJlc2V0QnRuOmhvdmVye1xuICAgIGJhY2tncm91bmQ6ICNjNzQwMDk7XG59XG5cbi5vdHBfaW1ne1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLm90cF9pbWdfb3ZlcmxheXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6MDtcbiAgICB0b3A6IDBweDtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjE0Ljk5ZGVnLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNCkgMCUsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMCkgMTAwJSk7XG59XG4ub3RwX291dGVye1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5vdHBfb3V0ZXIgLmVtYWlsX3JlZ2lzdGVye1xucG9zaXRpb246IGluaGVyaXQ7XG59XG4uZ29fYmFja19wYWdle1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDI3cHg7XG59XG4uZ29fYmFja19wYWdlIHB7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjAxNWVtO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM3NDg0OTQ7XG59XG4uZ29fYmFja19wYWdlIHAgYXtcbiAgICBjb2xvcjogI0YxNUMyMDtcbn1cblxuYnV0dG9uW2Rpc2FibGVkXSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ViY2NjMDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAgI2ViY2NjMDtcbn1cbmJ1dHRvbltkaXNhYmxlZF06aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlYmNjYzA7XG4gICAgYm9yZGVyOiAxcHggc29saWQgICNlYmNjYzA7XG5cbn1cblxubWF0LWljb24uZmllbGQtaWNvbiB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAtMzdweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMjtcbiAgICBcbn1cbi8qIGNoZWNrIGJveCBjc3MgKi9cblxuLmNoLXJhZGlvIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNHM7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNHM7XG4gIH1cbiAgXG4gIC5jaC1yYWRpbyAqIHtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjRzO1xuICB9XG4gIFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICBcbiAgLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkICsgc3BhbjpiZWZvcmUsXG4gIC5jaC1yYWRpbyBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl06Y2hlY2tlZCB+IHNwYW46YmVmb3JlLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgKyBzcGFuOmJlZm9yZSxcbiAgLmNoLXJhZGlvIGlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkIH4gc3BhbjpiZWZvcmUge1xuICAgIGJvcmRlci1jb2xvcjogIzUwYTA1MDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTBhMDUwO1xuICB9XG4gIFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgKyBzcGFuIC5jaC1pY29uLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdOmNoZWNrZWQgfiBzcGFuIC5jaC1pY29uLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgKyBzcGFuIC5jaC1pY29uLFxuICAuY2gtcmFkaW8gaW5wdXRbdHlwZT1cInJhZGlvXCJdOmNoZWNrZWQgfiBzcGFuIC5jaC1pY29uIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG4gIFxuICAuY2gtcmFkaW8gc3BhbiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZy1sZWZ0OiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGNvbG9yOiAjNzQ4NDk0O1xuICB9XG4gIFxuICAuY2gtcmFkaW8gc3BhbjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMXB4O1xuICAgIGxlZnQ6IDFweDtcbiAgICB3aWR0aDogMTVweDtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBpbmhlcml0O1xuICAgIHRyYW5zaXRpb246IGluaGVyaXQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M1Q0VENjtcbiAgICBib3JkZXItcmFkaXVzOiAxcHg7XG4gIH1cbiAgXG4gIC5jaC1yYWRpbyBzcGFuIC5jaC1pY29uIHtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOjA7XG4gICAgbGVmdDogNXB4O1xuICAgIGZvbnQtc2l6ZTogOXB4O1xuICAgIG9wYWNpdHk6IDA7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDApO1xuICAgIGxpbmUtaGVpZ2h0OiAxN3B4O1xuICB9XG4gIC5jaGVja19saXN0e1xuICAgIG1hcmdpbi10b3A6IDIzcHg7XG4gIH1cblxuICAuY2hlY2tfbGlzdCAuY2gtcmFkaW97XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDQ0cHg7XG4gIH0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/forgot-password/forgot-password.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/pages/forgot-password/forgot-password.component.ts ***!
    \********************************************************************/

  /*! exports provided: ForgotPasswordComponent */

  /***/
  function srcAppPagesForgotPasswordForgotPasswordComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function () {
      return ForgotPasswordComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/core/services/api.service.service */
    "./src/app/core/services/api.service.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var ng2_validation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ng2-validation */
    "./node_modules/ng2-validation/dist/index.js");
    /* harmony import */


    var ng2_validation__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_6__);
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");

    var ForgotPasswordComponent = /*#__PURE__*/function () {
      function ForgotPasswordComponent(router, apiservice, snack, ngxLoader) {
        _classCallCheck(this, ForgotPasswordComponent);

        this.router = router;
        this.apiservice = apiservice;
        this.snack = snack;
        this.ngxLoader = ngxLoader;
        this.isForgot = true;
        this.isOTP = false;
        this.isNewPassword = false;
        this.otpResponse = {};
        this.isConfirmPassword = false;
        this.credentials = {
          email: '',
          password: '',
          newPassword: '',
          confirmPassword: '',
          otp: '',
          otpToken: ''
        };
        this.message = '';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        var config = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarConfig"]();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
      }

      _createClass(ForgotPasswordComponent, [{
        key: "otpRequest",
        value: function otpRequest() {
          var _this = this;

          this.ngxLoader.start();
          this.apiservice.otpRequest(this.credentials.email).subscribe(function (res) {
            _this.otpResponse = res;

            _this.ngxLoader.stop();

            if (_this.otpResponse.isSuccess === true) {
              _this.isOTP = true;
              _this.isNewPassword = false;
              _this.isForgot = false;
              _this.credentials.otpToken = _this.otpResponse.message.otpToken, console.log('response', _this.otpResponse);

              _this.snack.open(_this.otpResponse.message.message, 'OK', {
                duration: 5000
              });
            } else {
              if (_this.otpResponse.isSuccess === false) {
                _this.isForgot = true;
                _this.isOTP = false;
                _this.isNewPassword = false;

                _this.snack.open(_this.otpResponse.error, 'OK', {
                  duration: 5000
                });
              } else {
                _this.isForgot = true;
                _this.isOTP = false;
                _this.isNewPassword = false;
                var msg = 'Something Went Wrong!';

                _this.snack.open(msg, 'OK', {
                  duration: 5000
                });
              }
            }
          });
        }
      }, {
        key: "otpVerify",
        value: function otpVerify() {
          var _this2 = this;

          this.ngxLoader.start();
          this.apiservice.otpVerify(this.credentials).subscribe(function (res) {
            _this2.otpVerifyResponse = res;

            _this2.ngxLoader.stop();

            if (_this2.otpVerifyResponse.isSuccess === true) {
              _this2.isOTP = false;
              _this2.isNewPassword = true;
              _this2.isForgot = false;
              console.log('verifyotp response', res);

              _this2.snack.open(_this2.otpVerifyResponse.message.message, 'OK', {
                duration: 5000
              });
            } else {
              if (_this2.otpVerifyResponse.isSuccess === false) {
                _this2.isOTP = true;
                _this2.isNewPassword = false;
                _this2.isForgot = false;

                _this2.snack.open(_this2.otpVerifyResponse.error, 'OK', {
                  duration: 5000
                });
              } else {
                _this2.isOTP = true;
                _this2.isNewPassword = false;
                _this2.isForgot = false;
                var msg = 'Something went Wrong!';

                _this2.snack.open(msg, 'OK', {
                  duration: 5000
                });
              }
            }
          });
        }
      }, {
        key: "resetPassword",
        value: function resetPassword() {
          var _this3 = this;

          this.ngxLoader.start();
          this.apiservice.forgotPassword(this.credentials).subscribe(function (res) {
            _this3.resetPasswordResponse = res;

            _this3.ngxLoader.stop();

            if (_this3.resetPasswordResponse.isSuccess === true) {
              _this3.router.navigate(['/login']);

              console.log('resetpassword response', res);

              _this3.snack.open(_this3.resetPasswordResponse.message, 'OK', {
                duration: 5000
              });
            } else {
              if (_this3.resetPasswordResponse.isSuccess === false) {
                _this3.isOTP = false;
                _this3.isNewPassword = true;
                _this3.isForgot = false;

                _this3.snack.open(_this3.resetPasswordResponse.error, 'OK', {
                  duration: 5000
                });
              } else {
                _this3.isOTP = false;
                _this3.isNewPassword = true;
                _this3.isForgot = false;
                var msg = 'Something went Wrong!';

                _this3.snack.open(msg, 'OK', {
                  duration: 5000
                });
              }
            }
          });
        }
      }, {
        key: "cancel",
        value: function cancel() {
          this.router.navigate(['/login']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.otpSendForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email])
          });
          this.otpVerifyForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            otp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(4)])
          });
          var newPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]);
          var confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_6__["CustomValidators"].equalTo(newPassword)]);
          this.passwordResetForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            newPassword: newPassword,
            confirmPassword: confirmPassword
          });
        }
      }]);

      return ForgotPasswordComponent;
    }();

    ForgotPasswordComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: src_app_core_services_api_service_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"]
      }, {
        type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderService"]
      }];
    };

    ForgotPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-forgot-password',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./forgot-password.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./forgot-password.component.css */
      "./src/app/pages/forgot-password/forgot-password.component.css"))["default"]]
    })], ForgotPasswordComponent);
    /***/
  },

  /***/
  "./src/app/pages/forgot-password/forgot-password.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/forgot-password/forgot-password.module.ts ***!
    \*****************************************************************/

  /*! exports provided: ForgotPasswordModule */

  /***/
  function srcAppPagesForgotPasswordForgotPasswordModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordModule", function () {
      return ForgotPasswordModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _forgot_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./forgot-password.component */
    "./src/app/pages/forgot-password/forgot-password.component.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");

    var routes = [{
      path: '',
      component: _forgot_password_component__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordComponent"],
      children: []
    }];

    var ForgotPasswordModule = function ForgotPasswordModule() {
      _classCallCheck(this, ForgotPasswordModule);
    };

    ForgotPasswordModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_forgot_password_component__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)]
    })], ForgotPasswordModule);
    /***/
  }
}]);
//# sourceMappingURL=app-pages-forgot-password-forgot-password-module-es5.js.map